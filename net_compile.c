//Provides networking interface for a master BBB to compile binary files

/*Version control
 v0.01 TD 11/05/15 Untested version
 v0.02 TD 12/05/15 Bug fixes
 v0.03 TD 13/05/15 Bug fixes
 v0.04 TD 15/05/15 Memory leak fix, Renamed to compile.c
 v0.05 TD 19/05/15 Added status message "attempting to connect"
 v0.06 TD 22/05/15 Run make_binary directly if <wave> is not specified
 v0.07 TD 27/05/15 Bug fix in sending <wave> document
 v0.08 TD 19/01/16 Updated to v0.01 of compiler.c
 v0.09 TD 28/01/16 Mallocs cast to type
 v0.10 TD 09/02/16 Macros for network messages
 v0.11 TD 01/12/16 Displays if slaves failed to compile
 v0.12 TD 02/02/17 XML parser made thread safe
 v0.13 TD 07/02/17 Display completion message
 v0.14 TD 21/02/17 Display total compile time
 v0.15 TD 05/04/17 Return wave duration
 v0.16 TD 08/04/17 Each interval created in own thread
 v0.17 TD 20/04/17 Use snprintf and initialise XPath
 v0.18 TD 22/05/17 XML names changed to camel case & motor0xPos optional in master file
 v0.19 TD 23/05/17 Number of instructions in performance stats
 v0.20 TD 20/06/17 Made compatible with MSVS2012 updates - compiler v0.20
 v0.21 TD 28/07/17 Use xmlStrEqual
 v0.22 TD 21/11/17 Abort compile on error and return some messages
 */

#include <unistd.h>
#include <netdb.h>
#include "compiler.h"

void closesockets(int sockets[], int numsockets)
{
    int k;
    for (k = 0; k < numsockets; k++) {
        close(sockets[k]);
    }
}

struct listen_params {
    //For listenStopSig()
    int *sockets;
    int numsockets;
    int *stopSig;       //Tells all compiler threads to stop
    int *stopListen;    //Tells listener thread to terminate
};

void* listenStopSig(void *params)
{
    //Listens for stop message from slave BBBs - to be run in separate thread
    struct listen_params *params_struct = (struct listen_params*) params;

    //Create set of file descriptors
    fd_set fdset;
    int socketID;
    char buffer[1];
    struct timeval checkStopPeriod;
    
    //Set ignore flags for any slave BBBs that return successful completion
    int listenBBB[params_struct->numsockets];
    for (socketID = 0; socketID < params_struct->numsockets; socketID++) {
        listenBBB[socketID] = 1;
    }
    
    while (*(params_struct->stopListen) == 0) {
        //Need to set checkStopPeriod each time, as may be changed by select()
        checkStopPeriod.tv_sec = 1;
        checkStopPeriod.tv_usec = 0;
        
        //Need to reset set of file descriptors each time
        int numMonitored = 0;
        FD_ZERO(&fdset);
        for (socketID = 0; socketID < params_struct->numsockets; socketID++) {
            if (listenBBB[socketID]) {
                FD_SET(params_struct->sockets[socketID], &fdset);
                numMonitored++;
            }
        }
        if (numMonitored == 0) { break; }  //No sockets left to listen to
        
        //Wait for a signal for up to 1s
        int selectRtn = select(FD_SETSIZE, &fdset, NULL, NULL, &checkStopPeriod);
        if (selectRtn > 0) {
            for (socketID = 0; socketID < params_struct->numsockets; socketID++) {
                if (FD_ISSET(params_struct->sockets[socketID], &fdset)) {
                    int rcvd = recv(params_struct->sockets[socketID], buffer, 1, MSG_PEEK);
                    if (rcvd < 1) {
                        //Error - quietly ignore this socket; if it has closed, program will pick it up later
                        listenBBB[socketID] = 0;
                    } else if (buffer[0] == COMPILE_FAIL) {
                        //Compiler error - signal stop
                        *(params_struct->stopSig) = 1;
                        *(params_struct->stopListen) = 1;
                        break;
                    } else {
                        //Other message - stop listening
                        listenBBB[socketID] = 0;
                    }
                }
            }
        }
    }
    
    return NULL;
}

int make_from_master(char* masterfilename)
{
    //Start timer - for performance benchmarking
    TIME_TYPE start_t, end_t;
    GET_TIME(start_t);
    double global_wave_end = 0;
    long int total_wave_instructions;
    
    //Generic counters and returns
    int rtn, compile_status, stopSig;
    
    //Check and get maximum number of open files
    FLIM_TYPE filelimit = GET_FILELIMIT;
    if (filelimit == 0) { return EXIT_FAILURE; }   //Error message displayed by get_filelimit()
    
    //Open and read XML input
    xmlDocPtr xmlRawDoc;
    xmlNodePtr xmlNode;
    xmlChar * xmlText;
    
    xmlRawDoc = xmlParseFile(masterfilename);
    if (xmlRawDoc == NULL)
    {
        fprintf(stderr, "XML input file parse unsuccessful\n");
        return(EXIT_FAILURE);
    }
    
    //Validate XML doc
    const char schema_path[200] = "ASWaMMasterSchema.xsd";
    xmlDocPtr schema_doc = NULL;
    xmlSchemaParserCtxtPtr parser_ctxt = NULL;
    xmlSchemaPtr schema = NULL;
    xmlSchemaValidCtxtPtr valid_ctxt = NULL;
    schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
    parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
    schema = xmlSchemaParse(parser_ctxt);
    valid_ctxt = xmlSchemaNewValidCtxt(schema);
    int validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
    if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
    if (schema) xmlSchemaFree(schema);
    if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
    if (schema_doc) xmlFreeDoc(schema_doc);
    if (validationResult != 0)
    {
        fprintf(stderr, "Invalid syntax in XML input file: %d\n", validationResult);
        //Avoid memory leaks
        xmlFreeDoc(xmlRawDoc);
        return(EXIT_FAILURE);
    }
    
    //WARNING: Have not applied a stylesheet so there may be comment nodes, avoided using e.g. xmlFirstElementChild, but ->children is dangerous
    
    xmlNode = xmlDocGetRootElement(xmlRawDoc);  //<master>
    int noBBBs = xmlChildElementCount(xmlNode); //Number of BBBs
    xmlNode = xmlFirstElementChild(xmlNode);    //<wave> or <bbb>
    
    xmlNodePtr waveNode = NULL;
    char* WaveDocString = NULL;
    int WaveDocSize = 0;
    
    if (xmlStrEqual(xmlNode->name, (const xmlChar*) "wave")) {
        //Wave form given: send a copy of the wave form to each slave, which will then modify the pins file
        //Need to send: x position of motor 0 on each slave & <wave> with all descendents
        waveNode = xmlNode;
        
        //Put <wave> element and all children into a buffer
        //Create new XML document with <wave> as the root and save it as string WaveDocString
        xmlDocPtr xmlWaveDoc = xmlNewDoc((const xmlChar*) "1.0");
        xmlNodePtr waveNodeCopy = xmlDocCopyNode(waveNode, xmlRawDoc, 1);
        xmlDocSetRootElement(xmlWaveDoc, waveNodeCopy);
        
        xmlChar* xmlWaveDocString;
        xmlDocDumpMemory(xmlWaveDoc, &xmlWaveDocString, &WaveDocSize);
        xmlFreeDoc(xmlWaveDoc);
        WaveDocSize++;  //+1 to include terminating NULL character
        
        WaveDocString = (char*) malloc(WaveDocSize * sizeof(char));
        STR_CPY(WaveDocString, (const char*) xmlWaveDocString, WaveDocSize)
        xmlFree(xmlWaveDocString);
        
        xmlNode = xmlNextElementSibling(xmlNode);       //First <bbb>
        noBBBs--;       //One of the child elements of <master> is this <wave> node
    }
    //Else: use wave XML file at address pinsfilename already present on slave
    
    //Prepare to open network connections
    struct addrinfo hints;
    struct addrinfo *servinfo;  // will point to the results
    int slave_sockets[noBBBs - 1];      //To be fed into run_pruss on master
    char masterbinfilename[256] = "";
    char binaryfilename[256] = "";
    char masterpinsfilename[256] = "";
    char pinsfilename[256] = "";
    double masterstartPos = 0;  //Initialise to shut up compiler (unnecessary based on control flow)
    double startPos = 0;  //Initialise to shut up compiler (unnecessary based on control flow)
    unsigned char err_occ = 0;
    
    memset(&hints, 0, sizeof hints); // make sure the struct is empty
    hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    
    int numsockets = -1;
    while (xmlNode) {
        xmlNodePtr xmlProperty = xmlFirstElementChild(xmlNode);    //<ip>
        xmlNodePtr xmlTextNode;
        
        //Get binary file name
        xmlProperty = xmlNextElementSibling(xmlProperty);   //<binary>
        xmlTextNode = xmlProperty->children;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        xmlText = xmlNodeListGetString(xmlRawDoc, xmlTextNode, 1);
        STR_CPY(binaryfilename, (char *) xmlText, 256)
        xmlFree(xmlText);
        
        //Get pins file name
        xmlProperty = xmlNextElementSibling(xmlProperty);   //<pins>
        xmlTextNode = xmlProperty->children;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        xmlText = xmlNodeListGetString(xmlRawDoc, xmlTextNode, 1);
        STR_CPY(pinsfilename, (char *) xmlText, 256)
        xmlFree(xmlText);
        
        if (waveNode) {
            //Get pins file attributes: x position of motor 0 in mm
            xmlText = xmlGetProp(xmlProperty, (const xmlChar*)"motor0xPos");
            if (xmlText) {
                startPos = atof((const char*) xmlText);
                xmlFree(xmlText);
            } else {
                startPos = DBL_MAX;  //An unusably large number to indicate moto0xPos not set
            }
        }
        
        xmlProperty = xmlFirstElementChild(xmlNode);   //<ip>
        
        xmlTextNode = xmlProperty->children;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        
        if (!(xmlTextNode) || xmlIsBlankNode(xmlTextNode)) {
            //Refers to this BBB, the master
            if (strlen(masterbinfilename)) {
                //Found two masters: error
                fprintf(stderr, "ERROR: Cannot have two master BBBs. Must specify an IP address for all slaves.\n");
                err_occ = 1;
                break;
            }
            STR_CPY(masterbinfilename, binaryfilename, 256)
            STR_CPY(masterpinsfilename, pinsfilename, 256)
            masterstartPos = startPos;
        }
        else {
            //Slave
            xmlText = xmlNodeListGetString(xmlRawDoc, xmlProperty->children, 1);
            rtn = getaddrinfo((char *) xmlText, PORTNUM, &hints, &servinfo);
            xmlFree(xmlText);
            if (rtn != 0) {
                fprintf(stderr, "Failed to get address info for slave %d: %s.\n", numsockets + 1, gai_strerror(rtn));
                err_occ = 1;
                break;
            }
            
            numsockets++;
            slave_sockets[numsockets] = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
            if (slave_sockets[numsockets] == -1) {
                fprintf(stderr, "Failed to get socket for slave %d.\n", numsockets);
                freeaddrinfo(servinfo);     //Free to avoid memory leak
                numsockets--;               //Because socket didn't open
                err_occ = 1;
                break;
            }
            
            printf("Attempting to connect to slave %d. Press Ctrl+C to abort.\n", numsockets);
            rtn = connect(slave_sockets[numsockets], servinfo->ai_addr, servinfo->ai_addrlen);
            freeaddrinfo(servinfo);     //Free to avoid memory leak
            
            if (rtn == -1) {
                fprintf(stderr, "Failed to connect to slave %d.\n", numsockets);
                err_occ = 2;        //Means socket open but disconnected
                break;
            }
            printf("Connected to slave %d.\n", numsockets);
            
            //Send operation mode
            char modechar;
            if (waveNode) {
                modechar = ASWAM_MAKEXML;     //Make XML file
            } else {
                modechar = ASWAM_COMPILE;     //Compile binary file
            }
            rtn = send(slave_sockets[numsockets], &modechar, 1, MSG_NOSIGNAL);  //Send binary file name size
            if (rtn < 1) {  //Send is blocking
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Send stuff for making XML file
            if (waveNode) {
                char numString[25];
                unsigned char numString_len;
                int bytessent;
                
                //Send x position of motor 0
                S_PRINTF(numString, 25, "a%.16g", startPos)      //a is to fill first character to be overwritten shortly
                numString_len = strlen(numString) - 1;
                numString[0] = numString_len;      //Message size
                
                //Keep sending number until all is sent
                bytessent = 0;
                rtn = 1;
                while (bytessent < (numString_len + 1) && rtn > 0) {
                    rtn = send(slave_sockets[numsockets], numString + bytessent, (numString_len + 1) - bytessent, MSG_NOSIGNAL);
                    bytessent += rtn;
                }
                if (rtn < 1) {
                    fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                    err_occ = 2;
                    break;
                }
                
                //Send size of <wave> document
                S_PRINTF(numString, 25, "a%d", WaveDocSize)      //a is to fill first character to be overwritten shortly
                numString_len = strlen(numString) - 1;
                numString[0] = numString_len;      //Message size
                
                //Keep sending number until all is sent
                bytessent = 0;
                rtn = 1;
                while (bytessent < (numString_len + 1) && rtn > 0) {
                    rtn = send(slave_sockets[numsockets], numString + bytessent, (numString_len + 1) - bytessent, MSG_NOSIGNAL);
                    bytessent += rtn;
                }
                if (rtn < 1) {
                    fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                    err_occ = 2;
                    break;
                }
                
                //Send <wave> document
                bytessent = 0;
                rtn = 1;
                while (bytessent < WaveDocSize && rtn > 0) {
                    rtn = send(slave_sockets[numsockets], WaveDocString + bytessent, WaveDocSize - bytessent, MSG_NOSIGNAL);
                    bytessent += rtn;
                }
                if (rtn < 1) {
                    fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                    err_occ = 2;
                    break;
                }
            }
            
            //Send binary file name, with first character being the length
            unsigned char filenamesize[1];
            filenamesize[0] = (unsigned char) strlen(binaryfilename);
            
            rtn = send(slave_sockets[numsockets], filenamesize, 1, MSG_NOSIGNAL);  //Send binary file name size
            if (rtn < 1) {  //Send is blocking
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Keep sending file name until all is sent
            int bytessent = 0;
            rtn = 1;
            while (bytessent < filenamesize[0] && rtn > 0) {
                rtn = send(slave_sockets[numsockets], binaryfilename+bytessent, filenamesize[0] - bytessent, MSG_NOSIGNAL);
                bytessent += rtn;
            }
            if (rtn < 1) {
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Send pins file name, with first character being the length
            filenamesize[0] = (unsigned char) strlen(pinsfilename);
            
            rtn = send(slave_sockets[numsockets], filenamesize, 1, MSG_NOSIGNAL);  //Send pins file name size
            if (rtn < 1) {  //Send is blocking
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Keep sending file name until all is sent
            bytessent = 0;
            rtn = 1;
            while (bytessent < filenamesize[0] && rtn > 0) {
                rtn = send(slave_sockets[numsockets], pinsfilename+bytessent, filenamesize[0] - bytessent, MSG_NOSIGNAL);
                bytessent += rtn;
            }
            if (rtn < 1) {
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
        }
        
        xmlNode = xmlNextElementSibling(xmlNode);   //<bbb>, will go to NULL on last iteration
    }
    numsockets++;
    
    //XML Cleanup
    xmlFreeDoc(xmlRawDoc);
    
    //Exit if an error has occurred
    if (err_occ) {
        free(WaveDocString);
        closesockets(slave_sockets, numsockets);
        return EXIT_FAILURE;
    }
    
    //Check that a master has been specified
    if (strlen(masterbinfilename) == 0) {
        fprintf(stderr, "No master specified. One BBB must have an empty IP address.\n");
        free(WaveDocString);
        closesockets(slave_sockets, numsockets);
        return(EXIT_FAILURE);
    }
    
    //Start listening for compile failures from slaves
    pthread_t listenThread;
    pthread_attr_t listenThreadAttr;
    struct listen_params listenThreadParams;
    int stopListen = 0;
    stopSig = 0;
    listenThreadParams.sockets = slave_sockets;
    listenThreadParams.numsockets = numsockets;
    listenThreadParams.stopSig = &stopSig;
    listenThreadParams.stopListen = &stopListen;
    pthread_attr_init(&listenThreadAttr);
    pthread_attr_setdetachstate(&listenThreadAttr, PTHREAD_CREATE_DETACHED);

    //Don't bother checking whether thread was created successfully, as will never need to be joined for terminationi: set stopListen = 1
    pthread_create(&listenThread, &listenThreadAttr, listenStopSig, (void*)&listenThreadParams);
    pthread_attr_destroy(&listenThreadAttr);
    
    //Open file counters
    int BBBthreads = 1;
    int motorthreads = 0;
    pthread_mutex_t ThreadCounterMutex;
    if (pthread_mutex_init(&ThreadCounterMutex, NULL)) {
        perror("ERROR: Could not initialise mutex");
        free(WaveDocString);
        closesockets(slave_sockets, numsockets);
        return EXIT_FAILURE;
    }
    pthread_cond_t ThreadCounterCondition;
    if (pthread_cond_init(&ThreadCounterCondition, NULL)) {
        perror("ERROR: Could not initialise mutex condition");
        free(WaveDocString);
        closesockets(slave_sockets, numsockets);
        pthread_mutex_destroy(&ThreadCounterMutex);
        return EXIT_FAILURE;
    }

    if (WaveDocString) {
        compile_status = compile_from_master(&global_wave_end, &total_wave_instructions, masterstartPos, WaveDocString, masterbinfilename, masterpinsfilename, 0, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
        free(WaveDocString);        //Allocated using malloc, or still set to NULL
    } else {
        compile_status = make_binary(&global_wave_end, &total_wave_instructions, masterpinsfilename, masterbinfilename, 0, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
    }
    
    //Cleanup pthreads
    stopListen = 1; //Stops detached listener thread
    pthread_mutex_destroy(&ThreadCounterMutex);
    pthread_cond_destroy(&ThreadCounterCondition);
    
    //Send master compile outcome to slaves - will signal stop on error
    int slave_id;
    char slave_response[1];
    if (compile_status) {
        slave_response[0] = COMPILE_FAIL;
    } else {
        slave_response[0] = COMPILE_SUCCESS;
    }
    for (slave_id = 0; slave_id < numsockets; slave_id++) {
        //Send message and fail silently on error, as slave may have already finished and closed socket
        send(slave_sockets[numsockets], slave_response, 1, MSG_NOSIGNAL);
    }
    
    printf("Waiting for compile outcome from slaves...\n");
    
    for (slave_id = 0; slave_id < numsockets; slave_id++) {
        rtn = recv(slave_sockets[slave_id], slave_response, 1, 0);
        if (rtn < 1) {
            fprintf(stderr, "Connection lost to slave %d.\n", slave_id);
        } else {
            if (slave_response[0] == COMPILE_SUCCESS) {
                printf("Slave BBB%d compiled successfully.\n", slave_id);
                
                //Update global_wave_end
                unsigned char buffer[256];
                int bytesrcv, stringlength;
                
                //Receive size of wave_end formatted as string
                rtn = recv(slave_sockets[slave_id], buffer, 1, 0);
                if (rtn < 1) {
                    perror("Connection closed unexpectedly");
                } else {
                    stringlength = (int) buffer[0];
                    bytesrcv = 0;
                    while (bytesrcv < stringlength && rtn > 0) {
                        rtn = recv(slave_sockets[slave_id], buffer + bytesrcv, stringlength - bytesrcv, 0);
                        bytesrcv += rtn;
                    }
                    if (rtn < 1) {
                        perror("Connection closed unexpectedly");
                    } else {
                        buffer[stringlength] = '\0';
                        double slave_wave_end = atof((const char*) buffer);
                        if (slave_wave_end > global_wave_end) {
                            global_wave_end = slave_wave_end;
                        }
                    }
                }
                
                //Update total_wave_instructions
                //Receive size of wave_end formatted as string
                rtn = recv(slave_sockets[slave_id], buffer, 1, 0);
                if (rtn < 1) {
                    perror("Connection closed unexpectedly");
                } else {
                    stringlength = (int) buffer[0];
                    bytesrcv = 0;
                    while (bytesrcv < stringlength && rtn > 0) {
                        rtn = recv(slave_sockets[slave_id], buffer + bytesrcv, stringlength - bytesrcv, 0);
                        bytesrcv += rtn;
                    }
                    if (rtn < 1) {
                        perror("Connection closed unexpectedly");
                    } else {
                        buffer[stringlength] = '\0';
                        double slave_wave_instructions = atol((const char*) buffer);
                        total_wave_instructions += slave_wave_instructions;
                    }
                }
            } else if (slave_response[0] == COMPILE_FAIL) {
                //Receive error message
                rtn = recv(slave_sockets[slave_id], slave_response, 1, 0);
                printf("Compile failed on slave BBB%d", slave_id);
                if (rtn < 1) {
                    printf(".\n");
                    fprintf(stderr, "Connection lost to slave %d.\n", slave_id);
                    compile_status = 1;
                } else {
                    compile_status = slave_response[0];
                    switch (compile_status) {
                        case ERR_DRIVE:
                            printf(": Could not write output files. The hard drive might be full.\n");
                            break;
                        case ERR_DISCONTINUOUS:
                            printf(": Discontinuity in input function.\n");
                            break;
                        case ERR_PERIOD:
                            printf("Period is set incorrectly.\n");
                            break;
                        default:
                            printf("\n");
                    }
                }
            } else {
                fprintf(stderr, "Unrecognised message received from slave %d: %c\n", slave_id, slave_response[0]);
            }
        }
    }
    
    if (!compile_status) {
        //Display compile time
        double total_t;
        GET_TIME(end_t);
        TIME_DIF(total_t, start_t, end_t);
        printf("Total time to compile = %g s.\n", total_t);
        
        if (global_wave_end > 0) {
            //Don't want to divide by zero
            double perf_ratio = global_wave_end / total_t;
            double perf_score = (double)total_wave_instructions / total_t;
            printf("Waveform length = %g s, ratio = %g.\nNumber of instructions = %ld, score = %g.\n", global_wave_end, perf_ratio, total_wave_instructions, perf_score);
        }
    }
    
    closesockets(slave_sockets, numsockets);
    return compile_status;
}

int main(int argc, char *argv[])
{
    //Program information
    if (argc != 2 || !strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
    {
        printf("Please provide one argument: XML configuration file\n");
        return(EXIT_FAILURE);
    }
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();
    
    int rtn = make_from_master(argv[1]);
    
    //Cleanup XML & XSLT once all threads are done with it
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    return rtn;
}
