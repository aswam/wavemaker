﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:libxslt="http://xmlsoft.org/XSLT/namespace">
	<xsl:output method="xml" encoding="utf-8" indent="yes" />
	<xsl:include href="FuncStringToTree.xsl" />
	
	<!--Expand the variable definitions and store in variable defvars. Do this to avoid reexpanding each time. Also, allows for future introduction of preevaluate attribute that tells the compiler to evaluate the variable separately, before feeding to calcalgebraicfunc - may speed up computation if variable is used multiple times in function.-->
	<!-- This is an identity template - it copies everything that doesn't match another template -->
	<xsl:template match="@* | node()" mode="expandvardefs">
		<xsl:param name="dependents" select="0" /><!--For avoiding circular variable definitions: sandwich variable names between semicolons.-->
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" mode="expandvardefs">
				<xsl:with-param name="dependents" select="$dependents" />
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="var" mode="expandvardefs">
		<xsl:param name="dependents" select="0" /><!--For avoiding circular variable definitions: sandwich variable names between semicolons.-->
		<xsl:variable name="varname" select="@name" />
		<!--Check for circular variable definition-->
		<xsl:if test="contains($dependents, concat(';', $varname, ';'))">
			<xsl:message terminate="yes">Circular variable definition of <xsl:value-of select="$varname" />.</xsl:message>
		</xsl:if>
		<xsl:variable name="matcheddef" select="libxslt:node-set($rawdefvars)/def[@name = $varname]" />
		<xsl:choose>
			<xsl:when test="$matcheddef">
				<xsl:apply-templates select="$matcheddef/node()" mode="expandvardefs">
					<xsl:with-param name="dependents" select="concat($dependents, ';', $varname, ';')" />
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="." />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:variable name="rawdefvars">
		<xsl:for-each select="/wave/def">
			<xsl:copy>
				<xsl:copy-of select="@name" />
				<xsl:call-template name="funcstring">
					<xsl:with-param name="wholestring" select="translate(normalize-space(),' ','')" />
				</xsl:call-template>
			</xsl:copy>
		</xsl:for-each>
	</xsl:variable>
	
	<xsl:variable name="defvars">
		<xsl:for-each select="libxslt:node-set($rawdefvars)/def">
			<xsl:copy>
				<xsl:copy-of select="@name" />
				<xsl:apply-templates mode="expandvardefs" />
			</xsl:copy>
		</xsl:for-each>
	</xsl:variable>
	
	<!--Substitue custom variables out of function trees using defvars. This method does not require the variables to have been defined in the correct order, although other parts of the software may enforce it.-->
	<!-- This is an identity template - it copies everything that doesn't match another template -->
	<xsl:template match="@* | node()" mode="substitutetree">
		<xsl:variable name="varname" select="@name" />
		<xsl:choose>
			<xsl:when test="name() = 'var' and libxslt:node-set($defvars)/def[@name = $varname]">
				<xsl:copy-of select="libxslt:node-set($defvars)/def[@name = $varname]/node()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@* | node()" mode="substitutetree" />
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
  
	<!--Root element match-->
	<xsl:template match="/">
		<wave>
			<xsl:copy-of select="/wave/@end" />
			<xsl:attribute name="resolution">
				<xsl:choose>
					<xsl:when test="/wave/@resolution">
						<xsl:value-of select="/wave/@resolution" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:copy-of select="/wave/@initialSplit" />
			<xsl:copy-of select="/wave/@maxStepRate" />
			<xsl:attribute name="fallingEdge">
				<xsl:choose>
					<xsl:when test="number(/wave/@fallingEdge) = 1 or /wave/@fallingEdge = 'true'">
						<xsl:text>1</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="motor0xPos">
				<xsl:choose>
					<xsl:when test="/wave/@motor0xPos">
						<xsl:value-of select="/wave/@motor0xPos" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>0</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:copy-of select="/wave/@gpioMapFile" />
			<xsl:copy-of select="/wave/@minDisplacement" />
			<xsl:copy-of select="/wave/@maxDisplacement" />
			
			<xsl:apply-templates select="/wave/motor" />
		</wave>
	</xsl:template>

	<xsl:template match="motor">
		<xsl:copy>
			<xsl:attribute name="spacing">
				<xsl:choose>
					<xsl:when test="@spacing">
						<xsl:value-of select="@spacing" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="not(/wave/@defaultSpacing)">
							<xsl:message terminate="yes">Spacing unspecified on motor.</xsl:message>
						</xsl:if>
						<xsl:value-of select="/wave/@defaultSpacing" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="travelPerStep">
				<xsl:choose>
					<xsl:when test="@travelPerStep">
						<xsl:value-of select="@travelPerStep" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="not(/wave/@defaultTravelPerStep)">
							<xsl:message terminate="yes">Travel per step unspecified on motor.</xsl:message>
						</xsl:if>
						<xsl:value-of select="/wave/@defaultTravelPerStep" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="pulseHeader">
				<xsl:value-of select="@pulseHeader" />
			</xsl:attribute>
			<xsl:attribute name="pulsePin">
				<xsl:value-of select="@pulsePin" />
			</xsl:attribute>
			<xsl:attribute name="directionHeader">
				<xsl:value-of select="@directionHeader" />
			</xsl:attribute>
			<xsl:attribute name="directionPin">
				<xsl:value-of select="@directionPin" />
			</xsl:attribute>
			<xsl:apply-templates select="interval" />
		</xsl:copy>
	</xsl:template>
  
	<xsl:template name="attrToElement">
		<xsl:param name="wholestring" /><!--Must not contain any spaces-->
		<!--Do not make function tree of empty strings-->
		<xsl:if test="$wholestring">
			<xsl:call-template name="funcstring">
				<xsl:with-param name="wholestring" select="$wholestring" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template match="interval">
		<interval>
			<!--Get these in the right order and convert to elements-->
			<start>
				<xsl:variable name="functree">
					<xsl:call-template name="attrToElement">
						<xsl:with-param name="wholestring" select="translate(normalize-space(@start),' ','')" />
					</xsl:call-template>
				</xsl:variable>
				<!--Substitute variable definitions-->
				<xsl:apply-templates select="libxslt:node-set($functree)" mode="substitutetree" />
			</start>
			<end>
				<xsl:variable name="functree">
					<xsl:call-template name="attrToElement">
						<xsl:with-param name="wholestring" select="translate(normalize-space(@end),' ','')" />
					</xsl:call-template>
				</xsl:variable>
				<!--Substitute variable definitions-->
				<xsl:apply-templates select="libxslt:node-set($functree)" mode="substitutetree" />
			</end>
			<period>
				<xsl:variable name="periodString" select="translate(normalize-space(@period),' ','')" />
				<xsl:choose>
					<xsl:when test="$periodString">
						<xsl:variable name="functree">
							<xsl:call-template name="funcstring">
								<xsl:with-param name="wholestring" select="$periodString" />
							</xsl:call-template>
						</xsl:variable>
						<!--Substitute variable definitions-->
						<xsl:apply-templates select="libxslt:node-set($functree)" mode="substitutetree" />
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</period>

			<xsl:variable name="wholestring" select="translate(normalize-space(),' ','')" />
			<func>
				<xsl:if test="$wholestring">
					<xsl:variable name="modstring">
						<xsl:if test="number(../@reverse) = 1 or ../@reverse = 'true' or number(/wave/@reverse) = 1 or /wave/@reverse = 'true'"><xsl:text>-</xsl:text></xsl:if>
						<xsl:text>(</xsl:text>
						<xsl:value-of select="$wholestring" />
						<xsl:text>)</xsl:text>
					</xsl:variable>
					<xsl:variable name="functree">
						<xsl:call-template name="funcstring">
							<xsl:with-param name="wholestring" select="$modstring" />
						</xsl:call-template>
					</xsl:variable>
					<!--Substitute variable definitions-->
					<xsl:apply-templates select="libxslt:node-set($functree)" mode="substitutetree" />
				</xsl:if>
			</func>
		</interval>
	</xsl:template>
</xsl:stylesheet>
