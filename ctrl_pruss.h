//Header file for starting and stopping PRUSS

/*Version control
v0.01 18/01/15 TD
v0.02 30/01/15 TD Added reset_clock()
v0.03 19/02/15 TD Added expansion_to_gpio()
v0.04 TD 13/04/15 Moved run_pruss() from load_pruss
v0.05 TD 15/04/15 Added pause option, Removed error checking where errors cannot be returned
v0.06 TD 16/04/15 Send network message slave clock started, Pause/resume works when PRUSS is disabled, Memory leak fix
v0.07 TD 17/04/15 Bug fixes
v0.08 TD 22/04/15 Added pins file
v0.09 TD 23/04/15 Added tracking of stopping location
v0.10 TD 25/04/15 Further work on stopping location & reset
v0.11 TD 27/04/15 Slave complete message moved into run_pruss + many other changes
v0.12 TD 28/04/15 Creation of zeroing file, added orig_numsockets as arg to run_pruss(), moved in much of make_pru_input
v0.13 TD 29/04/15 Libxml memory leak fixes
v0.14 TD 07/05/15 Bug fix /->* in zeroing.xml
v0.15 TD 11/05/15 ASWaMStylesheet.xsl -> ASWaMWaveStylesheet.xsl, Added compile_from_master()
v0.16 TD 12/05/15 Bug fixes
v0.17 TD 13/05/15 Bug fixes
v0.18 TD 14/05/15 Bug fixes
v0.19 TD 16/05/15 Updated to calcalgebraicfunc v0.09, Memory leak fixes, Clear interrupt before starting, Improved prussdrv_open() error messeage
v0.20 TD 19/05/15 Improved discontinuity error message
v0.21 TD 20/05/15 Fixed mode s->n for reporting motor state, Added invertTimeBeacon
v0.22 TD 09/12/15 Reset clock moved earlier, Reduced max speed to 1640 steps/s
v0.23 TD 10/12/15 Min reset time increased to 1.2s for new max speed
v0.24 TD 18/01/16 Moved compiler to compiler.c
 v0.25 TD 27/01/16 Try again with unrecognised network messages
 v0.26 TD 09/02/16 Mode and network codes put in macros
 v0.27 TD 10/02/16 Added message from master to slaves saying stopped
 v0.28 TD 23/02/16 Fixed double usage of rtn when master complete
 v0.30 TD 01/12/16 Handling of unable to write zeroing file
 v0.31 TD 02/02/17 XML parser made thread safe
 v0.32 TD 07/02/17 Bug fixes
 v0.33 TD 09/02/17 Save zeroing files in tmp folder, update zeroing XML syntax
 v0.34 TD 05/04/17 Return compiled wave time
 v0.35 TD 08/04/17 Each interval created in own thread
 v0.36 TD 20/04/17 Use safer snprintf
 v0.37 TD 22/05/17 XML names changed to camel case
 v0.38 TD 23/05/17 Number of instructions in performance stats
 v0.39 TD 29/06/17 Handles validation info appended to input
 v0.40 TD 30/06/17 Updated to v0.24 of compiler.c
 v0.41 TD 03/08/17 Handles compiler displacement limits when zeroing & zeroing messages fix
 v0.42 TD 21/11/17 Abort compile on error and return some messages
 v0.43 TD 22/01/20 Move invertTimeBeacon parameter to pins file
*/

#ifndef ctrl_pruss_h  //Prevents double inclusion
#define ctrl_pruss_h

/* Driver header file */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include <sys/socket.h>
#include "compiler.h"

#define PRU_CLOCK 0
#define PRU_GPIO 1

#define INSTR_PER_RAM 2730
#define BYTES_PER_RAM (BYTES_PER_INSTR * INSTR_PER_RAM)

#define NUM_REGS 32     //Number of registers on a PRU
//#define REG_SIZE 4      //Size of a PRU register in bytes (32 bits)

//Register numbers for debugging
#define R_MEM_LOC 3
#define R_INDEX 4

//Local addresses of RAM stored in PRU register
#define RAM_PRU 0x00000000
#define RAM_SHARED 0x00010000

#define AM18XX_PRU0CONTROL_PHYS_BASE         0x01C37000
#define AM18XX_PRU1CONTROL_PHYS_BASE         0x01C37800
#define AM33XX_PRU0CONTROL_PHYS_BASE         0x4a322000
#define AM33XX_PRU1CONTROL_PHYS_BASE         0x4a324000
#define AM18XX_PRU0DEBUG_PHYS_BASE           0x01C37400
#define AM18XX_PRU1DEBUG_PHYS_BASE           0x01C37C00
#define AM33XX_PRU0DEBUG_PHYS_BASE           0x4a322400
#define AM33XX_PRU1DEBUG_PHYS_BASE           0x4a324400

#define PAUSE_PRU 0xfffffffd
#define RESUME_PRU 2

#define MIN_RESET_TIME 1.2      //Minimum number of seconds to do reset = 50.8mm at speed limit

//Error code
#define PRUSS_FAIL 90

//Operation modes
#define MODE_NET_MASTER 'c' //Networked master, effectively a client
#define MODE_NET_SLAVE 'n'
#define MODE_IND_MASTER 'm' //Standalone master
#define MODE_IND_SLAVE 's'

//Network messages:
//From master
#define ASWAM_ABORT 'a'
#define ASWAM_RUN 'g'
#define ASWAM_ZERO_NO 'l'   //Do not zero motors
#define ASWAM_ZERO_YES 'm'  //Zero motors
#define ASWAM_PAUSE 'p'
#define ASWAM_RESUME 'r'

//From slave
#define ASWAM_STOPPED 'c'   //Slave clock has stopped - end of input stream
#define ASWAM_ZERO_DISPLACED 'd' //Motor displaced
#define ASWAM_ZERO_FAIL 'f' //Motor tracking failed
#define ASWAM_STARTED 's'   //Slave clock started
#define ASWAM_ZERO_DONE 'z' //All motors are in original position

int pru_setup();
void pru_cleanup();
int reset_clock(unsigned char invertTimeBeacon);
unsigned int* pru_ctrl_reg(unsigned int prunum);
unsigned int* debug_reg(unsigned char prunum, unsigned char reg_no);
char wait_pause_interrupt(int pausefd, int interruptfd);
void resume_clock(unsigned int* pru_ctrl_reg);
int pause_clock(unsigned int *pru_ctrl_reg, char *mode, int sockets[], int *numsockets, int *pausefd);
int wait_pause_slaves(unsigned int *clock_ctrl_reg, char *mode, int sockets[], int *numsockets, char msg);
void update_motor_pos(int motor_pos[], int motor_dir[], unsigned char gpio_pulse[], unsigned char gpio_direction[], unsigned char *motors, unsigned char instr_set[BYTES_PER_RAM], unsigned int instr_read);
void slave_closed(char *mode, int sockets[], int *numsockets, unsigned int socket_closed);
int run_pruss(char mode, char *binaryfilename, char *pinsfilename, int sockets[], int numsockets, int orig_numsockets, unsigned char invertTimeBeacon);

#endif
