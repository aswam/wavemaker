CFLAGS = -Wall -Werror -I/usr/include/libxml2
LDLIBS = -L/usr/lib -lxml2 -lxslt -lm

ifneq ($(shell uname -s),Darwin)
#Not required on macOS or doesn't exist (librt)
LDLIBS += -pthread -lrt
else
#Look in these places if Homebrew is used
CFLAGS += -I/usr/local/opt/libxml2/include/libxml2
endif

ifeq ($(DEBUG), 1)
CFLAGS += -g -O0
else
CFLAGS += -O3
endif

.PHONY: all nodoc bbbcore compile legacy doc clean

all: nodoc doc
nodoc: bbbcore compile legacy
bbbcore: align.bin clock_master.bin clock_master_itb.bin clock_slave.bin clock_stop.bin clock_stop_itb.bin gpio_driver.bin align net_compile net_run net_slave setup_BBB
compile: local_compile net_compile make_pru_input
legacy: load_pruss make_pru_input stop_pruss
doc: ASWaMDocumentation.pdf

clean:
	rm -f align load_pruss local_compile make_pru_input stop_pruss setup_BBB net_compile net_run net_slave *.o *.bin *.pdf *.log *.aux *.out *.toc *.bbl *.blg *.synctex.gz


#Documentation - requires LaTeX

ASWaMDocumentation.pdf: ASWaMDocumentation.tex library.bib
	pdflatex ASWaMDocumentation
	bibtex ASWaMDocumentation
	pdflatex ASWaMDocumentation
	pdflatex ASWaMDocumentation


#Compiler only - can run not on BBB

local_compile: local_compile.o brent.o calcalgebraicfunc.o compiler.o
make_pru_input: make_pru_input.o brent.o calcalgebraicfunc.o compiler.o
net_compile: net_compile.o brent.o calcalgebraicfunc.o compiler.o


#Driver (and compiler) - requires BBB

gpio_driver.bin: gpio_driver.p
	pasm -b -V3 $^

clock_master.bin: clock_master.p
	pasm -b -V3 $^

clock_master_itb.bin: clock_master_itb.p
	pasm -b -V3 $^

clock_slave.bin: clock_slave.p
	pasm -b -V3 $^

clock_stop.bin: clock_stop.p
	pasm -b -V3 $^

clock_stop_itb.bin: clock_stop_itb.p
	pasm -b -V3 $^

align.bin: align.p
	pasm -b -V3 $^

align: LDLIBS += -lprussdrv
align: align.o ctrl_pruss.o brent.o calcalgebraicfunc.o compiler.o

load_pruss: LDLIBS += -lprussdrv
load_pruss: load_pruss.o ctrl_pruss.o brent.o calcalgebraicfunc.o compiler.o

net_run: LDLIBS += -lprussdrv
net_run: net_run.o ctrl_pruss.o brent.o calcalgebraicfunc.o compiler.o

net_slave: LDLIBS += -lprussdrv
net_slave: net_slave.o ctrl_pruss.o brent.o calcalgebraicfunc.o compiler.o

stop_pruss: LDLIBS += -lprussdrv
stop_pruss: stop_pruss.o ctrl_pruss.o brent.o calcalgebraicfunc.o compiler.o

setup_BBB: LDLIBS += -lprussdrv
setup_BBB: setup_BBB.o ctrl_pruss.o brent.o calcalgebraicfunc.o compiler.o