//Provides functions for starting and stopping PRUSS, and GPIO mapping.

/*Version control
 v0.01 TD 18/01/15
 v0.02 TD 30/01/15 Added reset_clock()
 v0.03 TD 19/02/15 Added expansion_to_gpio()
 v0.04 TD 13/04/15 Moved run_pruss() from load_pruss
 v0.05 TD 15/04/15 Added pause option, Removed error checking where errors cannot be returned
 v0.06 TD 16/04/15 Send network message slave clock started, Pause/resume works when PRUSS is disabled, Memory leak fix
 v0.07 TD 17/04/15 Bug fixes
 v0.08 TD 22/04/15 Added pins file
 v0.09 TD 23/04/15 Added tracking of stopping location
 v0.10 TD 25/04/15 Further work on stopping location & reset
 v0.11 TD 27/04/15 Slave complete message moved into run_pruss + many other changes
 v0.12 TD 28/04/15 Creation of zeroing file, added orig_numsockets as arg to run_pruss(), moved in much of make_pru_input
 v0.13 TD 29/04/15 Libxml memory leak fixes
 v0.14 TD 07/05/15 Bug fix /->* in zeroing.xml
 v0.15 TD 11/05/15 ASWaMStylesheet.xsl -> ASWaMWaveStylesheet.xsl, Added compile_from_master()
 v0.16 TD 12/05/15 Bug fixes
 v0.17 TD 13/05/15 Bug fixes
 v0.18 TD 14/05/15 Bug fixes
 v0.19 TD 16/05/15 Updated to calcalgebraicfunc v0.09, Memory leak fixes, Clear interrupt before starting, Improved prussdrv_open() error messeage
 v0.20 TD 19/05/15 Improved discontinuity error message
 v0.21 TD 20/05/15 Fixed mode s->n for reporting motor state, Added invertTimeBeacon
 v0.22 TD 09/12/15 Reset clock moved earlier, Reduced max speed to 1640 steps/s
 v0.23 TD 10/12/15 Min reset time increased to 1.2s for new max speed
 v0.24 TD 18/01/16 Moved compiler to compiler.c
 v0.25 TD 27/01/16 Try again with unrecognised network messages
 v0.26 TD 09/02/16 Mode and codes put in macros
 v0.27 TD 10/02/16 Added message from master to slaves saying stopped
 v0.28 TD 23/02/16 Fixed double usage of rtn when master complete
 v0.30 TD 01/12/16 Handling of unable to write zeroing file
 v0.31 TD 02/02/17 XML parser made thread safe
 v0.32 TD 07/02/17 Bug fixes
 v0.33 TD 09/02/17 Save zeroing files in tmp folder, update zeroing XML syntax
 v0.34 TD 05/04/17 Return compiled wave time
 v0.35 TD 08/04/17 Each interval created in own thread
 v0.36 TD 20/04/17 Use safer snprintf
 v0.37 TD 22/05/17 XML names changed to camel case
 v0.38 TD 23/05/17 Number of instructions in performance stats
 v0.39 TD 29/06/17 Handles validation info appended to input
 v0.40 TD 30/06/17 Updated to v0.24 of compiler.c
 v0.41 TD 03/08/17 Handles compiler displacement limits when zeroing & zeroing messages fix
 v0.42 TD 21/11/17 Abort compile on error and return some messages
 v0.43 TD 22/01/20 Move invertTimeBeacon parameter to pins file
 */

/* Driver header file */
#include "ctrl_pruss.h"

/* If run_pruss fails, it returns -1. This can only occur before the clock starts running. The calling function will then close the sockets, causing the remaining networked BBBs to ignore the failed one, but present extra messages to compensate. */

/*** pru_setup() -- initialize PRU and interrupt handler
 
 Initializes the PRU specified by PRU_NUM and sets up PRU_EVTOUT_1 handler.
 
 The argument is a pointer to a nul-terminated string containing the path
 to the file containing the PRU program binary.
 
 Returns 0 on success, non-0 on error.
 ***/
int pru_setup()
{
    int rtn;
    
    tpruss_intc_initdata intc = PRUSS_INTC_INITDATA;
    
    /* initialize PRU, doesn't alter control register */
    prussdrv_init();
    
    /* open the interrupt, doesn't alter control register */
    if ((rtn = prussdrv_open(PRU_EVTOUT_1)) != 0) {
        fprintf(stderr, "Failed to open PRUSS driver. Must run ./initialise first.\n");
        return PRUSS_FAIL;
    }
    
    /* initialize interrupt, doesn't alter control registers */
    if ((rtn = prussdrv_pruintc_init(&intc)) != 0) {
        fprintf(stderr, "prussdrv_pruintc_init() failed\n");
        return rtn;
    }
    
    return rtn;
}

void pru_cleanup()
//Do not run if prussdrv_open failed (code PRUSS_FAIL = 100)
{
    int k;
    
    /* clear the event (if asserted) - always returns zero*/
    prussdrv_pru_clear_event(PRU_EVTOUT_1, PRU1_ARM_INTERRUPT);
    
    /* halt and disable the PRUs (if running), but does not reset PRU */
    for (k = 0; k <= 1; k++)
    {
        if (prussdrv_pru_disable(k)) {
            fprintf(stderr, "prussdrv_pru_disable() failed on PRU%d", k);
        }
    }
    
    /* release the PRU clocks and disable prussdrv module */
    prussdrv_exit();
}

int reset_clock(unsigned char invertTimeBeacon)
{
    //Switches off master timing beacon and disables PRUSS
    int rtn;
    /* initialize the library, PRUSS and interrupt*/
    
    if ((rtn = pru_setup())) {
        if (rtn != PRUSS_FAIL) pru_cleanup();
        return -1;
    }
    
    // halt and disable the GPIO PRU
    if ((rtn = prussdrv_pru_disable(PRU_GPIO)) != 0) {
        fprintf(stderr, "prussdrv_pru_disable() failed on PRU%d\n", PRU_GPIO);
        return -1;
    }
    
    printf("Switching off master clock beacon\n");
    if (invertTimeBeacon) {
        rtn = prussdrv_exec_program(PRU_CLOCK, "clock_stop_itb.bin");
    } else {
        rtn = prussdrv_exec_program(PRU_CLOCK, "clock_stop.bin");
    }
    if (rtn < 0)     //Else 0
    {
        fprintf(stderr, "prussdrv_exec_program() failed on PRU%d\n", PRU_CLOCK);
        pru_cleanup();
        return rtn;
    }
    sleep(1);
    
    pru_cleanup();
    return 0;
}

unsigned int* pru_ctrl_reg(unsigned int prunum)
{
    unsigned int physaddr = 0;
    switch (prussdrv_version()) {
        case PRUSS_V1:
        switch (prunum) {
            case 0:
            physaddr = AM18XX_PRU0CONTROL_PHYS_BASE;
            break;
            case 1:
            physaddr = AM18XX_PRU1CONTROL_PHYS_BASE;
            break;
            default:
            fprintf(stderr, "Unrecognised PRU number %u\n", prunum);
        }
        break;
        case PRUSS_V2:
        switch (prunum) {
            case 0:
            physaddr = AM33XX_PRU0CONTROL_PHYS_BASE;
            break;
            case 1:
            physaddr = AM33XX_PRU1CONTROL_PHYS_BASE;
            break;
            default:
            fprintf(stderr, "Unrecognised PRU number %u\n", prunum);
        }
        break;
        default:
        fprintf(stderr, "Unrecognised PRUSS version number\n");
    }
    
    if (physaddr > 0) {
        return (unsigned int*) prussdrv_get_virt_addr(physaddr);
    }
    else {
        return 0;
    }
}

unsigned int* debug_reg(unsigned char prunum, unsigned char reg_no)
//Returns the virtual address of the debug register reg_no for PRU prunum
{
    unsigned int physaddr = 0;
    switch (prussdrv_version()) {
        case PRUSS_V1:
        switch (prunum) {
            case 0:
            physaddr = AM18XX_PRU0DEBUG_PHYS_BASE;
            break;
            case 1:
            physaddr = AM18XX_PRU1DEBUG_PHYS_BASE;
            break;
            default:
            fprintf(stderr, "Unrecognised PRU number %u\n", prunum);
        }
        break;
        case PRUSS_V2:
        switch (prunum) {
            case 0:
            physaddr = AM33XX_PRU0DEBUG_PHYS_BASE;
            break;
            case 1:
            physaddr = AM33XX_PRU1DEBUG_PHYS_BASE;
            break;
            default:
            fprintf(stderr, "Unrecognised PRU number %u\n", prunum);
        }
        break;
        default:
        fprintf(stderr, "Unrecognised PRUSS version number\n");
    }
    
    if (reg_no >= NUM_REGS) {
        physaddr = 0;
        fprintf(stderr, "PRU register number %u does not exist.\n", reg_no);
    }
    
    if (physaddr > 0) {
        return((unsigned int*) prussdrv_get_virt_addr(physaddr) + reg_no); //Do not times reg_no by 4 for some unknown reason
    }
    else {
        return 0;
    }
}

char wait_pause_interrupt(int pausefd, int interruptfd)
{
    //Waits for pause signal (expects 'p') or interrupt, resets fd and returns.
    
    //Create set of file descriptors
    fd_set fdset;
    FD_ZERO(&fdset);
    FD_SET(pausefd, &fdset);
    FD_SET(interruptfd, &fdset);
    
    //Wait for a signal
    select(FD_SETSIZE, &fdset, NULL, NULL, NULL);
    
    if (FD_ISSET(pausefd, &fdset)) {
        if (pausefd == STDIN_FILENO) {
            //Pause
            //Need to clear stdin buffer
            while (getchar() != '\n');
            return 'p';
        }
        else {
            char buffer[1];
            int rcvd = recv(pausefd, buffer, 1, 0);
            if (rcvd < 1) {
                return 'c'; //Connection closed
            }
            else {
                char oldbuffer = buffer[0];
                while (buffer[0] != ASWAM_PAUSE) {
                    //Try again for recognised message 'p', warning about unrecognised ones
                    fprintf(stderr, "WARNING: Unrecognised message received from master: %c. Trying buffer again.\n", buffer[0]);
                    rcvd = recv(pausefd, buffer, 1, MSG_DONTWAIT);   //Networked slave has one com socket
                    if (rcvd < 1) {
                        fprintf(stderr, "ERROR: Failed to get recognisable message from master in wait_pause_interrupt().\n");
                        break;
                    }
                    oldbuffer = buffer[0];
                }
                return oldbuffer;   //Should contain 'p' for pause
            }
        }
    }
    else {
        //Interrupt for new data
        //Read event from buffer to clear it
        prussdrv_pru_wait_event(PRU_EVTOUT_1);
        /* clear the event (if asserted) */
        prussdrv_pru_clear_event(PRU_EVTOUT_1, PRU1_ARM_INTERRUPT);
        
        return 'i';
    }
}

void resume_clock(unsigned int* clock_ctrl_reg)
//clock_ctrl_reg can be NULL
{
    if (clock_ctrl_reg) {
        *clock_ctrl_reg |= RESUME_PRU;    //Set bit 1
    }
    printf("Clock resumed.\n");
}

int pause_clock(unsigned int *clock_ctrl_reg, char *mode, int sockets[], int *numsockets, int *pausefd)
//clock_ctrl_reg can be NULL
{
    int k, rtn;
    char buffer[1];
    
    //Broadcast pause message if networked master, do before pausing clock to maximise synchronisation between master & slaves
    if (*mode == MODE_NET_MASTER) {
        buffer[0] = ASWAM_PAUSE;
        for (k = 0; k < *numsockets; k++) {
            rtn = send(sockets[k], buffer, 1, MSG_NOSIGNAL);
            if (rtn < 1) {
                //Connection broken
                slave_closed(mode, sockets, numsockets, k);
            }
        }
    }
    
    if (clock_ctrl_reg) {
        *clock_ctrl_reg &= PAUSE_PRU;     //Clear bit 1 in the PRU control register
    }
    
    switch (*mode) {
        case MODE_NET_SLAVE:
        printf("Clock paused.\n");
        rtn = recv(sockets[0], buffer, 1, 0);   //Networked slave has one com socket
        if (rtn == 1) {
            while (buffer[0] != ASWAM_RESUME && buffer[0] != ASWAM_ABORT) {
                //Try again for recognised message, warning about unrecognised ones
                fprintf(stderr, "WARNING: Unrecognised message received from master: %c. Trying buffer again.\n", buffer[0]);
                rtn = recv(sockets[0], buffer, 1, MSG_DONTWAIT);   //Networked slave has one com socket
                if (rtn != 1) {
                    fprintf(stderr, "ERROR: Failed to receive recognisable message from master.\n");
                    break;
                }
            }
            
            if (buffer[0] == ASWAM_RESUME) {
                //Resume
                resume_clock(clock_ctrl_reg);
                return 0;
            }
            
            //Abort if unrecognised message
            printf("Aborting.\n");
            return 110;
        }
        else {
            fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
            *mode = MODE_IND_SLAVE;
            *pausefd = STDIN_FILENO;
        }
        default:
        do {
            printf("Clock paused. Resume/abort? [%c/%c]: ", ASWAM_RESUME, ASWAM_ABORT);
            buffer[0] = getchar();
            if (buffer[0] != '\n') {
                while (getchar() != '\n');
            }
        } while (buffer[0] != ASWAM_RESUME && buffer[0] != ASWAM_ABORT);
        
        if (*mode == MODE_NET_MASTER) {
            for (k = 0; k < *numsockets; k++) {
                rtn = send(sockets[k], buffer, 1, MSG_NOSIGNAL);
                if (rtn < 1) {
                    //Connection broken
                    slave_closed(mode, sockets, numsockets, k);
                }
            }
        }
        
        if (buffer[0] == ASWAM_RESUME) {
            //Resume
            resume_clock(clock_ctrl_reg);
            return 0;
        }
        
        printf("Aborting.\n");
        return 110;
    }
}

int wait_pause_slaves(unsigned int *clock_ctrl_reg, char *mode, int sockets[], int *numsockets, char msg)
//Blocks until all slaves have sent message msg and allows pause/resume/abort simultaneously via stdin
//Returns 0 when all slaves have sent msg, 1 if a connection is lost, 2 if wrong message received, 110 on abort, -1 on error
//clock_ctrl_reg can be NULL
{
    int k, status;
    int rtn = 0;
    char buffer[1];
    
    //Create set of file descriptors
    fd_set fdset;
    
    for (k = 0; k < *numsockets; k++) {
        FD_ZERO(&fdset);
        FD_SET(STDIN_FILENO, &fdset);
        FD_SET(sockets[k], &fdset);
        
        //Keep looping through pause/resume until socket receive
        while (1) {
            select(FD_SETSIZE, &fdset, NULL, NULL, NULL);
            if (!FD_ISSET(STDIN_FILENO, &fdset)) {
                break;
            }
            
            //Need to clear stdin buffer
            while (getchar() != '\n');
            
            //Paused
            status = pause_clock(clock_ctrl_reg, mode, sockets, numsockets, NULL); //Only called in mode 'c', so don't need pausefd
            
            //Return errors/abort
            if (status) {
                return status;
            }
        }
        
        //Don't mark message as read by using MSG_PEEK. Will be marked as read once all slaves have returned the correct message. Then, STDIN can't leave the messages in limbo part-read.
        status = recv(sockets[k], buffer, 1, MSG_PEEK);
        if (status == 1) {
            while (buffer[0] != msg) {
                fprintf(stderr, "WARNING: Unrecognised message received from slave %d: %c. Trying buffer again.\n", k, buffer[0]);
                //Move incorrect message out of the way
                status = recv(sockets[k], buffer, 1, 0);
                if (status < 1) {
                    //Slave has just been lost in this split second. Loop and try again on it. If it is indeed lost, slave_closed() will be called.
                    break;  //Out of while
                }
                //Try the next message if it exists, but don't block
                status = recv(sockets[k], buffer, 1, MSG_DONTWAIT | MSG_PEEK);
                if (status != 1) {
                    fprintf(stderr, "ERROR: Failed to receive recognisable message from slave %d\n", k);
                    rtn = 2;
                    return rtn;
                }
            }
            if (status < 1) {
                //Looping to try again at this k, so subtract 1, which will be added back on again by for
                k--;
            }
        } else {
            if (status < 0) {
                rtn = status;
                perror("Networking error");
            } else {
                rtn = 1;
            }
            slave_closed(mode, sockets, numsockets, k);
            k--;    //Need to repeat at this value of k as this slave has been removed
        }
    }
    
    //Mark all messages as read and don't wait as they should all be ready and waiting
    for (k = 0; k < *numsockets; k++) {
        status = recv(sockets[k], buffer, 1, MSG_DONTWAIT);
        if (status < 1) {
            if (status < 0) {
                rtn = status;
                perror("Networking error");
            } else {
                rtn = 1;
            }
            slave_closed(mode, sockets, numsockets, k);
            k--;    //Need to repeat at this value of k as this slave has been removed
        }
    }
    
    return rtn;   //Message msg received from all slaves that are still connected
}

void update_motor_pos(int motor_pos[], int motor_dir[], unsigned char gpio_pulse[], unsigned char gpio_direction[], unsigned char *motors, unsigned char instr_set[BYTES_PER_RAM], unsigned int instr_read)
//Updates the motor positions, based on the number of instructions read (retrieved from PRU debug register)
{
    if (*motors) {
        //motors = 0 means there is a fault with the motor tracking
        unsigned int k;
        for (k = 0; k < instr_read; k++) {
            unsigned char gpio = instr_set[k * BYTES_PER_INSTR];
            if (gpio == 0) {
                //Blank instruction
                continue;
            }
            
            unsigned char motor_id;
            for (motor_id = 0; motor_id < *motors; motor_id++) {
                if (gpio_pulse[motor_id] == gpio) {
                    motor_pos[motor_id] += motor_dir[motor_id];
                    break;      //Break out of for loop, go on to next instruction
                } else if ((gpio_pulse[motor_id] | SC_BIT) == (gpio | SC_BIT)) {
                    //Pulse pin found but not a motor step
                    break;
                } else if (gpio_direction[motor_id] == gpio) {
                    //Set direction to zero found, which means move motor in positive direction
                    motor_dir[motor_id] = 1;
                    break;
                } else if (gpio_direction[motor_id] + SC_BIT == gpio) {
                    motor_dir[motor_id] = -1;
                    break;
                }
            }
            if (motor_id == *motors) {
                //Have reached end of for loop without break - instruction not found
                if (gpio >= SC_BIT) {
                    gpio -= SC_BIT;
                }
                fprintf(stderr, "GPIO pin %u not found. Motor locations cannot be remembered for resetting.\n", gpio);
                *motors = 0;
                break;
            }
        }
    }
}

void slave_closed(char *mode, int sockets[], int *numsockets, unsigned int socket_closed)
//Removes a closed socket from the list. For use in network master mode.
{
    fprintf(stderr, "WARNING: The connection to slave %u has closed unexpectedly.\n", socket_closed);
    *numsockets -= 1;
    if (*numsockets == 0) {
        fprintf(stderr, "All slave connections lost. Attempting to continue as non-networked master.\n");
        *mode = MODE_IND_MASTER;
    } else if (socket_closed < *numsockets) {
        memmove(sockets + (sizeof(int) * socket_closed), sockets + (sizeof(int) * (socket_closed + 1)), sizeof(int) * (*numsockets - socket_closed));
    }
}

int run_pruss(char mode, char *binaryfilename, char *pinsfilename, int sockets[], int numsockets, int orig_numsockets, unsigned char invertTimeBeacon)
//Set socket to NULL if mode='m' or using ./load_pruss -s
//Returns 0 if everything runs to completion, 110 on abort and -1 on error
{
    int rtn = 0;
    char buffer[256];
    unsigned char motors = 255;
    
    //Need to keep history of two instruction sets as cannot guarantee all instructions have been executed until after second interrupt (the last instruction will not have been executed on first interrupt)
    unsigned char instr_set[BYTES_PER_RAM], prev1_instr_set[BYTES_PER_RAM], prev2_instr_set[BYTES_PER_RAM];
    long instr_remaining = 0;
    int k, next_ram;
    
    xmlDocPtr xmlRawDoc = NULL;
    xmlNodePtr xmlNode;
    xmlDocPtr xmlTree = NULL;
    xmlChar* xmlText;
    
    //Load pins XML file
    if (strlen(pinsfilename) == 0) {
        motors = 0;
    } else {
        xmlRawDoc = xmlParseFile(pinsfilename);
        if (xmlRawDoc == NULL)
        {
            fprintf(stderr, "XML pins file parse unsuccessful. Motor positions will not be remembered.\nHardware inversion of timing beacon not read from file.\n");
            motors = 0;
        }
    }
    
    if (motors) {
        //Validate XML doc
        char* schema_path;
        //Use ASWaMPinsSchema.xsd if end attribute is missing from <wave>, otherwise ASWaMWaveSchema.xsd
        xmlNode = xmlDocGetRootElement(xmlRawDoc);  //<master>
        if ((xmlText = xmlGetProp(xmlNode, (const xmlChar*)"end"))) {
            //Use ASWaMWaveSchema
            schema_path = "ASWaMWaveSchema.xsd";
        }
        else {
            //Use ASWaMPinsSchema
            schema_path = "ASWaMPinsSchema.xsd";
        }
        xmlFree(xmlText);
        
        xmlDocPtr schema_doc = NULL;
        xmlSchemaParserCtxtPtr parser_ctxt = NULL;
        xmlSchemaPtr schema = NULL;
        xmlSchemaValidCtxtPtr valid_ctxt = NULL;
        schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
        parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
        schema = xmlSchemaParse(parser_ctxt);
        valid_ctxt = xmlSchemaNewValidCtxt(schema);
        int validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
        if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
        if (schema) xmlSchemaFree(schema);
        if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
        if (schema_doc) xmlFreeDoc(schema_doc);
        if (validationResult != 0)
        {
            fprintf(stderr, "Incorrect syntax in XML pins file. Motor positions will not be remembered.\nHardware inversion of timing beacon not read from file.\n");
            xmlFreeDoc(xmlRawDoc);
            motors = 0;
        }
    }
    
    if (motors) {       //motors = 0 corresponds to not tracking motors
		//Is the timing beacon hardware-inverted?
		//xmlNode still points at root element
		xmlText = xmlGetProp(xmlNode, (const xmlChar*)"invertTimeBeacon");
		if (xmlText) {
	        if (xmlStrEqual(xmlText, (const xmlChar*) "1") || xmlStrEqual(xmlText, (const xmlChar*) "true")) {
	            invertTimeBeacon = 1;
	        }
	        xmlFree(xmlText);
		}
		
        //Transform XML using XSLT
        xsltStylesheetPtr stylesheet_doc = xsltParseStylesheetFile((const xmlChar*) "ASWaMWaveStylesheet.xsl");
        xsltTransformContextPtr ctxt = xsltNewTransformContext(stylesheet_doc, xmlRawDoc);
        xmlTree = xsltApplyStylesheetUser(stylesheet_doc, xmlRawDoc, NULL, NULL, NULL, ctxt);
        
        //XSLT Cleanup
        xsltFreeStylesheet(stylesheet_doc);
        xmlFreeDoc(xmlRawDoc);
        
        if ((ctxt->state == XSLT_STATE_ERROR) || (ctxt->state == XSLT_STATE_STOPPED))
        {
            perror("Error in applying XSLT stylesheet. Motor positions will not be remembered.");
            xsltFreeTransformContext(ctxt);
            motors = 0;
        }
        xsltFreeTransformContext(ctxt);
    }
    
    unsigned char motor_id;
    unsigned char gpio_pulse[MAX_MOTORS], gpio_direction[MAX_MOTORS];   //gpio_pulse is the char code for step motor
    
    if (motors) {
        xmlNode = xmlDocGetRootElement(xmlTree);
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"fallingEdge");
        unsigned char fallingEdge = (unsigned char) atoi((const char*)xmlText);
        xmlFree(xmlText);
        
        //Open GPIO map file
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"gpioMapFile");
        FILE *gpiomap;
        F_OPEN(gpiomap, (const char*)xmlText, "r");
        xmlFree(xmlText);
        
        motor_id = 0;
        int expansion_header, expansion_pin;
        xmlNode = xmlFirstElementChild(xmlNode);		//<motor>
        
        if (!gpiomap) {
            fprintf(stderr, "Could not open GPIO map file. Motor positions will not be remembered.\n");
            xmlNode = NULL;
        }
        
        while (xmlNode)
        {
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"pulseHeader");
            expansion_header = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"pulsePin");
            expansion_pin = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            gpio_pulse[motor_id] = expansion_to_gpio(expansion_header, expansion_pin, gpiomap);
            if (gpio_pulse[motor_id] == 0) {
                fprintf(stderr, "Pulse pin on motor %d not found. Motor positions will not be remembered.\n", motor_id);
                motor_id = 0;       //To set motors = 0 outside while loop
                break;
            }
            if (fallingEdge == 0) {
                //Step on rising edge
                gpio_pulse[motor_id] += SC_BIT;
            }
            
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"directionHeader");
            expansion_header = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"directionPin");
            expansion_pin = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            gpio_direction[motor_id] = expansion_to_gpio(expansion_header, expansion_pin, gpiomap);
            if (gpio_direction[motor_id] == 0) {
                fprintf(stderr, "Direction pin on motor %d not found. Motor positions will not be remembered.\n", motor_id);
                motor_id = 0;       //To set motors = 0 outside while loop
                break;
            }
            
            motor_id++;
            xmlNode = xmlNextElementSibling(xmlNode);
        }
        
        motors = motor_id;
        if (gpiomap) fclose(gpiomap);
    }
    
    //Keep track of current motor positions: Initialise values
    int motor_pos[motors], motor_dir[motors];
    for (motor_id = 0; motor_id < motors; motor_id++) {
        motor_pos[motor_id] = 0;
        motor_dir[motor_id] = 0;
    }
    
    //Open binary file
    FILE *binaryfp = NULL;
    if (strlen(binaryfilename) > 0) {
        binaryfp = fopen(binaryfilename, "rb");
        if (!binaryfp)
        {
            fprintf(stderr, "Binary file not found.\n");
            //Cleanup XML
            if (xmlTree) xmlFreeDoc(xmlTree);
            return -1;      //Calling function must cope with this fatal error
        }
        
        //Read header giving offset of end of instructions - 4 bytes with little endian byte order
        if (fread(instr_set, sizeof instr_set[0], 4, binaryfp) < 4) {
            perror("ERROR: Could not read binary input file");
            if (xmlTree) xmlFreeDoc(xmlTree);
            fclose(binaryfp);
            return EXIT_FAILURE;
        }	//binaryfp is now at start of input
        
        //Little endian
        const unsigned long pruss_input_len = (unsigned long)instr_set[0] + ((unsigned long)instr_set[1] << 8) + ((unsigned long)instr_set[2] << 16) + ((unsigned long)instr_set[3] << 24);
        instr_remaining = (long)(pruss_input_len - 4) / BYTES_PER_INSTR;	//Counts down to 0, implementation sends it negative
    }
    
    long instr_found = 0;
    
    //Need to fill prev2_instr_set with blanks, not necessary to do this for prev1_instr_set
    for (k = 0; k < BYTES_PER_RAM; k++) {
        prev2_instr_set[k] = 0;
    }
    
    /* initialize the library, PRUSS and interrupt*/
    if ((rtn = pru_setup()) != 0) {
        if (strlen(binaryfilename) > 0) { fclose(binaryfp); }
        if (rtn != PRUSS_FAIL) pru_cleanup();
        //Cleanup XML
        if (xmlTree) xmlFreeDoc(xmlTree);
        return -1;
    }
    
    //Get file descriptors
    int interruptfd = prussdrv_pru_event_fd(PRU_EVTOUT_1);
    int pausefd;
    unsigned int* clock_ctrl_reg = pru_ctrl_reg(PRU_CLOCK);
    
    /* clear the event (if asserted) - ensures no leftover interrupts will muck up program */
    prussdrv_pru_clear_event(PRU_EVTOUT_1, PRU1_ARM_INTERRUPT);
    
    /* load array on PRUSS */
    if (instr_remaining > 0) {
        //instr_remaining = 0 if binaryfilename is empty
        instr_found = fread(instr_set, BYTES_PER_INSTR, INSTR_PER_RAM, binaryfp);
        instr_remaining -= instr_found;
        if (instr_remaining < 0) { instr_found += instr_remaining; }	//Subtract off excess instructions
    }
    for (k = instr_found * 3; k < BYTES_PER_RAM; k += 3)
    {
        *(instr_set + k) = 0;			//No GPIO
        *(instr_set + k + 1) = 0xff;	//Max time
        *(instr_set + k + 2) = 0xff;	//Max time
    }
    
    prussdrv_pru_write_memory(PRUSS0_PRU1_DATARAM, 0, (const unsigned int *)instr_set, BYTES_PER_RAM);
    memcpy(prev1_instr_set, instr_set, BYTES_PER_RAM);       //Archive instruction set to be analysed when interrupt saying it has all been used is received
    
    if (instr_remaining > 0) {
        instr_found = fread(instr_set, BYTES_PER_INSTR, INSTR_PER_RAM, binaryfp);
        instr_remaining -= instr_found;
        instr_found += instr_remaining;
    } else {
        instr_found = 0;
    }
    
    for (k = instr_found * 3; k < BYTES_PER_RAM; k += 3)
    {
        *(instr_set + k) = 0;			//No GPIO
        *(instr_set + k + 1) = 0xff;	//Max time
        *(instr_set + k + 2) = 0xff;	//Max time
    }
    prussdrv_pru_write_memory(PRUSS0_SHARED_DATARAM, 0, (const unsigned int *)instr_set, BYTES_PER_RAM);
    
    /* Load and execute binary on PRU */
    if ((rtn = prussdrv_exec_program(PRU_GPIO, "gpio_driver.bin")) < 0) {
        fprintf(stderr, "prussdrv_exec_program() failed on PRU%d\n", PRU_GPIO);
        if (strlen(binaryfilename) > 0) { fclose(binaryfp); }
        pru_cleanup();
        //Cleanup XML
        if (xmlTree) xmlFreeDoc(xmlTree);
        return rtn;
    }
    printf("GPIO driver loaded on PRU%d\n", PRU_GPIO);
    
    sleep(1);
    //Load clock
    switch (mode) {
        case MODE_NET_MASTER:
        //Network master, effectively "client"; slave clocks will all be running before this is called
        if (numsockets < orig_numsockets) {
            printf("Do not start the master clock until all the slave clocks are running. Press enter to start the master clock.");
            while (getchar() != '\n');
        }
        if (invertTimeBeacon) {
            rtn = prussdrv_exec_program(PRU_CLOCK, "clock_master_itb.bin");
        } else {
            rtn = prussdrv_exec_program(PRU_CLOCK, "clock_master.bin");
        }
        pausefd = STDIN_FILENO;
        break;
        case MODE_IND_MASTER:
        //Standalone master
        printf("Do not start the master clock until all the slave clocks are running. Press enter to start the master clock.");
        while (getchar() != '\n');
        if (invertTimeBeacon) {
            rtn = prussdrv_exec_program(PRU_CLOCK, "clock_master_itb.bin");
        } else {
            rtn = prussdrv_exec_program(PRU_CLOCK, "clock_master.bin");
        }
        pausefd = STDIN_FILENO;
        break;
        case MODE_NET_SLAVE:
        //Network slave
        rtn = prussdrv_exec_program(PRU_CLOCK, "clock_slave.bin");
        pausefd = sockets[0];
        break;
        case MODE_IND_SLAVE:
        //Standalone slave
        rtn = prussdrv_exec_program(PRU_CLOCK, "clock_slave.bin");
        pausefd = STDIN_FILENO;
        break;
        default:
        fprintf(stderr, "Unrecognised mode %c\n", mode);
    }
    if (rtn < 0) {
        fprintf(stderr, "prussdrv_exec_program() failed on PRU%d\n", PRU_CLOCK);
        if (strlen(binaryfilename) > 0) { fclose(binaryfp); }
        pru_cleanup();
        //Cleanup XML
        if (xmlTree) xmlFreeDoc(xmlTree);
        return rtn;
    }
    
    if (mode == MODE_NET_SLAVE) {
        //Send message to master to say slave is ready: clock has started 's'
        buffer[0] = ASWAM_STARTED;
        rtn = send(sockets[0], buffer, 1, MSG_NOSIGNAL);
        if (rtn < 1) {
            fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
            mode = MODE_IND_SLAVE;
            pausefd = STDIN_FILENO;
        }
    }
    printf("Clock running on PRU%d.\n", PRU_CLOCK);
    
    if (mode != MODE_NET_SLAVE) {
        printf("Press enter to pause the clock.\n");
    }
    
    next_ram = 1;
    rtn = 0;
    
    if (strlen(binaryfilename) > 0)
    {
        do	//Do while not at the end of the input stream, will produce a whole empty RAM before moving on with instr_found = 0
        {
            buffer[0] = wait_pause_interrupt(pausefd, interruptfd);
            if (buffer[0] == 'p') {
                //Pause - returns 0 on resume, 110 on abort
                rtn = pause_clock(clock_ctrl_reg, &mode, sockets, &numsockets, &pausefd);
            } else if (buffer[0] == 'i') {
                //Interrupt for new data
                
                //Need to update current motor positions using previous instruction set
                update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev2_instr_set, INSTR_PER_RAM);
                
                //Update instruction set history
                memcpy(prev2_instr_set, prev1_instr_set, BYTES_PER_RAM);
                memcpy(prev1_instr_set, instr_set, BYTES_PER_RAM);
                
                /* load array on PRUSS */
                if (instr_remaining > 0) {
                    instr_found = fread(instr_set, BYTES_PER_INSTR, INSTR_PER_RAM, binaryfp);
                    instr_remaining -= instr_found;
                    if (instr_remaining < 0) { instr_found += instr_remaining; }	//Subtract off excess instructions
                } else {
                    instr_found = 0;
                }
                
                //Fast file so will always return required buffer size, unless eof
                for (k = instr_found * 3; k < BYTES_PER_RAM; k += 3)
                {
                    *(instr_set + k) = 0;			//No GPIO
                    *(instr_set + k + 1) = 0xff;	//Max time
                    *(instr_set + k + 2) = 0xff;	//Max time
                }
                
                if (next_ram == 1)
                {
                    prussdrv_pru_write_memory(PRUSS0_PRU1_DATARAM, 0, (const unsigned int *)instr_set, BYTES_PER_RAM);
                    next_ram = 2;
                }
                else
                {
                    prussdrv_pru_write_memory(PRUSS0_SHARED_DATARAM, 0, (const unsigned int *)instr_set, BYTES_PER_RAM);
                    next_ram = 1;
                }
            } else if (buffer[0] == 'c') {
                //Connection closed: switch mode to s and continue. Can only get this in mode n.
                fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
                printf("Press enter to pause the clock.\n");
                mode = MODE_IND_SLAVE;
                pausefd = STDIN_FILENO;
            } else {
                //Unrecognised response
                rtn = -1;
            }
        } while (instr_found > 0 && rtn == 0);	//Don't use instr_remaining > 0 as need whole empty RAM before moving on
        
        fclose(binaryfp);
    }
    
    if (rtn == 0) {
        printf("End of input stream\n");
        //One RAM is now entirely blank, wait one more interrupt to say that now on blank RAM, i.e. no more instructions
        
        do {
            buffer[0] = wait_pause_interrupt(pausefd, interruptfd);
            if (buffer[0] == 'p') {
                rtn = pause_clock(clock_ctrl_reg, &mode, sockets, &numsockets, &pausefd);
            }
        } while (buffer[0] == 'p' && rtn == 0);     //Don't move on until it is an interrupt
        sleep(1);		//Must not be greater than 5.3s, wait in case of delay in driving motors
    }
    
    //Stop PRUSS
    printf("Disabling PRUSS\n");
    
    // halt and disable the GPIO PRU
    prussdrv_pru_disable(PRU_GPIO);
    sleep(1);   //Allow time before attempting to read debug registers
    
    //Need to update current motor positions using previous instruction set
    if (rtn == 0 || motors == 0) {
        update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev2_instr_set, INSTR_PER_RAM);
        update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev1_instr_set, INSTR_PER_RAM);
        //Don't need to use the final instruction set as it is all blanks
    } else {
        //Extract how many instructions have been executed from PRU debug register
        //Disable PRU_GPIO before reading debug register - already done
        
        unsigned int instr_exec;
        unsigned int *reg_index = debug_reg(PRU_GPIO, R_INDEX);
        
        //Register R_INDEX contains the location in bytes of the next instruction to be loaded. This is one instruction more than the one that is currently waiting for the clock. Since first instruction has index zero, this is one more than the number already executed.
        instr_exec = (*reg_index / 3) - 1;
        
        if (instr_exec < INSTR_PER_RAM) {  //instr_exec != -1, so must be in prev1_instr_set
            update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev2_instr_set, INSTR_PER_RAM);
            update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev1_instr_set, instr_exec);
        } else {
            unsigned char end_ram;
            unsigned int *reg_mem_loc = debug_reg(PRU_GPIO, R_MEM_LOC);
            if (*reg_mem_loc == RAM_PRU) {
                end_ram = 1;
            } else {
                end_ram = 2;
            }
            
            //Have executed all but the last instruction on a RAM
            if (end_ram == next_ram) {
                //Have not read any instructions on prev1 yet (only just loaded new data), so have executed all but one instruction on prev2
                update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev2_instr_set, INSTR_PER_RAM - 1);
            } else {
                //There is a change-RAM interrupt waiting, so have executed all but one instruction on prev1 (and all on prev2)
                update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev2_instr_set, INSTR_PER_RAM);
                update_motor_pos(motor_pos, motor_dir, gpio_pulse, gpio_direction, &motors, prev1_instr_set, INSTR_PER_RAM - 1);
            }
        }
    }
    
    // halt and disable the clock PRU
    switch (mode) {
        case MODE_NET_SLAVE:
        //Send message to master to say complete
        buffer[0] = ASWAM_STOPPED;      //Code 'c' means slave is complete
        
        rtn = send(sockets[0], buffer, 1, MSG_NOSIGNAL);
        if (rtn < 1) {
            fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
            mode = MODE_IND_SLAVE;
            pausefd = STDIN_FILENO;
        }
        
        //Block, waiting for master to say complete. Necessary as master could abort, confusing messages
        do {
            rtn = recv(sockets[0], buffer, 1, 0);
        } while (rtn == 1 && buffer[0] != ASWAM_STOPPED);
        if (rtn < 1) {
            fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
            mode = MODE_IND_SLAVE;
            pausefd = STDIN_FILENO;
        }
        
        break;
        case MODE_NET_MASTER:
        //Wait for slaves to be complete: message 'c'
        do {
            rtn = wait_pause_slaves(clock_ctrl_reg, &mode, sockets, &numsockets, ASWAM_STOPPED);
        } while (rtn == 110);   //Abort requested, try again to get message from slave
        //If an extra abort is requested during time slaves are terminating, slaves will ignore it, but UI may look weird.
        
        //Send message 'c' to slaves to say run complete
        buffer[0] = ASWAM_STOPPED;
        for (k = 0; k < numsockets; k++) {
            int sendrtn = send(sockets[k], buffer, 1, MSG_NOSIGNAL);
            if (sendrtn < 1) {
                slave_closed(&mode, sockets, &numsockets, k);
            }
        }
        
        if (numsockets == orig_numsockets && (rtn == 0 || rtn == 110)) {   //No slaves lost and ending normally or aborted
            break;
        }
        case MODE_IND_MASTER:
        printf("Do not disable the master clock until all slaves have been stopped. Press enter to stop the master clock.");
        while (getchar() != '\n');
    }
    
    //Before this point, the master sends pause/resume/abort etc. messages, then listens for 'c' from slaves, so first message received is 'c'
    //After this point, slaves send their status and master responds, so master always hears 'c' first
    
    //Stop clock
    reset_clock(invertTimeBeacon);			//Stops master timing beacon and disables PRUSS
    
    int zeroing_status = 0; //For function return
    
    //Return motors to their original positions?
    if (motors == 0) {
        if (mode == MODE_NET_SLAVE) {
            //Send message to master saying motor tracking has failed = f
            buffer[0] = ASWAM_ZERO_FAIL;
            rtn = send(sockets[0], buffer, 1, MSG_NOSIGNAL);
            if (rtn < 1) {
                fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
                mode = MODE_IND_SLAVE;
                pausefd = STDIN_FILENO;
            }
            //Return and close sockets from calling function
        }
    } else {    //motors > 0
        unsigned char motor_displaced = 0;
        for (motor_id = 0; motor_id < motors; motor_id++) {
            if (motor_pos[motor_id] != 0) {
                motor_displaced = 1;
                break;
            }
        }
        
        if (motor_displaced == 1) {
            printf("Motors are not in their original positions.\n");
        } else {
            printf("Motors are in their original positions.\n");
        }
        
        if (mode == MODE_NET_SLAVE) {
            //Networked slave: send message to master
            if (motor_displaced == 1) {
                buffer[0] = ASWAM_ZERO_DISPLACED;        //d = displaced
            } else {
                buffer[0] = ASWAM_ZERO_DONE;        //z = zeroed
            }
            rtn = send(sockets[0], buffer, 1, MSG_NOSIGNAL);
            if (rtn < 1) {
                fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
                mode = MODE_IND_SLAVE;
                pausefd = STDIN_FILENO;
            }
        } else if (mode == MODE_NET_MASTER) {
            //Networked master: receive status messages from slaves
            //Check whether slaves are displaced
            for (k = 0; k < numsockets; k++) {
                rtn = recv(sockets[k], buffer, 1, 0);
                if (rtn < 1) {
                    motor_displaced = 2;        //Unknown slave dispacement
                    slave_closed(&mode, sockets, &numsockets, k);
                } else {
                    while (buffer[0] != ASWAM_ZERO_FAIL && buffer[0] != ASWAM_ZERO_DISPLACED && buffer[0] != ASWAM_ZERO_DONE) {
                        //Try again for recognised message, warning about unrecognised ones
                        char oldmsg = buffer[0];
                        fprintf(stderr, "WARNING: Unrecognised message received from slave %d: %c. Trying buffer again.\n", k, buffer[0]);
                        rtn = recv(sockets[0], buffer, 1, MSG_DONTWAIT);   //Networked slave has one com socket
                        if (rtn < 1) {
                            fprintf(stderr, "ERROR: Failed to receive recognisable message from slave %d.\n", k);
                            buffer[0] = oldmsg;
                            break;
                        }
                    }
                    
                    if (buffer[0] == ASWAM_ZERO_DISPLACED) {
                        motor_displaced = 1;        //A slave has a known displacement. Don't break for loop as need to check for unknowns.
                    } else if (buffer[0] == ASWAM_ZERO_FAIL) {
                        //buffer[0] = f, meaning tracking failed
                        motor_displaced = 2;
                    } //buffer[0] = z -> motor_displaced remains at 0
                }
            }
            
            if (numsockets < orig_numsockets) {
                motor_displaced = 2;       //Unknown slave displacement
            }
            
            if (motor_displaced == 0) {
                printf("All motors on all BBBs are in their original positions.\n");
            } else if (motor_displaced == 1) {
                printf("The motors on at least one BBB are not in their original positions.\n");
            } else {
                printf("The motor positions on some BBBs are unknown. Check their terminal for status.\n");
            }
        }
        
        unsigned char recal = 'n';   //Recalibration mode: y/n
        float reset_time;           //Time in seconds for motors to return to their starting positions
        
        if (mode == MODE_NET_SLAVE) {      //Networked slave
            rtn = recv(sockets[0], buffer, 1, MSG_NOSIGNAL);
            if (rtn < 1) {
                //Connection to master lost
                fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
                mode = MODE_IND_SLAVE;
                pausefd = STDIN_FILENO;
            } else {
                while (buffer[0] != ASWAM_ZERO_NO && buffer[0] != ASWAM_ZERO_YES) {
                    //Try again for recognised message, warning about unrecognised ones
                    char oldmsg = buffer[0];
                    fprintf(stderr, "WARNING: Unrecognised message received from master: %c. Trying buffer again.\n", buffer[0]);
                    rtn = recv(sockets[0], buffer, 1, MSG_DONTWAIT);   //Networked slave has one com socket
                    if (rtn < 1) {
                        fprintf(stderr, "ERROR: Failed to receive recognisable message from master.\n");
                        buffer[0] = oldmsg;
                        break;
                    }
                }
                
                if (buffer[0] == ASWAM_ZERO_YES) {
                    //Move back to original positions
                    recal = 'y';
                    rtn = recv(sockets[0], buffer, 1, 0);
                    if (rtn < 1) {
                        //Connection to master lost
                        fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
                        mode = MODE_IND_SLAVE;
                        pausefd = STDIN_FILENO;
                    }
                    
                    unsigned char reset_time_strlen = buffer[0];
                    unsigned char bytesrcv = 0;
                    rtn = 1;
                    while (bytesrcv < reset_time_strlen && rtn > 0) {
                        rtn = recv(sockets[0], buffer + bytesrcv, reset_time_strlen - bytesrcv, 0);
                        bytesrcv += rtn;
                    }
                    if (rtn < 1) {
                        //Connection to master lost
                        fprintf(stderr, "Connection closed unexpectedly. Continuing as a non-networked slave.\n");
                        mode = MODE_IND_SLAVE;
                        pausefd = STDIN_FILENO;
                    } else {
                        buffer[reset_time_strlen] = '\0';       //Null terminate string
                        sscanf(buffer, "%f", &reset_time);
                    }
                }
            }
        }
        
        if (motor_displaced > 0 && mode != MODE_NET_SLAVE) {
            do {
                printf("Return motors to original positions? [y/n]: ");
                recal = getchar();
                if (recal != '\n') {
                    while (getchar() != '\n');
                }
            } while (recal != 'y' && recal != 'n');
            
            if (recal == 'y') {
                do {
                    printf("How long to make reset in seconds (>=%.1f)? ", MIN_RESET_TIME);
                    scanf("%f", &reset_time);
                    while (getchar() != '\n');      //Scanf discards whitespace (inc \n) when searching for %f, so only need to do this the last time
                } while (reset_time < MIN_RESET_TIME || reset_time > 9e9);
                
                if (mode == MODE_NET_MASTER) {
                    snprintf(buffer, 256, "aa%.3f", reset_time);      //aa is to fill first two characters to be overwritten shortly
                    unsigned char reset_time_strlen;
                    reset_time_strlen = strlen(buffer) - 2;
                    buffer[0] = ASWAM_ZERO_YES;        //m = move motors
                    buffer[1] = reset_time_strlen;      //Message size
                    
                    for (k = 0; k < numsockets; k++) {
                        //Keep sending number until all is sent
                        unsigned char bytessent = 0;
                        rtn = 1;
                        while (bytessent < (reset_time_strlen + 2) && rtn > 0) {
                            rtn = send(sockets[k], buffer + bytessent, (reset_time_strlen + 2) - bytessent, MSG_NOSIGNAL);
                            bytessent += rtn;
                        }
                        if (rtn < 1) {
                            slave_closed(&mode, sockets, &numsockets, k);
                        }
                    }
                }
            }
        }
        
        if (recal == 'n') {
            if (mode == MODE_NET_MASTER) {
                //Send message to all slaves: l = leave motors
                buffer[0] = ASWAM_ZERO_NO;
                for (k = 0; k < numsockets; k++) {
                    rtn = send(sockets[k], buffer, 1, MSG_NOSIGNAL);
                    if (rtn < 1) {
                        //Connection broken
                        slave_closed(&mode, sockets, &numsockets, k);
                    }
                }
            }
        } else {
            //Start recalibration process
            
            //Check and get maximum number of open files
            rlim_t filelimit = get_filelimit();
            if (filelimit == 0) { zeroing_status = 2; }   //Error message displayed by get_filelimit()
            else {
                double minDisplacement, maxDisplacement, curDisplacement;
                
                //xmlNode is currently NULL
                xmlNode = xmlDocGetRootElement(xmlTree);
                
                //Attributes: set end=reset_time and initialSplit=reset_time, because monotonic interval
                char contString[50];
                snprintf(contString, 50, "%.3f", reset_time);
                xmlSetProp(xmlNode, (const xmlChar*) "end", (const xmlChar*) contString);
                xmlSetProp(xmlNode, (const xmlChar*) "initialSplit", (const xmlChar*) contString);
                //Any reverse attribute will have been removed by XSLT
                
                //Zero displacement guaranteed to be in required range
                minDisplacement = 0;
                maxDisplacement = 0;
                
                xmlNode = xmlFirstElementChild(xmlNode);    //<motor>
                for (motor_id = 0; motor_id < motors; motor_id++) {
                    //Get motor pitch
                    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"travelPerStep");
                    double travelPerStep = atof((const char*)xmlText);
                    xmlFree(xmlText);
                    
                    //Remove all old children
                    xmlNodePtr subNode = xmlNode->children;     //Gets first child element
                    while (subNode) {
                        xmlUnlinkNode(subNode);
                        xmlFreeNode(subNode);
                        subNode = xmlNode->children;
                    }
                    //subNode is now NULL
                    
                    snprintf(contString, 50, "- (t / %.3f) * (%d * %.5f)", reset_time, motor_pos[motor_id], travelPerStep);
                    xmlNewTextChild(xmlNode, NULL, (const xmlChar*) "interval", (const xmlChar*) contString);
                    
                    //Update minimum and maximum displacements
                    curDisplacement = -motor_pos[motor_id] * travelPerStep;
                    //Provide safety margin of one step against rounding errors - probably actually only need 1e-14
                    if (curDisplacement - travelPerStep < minDisplacement) {
                        minDisplacement = curDisplacement - travelPerStep;
                    }
                    if (curDisplacement + travelPerStep > maxDisplacement) {
                        maxDisplacement = curDisplacement + travelPerStep;
                    }
                    
                    xmlNode = xmlNextElementSibling(xmlNode);
                }
                
                //xmlNode = NULL
                xmlNode = xmlDocGetRootElement(xmlTree);
                
                //Set minimum and maximum displacements
                S_PRINTF(contString, 50, "%.16f", minDisplacement)
                xmlSetProp(xmlNode, (const xmlChar*)"minDisplacement", (const xmlChar*)contString);
                S_PRINTF(contString, 50, "%.16f", maxDisplacement)
                xmlSetProp(xmlNode, (const xmlChar*)"maxDisplacement", (const xmlChar*)contString);
            }
            
            //Set up temporary folder
            char tempfoldername[4] = "tmp";
            struct stat folderstat;
            if (stat(tempfoldername, &folderstat) == -1) {
                if (mkdir(tempfoldername, S_IRWXU)) {
                    perror("Failed to create temporary folder");
                    zeroing_status = 2;
                }
            }
            
            //Save XML file
            char zeroingxmlname[16];
            strcpy(zeroingxmlname, tempfoldername);
            strcat(zeroingxmlname, "/zeroing.xml");
            
            char zeroingbinname[15];
            strcpy(zeroingbinname, tempfoldername);
            strcat(zeroingbinname, "/zeroing.in");
            
            if (zeroing_status == 0) {
                rtn = xmlSaveFile(zeroingxmlname, xmlTree);
                if (rtn == -1) {
                    perror("Recalibration XML file save failed");
                    //Exit -> will close any network sockets
                    zeroing_status = 2;
                }
                
                //Open file counters
                int BBBthreads = 1;
                int motorthreads = 0;
                pthread_mutex_t ThreadCounterMutex;
                if (pthread_mutex_init(&ThreadCounterMutex, NULL)) {
                    perror("ERROR: Could not initialise mutex");
                    zeroing_status = 2;
                }
                pthread_cond_t ThreadCounterCondition;
                if (pthread_cond_init(&ThreadCounterCondition, NULL)) {
                    perror("ERROR: Could not initialise mutex condition");
                    zeroing_status = 2;
                }
                
                if (zeroing_status == 0) {
                    //Free xmlTree before calling make_binary to avoid memory leak
                    if (xmlTree) {
                        xmlFreeDoc(xmlTree);    //NB. Does not set pointer to NULL
                        xmlTree = NULL;
                    }
                    
                    //Compile XML to binary file
                    int stopSig = 0;
                    zeroing_status = make_binary(NULL, NULL, zeroingxmlname, zeroingbinname, 0, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
                    
                    //Cleanup pthreads
                    pthread_mutex_destroy(&ThreadCounterMutex);
                    pthread_cond_destroy(&ThreadCounterCondition);
                }
            }
            
            //Delay execution if network master
            if (mode == MODE_NET_MASTER) {
                if (zeroing_status) {
                    printf("Zeroing failed on master BBB. Motors will not move during zeroing operation.\n");
                }
                
                //Get compile response from slave
                for (k = 0; k < numsockets; k++) {
                    rtn = recv(sockets[k], buffer, 1, 0);
                    if (buffer[0] == COMPILE_FAIL) {
                        printf("Zeroing failed on slave %d. Motors will not move during zeroing operation.\n", k);
                    } else if (buffer[0] != COMPILE_SUCCESS) {
                        fprintf(stderr, "ERROR: Unrecognised response from slave %d: %c.\n", k, buffer[0]);
                    }
                }
                
                //Wait for slaves to be running: message 's'
                rtn = wait_pause_slaves(NULL, &mode, sockets, &numsockets, ASWAM_STARTED);
                //Manual master clock start will be requested in recursive run_pruss if some slaves have been lost: rtn = 1 or -1
                if (rtn == 2)
                {
                    //Unrecognised response
                    printf("Do not start the master clock until all the slave clocks are running. Press enter to start the master clock.");
                    while (getchar() != '\n');
                }
                
                if (zeroing_status) {
                    run_pruss(mode, "", "", sockets, numsockets, orig_numsockets, invertTimeBeacon);
                } else {
                    //Execute binary file, but with no pins file to suppress option of undoing recalibration
                    run_pruss(mode, zeroingbinname, "", sockets, numsockets, orig_numsockets, invertTimeBeacon);
                    //If it fails, function returns -> works
                }
            } else if (mode == MODE_NET_SLAVE) {
                if (zeroing_status) {
                    //Zeroing failed - send message to master
                    buffer[0] = COMPILE_FAIL;
                } else {
                    buffer[0] = COMPILE_SUCCESS;
                }
                rtn = send(sockets[0], buffer, 1, MSG_NOSIGNAL);
                if (rtn < 1) {
                    fprintf(stderr, "Connection to master closed unexpectedly. Continuing as non-networked slave.\n");
                    mode = MODE_IND_SLAVE;
                }
                
                if (zeroing_status) {
                    run_pruss(mode, "", "", sockets, numsockets, orig_numsockets, invertTimeBeacon);
                } else {
                    //Execute binary file, but with no pins file to suppress option of undoing recalibration
                    run_pruss(mode, zeroingbinname, "", sockets, numsockets, orig_numsockets, invertTimeBeacon);
                    //If it fails, function returns -> works
                }
            } else {
                
                if (!zeroing_status) {
                    //Execute binary file, but with no pins file to suppress option of undoing recalibration
                    run_pruss(mode, zeroingbinname, "", sockets, numsockets, orig_numsockets, invertTimeBeacon);
                    //If it fails, function returns -> works
                }
            }
        }
    }
    
    //Cleanup XML
    if (xmlTree) xmlFreeDoc(xmlTree);
    
    return zeroing_status;
}
