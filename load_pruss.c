//Initializes the PRUSS, runs clock and gpio_driver for BBB.

/*Version control
v0.01 TD 18/01/15 Complete
v0.02 TD 30/01/15 Moved turning off of timing beacon to ctrl_pruss
v0.03 TD 19/02/15 Input file moved to a program argument, rather than question
v0.04 TD 09/04/15 Mode selection moved to program argument
v0.05 TD 13/04/15 Connected to work with slave.c networking
v0.06 TD 15/04/15 Updated to v0.05 of ctrl_pruss
v0.07 TD 23/04/15 Updated to v0.09 of ctrl_pruss: pins file
v0.08 TD 25/04/15 Pins file optional
v0.09 TD 28/04/15 Extra argument in run_pruss
v0.10 TD 29/04/15 Send empty string if pinsfilename is not specified
v0.11 TD 20/05/15 Added invertTimeBeacon
 v0.12 TD 09/02/15 Added macros for modes
 v0.13 TD 02/02/17 XML parser initiation and cleanup thread safe
 v0.14 TD 08/04/17 Each interval created in own thread
 v0.15 TD 20/04/17 Initialise XPath
 v0.16 TD 22/01/20 Move invertTimeBeacon parameter to pins file
*/

#include "ctrl_pruss.h"

int main (int argc, char *argv[])
/*argv[2]=binary input file*/
{
	//Program information
	if (argc < 3 || argc > 4 || !strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
	{
		printf("Please provide two or three arguments:\n-m/i/s for master/master with hardware-inverted timing beacon/slave,\nbinary input file, pins XML file (required only for automatic zeroing)\n");
		return(EXIT_FAILURE);
	}
	
    char * binaryfilename = argv[2];
    
    char pinsfilename[256] = "";
    if (argc >= 4) {
        strcpy(pinsfilename, argv[3]);
    }

	char mode = 0;
	unsigned char invertTimeBeacon = 0;
    if (!strcmp(argv[1], "-m"))
    {
        mode = MODE_IND_MASTER;
    }
    else if (!strcmp(argv[1], "-s"))
    {
        mode = MODE_IND_SLAVE;
    }
	else if (!strcmp(argv[1], "-i")) {
        mode = MODE_IND_MASTER;
        invertTimeBeacon = 1;
    }
    else
    {
        fprintf(stderr, "Unrecognised mode paramter\n");
        return(EXIT_FAILURE);
    }
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();
    
    int rtn = run_pruss(mode, binaryfilename, pinsfilename, NULL, 0, 0, invertTimeBeacon);
    
    //Cleanup XML & XSLT once all threads are done with it
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    return rtn;
}
