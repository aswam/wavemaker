//Reads in displacements of motors and outputs binary instruction file for ASWaM PRU.

/* Version control
v0.01 24/01/15 TD Untested version complete
v0.02 26/01/15 TD Compiles and GPIO pins populated - still untested
v0.03 27/01/15 TD Mid-debugging - prior to changing variables in ordering process from ULL to LL
v0.04 27/01/15 TD Bugs fixed with LL. Before tidying and reducing code.
v0.05 27/01/15 TD Tested and tidied
v0.06 02/02/15 TD Added line break at end of moving too fast warning
v0.07 18/02/15 TD Changed default to steps on pulse rising edge, with option for falling in XML input.
v0.08 19/02/15 TD Adding clearing of all pins at end of wave, moved expansion_to_gpio to ctrl_pruss, moved file name inputs to program arguments
v0.09 26/02/15 TD Reinstate deletion of temporary files
v0.10 26/02/15 TD Memory management fixes
v0.11 TD 15/04/15 Program information to show when argc!=2, schema file changed to ASWaMWaveSchema.xsd
v0.12 TD 17/04/15 Bug fix: argc!=3, checking for blank node
v0.13 TD 21/04/15 Fix: constant interval updates absolute time, roundll takes negative halfway towards zero
v0.14 TD 22/04/15 Changed exit() to return including cleanup
v0.15 TD 23/04/15 Moved MAXMOTORS macro to ctrl_pruss.h
v0.16 TD 25/04/15 Moved some header files to ctrl_pruss.h
v0.17 TD 27/04/15 Moved main content to make_binary()
v0.18 TD 28/04/15 Moved to ctrl_pruss.c
 v0.19 TD 19/01/16 Updated to v0.01 of compiler.c
 v0.20 TD 07/02/17 XML parser made thread safe
 v0.21 TD 05/04/17 Display compile time & prevent multiple instances of program
 v0.23 TD 08/04/17 Each interval created in own thread
 v0.24 TD 20/04/17 Initialise XPath
 v0.25 TD 23/05/17 Number of instructions in performance stats
 v0.26 TD 18/06/17 Ported to MSVS2012
 v0.27 TD 21/11/17 Compiler stops on error and returns some error codes
*/

#include "compiler.h"

int main(int argc, char *argv[])
{
    char tempfname[14];
    struct stat folderstat;
	FILE *lockfd;
    PID_TYPE readpid;
	FLIM_TYPE filelimit;
	TIME_TYPE start_t, end_t;
    double global_wave_end = 0;
	double total_t;
    long total_wave_instructions;
	int BBBthreads, motorthreads, rtn;
    pthread_mutex_t ThreadCounterMutex;
    pthread_cond_t ThreadCounterCondition;
    int stopSig = 0;

	//Program information
    if (argc != 3 || !strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
    {
        printf("Please provide two arguments: XML input file, binary output file\n");
        return(EXIT_FAILURE);
    }
    
    //Prevent multiple instances running in same folder - other programs ok as behind dtach
    //Set up temporary folder
    STR_CPY(tempfname, "tmp", 14)
    if (stat(tempfname, &folderstat) == -1) {
        if (MKDIR(tempfname)) {
            perror("Failed to create temporary folder");
            return 2;
        }
    }
    
    STR_CAT(tempfname, "/pid.txt", 14)
    //Check if file already exists
    F_OPEN(lockfd, tempfname, "r");
    if (lockfd) {
        fclose(lockfd);
        fprintf(stderr, "Cannot have two open instances of compiler in same folder. This may have occurred because a previous instance was terminated early. Check %s for process id. Delete file if it has finished.\n", tempfname);
        return 3;
    }
    
    F_OPEN(lockfd, tempfname, "w+");
    if (!lockfd) {
        fprintf(stderr, "Cannot have two open instances of compiler in same folder. This may have occurred because a previous instance was terminated early. Check %s for process id. Delete file if it has finished.\n", tempfname);
        return 3;
    }
    //Print PID to file
    fprintf(lockfd, PID_STRING_SPEC, GET_PID);
    //Wait to see if another process has also written to file
    SLEEP_MS(10);
    //Check PID
    fseek(lockfd, 0, SEEK_SET); //Move file pointer to start of file
    F_SCANF(lockfd, PID_STRING_SPEC, &readpid);
    if (readpid != GET_PID) {
        fclose(lockfd);
        fprintf(stderr, "Cannot have two open instances of compiler in same folder. This may have occurred because a previous instance was terminated early. Check %s for process id. Delete file if it has finished.\n", tempfname);
        return 3;
    }
    
    //Check and get maximum number of open files
    filelimit = GET_FILELIMIT;
    if (filelimit == 0) {
        //Unlock, i.e. delete lock file
        fclose(lockfd);
        remove(tempfname);
        return EXIT_FAILURE;
    }   //Error message displayed by get_filelimit()
    
    //Start timer - for performance benchmarking
	GET_TIME(start_t);
    
    //Open file counters
    BBBthreads = 1;
	motorthreads = 0;
    if (pthread_mutex_init(&ThreadCounterMutex, NULL)) {
        perror("ERROR: Could not initialise mutex");
        //Unlock, i.e. delete lock file
        fclose(lockfd);
        remove(tempfname);
        return EXIT_FAILURE;
    }
    if (pthread_cond_init(&ThreadCounterCondition, NULL)) {
        perror("ERROR: Could not initialise mutex condition");
        pthread_mutex_destroy(&ThreadCounterMutex);
        //Unlock, i.e. delete lock file
        fclose(lockfd);
        remove(tempfname);
        return EXIT_FAILURE;
    }
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();
    
    rtn = make_binary(&global_wave_end, &total_wave_instructions, argv[1], argv[2], 0, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
    
    //Cleanup pthreads
    pthread_mutex_destroy(&ThreadCounterMutex);
    pthread_cond_destroy(&ThreadCounterCondition);
    
    //Cleanup XML & XSLT once all threads are done with it
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    switch (rtn) {
        case 0:
            //Display compile time
            GET_TIME(end_t);
            TIME_DIF(total_t, start_t, end_t);
            printf("Total time to compile = %g s.\n", total_t);
            
            if (global_wave_end > 0) {
                //Don't want to divide by zero
                double perf_ratio = global_wave_end / total_t;
                double perf_score = (double)total_wave_instructions / total_t;
                printf("Waveform length = %g s, ratio = %g.\nNumber of instructions = %ld, score = %g.\n", global_wave_end, perf_ratio, total_wave_instructions, perf_score);
            }
            break;
        case ERR_DRIVE:
            fprintf(stderr, "ERROR: Could not write output file. The hard drive might be full.\n");
            break;
        case ERR_DISCONTINUOUS:
            fprintf(stderr, "ERROR: Discontinuity in input function. Scroll up to find error message.\n");
            break;
        case ERR_PERIOD:
            fprintf(stderr, "ERROR: Period is set incorrectly. Scroll up to find error message.\n");
            break;
        default:
            fprintf(stderr, "ERROR: Input failed to compile.\n");
    }
    
    //Unlock, i.e. delete lock file
    fclose(lockfd);
    remove(tempfname);

    return rtn;
}
