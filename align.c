//Program to manually move motors, much faster than making binary file and using gpio_driver

/*Version control
v0.01 08/12/14 TD 5 Motors, single input
v0.02 19/02/15 TD Rewritten for 25 motors, XML to give motor locations, can run program multiple times, setup removed
v0.03 26/02/15 TD Removed printf for debugging
v0.04 TD 22/04/15 Changed exit() to return, fixed memory leaks, added support for pins schema
v0.05 TD 23/04/15 Moved libxml header files to ctrl_pruss.h
v0.06 TD 18/05/15 Stylesheet name changed to ASWaMWaveStylesheet.xsl
v0.07 TD 08/09/15 Input error checking
 v0.08 TD 19/01/16 Variable maximum number of motors, removed redundant header files
 v0.09 TD 20/04/17 Initialise LibXML
 v0.10 TD 22/05/17 XML names changed to camel case
 v0.11 TD 30/06/17 Updated to v0.24 of compiler.c
*/

/* Driver header file */
#include <stdint.h>
#include <math.h>
#include "ctrl_pruss.h"

#define OP_TIME	5		//Number of nanoseconds per line of assembly code
#define OPS_PER_LOOP 102		//Number of operations per loop
#define TIME_PER_LOOP (OP_TIME * OPS_PER_LOOP)		//Nanoseconds

#define TIME_PER_DELAY_CYCLE 10		//Nanoseconds

#define GPIO_BITS 32
#define GPIO0 0x44e07000
#define GPIO1 0x4804c000
#define GPIO2 0x481ac000
#define GPIO3 0x481ae000
#define GPIO_CLEARDATAOUT 0x190
#define GPIO_SETDATAOUT 0x194

int main (int argc, char *argv[])
/*argv[0]=program name, argv[1]=XML input file (for pin connections)*/
{
	//Program information
	if (argc == 1 || !strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
	{
		printf("Please provide one argument: XML input file containing pin connections\n");
		return(EXIT_FAILURE);
	}
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();

	uint32_t pwms[2048];
	unsigned int motor, direction;
	int k, rtn, steps;
	float frequency;

	xmlDocPtr xmlRawDoc = xmlParseFile(argv[1]);
	if (xmlRawDoc == NULL)
	{
		perror("XML input file parse unsuccessful");
        xmlCleanupParser();
		return(EXIT_FAILURE);
	}
	xmlChar* xmlText;

	//Validate XML doc
    char* schema_path;
    //Use ASWaMPinsSchema.xsd if end attribute is missing from <wave>, otherwise ASWaMMasterSchema.xsd
    xmlNodePtr xmlNode = xmlDocGetRootElement(xmlRawDoc);  //<master>
    if ((xmlText = xmlGetProp(xmlNode, (const xmlChar*)"end"))) {
        //Use ASWaMWaveSchema
        schema_path = "ASWaMWaveSchema.xsd";
    }
    else {
        //Use ASWaMPinsSchema
        schema_path = "ASWaMPinsSchema.xsd";
    }
    xmlFree(xmlText);

    xmlDocPtr schema_doc = NULL;
	xmlSchemaParserCtxtPtr parser_ctxt = NULL;
	xmlSchemaPtr schema = NULL;
	xmlSchemaValidCtxtPtr valid_ctxt = NULL;
	schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
	parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
	schema = xmlSchemaParse(parser_ctxt);
	valid_ctxt = xmlSchemaNewValidCtxt(schema);
	int validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
	if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
	if (schema) xmlSchemaFree(schema);
	if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
	if (schema_doc) xmlFreeDoc(schema_doc);
	if (validationResult != 0)
	{
		perror("Incorrect syntax in XML input file");
        xmlFreeDoc(xmlRawDoc);
        xmlCleanupParser();
		return(EXIT_FAILURE);
	}

	//Transform XML using XSLT
	xsltStylesheetPtr stylesheet_doc = xsltParseStylesheetFile((const xmlChar*) "ASWaMWaveStylesheet.xsl");
	xmlDocPtr xmlTree = xsltApplyStylesheet(stylesheet_doc, xmlRawDoc, NULL);
    
	//XSLT Cleanup
	xsltFreeStylesheet(stylesheet_doc);
	xmlFreeDoc(xmlRawDoc);
	xsltCleanupGlobals();
    
    if (!xmlTree)
	{
		perror("Error in applying XSLT stylesheet");
        xmlCleanupParser();
		return(EXIT_FAILURE);
	}

	xmlNode = xmlDocGetRootElement(xmlTree);
	xmlText = xmlGetProp(xmlNode, (const xmlChar*)"fallingEdge");
	int fallingEdge = atoi((const char*)xmlText);
    xmlFree(xmlText);
    
    //Open GPIO map file
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"gpioMapFile");
    FILE *gpiomap;
    F_OPEN(gpiomap, (const char*)xmlText, "r");
    xmlFree(xmlText);
    if (!gpiomap) {
        perror("ERROR: Could not open GPIO map file");
        xmlFreeDoc(xmlTree);
        xmlCleanupParser();
        return EXIT_FAILURE;
    }

	printf("Step frequency (Hz): ");
	scanf("%f", &frequency);
    while (getchar() != '\n');
    
    //Check for invalid values
    if (!frequency) {
        fprintf(stderr, "Frequency must be greater than zero\n");
        xmlFreeDoc(xmlTree);
        xmlCleanupParser();
        return EXIT_FAILURE;
    }

	printf("Which motor? [1-%d, 0 for exit:] ", MAX_MOTORS);
	scanf("%d", &motor);
    while (getchar() != '\n');

	int expansion_header, expansion_pin;
	unsigned char gpio_pulse, gpio_direction;

	while (motor)
	{
		printf("Direction? [0/1:] ");
		scanf("%d", &direction);
        while (getchar() != '\n');
        //Check
        if (direction != 1) { direction = 0; }
        
		printf("Number of steps: ");
		scanf("%d", &steps);
        while (getchar() != '\n');
        //Proceed only if steps > 0
        if (steps > 0) {
            //Find GPIO pins
            xmlNode = xmlDocGetRootElement(xmlTree);		//<wave>
            xmlNode = xmlFirstElementChild(xmlNode);		//<motor>
            for (k = 2; k <= motor; k++)
            {
                xmlNode = xmlNextElementSibling(xmlNode);
                if (!xmlNode)
                {
                    fprintf(stderr, "Motor %d does not exist in XML file", motor);
                    xmlFreeDoc(xmlTree);
                    xmlCleanupParser();
                    return(EXIT_FAILURE);
                }
            }

            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"pulseHeader");
            expansion_header = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"pulsePin");
            expansion_pin = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            gpio_pulse = expansion_to_gpio(expansion_header, expansion_pin, gpiomap);
            if (gpio_pulse == 0) {
                fprintf(stderr, "Unrecognised pin P%d_%d\n", expansion_header, expansion_pin);
                break;
            }

            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"directionHeader");
            expansion_header = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"directionPin");
            expansion_pin = atoi((const char*)xmlText);
            xmlFree(xmlText);
            
            gpio_direction = expansion_to_gpio(expansion_header, expansion_pin, gpiomap);
            if (gpio_direction == 0) {
                fprintf(stderr, "Unrecognised pin P%d_%d\n", expansion_header, expansion_pin);
                break;
            }

            pwms[0] = steps;
            pwms[1] = (unsigned int)ceil((0.5 / fabs(frequency)) * (1e9 / TIME_PER_DELAY_CYCLE));

            //Direction
            pwms[2] = 1 << (gpio_direction % GPIO_BITS);
            switch (gpio_direction / GPIO_BITS)
            {
            case 0:
                pwms[3] = GPIO0;
                break;
            case 1:
                pwms[3] = GPIO1;
                break;
            case 2:
                pwms[3] = GPIO2;
                break;
            default:
                pwms[3] = GPIO3;
            }

            if (direction)
            {
                pwms[3] |= 0x194;	//Set GPIOx
            }
            else
            {
                pwms[3] |= 0x190;	//Clear GPIOx
            }

            //Pulse
            pwms[4] = 1 << (gpio_pulse % GPIO_BITS);
            switch (gpio_pulse / GPIO_BITS)
            {
            case 0:
                pwms[5] = GPIO0;
                break;
            case 1:
                pwms[5] = GPIO1;
                break;
            case 2:
                pwms[5] = GPIO2;
                break;
            default:
                pwms[5] = GPIO3;
            }

            if (fallingEdge)
            {
                pwms[6] = pwms[5] | GPIO_CLEARDATAOUT;	//Step motor
                pwms[5] |= GPIO_SETDATAOUT;				//Prepare pulse
            }
            else
            {
                pwms[6] = pwms[5] | GPIO_SETDATAOUT;	//Step motor
                pwms[5] |= GPIO_CLEARDATAOUT;				//Prepare pulse
            }

            /* initialize the library, PRU and interrupt; launch our PRU program */
            if (pru_setup()) {
                pru_cleanup();
                return -1;
            }

            /* load array on PRU */
            prussdrv_pru_write_memory(PRUSS0_PRU1_DATARAM, 0, pwms, sizeof(unsigned int)*(2048));

            /* Load and execute binary on PRU */
            if ((rtn = prussdrv_exec_program(PRU_GPIO, "./align.bin")) < 0) {
                fprintf(stderr, "prussdrv_exec_program() failed\n");
                pru_cleanup();
                break;
            }

            /* The prussdrv_pru_wait_event() function returns the number of times
            the event has taken place, as an unsigned int. There is no out-of-
            band value to indicate error (and it can wrap around to 0 if you
            run the program just a whole lot of times). */
            rtn = prussdrv_pru_wait_event(PRU_EVTOUT_1);
            /* clear the event, disable the PRU and let the library clean up */
            pru_cleanup();
            printf("Movement complete.\n");
        } else { printf("Number of steps must be greater than zero.\n"); }

		printf("Which motor? [1-%d, 0 for exit:] ", MAX_MOTORS);
		scanf("%d", &motor);
        while (getchar() != '\n');
	}
    
    fclose(gpiomap);
    xmlFreeDoc(xmlTree);
    xmlCleanupParser();

	return EXIT_SUCCESS;
}
