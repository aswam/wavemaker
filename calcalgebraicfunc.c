/*Reads XML tree, taking children from a given node, having applied FuncStringToTree.xsl, calculates output values[] based on tree. Note non-standard definition 0/0 = 0^x = 0.
 
 Versions:
 v0.01 27/11/14 TD Written for v0.03 of fill.cpp
 v0.02 20/01/15 TD Any single character usable as independent variable, changed from z. Removed print_error support, changed to return boolean.
 v0.03 26/01/15 TD Removed obsolete header ncurses.h
 v0.04 28/02/15 TD Libxml pointers > pointer pointers & added xmlFree(xmlText) to avoid memory leak
 v0.05 TD 05/05/15 Added Heaviside step function
 v0.06 TD 12/05/15 Bug fix: variable names in heaviside()
 v0.07 TD 13/05/15 An arbitrary number of string variable names can be used
 v0.08 TD 14/05/15 Check for division by zero, Tidy error messages
 v0.09 TD 14/05/15 Passing 2D array inlist modified for C++ compatibility
 v0.10 TD 19/01/16 fprintf format correction
 v0.11 TD 27/01/16 Use heap for left and right arrays
 v0.12 TD 28/01/16 Malloc cast to type (bug fix)
 v0.13 TD 05/02/16 Added inverse hyperbolic functions
 v0.14 TD 07/04/17 xmlTree made constant single pointer
 v0.15 TD 18/05/17 Do not calculate RHS of times, divide or raise to power when LHS = 0 for end of array
 v0.16 TD 18/06/17 Ported for MSVS2012
 v0.17 TD 28/07/17 Defined 0/0 = 0^x = 0 for consistency with optimisation - does not throw error
 v0.18 TD 04/01/18 Corrected IS_FINITE macro
 v0.19 TD 14/02/19 Added missing brackets around negation of IS_FINITE macro
 */

#include "calcalgebraicfunc.h"

void heaviside(double output[], const double input[], const int points)
//Heaviside step function
{
    int k;
    for (k = 0; k < points; k++) {
        if (input[k] < 0) {
            output[k] = 0;
        } else if (input[k] > 0) {
            output[k] = 1;
        } else {
            output[k] = 0.5;
        }
    }
    
}

int findlastnonzero(double values[], const int length)
//Returns the position of the nonzero number nearest the end of the array
//Returns -1 if all zeroes
{
    int k;
    for (k = length - 1; k >= 0; k--) {
        if (values[k] != 0.0) {
            return k;
        }
    }
    return -1;
}

int calcalgfunc(double values[], const int points, const double* inlist[], const char* invar[], const int variables, const xmlDocPtr xmlTree, const xmlNodePtr *parentnode)
//values is the output of size points
//inlist is an array of pointers to arrays of doubles: each array corresponds to an independent variable and has size points
//Must reach end of function if no errors occur to check for numerical overflow
//Includes some C99 functions - check for INFINITY macro, as this was introduced in C99
{
    int rtn, k, z;
    xmlChar* xmlText;
    xmlNodePtr childnode = xmlFirstElementChild(*parentnode);
    if (childnode)
    {
        char varname[10];
        char* nodename = (char*)childnode->name;
        xmlText = xmlGetProp(childnode, (const xmlChar*) "name");
        if (xmlStrlen(xmlText) > 9) {
            fprintf(stderr, "ERROR: Illegal variable name %s - longer than 9 characters.\n", (const char*) xmlText);
            xmlFree(xmlText);
            return 1;
        }
        STR_CPY(varname, (const char*)xmlText, 10);
        xmlFree(xmlText);
        
        if (!strcmp(nodename, "var"))	//strcmp returns 0 if the strings are equal
        {
            for (k = 0; k < variables; k++) {
                if (!strcmp(varname, invar[k]))
                {
                    //C reads first index as primary index
                    memcpy(values, inlist[k], points * sizeof(double));
                    break;
                }
            }
            
            if (k == variables) {
                //k < variables only if break out of for loop, i.e. variable name found
                if (!strcmp(varname, "pi"))
                {
                    for (z = 0; z < points; z++)
                    {
                        values[z] = 3.14159265358979;	//pi correct to 15sf
                    }
                }
                else
                {
                    char errormessage[200];
                    STR_CPY(errormessage, "Unrecognised variable name ", 200)
                    STR_CAT(errormessage, varname, 200)
                    STR_CAT(errormessage, ".\n", 200)
                    fprintf(stderr, "%s", errormessage);
                    return 1;
                }
            }
        }
        else if (!strcmp(nodename, "unifunc"))
        {
            //Supports real functions of one argument before C99, but can be easily extended
            rtn = calcalgfunc(values, points, inlist, invar, variables, xmlTree, &childnode);
            if (rtn) { return 1; }
            if (!strcmp(varname, "minus")) {
                for (z = 0; z < points; z++) {
                    values[z] = -values[z];
                }
            }
            else if (!strcmp(varname, "acos"))
            {
                for (z = 0; z < points; z++)
                {
                    if (values[z] < -1 || values[z] > 1) { fprintf(stderr, "Argument out of range in acos.\n"); return 1; }
                    values[z] = acos(values[z]);
                }
            }
            else if (!strcmp(varname, "acosh"))
            {
                for (z = 0; z < points; z++)
                {
                    if (values[z] < 1) { fprintf(stderr, "Argument out of range in acosh.\n"); return 1; }
#ifdef INFINITY
                    values[z] = acosh(values[z]);
#else
                    values[z] = log(values[z] + sqrt(values[z] * values[z] - 1));
#endif
                }
            }
            else if (!strcmp(varname, "asin"))
            {
                for (z = 0; z < points; z++)
                {
                    if (values[z] < -1 || values[z] > 1) { fprintf(stderr, "Argument out of range in asin.\n"); return 1; }
                    values[z] = asin(values[z]);
                }
            }
            else if (!strcmp(varname, "asinh"))
            {
                for (z = 0; z < points; z++) {
#ifdef INFINITY
                    values[z] = asinh(values[z]);
#else
                    values[z] = log(values[z] + sqrt(values[z] * values[z] + 1));
#endif
                }
            }
            else if (!strcmp(varname, "atan"))
            {
                for (z = 0; z < points; z++) { values[z] = atan(values[z]); }
            }
            else if (!strcmp(varname, "atanh"))
            {
                for (z = 0; z < points; z++) {
#ifdef INFINITY
                    values[z] = atanh(values[z]);
#else
                    values[z] = 0.5 * log((1 + values[z]) / (1 - values[z]));
#endif
                }
            }
            else if (!strcmp(varname, "ceil"))
            {
                for (z = 0; z < points; z++) { values[z] = ceil(values[z]); }
            }
            else if (!strcmp(varname, "cos"))
            {
                for (z = 0; z < points; z++) { values[z] = cos(values[z]); }
            }
            else if (!strcmp(varname, "cosh"))
            {
                for (z = 0; z < points; z++) { values[z] = cosh(values[z]); }
            }
            else if (!strcmp(varname, "exp"))
            {
                for (z = 0; z < points; z++) { values[z] = exp(values[z]); }
            }
            else if (!(strcmp(varname, "fabs") && strcmp(varname, "abs")))
            {
                for (z = 0; z < points; z++) { values[z] = fabs(values[z]); }
            }
            else if (!strcmp(varname, "floor"))
            {
                for (z = 0; z < points; z++) { values[z] = floor(values[z]); }
            }
            else if (!(strcmp(varname, "log") && strcmp(varname, "ln")))
            {
                for (z = 0; z < points; z++)
                {
                    if (values[z] <= 0) { fprintf(stderr, "Non-strictly positive argument in log.\n"); return 1; }
                    values[z] = log(values[z]);
                }
            }
            else if (!strcmp(varname, "log10"))
            {
                for (z = 0; z < points; z++)
                {
                    if (values[z] <= 0) { fprintf(stderr, "Non-strictly positive argument in log10.\n"); return 1; }
                    values[z] = log10(values[z]);
                }
            }
            else if (!strcmp(varname, "sin"))
            {
                for (z = 0; z < points; z++) { values[z] = sin(values[z]); }
            }
            else if (!strcmp(varname, "sinh"))
            {
                for (z = 0; z < points; z++) { values[z] = sinh(values[z]); }
            }
            else if (!strcmp(varname, "sqrt"))
            {
                for (z = 0; z < points; z++)
                {
                    if (values[z] < 0) { fprintf(stderr, "Negative argument in sqrt.\n"); return 1; }
                    values[z] = sqrt(values[z]);
                }
            }
            else if (!strcmp(varname, "tan"))
            {
                for (z = 0; z < points; z++) { values[z] = tan(values[z]); }
            }
            else if (!strcmp(varname, "tanh"))
            {
                for (z = 0; z < points; z++) { values[z] = tanh(values[z]); }
            }
            else if (!strcmp(varname, "H")) {
                //Heaviside step function
                heaviside(values, values, points);
            }
            else
            {
                char errormessage[200];
                STR_CPY(errormessage, "Unrecognised function name ", 200)
                STR_CAT(errormessage, varname, 200)
                STR_CAT(errormessage, ".\n", 200)
                fprintf(stderr, "%s", errormessage);
                return 1;
            }
        }
        else //<binfunc>
        {
            //TODO: Add min and max binary functions
            
            xmlNodePtr ccnode;
            
            //Safer to use malloc - avoid stack overflow, but don't forget to free!
            double* right;  //Right amount of memory is allocated later
            double* left = (double*) malloc(sizeof(double) * points);
            if (!left) {
                perror("Out of memory");
                return 2;
            }
            
            ccnode = xmlFirstElementChild(childnode);
            rtn = calcalgfunc(left, points, inlist, invar, variables, xmlTree, &ccnode);
            if (rtn) {
                free(left);
                return 1;
            }
            ccnode = xmlLastElementChild(childnode);
            
            if (!strcmp(varname, "*") || !strcmp(varname, "/") || !strcmp(varname, "^")) {
                //Do not evaluate RHS if LHS == 0 - most likely from heaviside step function
                //Can inexpensively truncated arrays so look for last nonzero element
                //Looking for all zeroes, even if not contiguous, requires extra mallocs and slows code down by 20%
                
                int nonzeroes = findlastnonzero(left, points) + 1;
                
                if (nonzeroes > 0) {
                    //malloc(0) gets messy and may be set to null, so avoid
                    right = (double*) malloc(sizeof(double) * nonzeroes);
                    if (!right) {
                        perror("Out of memory");
                        free(left);
                        return 2;
                    }
                    
                    rtn = calcalgfunc(right, nonzeroes, inlist, invar, variables, xmlTree, &ccnode);
                    if (rtn) {
                        free(left);
                        free(right);
                        return 1;
                    }
                    
                    if (!strcmp(varname, "*")) {
                        //Multiply
                        for (z = 0; z < nonzeroes; z++) { values[z] = left[z] * right[z]; }
                    } else if (!strcmp(varname, "/")) {
                        //Divide
                        for (z = 0; z < nonzeroes; z++) {
                            if (right[z] == 0.0) {
                                if (left[z] == 0.0) {
                                    //Define 0/0 = 0 for consistency with optimisation - do not throw error
                                    values[z] = 0.0;
                                } else {
                                    fprintf(stderr, "Dividing by zero.\n");
                                    free(left);
                                    free(right);
                                    return 1;
                                }
                            } else {
                                values[z] = left[z] / right[z];
                            }
                        }
                    } else {
                        //Raise to the power
                        for (z = 0; z < nonzeroes; z++)
                        {
                            if (left[z] < 0 && right[z] != floor(right[z])) {
                                fprintf(stderr, "Cannot raise negative number to a fractional power.\n");
                                free(left);
                                free(right);
                                return 1;
                            } else if (left[z] == 0.0) {
                                //Define 0^0 = 0 for consistency with optimisation - do not throw error
                                values[z] = 0.0;
                            } else {
                                values[z] = pow(left[z], right[z]);
                            }
                        }
                    }
                    
                    //Remaining points must be zero
                    //Not checking for 0^ non-positive power
                    for (z = nonzeroes; z < points; z++) {
                        values[z] = 0.0;
                    }
                } else {
                    free(left);
                    for (z = 0; z < points; z++) { values[z] = 0.0; }
                    return 0;   //No need to check for overflow
                }
            } else {
                right = (double*) malloc(sizeof(double) * points);
                if (!right) {
                    perror("Out of memory");
                    free(left);
                    return 2;
                }
                
                rtn = calcalgfunc(right, points, inlist, invar, variables, xmlTree, &ccnode);
                if (rtn) {
                    free(left);
                    free(right);
                    return 1;
                }
                
                if (!strcmp(varname, "+"))
                {
                    for (z = 0; z < points; z++) { values[z] = left[z] + right[z]; }
                }
                else if (!strcmp(varname, "-"))
                {
                    for (z = 0; z < points; z++) { values[z] = left[z] - right[z]; }
                }
                else
                {
                    char errormessage[200];
                    STR_CPY(errormessage, "Unrecognised binary function name ", 200)
                    STR_CAT(errormessage, varname, 200)
                    STR_CAT(errormessage, ".\n", 200)
                    fprintf(stderr, "%s", errormessage);
                    
                    free(left);
                    free(right);
                    return 1;
                }
            }
            
            //Free memory
            free(left);
            free(right);
        }
    }
    else
    {
        xmlChar* xmlText = xmlNodeListGetString(xmlTree, (*parentnode)->xmlChildrenNode, 1);
        double valueread = strtod((const char*)xmlText, NULL);
        xmlFree(xmlText);
        for (z = 0; z < points; z++)
        {
            values[z] = valueread;
        }
    }
    
    //Check for Nan
    for (z = 0; z < points; z++) {
        if (!(IS_FINITE(values[z]))) {
            fprintf(stderr, "Numerical overflow occurred.\n");
            return 1;
        }
    }
    return 0;
}
