//Stops PRUSS.

/*Version control
v0.01 TD 18/01/15
v0.02 TD 29/01/15 Additionally switches off master clock beacon
v0.03 TD 30/01/15 Moved all code to reset_clock in ctrl_pruss
v0.04 TD 20/05/15 Added invertTimeBeacon
*/

/* Driver header file */
#include "ctrl_pruss.h"

int main (int argc, char *argv[])
{
    unsigned char displaymessage = 0;
    unsigned char invertTimeBeacon = 0;

    if (argc > 2) {
        displaymessage = 1;
    } else if (argc == 2) {
        if (!strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
            displaymessage = 1;
        } else if (strcmp(argv[1], "-i")) {
            displaymessage = 1;
        } else {
            invertTimeBeacon = 1;
        }
    }
    //Program information
    if (displaymessage == 1) {
        printf("Please provide at most one argument: -i for an inverted timing beacon, blank otherwise\n");
        return(EXIT_FAILURE);
    }
    
	return reset_clock(invertTimeBeacon);
}
