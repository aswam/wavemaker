//Provides networking interface for a master BBB in run mode

/*Version control
 v0.01 TD 16/04/15
 v0.02 TD 17/04/15 IP address get fix, Other bug fixes
 v0.03 TD 21/04/15 Partially complete: Added pins file, Allowed for comments before text nodes
 v0.04 TD 23/04/15 Updated to ctrl_pruss v0.09
 v0.05 TD 27/04/15 Removed read receipt
 v0.06 TD 28/04/15 Extra argument in run_pruss
 v0.07 TD 29/04/15 Added LIBXML_TEST_VERSION
 v0.08 TD 30/04/15 Stop all slaves if one fails on startup
 v0.09 TD 05/05/15 Skip over <wave> to reach <bbb> elements
 v0.10 TD 06/05/15 Renamed to master_run from master. Transmit 'g' to put in run mode.
 v0.11 TD 19/05/15 Added status message "attempting to connect"
 v0.12 TD 20/05/15 Added invertTimeBeacon
 v0.13 TD 19/01/16 Removed redundant #includes
 v0.14 TD 09/02/16 Macros for network messages; Bug fix in send_cancellation
 v0.15 TD 25/10/16 bytessent updates in send_cancellation()
 v0.16 TD 02/02/17 XML parser made thread-safe
 v0.17 TD 08/04/17 Each interval created in own thread
 v0.18 TD 20/04/17 Initialise XPath
 v0.19 TD 28/07/17 Use xmlStrEqual
 v0.20 TD 21/11/17 Send cancellation bug fix - works if slave is down
 v0.21 TD 22/01/20 Move invertTimeBeacon parameter to pins file
 */

#include <netdb.h>
#include "ctrl_pruss.h"

void send_cancellation(int sockets[], int numsockets)
//Send message 'pal' to slaves to tell them to abort without resetting
{
    char message[] = {ASWAM_PAUSE, ASWAM_ABORT, ASWAM_ZERO_NO};
    int k, bytessent, rtn;
    for (k = 0; k < numsockets; k++) {
        bytessent = 0;
        rtn = 1;    //Initialise for while loop
        while (bytessent < 3 && rtn > 0) {
            rtn = send(sockets[k], message + bytessent, 3 - bytessent, MSG_NOSIGNAL);
            bytessent += rtn;
            //Abandon but don't bother closing socket on failure, as closesockets() is called next if required
        }
    }
}

void closesockets(int sockets[], int numsockets)
{
    int k;
    for (k = 0; k < numsockets; k++) {
        close(sockets[k]);
    }
}

int main(int argc, char *argv[])
{
    //Program information
    if (argc != 2 || !strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
    {
        printf("Please provide one argument: XML configuration file\n");
        return(EXIT_FAILURE);
    }
    
    //Generic counters and returns
    int rtn;
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();
    
    //Open and read XML input
    xmlDocPtr xmlRawDoc;
    xmlNodePtr xmlNode;
    xmlChar * xmlText;
    
    xmlRawDoc = xmlParseFile(argv[1]);
    if (xmlRawDoc == NULL)
    {
        fprintf(stderr, "XML input file parse unsuccessful\n");
        xmlCleanupParser();
        return(EXIT_FAILURE);
    }
    
    //Validate XML doc
    const char schema_path[200] = "ASWaMMasterSchema.xsd";
    xmlDocPtr schema_doc = NULL;
    xmlSchemaParserCtxtPtr parser_ctxt = NULL;
    xmlSchemaPtr schema = NULL;
    xmlSchemaValidCtxtPtr valid_ctxt = NULL;
    schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
    parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
    schema = xmlSchemaParse(parser_ctxt);
    valid_ctxt = xmlSchemaNewValidCtxt(schema);
    int validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
    if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
    if (schema) xmlSchemaFree(schema);
    if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
    if (schema_doc) xmlFreeDoc(schema_doc);
    if (validationResult != 0)
    {
        fprintf(stderr, "Invalid syntax in XML input file: %d\n", validationResult);
        //Avoid memory leaks
        xmlFreeDoc(xmlRawDoc);
        xmlCleanupParser();
        return(EXIT_FAILURE);
    }
    
    //WARNING: Have not applied a stylesheet so there may be comment nodes, avoided using e.g. xmlFirstElementChild, but ->xmlChildrenNode is dangerous
    
    xmlNode = xmlDocGetRootElement(xmlRawDoc);  //<master>
    
    int noBBBs = xmlChildElementCount(xmlNode); //Number of BBBs
    xmlNode = xmlFirstElementChild(xmlNode);    //<bbb>
    while (!xmlStrEqual(xmlNode->name, (const xmlChar*) "bbb")) {
        //Not <bbb>
        xmlNode = xmlNextElementSibling(xmlNode);
        //<bbb> will be found, otherwise Schema validation would have failed
    }
    
    //Prepare to open network connections
    struct addrinfo hints;
    struct addrinfo *servinfo;  // will point to the results
    int slave_sockets[noBBBs - 1];      //To be fed into run_pruss on master
    char masterbinfilename[256] = "";
    char binaryfilename[256] = "";
    char masterpinsfilename[256] = "";
    char pinsfilename[256] = "";
    unsigned char err_occ = 0;
    
    memset(&hints, 0, sizeof hints); // make sure the struct is empty
    hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    
    int numsockets = -1;
    while (xmlNode) {
        xmlNodePtr xmlProperty = xmlFirstElementChild(xmlNode);    //<ip>
        xmlNodePtr xmlTextNode;
        
        //Get binary file name
        xmlProperty = xmlNextElementSibling(xmlProperty);   //<binary>
        xmlTextNode = xmlProperty->xmlChildrenNode;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        xmlText = xmlNodeListGetString(xmlRawDoc, xmlTextNode, 1);
        strcpy(binaryfilename, (char *) xmlText);
        xmlFree(xmlText);
        
        //Get pins file name
        xmlProperty = xmlNextElementSibling(xmlProperty);   //<pins>
        xmlTextNode = xmlProperty->xmlChildrenNode;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        xmlText = xmlNodeListGetString(xmlRawDoc, xmlTextNode, 1);
        strcpy(pinsfilename, (char *) xmlText);
        xmlFree(xmlText);
        
        xmlProperty = xmlFirstElementChild(xmlNode);   //<ip>

        xmlTextNode = xmlProperty->xmlChildrenNode;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        
        if (!(xmlTextNode) || xmlIsBlankNode(xmlTextNode)) {
            //Refers to this BBB, the master
            if (strlen(masterbinfilename)) {
                //Found two masters: error
                fprintf(stderr, "ERROR: Cannot have two master BBBs. Must specify an IP address for all slaves.\n");
                err_occ = 1;
                break;
            }
            strcpy(masterbinfilename, binaryfilename);
            strcpy(masterpinsfilename, pinsfilename);
        }
        else {
            //Slave
            xmlText = xmlNodeListGetString(xmlRawDoc, xmlProperty->xmlChildrenNode, 1);
            rtn = getaddrinfo((char *) xmlText, PORTNUM, &hints, &servinfo);
            xmlFree(xmlText);
            if (rtn != 0) {
                fprintf(stderr, "Failed to get address info for slave %d: %s.\n", numsockets + 1, gai_strerror(rtn));
                err_occ = 1;
                break;
            }
            
            numsockets++;
            slave_sockets[numsockets] = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
            if (slave_sockets[numsockets] == -1) {
                fprintf(stderr, "Failed to get socket for slave %d\n", numsockets);
                freeaddrinfo(servinfo);     //Free to avoid memory leak
                numsockets--;               //Because socket didn't open
                err_occ = 1;
                break;
            }
            
            printf("Attempting to connect to slave %d. Press Ctrl+C to abort.\n", numsockets);
            rtn = connect(slave_sockets[numsockets], servinfo->ai_addr, servinfo->ai_addrlen);
            freeaddrinfo(servinfo);     //Free to avoid memory leak
            
            if (rtn == -1) {
                fprintf(stderr, "Failed to connect to slave %d.\n", numsockets);
                err_occ = 2;        //Means socket open but disconnected
                break;
            }
            printf("Connected to slave %d.\n", numsockets);
            
            //Send mode: g = "go" run wave maker
            char modechar = 'g';
            rtn = send(slave_sockets[numsockets], &modechar, 1, MSG_NOSIGNAL);  //Send binary file name size
            if (rtn < 1) {  //Send is blocking
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Send binary file name, with first character being the length
            unsigned char filenamesize[1];
            filenamesize[0] = (unsigned char) strlen(binaryfilename);

            rtn = send(slave_sockets[numsockets], filenamesize, 1, MSG_NOSIGNAL);  //Send binary file name size
            if (rtn < 1) {  //Send is blocking
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Keep sending file name until all is sent
            int bytessent = 0;
            rtn = 1;
            while (bytessent < filenamesize[0] && rtn > 0) {
                rtn = send(slave_sockets[numsockets], binaryfilename+bytessent, filenamesize[0] - bytessent, MSG_NOSIGNAL);
                bytessent += rtn;
            }
            if (rtn < 1) {
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Send pins file name, with first character being the length
            filenamesize[0] = (unsigned char) strlen(pinsfilename);
            
            rtn = send(slave_sockets[numsockets], filenamesize, 1, MSG_NOSIGNAL);  //Send pins file name size
            if (rtn < 1) {  //Send is blocking
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
            
            //Keep sending file name until all is sent
            bytessent = 0;
            rtn = 1;
            while (bytessent < filenamesize[0] && rtn > 0) {
                rtn = send(slave_sockets[numsockets], pinsfilename+bytessent, filenamesize[0] - bytessent, MSG_NOSIGNAL);
                bytessent += rtn;
            }
            if (rtn < 1) {
                fprintf(stderr, "Connection to slave %d closed unexpectedly.\n", numsockets);
                err_occ = 2;
                break;
            }
        }
        
        xmlNode = xmlNextElementSibling(xmlNode);   //<bbb>, will go to NULL on last iteration
    }
    numsockets++;
    
    //XML Cleanup
    xmlFreeDoc(xmlRawDoc);
   
    //Exit if an error has occurred
    if (err_occ) {
        int sockets_to_message = numsockets;
        if (err_occ == 2) {
            sockets_to_message--;
        }
        send_cancellation(slave_sockets, sockets_to_message);
        closesockets(slave_sockets, numsockets);
        return EXIT_FAILURE;
    }
    
    //Check that a master has been specified
    if (strlen(masterbinfilename) == 0) {
        fprintf(stderr, "No master specified. One BBB must have an empty IP address.\n");
        send_cancellation(slave_sockets, numsockets);
        closesockets(slave_sockets, numsockets);
        return(EXIT_FAILURE);
    }

    //Wait for slaves to be running: message 's'
    char mastermode = MODE_NET_MASTER;
    rtn = wait_pause_slaves(NULL, &mastermode, slave_sockets, &numsockets, ASWAM_STARTED);
    if (rtn) {
        //Abort if any form of error, including network connection lost. This could occur, e.g. if binary file not found on slave.
        send_cancellation(slave_sockets, numsockets);
        closesockets(slave_sockets, numsockets);
        return(rtn);
    }
    
    //Set master running only if all slaves responded to say ready
	//Let timing beacon inversion be read from pins file
    rtn = run_pruss(mastermode, masterbinfilename, masterpinsfilename, slave_sockets, numsockets, numsockets, 0);
    
    //Cleanup XML & XSLT once all threads are done with it
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    closesockets(slave_sockets, numsockets);
    return 0;
}
