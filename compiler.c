// Compiler for wave maker. This can be run on any computer.

/* Version control
 v0.01 TD 18/01/16 Migrated from ctrl_pruss, Temporary files placed in separate folder
 v0.02 TD 20/01/16 Made some function parameters constant for safer code
 v0.03 TD 27/01/16 Malloc xPos list
 v0.04 TD 28/01/16 Mallocs cast to type
 v0.05 TD 25/11/16 Variable travel per step and max step rate
 v0.06 TD 02/12/16 Return error message if file write error & store new XML input in tmp folder
 v0.07 TD 02/02/17 XML parser made thread safe
 v0.08 TD 13/02/17 Wave resolution parameter optional
 v0.09 TD 24/03/17 Start and end interval as function of x
 v0.10 TD 26/03/17 Repeat periodic section of interval
 v0.11 TD 05/04/17 Improved discontinuity error messages & return compiled wave time
 v0.12 TD 08/04/17 Each interval calculated in own thread & direction signed char
 v0.13 TD 19/04/17 evalfx() free XSLT context and add error message
 v0.14 TD 20/04/17 Substitute once for numerical value of x & use heap for step array
 v0.15 TD 16/05/17 Resolution = desired effective clock tick and now used in minimiser
 v0.16 TD 19/05/17 Step times calculated at halfway between steps
 v0.17 TD 22/05/17 XML names changed to camel case & motor0xPos optional in master file
 v0.18 TD 23/05/17 Choose min/max first & number of instructions in performance stats
 v0.19 TD 26/05/17 Improved error message for bad output file
 v0.20 TD 20/06/17 Ported for MSVS2012 and throw error on fast step
 v0.21 TD 22/06/17 Interval start, end and period XSLTed to elements
 v0.22 TD 23/06/17 Read GPIO map from file
 v0.23 TD 28/06/17 Always compiles little endian, independent of machine
 v0.24 TD 30/06/17 Verification appended to output file
 v0.25 TD 17/07/17 Verification continues beyond first few instructions
 v0.26 TD 24/07/17 Evaluates constant sub function trees & Verification copes with variable travel per step
 v0.27 TD 02/08/17 Optimises function tree & reverse attribute moved to pins file
 v0.28 TD 07/11/17 Continue looping optimisation of multiplication and addition until no more collective changes
 v0.29 TD 21/11/17 Abort compile on error and return some messages
 v0.30 TD 11/12/17 Custom defined variables
 v0.31 TD 04/01/18 Bug fixes for Windows
 v0.32 TD 05/01/18 Determine extremum at end of subinterval by time
 v0.33 TD 14/02/19 Increase string copy length and added missing brackets around negation of IS_FINITE macro
 v0.34 TD 06/09/19 Removed possible infinite loops in optimisation of divisions and subtractions
 */

#include "compiler.h"

char* indep_variables[INDEP_VARS] = {"t"};

unsigned char expansion_to_gpio(int header, int pin, FILE* gpiomap)
//Converts expansion header and pin number to gpio pin number, returns 0 on error
{
    int gpio;
    char searchFormat[8];	//Up to 2 digits for header, 3 for pin plus 2 commas
    char readLine[13];	//Plus up to 4 for GPIO number and new line
    size_t searchFormatLen;
    
    //Return file pointer to start of file
    rewind(gpiomap);
    
    S_PRINTF(searchFormat, 8, "%d,%d,", header, pin)
    searchFormatLen = strlen(searchFormat);
    
    while (fgets(readLine, 13, gpiomap) != NULL) {
        if (strncmp(readLine, searchFormat, searchFormatLen) == 0) {
            if (S_SCANF(readLine + searchFormatLen, "%d", (int*)&gpio) == 1) { return (unsigned char) gpio; }	//Read integer, store as uint8 = unsigned char
        }
    }
    
    fprintf(stderr, "ERROR: Unrecognised expansion header P%d_%d.\n", header, pin);
    return 0;
}

long long roundll(double x)
//Rounds x to the nearest integer, taking halfway cases up (negative towards zero)
{
    long long rtn;
    if (x >= 0) { rtn = (long long)(x + 0.5); }		//Casting to long long (int) rounds towards zero
    else {
        rtn = (long long)(x - 0.5);
        if ((double) rtn + 0.5 <= x) {
            //Negative halfway case, must round towards zero
            rtn += 1;
        }
    }
    return rtn;
}

long long select_first(long long values[], int size, int *minimiser)
{
    int k;
    long long min_value;
    *minimiser = -1;
    min_value = LLONG_MAX;
    for (k = 0; k < size; k++)
    {
        if (values[k] < min_value)
        {
            *minimiser = k;
            min_value = values[k];
        }
    }
    return min_value;
}

int start_interval(long long time_index, double value, int *previous_s_steps, double travelPerStep, FILE **fp, const int motor_id, const int BBBid)
//Writes (if required) to the temporary motor file for a constant interval, or at the start of an interval.
{
    int dif = (int)roundll(value / travelPerStep) - *previous_s_steps;
    if (dif != 0)
    {
        long long instr_time;
        if (abs(dif) > 1)
        {
            fprintf(stderr, "ERROR: Discontinuity in input function for motor %d on BBB %d at time %f.\nRequested displacement step change from %f to %f, which is %d motor steps.\n", motor_id, BBBid, (time_index * TICK_DURATION), (*previous_s_steps * travelPerStep), value, abs(dif));
            return ERR_DISCONTINUOUS;
        }
        if (dif > 0) {
            instr_time = time_index;
            (*previous_s_steps)++;
        }
        else {
            instr_time = -time_index;
            (*previous_s_steps)--;
        }		//Negative indicates move backwards
        
        if (fwrite(&instr_time, sizeof instr_time, 1, *fp) < 1)
        {
            perror("Could not write to temporary file");
            return ERR_DRIVE;
        }
    }
    return 0;
}

int monotonic_interval(double start_t, double end_t, double start_s, double end_s, long long *time_index, int *previous_s_steps, double resolution, double travelPerStep, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode, FILE **fp, const int motor_id, const int BBBid)
//Calculates times to step motor, given a monotonic interval, returns 0 on success
{
    signed char direction;
    int rtn, k, end_s_steps, steps, brent_status;
    long long *instr_time;
    double step_boundary_offset, find_s, brent_arg, brent_value, *brent_arg_ptr;
    
    /* Set tolerance for zero-finding algorithm.
     Resolution is effective clock tick duration.
     zero_rc() returns with absolute error 2*zero_tol + error relative to machine precision.
     If true step due between ticks, it could go either way.
     If true step due on a tick, don't want algorithm to return a neighbouring tick.
     Therefore, need return value ± half a tick, so zero_tol = quarter tick.
     */
    const double zero_tol = resolution / 4;
    
    if (end_s > start_s) { direction = 1; }
    else { direction = -1; }
    
    //Half of direction - for finding halfway point between steps
    //Define as double rather than float to avoid cast later.
    step_boundary_offset = 0.5 * direction;
    
    if ((rtn = start_interval(*time_index, start_s, previous_s_steps, travelPerStep, fp, motor_id, BBBid))) {
        return rtn;
    }
    
    end_s_steps = (int)roundll(end_s / travelPerStep);  //Always rounds halfway up
    steps = abs(end_s_steps - *previous_s_steps);   //Up to 8016
    instr_time = (long long*) malloc(steps * sizeof(long long));        //Up to 64128B
    brent_arg_ptr = &brent_arg;
    
    for (k = 0; k < steps; k++)
    {
        struct zero_rc_statics statics;
        
        *previous_s_steps += direction;
        //Calculate times at halfway spatially between step points, so that location of rod always rounded to nearest step point. There is an approximately constant time lag in the rod moving -> negligible, consistent lag.
        find_s = ((double)*previous_s_steps - step_boundary_offset) * travelPerStep;
        
        brent_status = 0;
        brent_value = 0;
        zero_rc(start_t, end_t, zero_tol, &brent_arg, &brent_status, brent_value, &statics);
        while (brent_status > 0)
        {
            rtn = calcalgfunc(&brent_value, 1, (const double**)&brent_arg_ptr, (const char**)indep_variables, INDEP_VARS, xmlTree, intervalNode);
            brent_value -= find_s;
            if (rtn)
            {
                fprintf(stderr, "Error in function string in input file.\n");
                free(instr_time);
                return rtn;
            }
            zero_rc(start_t, end_t, zero_tol, &brent_arg, &brent_status, brent_value, &statics);
        }
        if (brent_status < 0)
        {
            //Error returned
            //Numerical errors may mean find_s is just outside [start_s,end_s]
            if ((find_s - start_s) * direction < 0.0) {
                //start_s is between find_s and end_s -> need to step at start of subinterval
                instr_time[k] = *time_index * direction;
            } else if ((find_s - end_s) * direction > 0.0) {
                //end_s is between start_s and find_s -> need to step at end of subinterval
                instr_time[k] = roundll((end_t - start_t) / TICK_DURATION + (double)*time_index) * direction;
            } else {
                fprintf(stderr, "ERROR: Discontinuity in input function for motor %d on BBB %d between times %f and %f.\nCannot find time when displacement should be %f.\n", motor_id, BBBid, start_t, end_t, find_s);
                free(instr_time);
                return ERR_DISCONTINUOUS;
            }
        } else {
            //Zero found -> time to step motor found
            //Calculate step time as double then round at end to reduce rounding errors - double can hold sufficient precision for no loss here
            instr_time[k] = roundll((brent_arg - start_t) / TICK_DURATION + (double)*time_index) * direction;
        }
    }
    
    rtn = fwrite(instr_time, sizeof *instr_time, steps, *fp);
    free(instr_time);   //Finished with
    if (rtn < steps)
    {
        perror("Could not write to temporary file");
        return ERR_DRIVE;
    }
    *time_index += roundll((end_t - start_t) / TICK_DURATION);		//Do this at the end to minimise build-up of numerical errors
    return 0;
}

int find_min(double* min_t, double* min_s, double start_t, double end_t, const double start_s, const double end_s, const double minitol, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode)
//Searches for a local minimum that is strictly in the interval (start_s,end_s)
//Returns 5 if a local minimum is found, 0 if evaluation ok but min not found
{
    struct local_min_rc_statics statics;
    double initial_start_t = start_t;
    double initial_end_t = end_t;
    int brent_status = 0;
    *min_s = 0;
    
    do {
        int rtn;
        *min_t = local_min_rc(&start_t, &end_t, &brent_status, *min_s, &statics);		//XML Schema ensures b > a
        rtn = calcalgfunc(min_s, 1, (const double**)&min_t, (const char**)indep_variables, INDEP_VARS, xmlTree, intervalNode);
        if (rtn)
        {
            fprintf(stderr, "Error in function string in input file.\n");
            return rtn;
        }
    } while (brent_status && (end_t - start_t >= minitol));	//No point in going more accurate than a half tick of clock. Brent algorithm only self-terminates when machine precision is reached.
    //Brent algorithm stops when min_t is known to within 2(1.5e-8 * min_t + 1e-16), which > clock period for min_t > 1.
    
    //Check whether found minimum is at an end point by whether start_t or end_t hasn't changed. Doing it by comparing displacments at found extremum and end points fails when subinterval is 1.5 periods, starting at a minimum of sine wave.
    if (initial_start_t == start_t || initial_end_t == end_t) {
        return 0;
    } else {
        return 5;   //Minimum found - calcalgfunc() returns 1 or 2 on error
    }
}

int find_max(double* max_t, double* max_s, double start_t, double end_t, const double start_s, const double end_s, const double minitol, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode)
//Searches for a local maximum that is strictly in the interval (start_s,end_s)
//Returns 6 if a local maximum is found, 0 if evaluation ok but max not found
{
    struct local_min_rc_statics statics;
    double initial_start_t = start_t;
    double initial_end_t = end_t;
    int brent_status = 0;
    *max_s = 0;
    
    do {
        int rtn;
        *max_s = -(*max_s); //Turn maximisation into a minimisation
        *max_t = local_min_rc(&start_t, &end_t, &brent_status, *max_s, &statics);		//XML Schema ensures b > a
        rtn = calcalgfunc(max_s, 1, (const double**)&max_t, (const char**)indep_variables, INDEP_VARS, xmlTree, intervalNode);
        if (rtn)
        {
            fprintf(stderr, "Error in function string in input file.\n");
            return rtn;
        }
    } while (brent_status && (end_t - start_t >= minitol));	//No point in going more accurate than a half tick of clock. Brent algorithm only self-terminates when machine precision is reached.
    //Brent algorithm stops when min_t is known to within 2(1.5e-8 * max_t + 1e-16), which > clock period for max_t > 1
    
    //Check whether found maximum is at an end point by whether start_t or end_t hasn't changed. Doing it by comparing displacments at found extremum and end points fails when subinterval is 1.5 periods, starting at a minimum of sine wave.
    if (initial_start_t == start_t || initial_end_t == end_t) {
        return 0;
    } else {
        return 6;   //Maximum found - calcalgfunc() returns 1 or 2 on error
    }
}

int split_interval(char tryMinFirst, double start_t, double end_t, double start_s, double end_s, double *min_s, double *max_s, long long *time_index, int *previous_s_steps, double resolution, double travelPerStep, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode, FILE **fp, const int motor_id, const int BBBid, int *stopSig)
//Subdivides an interval into monotonic sections, then calls monotonic_interval for each one. Returns 0 on success.
{
    /* Set tolerance for minimum-finding algorithm.
     Resolution is effective clock tick duration.
     If true minimum is between ticks, it could go either way.
     If true step due on a tick, don't want algorithm to return a neighbouring tick.
     Therefore, need considered interval to be less than half tick.
     */
    const double minitol = resolution / 2;
    
    double turning_t, turning_s;    //Coordinates of turning point
    int rtn;
    
    if (*stopSig) {
        //An error has occurred somewhere, so the compiling is being aborted
        return ERR_STOPSIG;
    }
    
    switch (tryMinFirst) {
        case 1:
            rtn = find_min(&turning_t, &turning_s, start_t, end_t, start_s, end_s, minitol, xmlTree, intervalNode);
            if (rtn) {
                break;
            }   //rtn = 0 -> evaluation ok but min not found
        default:
            rtn = find_max(&turning_t, &turning_s, start_t, end_t, start_s, end_s, minitol, xmlTree, intervalNode);
            if (rtn || tryMinFirst == 1) {
                break;
            }
            //Tried for max first, not found, so try for min
            rtn = find_min(&turning_t, &turning_s, start_t, end_t, start_s, end_s, minitol, xmlTree, intervalNode);
    }
    
    //Default: found a minimum, so look for a maximum next
    tryMinFirst = 0;
    
    switch (rtn) {
        case 6:
            //Found a maximum, so look for a minimum next
            tryMinFirst = 1;
        case 5:
            //Turning point found
            if ((rtn = split_interval(tryMinFirst, start_t, turning_t, start_s, turning_s, min_s, max_s, time_index, previous_s_steps, resolution, travelPerStep, xmlTree, intervalNode, fp, motor_id, BBBid, stopSig))) {
                //If rtn != 0, an error has occurred
                return rtn;
            }
            if ((rtn = split_interval(tryMinFirst, turning_t, end_t, turning_s, end_s, min_s, max_s, time_index, previous_s_steps, resolution, travelPerStep, xmlTree, intervalNode, fp, motor_id, BBBid, stopSig))) {
                //If rtn != 0, an error has occurred
                return rtn;
            }
            break;
        case 0:
            //Monotonic interval
            if ((rtn = monotonic_interval(start_t, end_t, start_s, end_s, time_index, previous_s_steps, resolution, travelPerStep, xmlTree, intervalNode, fp, motor_id, BBBid))) {
                //If rtn != 0, an error has occurred
                return rtn;
            }
            //Update extremal displacements
            if (start_s < *min_s) *min_s = start_s;
            if (end_s < *min_s) *min_s = end_s;
            if (start_s > *max_s) *max_s = start_s;
            if (end_s > *max_s) *max_s = end_s;
            break;
        default:
            //Return error code
            return rtn;
    }
    
    return 0;
}

int cleanandclose(FILE **out_fp, FILE *motor_fp[], unsigned char gpio_pulse[], unsigned char gpio_direction[], double travelPerStep[], int motors, int* BBBthreads, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition)
//Cleans up everything, closes file descriptors and deletes temporary files
//Returns whether output file was written successfully
{
    //Close and delete temporary files
    int motor_id;
    int rtn = 0;
    
    for (motor_id = 0; motor_id < motors; motor_id++)
    {
        fclose(motor_fp[motor_id]);
    }
    free(motor_fp);
    
    free(travelPerStep);
    
    if (out_fp) { rtn = fclose(*out_fp); }
    if (rtn) { perror("ERROR: Output file did not close properly. Data may have been lost."); }
    
    //Free pin lists
    free(gpio_pulse);
    free(gpio_direction);
    
    //Signal BBB thread is released
    pthread_mutex_lock(ThreadCounterMutex);
    *BBBthreads -= 1;
    pthread_cond_broadcast(ThreadCounterCondition);
    pthread_mutex_unlock(ThreadCounterMutex);
    
    return rtn;
}

int evalConstNode(xmlDocPtr xmlDocument, xmlNodePtr parentNode) {
    //Evaluates a constant function tree and replaces it by its value
    //Returns 0 on success, -4 on failure
    double funcValue;
    double tValue[] = {0};  //Value is arbitrary here, but must be initialised
    char newStr[25];
    xmlNodePtr newNode;
    
    if (calcalgfunc(&funcValue, 1, (const double**)&tValue, (const char**)indep_variables, INDEP_VARS, xmlDocument, &parentNode)) {
        return -4;   //Error in function string
    }
    
    //funcValue now contains the constant value of the interval
    //Create string representation of value
    S_PRINTF(newStr, 25, "%.16g", funcValue)	//Allows up to 999 in exponent
    newNode = xmlNewText((const xmlChar*)newStr);
    xmlFreeNode(xmlReplaceNode(xmlFirstElementChild(parentNode), newNode));  //Only has one child - either binfunc or unifunc
    
    return 0;
}

int checkUpTreeConstant(xmlDocPtr xmlDocument, xmlNodePtr funcNode, xmlNodePtr curNode) {
    //Checks up the tree if can evaluate to a constant after variables potentially removed
    //Returns 0 if whole tree is constant, -4 on error, 2 on no change, 1 otherwise
    int rtn = 2;
    
    //Keep going up until the current node is either funcNode or (its parent is <binfunc> and its sibling is a complex element)
    //One childElementCount is always zero, other is zero if simple, so want loop only if or=0
    //xml functions return NULL or 0 if input is NULL
    while (!(curNode == funcNode || (xmlStrEqual(curNode->parent->name, (const xmlChar*)"binfunc") && (xmlChildElementCount(xmlPreviousElementSibling(curNode)) || xmlChildElementCount(xmlNextElementSibling(curNode)))))) {
        curNode = curNode->parent;
    }
    
    //curNode is now either left, right or funcNode
    if (xmlChildElementCount(curNode)) {
        //Has child elements needing evalulating
        if (!(rtn = evalConstNode(xmlDocument, curNode))) {
            if (curNode != funcNode) {
                rtn = 1;
            }
        }
    }
    
    return rtn;
}

void insertUnaryMinus(xmlNodePtr oldNode) {
    //Inserts a unary minus function to act on oldNode
    xmlNodePtr newNode = xmlNewNode(NULL, (const xmlChar*)"unifunc");
    xmlSetProp(newNode, (const xmlChar*)"name", (const xmlChar*)"minus");
    oldNode = xmlReplaceNode(oldNode, newNode);
    xmlAddChild(newNode, oldNode);
}

int insertConstantMultiply(xmlDocPtr xmlDocument, xmlNodePtr oldNode, double nodeValue, const int divide) {
    //Inserts or merges a multiply by constant above/with oldNode - needs to be on right so that any H() can be on left
    //If divide == 1, divides by this number instead
    xmlChar *funcName = xmlGetProp(oldNode, (const xmlChar*)"name");
    xmlNodePtr newNode = xmlLastElementChild(oldNode);  //Contains <right> child of oldNode or NULL
    if (xmlStrEqual(funcName, (const xmlChar*)"*") && xmlChildElementCount(newNode) == 0) {
        //Merge with existing constant multiplication
        //Multiply its value by nodeValue
        xmlChar *oldValue;
        oldValue = xmlNodeListGetString(xmlDocument, newNode->children, 1);
        if (divide) {
            nodeValue = atof((const char*)oldValue) / nodeValue;
        } else {
            nodeValue *= atof((const char*)oldValue);
        }
        xmlFree(oldValue);
        
        //Check for infinity
        if (!(IS_FINITE(nodeValue))) {
            fprintf(stderr, "ERROR: Division by small number evaluated to infinity.\n");
            xmlFree(funcName);
            return -1;
        }
        
        if (nodeValue == 1.0 || nodeValue == -1.0) {
            //Identity or minus, so remove node
            newNode = xmlFirstElementChild(xmlPreviousElementSibling(newNode));
            xmlFreeNode(xmlReplaceNode(oldNode, newNode));
            if (nodeValue == -1.0) {
                //Replace with unary minus
                insertUnaryMinus(newNode);
            }
        } else {
            //Update value
            char newStr[25];
            S_PRINTF(newStr, 25, "%.16g", nodeValue)	//Allows up to 999 in exponent
            newNode = newNode->children;
            xmlFree(newNode->content);  //Free old string before overwriting with new
            newNode->content = xmlCharStrdup(newStr);
        }
    } else {
        char newStr[25];
        xmlNodePtr lrnode;
        //Create new <binfunc>
        newNode = xmlNewNode(NULL, (const xmlChar*)"binfunc");
        xmlSetProp(newNode, (const xmlChar*)"name", (const xmlChar*)"*");
        oldNode = xmlReplaceNode(oldNode, newNode);
        //Place old node under <left>
        lrnode = xmlNewNode(NULL, (const xmlChar*)"left");
        xmlAddChild(newNode, lrnode);
        xmlAddChild(lrnode, oldNode);
        
        //Put value in right
        if (divide) {
            nodeValue = 1.0 / nodeValue;
        }
        //Check for infinity
        if (!(IS_FINITE(nodeValue))) {
            fprintf(stderr, "ERROR: Division by small number evaluated to infinity.\n");
            xmlFree(funcName);
            return -1;
        }
        
        S_PRINTF(newStr, 25, "%.16g", nodeValue)	//Allows up to 999 in exponent
        xmlNewTextChild(newNode, NULL, (const xmlChar*)"right", (const xmlChar*)newStr);
    }
    xmlFree(funcName);
    return 0;
}

int insertConstantAdd(xmlDocPtr xmlDocument, xmlNodePtr oldNode, double nodeValue, const int subtract) {
    //Inserts or merges an addition by constant above/with oldNode - needs to be on right
    //If subtract == 1, subtracts by this number instead
    xmlChar *funcName = xmlGetProp(oldNode, (const xmlChar*)"name");
    xmlNodePtr newNode = xmlLastElementChild(oldNode);  //Contains <right> child of oldNode or NULL
    if (xmlStrEqual(funcName, (const xmlChar*)"+") && xmlChildElementCount(newNode) == 0) {
        //Merge with existing constant addition
        //Add its value to nodeValue
        xmlChar *oldValue;
        oldValue = xmlNodeListGetString(xmlDocument, newNode->children, 1);
        if (subtract) {
            nodeValue = atof((const char*)oldValue) - nodeValue;
        } else {
            nodeValue += atof((const char*)oldValue);
        }
        xmlFree(oldValue);
        
        if (nodeValue == 0.0) {
            //Identity, so remove node
            newNode = xmlFirstElementChild(xmlPreviousElementSibling(newNode));
            xmlFreeNode(xmlReplaceNode(oldNode, newNode));
        } else {
            //Update value
            char newStr[25];
            S_PRINTF(newStr, 25, "%.16g", nodeValue)	//Allows up to 999 in exponent
            newNode = newNode->children;
            xmlFree(newNode->content);  //Free old string before overwriting with new
            newNode->content = xmlCharStrdup(newStr);
        }
    } else {
        char newStr[25];
        xmlNodePtr lrnode;
        //Create new <binfunc>
        newNode = xmlNewNode(NULL, (const xmlChar*)"binfunc");
        xmlSetProp(newNode, (const xmlChar*)"name", (const xmlChar*)"+");
        oldNode = xmlReplaceNode(oldNode, newNode);
        //Place old node under <left>
        lrnode = xmlNewNode(NULL, (const xmlChar*)"left");
        xmlAddChild(newNode, lrnode);
        xmlAddChild(lrnode, oldNode);
        //Create right and put value in it
        if (subtract) {
            nodeValue = -nodeValue;
        }
        S_PRINTF(newStr, 25, "%.16g", nodeValue)	//Allows up to 999 in exponent
        xmlNewTextChild(newNode, NULL, (const xmlChar*)"right", (const xmlChar*)newStr);
    }
    xmlFree(funcName);
	return 0;
}

xmlNodePtr swapLeftRightNodes(xmlNodePtr leftNode) {
    //Swaps the left and right nodes including their contents, takes old left node and returns new left node
    xmlNodeSetName(leftNode, (const xmlChar*)"right");
    leftNode = xmlAddPrevSibling(leftNode, xmlNextElementSibling(leftNode));   //oldNode contains old <left>
    xmlNodeSetName(leftNode, (const xmlChar*)"left");
    return leftNode;
}

int optimiseFuncTree(xmlDocPtr xmlDocument, xmlNodePtr funcNode, double xPos)
//Substitutes value of motorxPos for all variable x in intervalNode and optimises function tree
//This should all be thread safe because this is only called on a child node of <motor>, so no overlap between possible calls.
{
    xmlXPathContextPtr xpathCtx;
    xmlXPathObjectPtr xpathObj;
    xmlNodeSetPtr xmlNodeList;
    xmlChar *xmlText;
    char newStr[25];
    int numNodes, nodeNum, change_occ, outerChangeOcc;
    xmlNodePtr newNode, oldNode;
    
    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(xmlDocument);
    if (xpathCtx == NULL) {
        fprintf(stderr, "ERROR: Unable to create new XPath context.\n");
        return -1;
    }
    
    //Set context node
    //xmlXPathSetContextNode and xmlXPathNodeEval require v2.9.1, but need v2.8.0, so don't use
    xpathCtx->node = funcNode;
    
    /* Evaluate xpath expression */
    xpathObj = xmlXPathEvalExpression((const xmlChar*)".//var[@name=\"x\"]", xpathCtx);
    if (xpathObj == NULL) {
        xmlXPathFreeContext(xpathCtx);
        fprintf(stderr,"ERROR: Unable to evaluate XPath expression: .//var[@name=\"x\"].\n");
        return -3;
    }
    
    //Create string representation of xPos
    S_PRINTF(newStr, 25, "%.16g", xPos)	//Allows up to 999 in exponent
    
    xmlNodeList = xpathObj->nodesetval;
    numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
    
    //Work through nodes, replacing with xPosStr
    //Good practice: start at end of list in case substituting descendant of another found node
    for (nodeNum = numNodes - 1; nodeNum >= 0; nodeNum--) {
        newNode = xmlNewText((const xmlChar*)newStr);
        oldNode = xmlXPathNodeSetItem(xmlNodeList, nodeNum);
        xmlFreeNode(xmlReplaceNode(oldNode, newNode));
        xmlNodeList->nodeTab[nodeNum] = NULL;   //Must not point to freed node, otherwise get errors later in XPathFreeObject
    }
    
    /* Cleanup of XPath data */
    xmlXPathFreeObject(xpathObj);
    
    if (!xmlChildElementCount(funcNode)) {
        //Already a constant
        xmlXPathFreeContext(xpathCtx);
        return 0;
    }
    
    //Check whether function evaluates to constant
    xpathCtx->node = funcNode;  //Needs resetting after each use
    xpathObj = xmlXPathEvalExpression((const xmlChar*)".//var[@name=\"t\"]", xpathCtx);
    if (xpathObj == NULL) {
        fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
        xmlXPathFreeContext(xpathCtx);
        return 3;
    }
    
    numNodes = xmlXPathNodeSetGetLength(xpathObj->nodesetval);
    xmlXPathFreeObject(xpathObj);
    
    if (numNodes == 0) {
        //No <var name="t" /> found -> constant to be evaluated
        xmlXPathFreeContext(xpathCtx);
        
        //Now a constant value, so no more optimisation possible
        //Replace unifunc or binfunc node - won't be a constant, as already checked
        return evalConstNode(xmlDocument, funcNode);
    }
    
    //Only time tree branches is in binfunc, so look for non-constant branches not containing variable t
    do {
        xpathCtx->node = funcNode;
        xpathObj = xmlXPathEvalExpression((const xmlChar*)".//left[* and not(.//var/@name=\"t\")] | .//right[* and not(.//var/@name=\"t\")]", xpathCtx);
        if (xpathObj == NULL) {
            fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
            xmlXPathFreeContext(xpathCtx);
            return 3;
        }
        xmlNodeList = xpathObj->nodesetval;
        numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
        oldNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
        xmlXPathFreeObject(xpathObj);
        
        if (numNodes) {
            if (evalConstNode(xmlDocument, oldNode)) {
                xmlXPathFreeContext(xpathCtx);
                return -4;   //Error in function string
            }
        }
    } while (numNodes > 1); //Says there may be more to come
    
    //Loop this next section until no more changes occur - potential for collapsing tree
    change_occ = 1;
    while (change_occ) {
        change_occ = 0;
        //Look for zero multiplications, divisions or power bases - nonstandard implementation of defining to be zero without throwing error
        do {
            xpathCtx->node = funcNode;
            xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"*\" and (left=0 or right=0)] | .//binfunc[(@name=\"/\" or @name=\"^\") and left=0]", xpathCtx);
            if (xpathObj == NULL) {
                fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                xmlXPathFreeContext(xpathCtx);
                return 3;
            }
            xmlNodeList = xpathObj->nodesetval;
            numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
            oldNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
            xmlXPathFreeObject(xpathObj);
            
            if (numNodes) {
                xmlChar *binfuncName;
                int checkRtn;
                
                //Check for zeroes propagating up tree
                newNode = oldNode->parent->parent;  //Grandparent always exists, even if it is <wave>, which won't have a relevant name attribute
                binfuncName = xmlGetProp(newNode, (const xmlChar*)"name");  //Returns NULL if not found
                //No need to check if it's a binfunc as no unifuncs have such names. If the parent is a unifunc, the grandparent is either a unifunc or left/right, so not binfunc.
                while (xmlStrEqual(binfuncName, (const xmlChar*)"*") || ((xmlStrEqual(binfuncName, (const xmlChar*)"/") || xmlStrEqual(binfuncName, (const xmlChar*)"^")) && xmlStrEqual(oldNode->parent->name, (const xmlChar*)"left"))) {
                    //Propagate zero up tree
                    xmlFree(binfuncName);
                    oldNode = newNode;
                    newNode = oldNode->parent->parent;
                    binfuncName = xmlGetProp(newNode, (const xmlChar*)"name");  //Returns NULL if not found
                }
                xmlFree(binfuncName);
                
                //Replace with zero
                STR_CPY(newStr, "0", 2)
                newNode = xmlNewText((const xmlChar*)newStr);
                xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                
                //Log change
                change_occ = 1;
                
                //May have created new subtree independent of t
                if ((checkRtn = checkUpTreeConstant(xmlDocument, funcNode, newNode)) < 1) {
                    //checkRtn = 0 if whole tree is now constant, negative on error
                    xmlXPathFreeContext(xpathCtx);
                    return checkRtn;
                }
            }
        } while (numNodes > 1); //Says could be another zero to deal with
        
        //Look for zero power exponents or one power bases
        do {
            xpathCtx->node = funcNode;
            xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[name=\"^\" and (left=1 or right=0)]", xpathCtx);
            if (xpathObj == NULL) {
                fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                xmlXPathFreeContext(xpathCtx);
                return 3;
            }
            xmlNodeList = xpathObj->nodesetval;
            numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
            oldNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
            xmlXPathFreeObject(xpathObj);
            
            if (numNodes) {
                xmlChar *binfuncName;
                int checkRtn;
                
                //Check for ones propagating up tree as power base
                newNode = oldNode->parent->parent;  //Grandparent always exists, even if it is <wave>, which won't have a relevant name attribute
                binfuncName = xmlGetProp(newNode, (const xmlChar*)"name");  //Returns NULL if not found
                //No need to check if it's a binfunc as no unifuncs have such names. If the parent is a unifunc, the grandparent is either a unifunc or left/right, so not binfunc.
                while (xmlStrEqual(binfuncName, (const xmlChar*)"^") && xmlStrEqual(oldNode->parent->name, (const xmlChar*)"left")) {
                    //Propagate one up tree
                    xmlFree(binfuncName);
                    oldNode = newNode;
                    newNode = oldNode->parent->parent;
                    binfuncName = xmlGetProp(newNode, (const xmlChar*)"name");  //Returns NULL if not found
                }
                xmlFree(binfuncName);
                
                STR_CPY(newStr, "1", 2)
                newNode = xmlNewText((const xmlChar*)newStr);
                xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                
                //Log change
                change_occ = 1;
                
                //May have created new subtree independent of t
                if ((checkRtn = checkUpTreeConstant(xmlDocument, funcNode, newNode)) < 1) {
                    //checkRtn = 0 if whole tree is now constant, negative on error
                    xmlXPathFreeContext(xpathCtx);
                    return checkRtn;
                }
            }
        } while (numNodes > 1); //Says there could be another one
    }
    
    //Simplify raising to the power of 1
    do {
        xpathCtx->node = funcNode;
        xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"^\" and right=1]/left/*", xpathCtx);
        if (xpathObj == NULL) {
            fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
            xmlXPathFreeContext(xpathCtx);
            return 3;
        }
        xmlNodeList = xpathObj->nodesetval;
        numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
        newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
        xmlXPathFreeObject(xpathObj);
        
        if (numNodes) {
            oldNode = newNode->parent->parent;  //Removing binfunc
            xmlFreeNode(xmlReplaceNode(oldNode, newNode));
        }
    } while (numNodes > 1); //Says could be another one
    
    //Raising to -1 better as division
    do {
        xpathCtx->node = funcNode;
        xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"^\" and right=-1]/left/*", xpathCtx);
        if (xpathObj == NULL) {
            fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
            xmlXPathFreeContext(xpathCtx);
            return 3;
        }
        xmlNodeList = xpathObj->nodesetval;
        numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
        newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
        xmlXPathFreeObject(xpathObj);
        
        if (numNodes) {
            //Move from <left> to <right>
            oldNode = newNode->parent->next->children;  //Removing text node
            xmlFreeNode(xmlReplaceNode(oldNode, newNode));
            //New old node
            STR_CPY(newStr, "1", 2)
            oldNode = xmlNewText((const xmlChar*)newStr);
            newNode = newNode->parent;  //<right>
            xmlAddChild(xmlPreviousElementSibling(newNode), oldNode);   //Inserting into <left>
            //Change binfunc name to divide
            xmlSetProp(newNode->parent, (const xmlChar*)"name", (const xmlChar*)"/");
        }
    } while (numNodes > 1); //Says could be another one
    
    //Loop through simplifying multiplications, divisions, additions and subtractions until no more changes occur
    outerChangeOcc = 1;
    while (outerChangeOcc) {
        outerChangeOcc = 0;
        
        //Change 0-, *-1 and /-1 to unary minus
        do {
            xpathCtx->node = funcNode;
            xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[(@name=\"-\" and left=0) or (@name=\"*\" and left=-1)]/right | .//binfunc[(@name=\"*\" or @name=\"/\") and right=-1]/left", xpathCtx);
            if (xpathObj == NULL) {
                fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                xmlXPathFreeContext(xpathCtx);
                return 3;
            }
            xmlNodeList = xpathObj->nodesetval;
            numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
            newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
            xmlXPathFreeObject(xpathObj);
            
            if (numNodes) {
                outerChangeOcc = 1;
                oldNode = newNode->parent;
                xmlNodeSetName(newNode, (const xmlChar*)"unifunc");
                xmlSetProp(newNode, (const xmlChar*)"name", (const xmlChar*)"minus");
                xmlFreeNode(xmlReplaceNode(oldNode, newNode));
            }
        } while (numNodes > 1); //Says could be another one
        
        //Collect all constants in multiplication/division subtrees into a multiplication at the top with constant on right
        //Unary minus is multiplying by -1, so move to top
        do {
            xpathCtx->node = funcNode;
            xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"*\" or @name=\"/\"]/*/unifunc[@name=\"minus\"]/*", xpathCtx);
            if (xpathObj == NULL) {
                fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                xmlXPathFreeContext(xpathCtx);
                return 3;
            }
            xmlNodeList = xpathObj->nodesetval;
            numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
            newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
            xmlXPathFreeObject(xpathObj);
            
            if (numNodes) {
                outerChangeOcc = 1;
                oldNode = newNode->parent;  //Taking out 1 unifunc
                xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                
                //Traverse up subtree to top multiplication/division or another unary minus
                oldNode = newNode->parent->parent;  //On <binfunc> found by xPath
                newNode = oldNode->parent;
                for (;;) {
                    if (newNode == funcNode) {
                        //Reached the top of the tree - insert unary minus and break
                        insertUnaryMinus(oldNode);
                        break;
                    }
                    xmlText = xmlGetProp(newNode, (const xmlChar*)"name");  //NULL if not found
                    if (xmlText) {
                        //Name attribute found
                        if (xmlStrEqual(xmlText, (const xmlChar*)"minus")) {
                            //Found another unary minus in newNode - remove it too as they cancel
                            xmlFree(xmlText);
                            xmlFreeNode(xmlReplaceNode(newNode, oldNode));
                            break;
                        }
                        if (!(xmlStrEqual(xmlText, (const xmlChar*)"*") || xmlStrEqual(xmlText, (const xmlChar*)"/"))) {
                            //Found a function that's not multiply or divide -> have left subtree
                            xmlFree(xmlText);
                            //If have hit a binfunc, old node is <left> or <right> so need to go down one before inserting
                            if (xmlStrEqual(newNode->name, (const xmlChar*)"binfunc")) {
                                oldNode = xmlFirstElementChild(oldNode);
                            }
                            //Insert a unary minus at this point
                            insertUnaryMinus(oldNode);
                            break;
                        }
                        xmlFree(xmlText);
                    }
                    //Look higher up tree
                    oldNode = newNode;
                    newNode = newNode->parent;
                }
            }
        } while (numNodes > 1); //Says could be another one
        
        //Collate constants in a subtree of multiplications and divisions - includes dealing with identities
        do {
            xpathCtx->node = funcNode;
            //Select all left and right text-only nodes of multiplications, except right multiplication with no M/D parent (top of subtree) but do select when right=1 (identity for removal) or has unary minus parent, and also select right text-only node on division
			//Other potential optimisations involving subtractions can result in infinite loops
            xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"*\"]/left[not(*)] | .//binfunc[@name=\"*\"]/right[.=1] | .//unifunc[@name=\"minus\"]/binfunc[@name=\"*\"]/right[not(*)] | .//binfunc[@name=\"*\" or @name=\"/\"]/*/binfunc[@name=\"*\"]/right[not(*)] | .//binfunc[@name=\"/\"]/right[not(*)]", xpathCtx);
            if (xpathObj == NULL) {
                fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                xmlXPathFreeContext(xpathCtx);
                return 3;
            }
            xmlNodeList = xpathObj->nodesetval;
            numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
            newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
            xmlXPathFreeObject(xpathObj);
            
            if (numNodes) {
                double nodeValue;
                int divide = 0; //Boolean representing whether to multiply or divide by nodeValue
                
                outerChangeOcc = 1;
                
                //Get value as a number
                xmlText = xmlNodeListGetString(xmlDocument, newNode->children, 1);
                nodeValue = atof((const char*)xmlText);
                xmlFree(xmlText);
                
                //Select contents of other side of binary operator
                if (xmlStrEqual(newNode->name, (const xmlChar*)"left")) {
                    newNode = xmlFirstElementChild(xmlNextElementSibling(newNode)); //Contents of <right>
                } else {
                    newNode = xmlFirstElementChild(xmlPreviousElementSibling(newNode)); //Contents of <left>
                }
                //Remove binfunc node
                oldNode = newNode->parent->parent;  //<binfunc>
                xmlText = xmlGetProp(oldNode, (const xmlChar*)"name");  //Get operator
                xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                if (xmlStrEqual(xmlText, (const xmlChar*)"/")) {
                    //Division taking place
					//Replacing f / const by f with multiplicative constant of (1/const) promoted to the top of the tree
                    divide = 1;
                }
                xmlFree(xmlText);
                
                if (nodeValue != 1.0) {
                    //Not identity
                    //Traverse up tree to top - will only come across a unary minus at the top
                    oldNode = newNode;  //Contents of multiplication
                    newNode = oldNode->parent;
                    for (;;) {
                        if (newNode == funcNode) {
                            //Reached the top of the tree
                            //Insert or merge multiplication by a constant
                            if (insertConstantMultiply(xmlDocument, oldNode, nodeValue, divide)) {
                                xmlXPathFreeContext(xpathCtx);
                                return -4;
                            }
                            break;
                        }
                        xmlText = xmlGetProp(newNode, (const xmlChar*)"name");  //NULL if not found
                        if (xmlText) {
                            //Name attribute found
                            if (xmlStrEqual(xmlText, (const xmlChar*)"minus")) {
                                //Found unary minus at top of subtree in newNode - replace it with a multiplication
                                xmlFree(xmlText);
                                xmlNodeSetName(newNode, (const xmlChar*)"binfunc");
                                xmlSetProp(newNode, (const xmlChar*)"name", (const xmlChar*)"*");
                                //Insert left above oldNode
                                newNode = xmlNewNode(NULL, (const xmlChar*)"left");
                                oldNode = xmlReplaceNode(oldNode, newNode);
                                xmlAddChild(newNode, oldNode);
                                //Insert right containing value
                                if (divide) {
                                    nodeValue = 1 / nodeValue;
                                }
                                S_PRINTF(newStr, 25, "%.16g", -nodeValue);	//Allows up to 999 in exponent
                                xmlNewTextChild(newNode->parent, NULL, (const xmlChar*)"right", (const xmlChar*)newStr);
                                break;
                            }
                            
                            if (!(xmlStrEqual(xmlText, (const xmlChar*)"*") || xmlStrEqual(xmlText, (const xmlChar*)"/"))) {
                                //Found a function that's not multiply or divide -> have left subtree
                                xmlFree(xmlText);
                                //If have hit a binfunc, old node is <left> or <right> so need to go down one before inserting
                                if (xmlStrEqual(newNode->name, (const xmlChar*)"binfunc")) {
                                    oldNode = xmlFirstElementChild(oldNode);
                                }
                                //Insert or merge multiplication by a constant
                                if (insertConstantMultiply(xmlDocument, oldNode, nodeValue, divide)) {
                                    xmlXPathFreeContext(xpathCtx);
                                    return -4;
                                }
                                break;
                            }
                            
                            if (xmlStrEqual(xmlText, (const xmlChar*)"/")) {
                                if (xmlStrEqual(oldNode->name, (const xmlChar*)"right")) {
                                    //Have come from <right>
                                    divide ^= 1;    //XOR changes divide between 0 and 1
                                }
                            }
                            xmlFree(xmlText);
                        }
                        //Look higher up tree
                        oldNode = newNode;
                        newNode = newNode->parent;
                    }
                }
            }
        } while (numNodes > 0); //May have created a new option not picked up by xPath
        
        //Simplifying additions and subtractions may result in further unary minus removal opportunities
        change_occ = 1;
        while (change_occ) {
            change_occ = 0;
            
            //Remove double unary minus - may be some not picked up by multiplication/division subtrees
            do {
                xpathCtx->node = funcNode;
                xpathObj = xmlXPathEvalExpression((const xmlChar*)".//unifunc[@name=\"minus\"]/unifunc[@name=\"minus\"]/*", xpathCtx);
                if (xpathObj == NULL) {
                    fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                    xmlXPathFreeContext(xpathCtx);
                    return 3;
                }
                xmlNodeList = xpathObj->nodesetval;
                numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
                newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
                xmlXPathFreeObject(xpathObj);
                
                if (numNodes) {
                    change_occ = 1; //Log change
                    oldNode = newNode->parent->parent;  //Taking out 2 unifuncs
                    xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                }
            } while (numNodes > 1); //Says could be another one
            
            //Remove unary minus underneath an addition
            do {
                xpathCtx->node = funcNode;
                xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"+\"]/*/unifunc[@name=\"minus\"]/*", xpathCtx);
                if (xpathObj == NULL) {
                    fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                    xmlXPathFreeContext(xpathCtx);
                    return 3;
                }
                xmlNodeList = xpathObj->nodesetval;
                numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
                newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
                xmlXPathFreeObject(xpathObj);
                
                if (numNodes) {
                    change_occ = 1; //Log change
                    oldNode = newNode->parent;  //<unifunc minus>
                    xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                    //If newNode is under <left>, need to move it to the right
                    newNode = newNode->parent;  //<left> or <right>
                    if (xmlStrEqual(newNode->name, (const xmlChar*)"left")) {
                        swapLeftRightNodes(newNode);
                    }
                    //Rename binfunc to -
                    xmlSetProp(newNode->parent, (const xmlChar*)"name", (const xmlChar*)"-");
                }
            } while (numNodes > 1); //Says could be another one
            
            //Remove unary minus underneath a subtraction, but can only do it on right side
            do {
                xpathCtx->node = funcNode;
                xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"-\"]/right/unifunc[@name=\"minus\"]/*", xpathCtx);
                if (xpathObj == NULL) {
                    fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                    xmlXPathFreeContext(xpathCtx);
                    return 3;
                }
                xmlNodeList = xpathObj->nodesetval;
                numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
                newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
                xmlXPathFreeObject(xpathObj);
                
                if (numNodes) {
                    change_occ = 1; //Log change
                    oldNode = newNode->parent;
                    xmlFreeNode(xmlReplaceNode(oldNode, newNode));
                    //Rename binfunc, which is above right
                    xmlSetProp(newNode->parent->parent, (const xmlChar*)"name", (const xmlChar*)"+");
                }
            } while (numNodes > 1); //Says could be another one
            
            //Collate constants in a subtree of additions and subtractions - includes dealing with identities
            do {
                xpathCtx->node = funcNode;
                //Select all left and right text-only nodes of additions, except right addition with no A/S parent (top of subtree) but do select when adding 0 (identity for removal), and also select right text-only node on subtraction
				//Other potential optimisations involving subtractions can result in infinite loops
                xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"+\"]/left[not(*)] | .//binfunc[@name=\"+\"]/right[.=0] | .//binfunc[@name=\"+\" or @name=\"-\"]/*/binfunc[@name=\"+\"]/right[not(*)] | .//binfunc[@name=\"-\"]/right[not(*)]", xpathCtx);
                if (xpathObj == NULL) {
                    fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
                    xmlXPathFreeContext(xpathCtx);
                    return 3;
                }
                xmlNodeList = xpathObj->nodesetval;
                numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
                newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
                xmlXPathFreeObject(xpathObj);
                
                if (numNodes) {
                    double nodeValue;
                    int subtract = 0; //Boolean representing whether to add or subtract by nodeValue
                    
                    change_occ = 1; //Log change
                    
                    //Get value as a number
                    xmlText = xmlNodeListGetString(xmlDocument, newNode->children, 1);
                    nodeValue = atof((const char*)xmlText);
                    xmlFree(xmlText);
                    
                    //Select contents of other side of binary operator
                    if (xmlStrEqual(newNode->name, (const xmlChar*)"left")) {
                        newNode = xmlFirstElementChild(xmlNextElementSibling(newNode)); //Contents of <right>
                    } else {
                        newNode = xmlFirstElementChild(xmlPreviousElementSibling(newNode)); //Contents of <left>
                    }
                    //Remove binfunc node
                    oldNode = newNode->parent->parent;  //<binfunc>
                    xmlText = xmlGetProp(oldNode, (const xmlChar*)"name");  //Get operator
                    xmlFreeNode(xmlReplaceNode(oldNode, newNode));	//Replace binfunc node with the contents of the other side and delete the binfunc node from memory
                    if (xmlStrEqual(xmlText, (const xmlChar*)"-")) {
                        //Subtraction taking place
						//Replacing f - const by f with additive constant of (-const) promoted to the top of the tree
						subtract = 1;
					}
                    xmlFree(xmlText);
                    
                    if (nodeValue != 0.0) {
                        //Not identity
                        //Traverse up tree to top - will only come across a unary minus at the top
                        oldNode = newNode;  //On contents of other half of addition/subtraction
                        newNode = oldNode->parent;
                        for (;;) {
                            if (newNode == funcNode) {
                                //Reached the top of the tree
                                //Insert or merge addition by a constant
                                insertConstantAdd(xmlDocument, oldNode, nodeValue, subtract);
                                break;
                            }
                            xmlText = xmlGetProp(newNode, (const xmlChar*)"name");  //NULL if not found
                            if (xmlText) {
                                //Name attribute found
                                if (!(xmlStrEqual(xmlText, (const xmlChar*)"+") || xmlStrEqual(xmlText, (const xmlChar*)"-"))) {
                                    //Found a function that's not add or subtract -> have left subtree
                                    xmlFree(xmlText);
                                    //If have hit a binfunc, old node is <left> or <right> so need to go down one before inserting
                                    if (xmlStrEqual(newNode->name, (const xmlChar*)"binfunc")) {
                                        oldNode = xmlFirstElementChild(oldNode);
                                    }
                                    //Insert or merge addition by a constant
                                    insertConstantAdd(xmlDocument, oldNode, nodeValue, subtract);
                                    break;
                                }
                                
                                if (xmlStrEqual(xmlText, (const xmlChar*)"-")) {
                                    if (xmlStrEqual(oldNode->name, (const xmlChar*)"right")) {
                                        //Constant has come from <right>
                                        subtract ^= 1;    //XOR changes subtract between 0 and 1
                                    }
                                }
                                xmlFree(xmlText);
                            }
                            //Look higher up tree
                            oldNode = newNode;
                            newNode = newNode->parent;
                        }
                    }
                }
            } while (numNodes > 0); //May have created a new option not picked up by xPath
            if (change_occ) {
                outerChangeOcc = 1;
            }
        }
    }
    
    //Put heaviside step functions on left side of multiplications, as more likely to be zero and calcalgfunc() contains an optimisation for string of zeroes on left. Prioritise child H then all descendants
    do {
        xpathCtx->node = funcNode;
        //Select left nodes satisfying condition requiring change
        xpathObj = xmlXPathEvalExpression((const xmlChar*)".//binfunc[@name=\"*\" and right/unifunc[@name=\"H\"]]/left[not(left/unifunc[@name=\"H\"])] | .//binfunc[@name=\"*\" and right//unifunc[@name=\"H\"]]/left[not(left//unifunc[@name=\"H\"])]", xpathCtx);
        if (xpathObj == NULL) {
            fprintf(stderr,"WARNING: Unable to evaluate XPath expression. Function evaluation not optimised.\n");
            xmlXPathFreeContext(xpathCtx);
            return 3;
        }
        xmlNodeList = xpathObj->nodesetval;
        numNodes = xmlXPathNodeSetGetLength(xmlNodeList);
        newNode = xmlXPathNodeSetItem(xmlNodeList, 0);  //Returns NULL if doesn't exist
        xmlXPathFreeObject(xpathObj);
        
        if (numNodes) {
            swapLeftRightNodes(newNode);    //newNode is selected <left>
        }
    } while (numNodes > 1); //Says could be another one
    
    //Need to free this
    xmlXPathFreeContext(xpathCtx);
    
    return 0;
}

//For multithreading
struct calc_motor_steps_params {
    //Contains all input arguments to calc_motor_steps()
    double global_end_time;
    double global_step_size;
    double resolution;
    double xPos;
    double travelPerStep;
    xmlDocPtr xmlTree;
    xmlNodePtr motorNode;
    FILE** motor_fp;
    FILE** interval_fp;
    int motor_id;
    int BBBid;
    int* initialMotorSteps;
    double *min_s;
    double *max_s;
    int* motorthreads;
    pthread_mutex_t* ThreadCounterMutex;
    pthread_cond_t* ThreadCounterCondition;
    int *stopSig;
};

void* calc_motor_steps_unpack(void* params) {
    //For use in pthreads to call calc_motor_steps()
    struct calc_motor_steps_params *params_struct = (struct calc_motor_steps_params*) params;
    int* rtn = (int*) malloc(sizeof(int));
    *rtn = calc_motor_steps(params_struct->global_end_time, params_struct->global_step_size, params_struct->resolution, params_struct->xPos, params_struct->travelPerStep, params_struct->xmlTree, params_struct->motorNode, params_struct->motor_fp, params_struct->interval_fp, params_struct->motor_id, params_struct->BBBid, params_struct->initialMotorSteps, params_struct->min_s, params_struct->max_s, params_struct->motorthreads, params_struct->ThreadCounterMutex, params_struct->ThreadCounterCondition, params_struct->stopSig);
    return rtn;
}

int calc_motor_steps(const double global_end_time, const double global_step_size, const double resolution, const double xPos, const double travelPerStep, const xmlDocPtr xmlTree, const xmlNodePtr motorNode, FILE** motor_fp, FILE** interval_fp, const int motor_id, const int BBBid, int* initialMotorSteps, double *min_s, double *max_s, int* motorthreads, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition, int *stopSig)
//Designed to be called inside a thread for each motor
/* global_end_time is value of end attribute on <wave> - the maximum duration of the waveform
 global_step_size is value of initialSplit attribute - the length of each subinterval assumed to be unimodal
 resolution is the value of resolution attribute - the desired effective tick duration of clock
 xPos is the x position of the motor
 travelPerStep is the distance moved per step of motor
 xmlTree is the transformed XML input document
 motorNode is a pointer to the XML node for the current motor
 motor_fp is a file pointer to where the step times should be saved
 interval_fp is a file pointer to where the step times for a periodic repeating interval are to be saved before writing to motor_fp
 motor_id is the number of the current motor
 BBBid is the number of the current BBB
 initialMotorSteps is initial displacement of motor
 min_s is minimum displacement found
 max_s is maximum displacement found
 motorthreads is the number of threads of calc_motor_steps open across whole program
 ThreadCounterMutex is the mutex lock for motorthreads
 ThreadCounterCondition is the signal condition waited for when new thread is not permitted to open
 stopSig is a flag used to stop the compiler in the event of an error in one thread
 */

{
    xmlNodePtr xmlInterval = xmlFirstElementChild(motorNode);		//<interval>
    xmlChar* xmlText;
    
    int previous_s_steps = INT_MAX;
    long long time_index = 0;                 //Absolute time, counting from 0
    double relative_end_t = 0;
    
    const double* xPos_dptr[1] = {&xPos};
    const char* xvarList[1] = {"x"};
    double* t_interval = NULL;
    int err_occ = 0;
    
    while (xmlInterval)
    {
        double abs_t0;
        xmlNodePtr funcNode, periodNode;
        
        printf("Calculating times to step motor %d on BBB %d for interval from time %.1f s...\n", motor_id, BBBid, time_index * TICK_DURATION);
        
        //Read and process attributes: start and end
        t_interval = (double*) malloc(sizeof(double));
        if (t_interval == NULL)
        {
            perror("Out of memory: increase initialSplit or subdivide into smaller intervals");
            err_occ = 1;
            break;
        }
        
        funcNode = xmlFirstElementChild(xmlInterval);   //<start>
        if (funcNode->children) {
            if (calcalgfunc(t_interval, 1, xPos_dptr, xvarList, 1, xmlTree, &funcNode)) {
                err_occ = 1;
                break;
            }
        }
        else { t_interval[0] = relative_end_t; }						//Default to same time as previous interval
        abs_t0 = (time_index * TICK_DURATION) - t_interval[0];      //Time offset in seconds between absolute time and interval time
        
        funcNode = xmlNextElementSibling(funcNode); //<end>
        if (funcNode->children) {
            if (calcalgfunc(&relative_end_t, 1, xPos_dptr, xvarList, 1, xmlTree, &funcNode)) {
                err_occ = 1;
                break;
            }
            if (relative_end_t + abs_t0 > global_end_time) { relative_end_t = global_end_time - abs_t0; }
        }
        else { relative_end_t = global_end_time - abs_t0; }
        
        periodNode = xmlNextElementSibling(funcNode);   //<period> - non-empty
        funcNode = xmlNextElementSibling(periodNode);   //<func>
		        
        //Find all instances of var x and replace with constant value, and evaluate it if independent of t
        if (optimiseFuncTree(xmlTree, funcNode, xPos) < 0) {
            //Failed to substitute all x
            err_occ = 1;
            break;
        }
		
        if (xmlChildElementCount(funcNode))
            //Speed up code by accounting for constant function. This case is not constant.
        {
            double int_period, periodic_end_t, *values;
            long long interval_points, time_ticks, *time_ptr;
            int k, rtn, steps_count, *steps_ptr;
            FILE* steps_fp;
            
            //Get interval period, if applicable
            if (calcalgfunc(&int_period, 1, xPos_dptr, xvarList, 1, xmlTree, &periodNode)) {
                err_occ = 1;
                break;
            }
            
            periodic_end_t = relative_end_t;
            if (int_period >= TICK_DURATION) {   //A period < tick duration is meaningless -> assume non-periodic
                periodic_end_t = t_interval[0] + int_period;
                if (periodic_end_t > relative_end_t) {
                    periodic_end_t = relative_end_t;
                }
            }
            
            interval_points = (long long)ceil((periodic_end_t - t_interval[0]) / global_step_size) + 1;
            t_interval = (double*) realloc(t_interval, (size_t)(sizeof(double) * interval_points));
            values = (double*) malloc((size_t)(sizeof(double) * interval_points));
            if (t_interval == NULL || values == NULL)
            {
                perror("Out of memory: increase initialSplit or subdivide into smaller intervals");
                err_occ = 1;
                free(values);
                break;
            }
            
            for (k = 1; k < interval_points - 1; k++) { t_interval[k] = t_interval[0] + k * global_step_size; }
            t_interval[interval_points - 1] = periodic_end_t;
            
            rtn = calcalgfunc(values, (const int)interval_points, (const double**)&t_interval, (const char**)indep_variables, INDEP_VARS, xmlTree, &funcNode);
            
            if (rtn)
            {
                fprintf(stderr, "Error in function string in input file.\n");
                err_occ = 1;
                free(values);
                break;
            }
            
            //Check for uninitialised step counter
            if (previous_s_steps == INT_MAX) {
                previous_s_steps = (int)roundll(values[0] / travelPerStep);
                *initialMotorSteps = previous_s_steps;
                *min_s = previous_s_steps * travelPerStep;
                *max_s = *min_s;
            }
            
            time_ticks = time_index;
            steps_count = previous_s_steps;
            if (periodic_end_t < relative_end_t) {
                //Periodic behaviour
                steps_fp = *interval_fp;
                time_ptr = &time_ticks;
                steps_ptr = &steps_count;
            } else {
                steps_fp = *motor_fp;
                time_ptr = &time_index;
                steps_ptr = &previous_s_steps;
            }
            
            for (k = 1; k < interval_points; k++)
            {
                //Search for a minimum first - somewhat arbitrary vs maximum
                rtn = split_interval(1, t_interval[k - 1], t_interval[k], values[k - 1], values[k], min_s, max_s, time_ptr, steps_ptr, resolution, travelPerStep, xmlTree, &funcNode, &steps_fp, motor_id, BBBid, stopSig);
                if (rtn) {
                    //Error has occurred
                    err_occ = rtn;
                    break;      //Breaks only out of for loop
                }
            }
            free(values);
            if (err_occ) {
                //Required as break above will only get out of for loop
                break;
            }
            
            if (periodic_end_t < relative_end_t) {
                //Periodic behaviour
                long long relative_end_t_ticks, ticks_per_period, periodoffset_ticks, step_instr;
                signed char step_dir;   //char alone depends on the compiler as to whether unsigned
                
                //Check for continuity
                if (previous_s_steps != steps_count) {
                    fprintf(stderr, "ERROR: Interval is not continuous on motor %d on BBB %d at end of period beginning at time %f.\nInterval starts at %f and is at %f at end of period, which is a difference of %d steps.\n", motor_id, BBBid, time_index * TICK_DURATION, previous_s_steps * travelPerStep, steps_count * travelPerStep, abs(previous_s_steps - steps_count));
                    err_occ = ERR_PERIOD;
                    break;
                }
                
                rewind(steps_fp); //Set FP to start of file
                
                //Copy step times provided neither interval end nor wave end have been reached
                relative_end_t_ticks = (long long)((relative_end_t + abs_t0) / TICK_DURATION); //Interval end, rounds down
                ticks_per_period = roundll(int_period / TICK_DURATION);
                periodoffset_ticks = 0;
                
                for (;;) {
                    if (fread(&step_instr, sizeof step_instr, 1, steps_fp) < 1) {
                        //EOF - go back to start
                        periodoffset_ticks += ticks_per_period;
                        rewind(steps_fp); //Set FP to start of file
                        if (fread(&step_instr, sizeof step_instr, 1, steps_fp) < 1) {
                            //No steps required (assuming file not corrupt)
                            break;
                        }
                    }
                    if (step_instr > 0) {
                        step_dir = 1;
                        time_ticks = periodoffset_ticks + step_instr;
                    } else {
                        step_dir = -1;
                        time_ticks = periodoffset_ticks - step_instr;
                    }
                    
                    //Check neither interval end nor wave end have been reached - included in relative_end_t
                    if (time_ticks > relative_end_t_ticks) {
                        //End reached
                        break;
                    }
                    
                    previous_s_steps += step_dir;
                    time_ticks *= step_dir; //Make instruction with direction
                    if (fwrite(&time_ticks, sizeof time_ticks, 1, *motor_fp) < 1) {
                        perror("ERROR: Could not write to temporary file");
                        err_occ = ERR_DRIVE;
                        break;
                    }
                }
                
                //Update time_index to end of interval
                time_index = relative_end_t_ticks;
            }
        }
        else
        {
            //Constant or blank interval. Blank interval means do nothing.
            if (funcNode->children) {
                //Not a blank interval
                double constvalue;
                xmlText = xmlNodeListGetString(xmlTree, funcNode->children, 1);
                constvalue = atof((const char*)xmlText);
                xmlFree(xmlText);
                
                //Check for uninitialised step counter
                if (previous_s_steps == INT_MAX) {
                    previous_s_steps = (int)roundll(constvalue / travelPerStep);
                    *initialMotorSteps = previous_s_steps;
                    *min_s = previous_s_steps * travelPerStep;
                    *max_s = *min_s;
                } else {
                    int rtn = start_interval(time_index, constvalue, &previous_s_steps, travelPerStep, motor_fp, motor_id, BBBid);
                    if (rtn) {
                        //Error has occurred
                        err_occ = rtn;
                        break;
                    }
                }
            }
            time_index += roundll((relative_end_t - t_interval[0]) / TICK_DURATION);    //Update absolute time
        }
        
        free(t_interval);
        xmlInterval = xmlNextElementSibling(xmlInterval);
    }	//Loop whilst there is still another interval to process
    
    if (!err_occ) {
        printf("Step points for motor %d on BBB %d calculated.\n", motor_id, BBBid);
    } else {
        free(t_interval);
        //Flag that all threads should terminate now
        if (*stopSig == 0) { *stopSig = 1; }
    }
    
    fclose(*interval_fp);
    rewind(*motor_fp); //Set FP to start of file
    
    //Motor thread closing -> release
    pthread_mutex_lock(ThreadCounterMutex);
    *motorthreads -= 1;
    pthread_cond_broadcast(ThreadCounterCondition);
    pthread_mutex_unlock(ThreadCounterMutex);
    
    return err_occ;
}

void assemble_instruction(uint8_t instr[], const unsigned char gpio, const int set, const unsigned timeDif) {
    //Assembles a 3B output file instruction
    instr[0] = gpio;
    if (set) { instr[0] |= SC_BIT; }
    
    //Little endian, so least significant bit first
    instr[1] = (uint8_t)timeDif;	//Overflows, so gets modulo 256
    instr[2] = (uint8_t)(timeDif >> 8);
}

void assemble_valiation(uint8_t instr[], const unsigned char timeDif, const unsigned char motor_id, const unsigned char dispBin) {
    //Assembles a 2B validation instruction
    //Bits 0-3 are the time difference, bits 4-8 are the motor number, bits 9-15 give the displacement bin
    instr[0] = timeDif | (motor_id << 4);
    instr[1] = (motor_id >> 4) | (dispBin << 1);
}

int make_binary(double* wavetime, long int* instructions, const char *xmlFile_addr, const char *binFile_addr, const int BBBid, int* BBBthreads, int* motorthreads, const FLIM_TYPE filelimit, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition, int *stopSig)
//wavetime is the time length of compiled wave - for performance benchmarking/error checking
{
    //Generic counters and returns
    int k, rtn;
    
    struct stat folderstat;
    xmlDocPtr xmlRawDoc, schema_doc, xmlTree;
    xmlNodePtr xmlNode;
    xmlChar * xmlText;
    const char schema_path[20] = "ASWaMWaveSchema.xsd";
    xmlSchemaParserCtxtPtr parser_ctxt;
    xmlSchemaPtr schema;
    xmlSchemaValidCtxtPtr valid_ctxt;
    int validationResult, fallingEdge, pulse_min_half_period, numMotors, expansion_header, expansion_pin, motor_id;
    xsltStylesheetPtr stylesheet_doc;
    xsltTransformContextPtr ctxt;
    FILE *out_fp = NULL;
    FILE **motor_fp, **interval_fp, *gpiomap;
    double global_step_size, global_end_time, resolution, xPos, maxStepRate;
    double *travelPerStep;  //Varies per motor
    unsigned char *gpio_pulse, *gpio_direction, err_occ;
    signed char gpiomap_motor[128], gpiomap_directionpin[128];    //directionpin is 0 for pulse, 1 for direction pin, -1 uninitialised - used in verification
    char file_path[43];
    pthread_t *interval_thread;
    pthread_attr_t thread_attr;
    int *intervalthread_rtn;
    struct calc_motor_steps_params *calc_params;
    long long *start_set, *end_set, *next_clear;
    signed char *cur_direction, *next_direction, *switch_direction;
    
    //These variable names refer to a falling edge driver board
    int next_clear_motor, first_start_set_motor, first_end_set_motor;
    long long motor_instr, next_clear_t, first_start_set, first_end_set, time_index;
    uint8_t instr[4], delay_instr[BYTES_PER_INSTR];	//Use array of 8-bit unsigned integers to enforce little endian
    
    //Validation
    int *motor_steps;
    unsigned char *dispBin;
    int **stepBinMax;    //Highest step number in each bin, DISP_STEPS bins for displacement
    long skipBytes, numInstructions;
    double *min_s, *max_s, global_min_s, global_max_s, min_s_lim, max_s_lim;    //Limits are user-specified
    long long lastWriteTime, newWriteTime;    //Time of last write in animation ticks
    double fraction;    //For extracting fractional part of floating point
    int exponent;
    
    //Set up temporary folder
    char tempfoldername[23] = "tmp";
    if (stat(tempfoldername, &folderstat) == -1) {
        if (MKDIR(tempfoldername)) {
            perror("Failed to create temporary folder");
            cleanandclose(NULL, NULL, NULL, NULL, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
            return 2;
        }
    }
    S_PRINTF(tempfoldername, 23, "tmp/compiler%d", BBBid);
    if (stat(tempfoldername, &folderstat) == -1) {
        if (MKDIR(tempfoldername)) {
            perror("Failed to create temporary folder");
            cleanandclose(NULL, NULL, NULL, NULL, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
            return 2;
        }
    }
    
    //Open and read XML input
    xmlRawDoc = xmlParseFile(xmlFile_addr);
    if (xmlRawDoc == NULL)
    {
        fprintf(stderr, "XML input file parse unsuccessful.\n");
        cleanandclose(NULL, NULL, NULL, NULL, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return EXIT_FAILURE;
    }
    
    //Validate XML doc
    schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
    parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
    schema = xmlSchemaParse(parser_ctxt);
    valid_ctxt = xmlSchemaNewValidCtxt(schema);
    validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
    if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
    if (schema) xmlSchemaFree(schema);
    if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
    if (schema_doc) xmlFreeDoc(schema_doc);
    if (validationResult != 0)
    {
        fprintf(stderr, "Incorrect syntax in XML input file.\n");
        //XML Cleanup
        xmlFreeDoc(xmlRawDoc);
        cleanandclose(NULL, NULL, NULL, NULL, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return(EXIT_FAILURE);
    }
    
    //Transform XML using XSLT
    stylesheet_doc = xsltParseStylesheetFile((const xmlChar*) "ASWaMWaveStylesheet.xsl");
    ctxt = xsltNewTransformContext(stylesheet_doc, xmlRawDoc);
    xmlTree = xsltApplyStylesheetUser(stylesheet_doc, xmlRawDoc, NULL, NULL, NULL, ctxt);
    
    //Free XML done with
    xsltFreeStylesheet(stylesheet_doc);
    xmlFreeDoc(xmlRawDoc);
    
    if ((ctxt->state == XSLT_STATE_ERROR) || (ctxt->state == XSLT_STATE_STOPPED))
    {
        fprintf(stderr, "Error in applying XSLT stylesheet.\n");
        //XML Cleanup
        xsltFreeTransformContext(ctxt);
        xmlFreeDoc(xmlTree);
        cleanandclose(NULL, NULL, NULL, NULL, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return EXIT_FAILURE;
    }
    xsltFreeTransformContext(ctxt);
    
    xmlNode = xmlDocGetRootElement(xmlTree);		//<wave>
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"initialSplit");
    global_step_size = atof((const char*)xmlText);
    xmlFree(xmlText);
    
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"end");
    global_end_time = atof((const char*)xmlText);
    xmlFree(xmlText);
    
    //Set resolution, which is the desired effective tick duration - if set greater than minimum, wave maker response may become jittery
    xmlText = xmlGetProp(xmlNode, (const xmlChar*) "resolution");
    resolution = atof((const char*) xmlText);
    xmlFree(xmlText);
    if (resolution < TICK_DURATION) {
        //Minimum useful value of resolution is one tick
        resolution = TICK_DURATION;
    }
    
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"fallingEdge");
    fallingEdge = atoi((const char*)xmlText);
    xmlFree(xmlText);
    
    xmlText = xmlGetProp(xmlNode, (const xmlChar*) "motor0xPos");
    xPos = atof((const char*) xmlText);
    xmlFree(xmlText);
    
    //Set minimum gap between steps - triggers warning if exceeded
    xmlText = xmlGetProp(xmlNode, (const xmlChar*) "maxStepRate");
    maxStepRate = atof((const char*) xmlText);
    xmlFree(xmlText);
    pulse_min_half_period = (int) ceil(0.5 / maxStepRate / TICK_DURATION);
    
    numMotors = xmlChildElementCount(xmlNode); //Number of motors
    
    /*Calculate the absolute 64-bit times of when to step each motor using brent.h:
     1. Numerically differentiate using global_step_size
     2. Find extrema using local_min wherever gradient is zero (use single interval) or changes sign (use double interval)
     3. Find step times (roots) using zero_rc with tolerance half the clock tick*/
    
    gpio_pulse = (unsigned char*) malloc(numMotors * sizeof(unsigned char));
    gpio_direction = (unsigned char*) malloc(numMotors * sizeof(unsigned char));
    motor_id = 0;
    
    //Setup pthreads
    interval_thread = (pthread_t*) malloc(numMotors * sizeof(pthread_t));
    pthread_attr_init(&thread_attr);
    pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);    //Make threads joinable
    intervalthread_rtn = (int*) malloc(numMotors * sizeof(int));
    calc_params = (struct calc_motor_steps_params*)malloc(numMotors * sizeof(struct calc_motor_steps_params));
    
    motor_fp = (FILE**) malloc(numMotors * sizeof(FILE*));		//Use double pointers in the subroutines to write to files, since fp is changed on writing. Need to keep track of current position.
    interval_fp = (FILE**) malloc(numMotors * sizeof(FILE*));  //Contains step points calculated for an interval, which will be copied into motor_fp. Have one for each motor to enable parallelisation by motor.
    
    //For validation - need to store starting position of each motor
    travelPerStep = (double*) malloc(numMotors * sizeof *travelPerStep);
    motor_steps = (int*) malloc(numMotors * sizeof *motor_steps);
    min_s = (double*) malloc(numMotors * sizeof *min_s);
    max_s = (double*) malloc(numMotors * sizeof *max_s);
    
    //Initialisation
    for (k = 0; k < numMotors; k++) {
        //Need initialising as program may use NULL pointers later
        motor_fp[k] = NULL;
        interval_fp[k] = NULL;
        
        motor_steps[k] = 0; //Default value for initial motor position
        //Default to HUGE_VAL in the event of blank interval - need to check for it later
        min_s[k] = HUGE_VAL;
        max_s[k] = HUGE_VAL;
    }
    
    //Open GPIO map file
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"gpioMapFile");
    F_OPEN(gpiomap, (const char*)xmlText, "r");
    xmlFree(xmlText);
    if (!gpiomap) {
        perror("ERROR: Could not open GPIO map file");
        xmlFreeDoc(xmlTree);
        cleanandclose(NULL, NULL, NULL, NULL, travelPerStep, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return EXIT_FAILURE;
    }
    
    //Initialise gpio_motor_map array
    for (k = 0; k < 128; k++) {
        gpiomap_motor[k] = -1;
        gpiomap_directionpin[k] = -1;
    }
    
    xmlNode = xmlFirstElementChild(xmlNode);		//<motor>
    err_occ = 0;
    while (xmlNode && err_occ == 0)
    {
        //Open temporary files
        S_PRINTF(file_path, 43, "%s/motor%d.tmp", tempfoldername, motor_id)
        F_OPEN(motor_fp[motor_id], file_path, "w+b");
        if (!motor_fp[motor_id]) {
            perror("ERROR: Could not open temporary motor file");
            err_occ = 1;
            break;
        }
        
        S_PRINTF(file_path, 43, "%s/interval%d.tmp", tempfoldername, motor_id)
        F_OPEN(interval_fp[motor_id], file_path, "w+b");
        if (!interval_fp[motor_id]) {
            perror("ERROR: Could not open temporary interval file");
            err_occ = 1;
            break;
        }
        
        if (motor_id > 0) {
            //Increase motorxPos by spacing
            double spacing;
            xmlText = xmlGetProp(xmlNode, (const xmlChar*)"spacing");
            spacing = atof((const char*)xmlText);
            xmlFree(xmlText);
            xPos += spacing;
        }
        
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"pulseHeader");
        expansion_header = atoi((const char*)xmlText);
        xmlFree(xmlText);
        
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"pulsePin");
        expansion_pin = atoi((const char*)xmlText);
        xmlFree(xmlText);
        
        gpio_pulse[motor_id] = expansion_to_gpio(expansion_header, expansion_pin, gpiomap);
        if (gpio_pulse[motor_id] == 0) {
            fprintf(stderr, "Pulse pin on motor %d on BBB %d not found.\n", motor_id, BBBid);
            err_occ = 1;
            break;
        }
        gpiomap_motor[gpio_pulse[motor_id]] = motor_id;
        gpiomap_directionpin[gpio_pulse[motor_id]] = 0;
        
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"directionHeader");
        expansion_header = atoi((const char*)xmlText);
        xmlFree(xmlText);
        
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"directionPin");
        expansion_pin = atoi((const char*)xmlText);
        xmlFree(xmlText);
        
        gpio_direction[motor_id] = expansion_to_gpio(expansion_header, expansion_pin, gpiomap);
        if (gpio_direction[motor_id] == 0) {
            fprintf(stderr, "Direction pin on motor %d on BBB %d not found.\n", motor_id, BBBid);
            err_occ = 1;
            break;
        }
        gpiomap_motor[gpio_direction[motor_id]] = motor_id;
        gpiomap_directionpin[gpio_direction[motor_id]] = 1;
        
        //Get linear distance travelled per step of motor
        xmlText = xmlGetProp(xmlNode, (const xmlChar*)"travelPerStep");
        travelPerStep[motor_id] = atof((const char*)xmlText);
        xmlFree(xmlText);
        
        //Calculate step points for each interval - multithreaded by motor
        calc_params[motor_id].global_end_time = global_end_time;
        calc_params[motor_id].global_step_size = global_step_size;
        calc_params[motor_id].resolution = resolution;
        calc_params[motor_id].xPos = xPos;
        calc_params[motor_id].travelPerStep = travelPerStep[motor_id];
        calc_params[motor_id].xmlTree = xmlTree;
        calc_params[motor_id].motorNode = xmlNode;
        calc_params[motor_id].motor_fp = &motor_fp[motor_id];
        calc_params[motor_id].interval_fp = &interval_fp[motor_id];
        calc_params[motor_id].motor_id = motor_id;
        calc_params[motor_id].BBBid = BBBid;
        calc_params[motor_id].initialMotorSteps = &motor_steps[motor_id];
        calc_params[motor_id].min_s = &min_s[motor_id];
        calc_params[motor_id].max_s = &max_s[motor_id];
        calc_params[motor_id].motorthreads = motorthreads;
        calc_params[motor_id].ThreadCounterMutex = ThreadCounterMutex;
        calc_params[motor_id].ThreadCounterCondition = ThreadCounterCondition;
        calc_params[motor_id].stopSig = stopSig;
        
        pthread_mutex_lock(ThreadCounterMutex);
        while (*BBBthreads * (MAX_MOTORS + 3) + (*motorthreads + 1) > filelimit) {
            //Open files: process 4 - stdout, stdin, stderr, filelock -, BBB 1 + MAX_MOTORS, motor thread 1 - interval
            //Add 1 onto motorthreads to show what would be required once new thread is created
            //Filelimit already has 4 process files removed
            //Not enough file quota available -> wait - releases mutex
            pthread_cond_wait(ThreadCounterCondition, ThreadCounterMutex);
        }
        *motorthreads += 1;
        pthread_mutex_unlock(ThreadCounterMutex);
        
        if ((intervalthread_rtn[motor_id] = pthread_create(&interval_thread[motor_id], &thread_attr, calc_motor_steps_unpack, (void*) &calc_params[motor_id]))) {
            fprintf(stderr, "Failed to create interval thread for motor %d on BBB %d; will calculate in main thread.\n", motor_id, BBBid);
            err_occ = calc_motor_steps(global_end_time, global_step_size, resolution, xPos, travelPerStep[motor_id], xmlTree, xmlNode, &motor_fp[motor_id], &interval_fp[motor_id], motor_id, BBBid, &motor_steps[motor_id], &min_s[motor_id], &max_s[motor_id], motorthreads, ThreadCounterMutex, ThreadCounterCondition, stopSig);
        }
        
        motor_id++;
        xmlNode = xmlNextElementSibling(xmlNode);
    }	//Loop if there is another motor to process
    
    //Join threads; err_occ = 0 set above
    for (k = 0; k < numMotors; k++) {
        if (intervalthread_rtn[k] == 0) {
            void* val;
            if (!pthread_join(interval_thread[k], &val)) {
                int* intptr = (int*) val;
                if (*intptr == ERR_STOPSIG) {
                    //Do not overwrite another genuine error code: this one indicates a stop was requested
                    if (err_occ == 0) {
                        err_occ = ERR_STOPSIG;
                    }
                } else if (*intptr) {
                    err_occ = *intptr;
                }
                free(val);
            }
        }
    }
    //err_occ now contains 0 if ok, the code if an error occurred and ERR_STOPSIG if a stop was requested by another BBB
    
    //Cleanup threads
    fclose(gpiomap);
    free(interval_thread);
    free(intervalthread_rtn);
    free(interval_fp);
    free(calc_params);
    pthread_attr_destroy(&thread_attr);
    
    //Dump optimised XML file
    S_PRINTF(file_path, 43, "%s/optimised.xml", tempfoldername)
    xmlSaveFile(file_path, xmlTree);
    
    numMotors = motor_id;
    
    //Recover displacement extrema - initialise to initial displacment of motor 0, which is itself initialised (defaults) to 0
    global_min_s = motor_steps[0] * travelPerStep[0];
    global_max_s = motor_steps[0] * travelPerStep[0];
    for (motor_id = 0; motor_id < numMotors; motor_id++) {
        //HUGE_VAL is a flag for uninitialised
        if (min_s[motor_id] < global_min_s && min_s[motor_id] < HUGE_VAL) global_min_s = min_s[motor_id];
        if (max_s[motor_id] > global_max_s && min_s[motor_id] < HUGE_VAL) global_max_s = max_s[motor_id];
    }
    free(min_s);
    free(max_s);
    
    //Check for out of range
    xmlNode = xmlDocGetRootElement(xmlTree);
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"minDisplacement");
    min_s_lim = atof((const char*)xmlText);
    xmlFree(xmlText);
    
    xmlText = xmlGetProp(xmlNode, (const xmlChar*)"maxDisplacement");
    max_s_lim = atof((const char*)xmlText);
    xmlFree(xmlText);
    
    //Free XML as no longer required
    xmlFreeDoc(xmlTree);
    
    if (global_min_s < min_s_lim) {
        fprintf(stderr, "ERROR: Minimum calculated displacement of %f is less than the specified limit of %f on BBB%d.\n", global_min_s, min_s_lim, BBBid);
        err_occ = 1;
    }
    if (global_max_s > max_s_lim) {
        fprintf(stderr, "ERROR: Maximum calculated displacement of %f is greater than the specified limit of %f on BBB%d.\n", global_max_s, max_s_lim, BBBid);
        err_occ = 1;
    }
    
    if (err_occ != 0) {
        //Error has occurred
        free(motor_steps);
        cleanandclose(NULL, motor_fp, gpio_pulse, gpio_direction, travelPerStep, numMotors, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return err_occ;
    }
    
    //Create output file
    F_OPEN(out_fp, binFile_addr, "w+b");
    if (!out_fp) {
        if (errno == ENOENT) {
            char* lastslash = (char*) strrchr(binFile_addr, '/');
            //Truncate string by setting this location to NULL
            *lastslash = '\0';
            fprintf(stderr, "ERROR: Could not create binary output file %s on BBB%d: specified directory does not exist.\n", binFile_addr, BBBid);
        } else {
            perror("ERROR: Could not create binary output file");
        }
        //Cleanup
        free(motor_steps);
        cleanandclose(NULL, motor_fp, gpio_pulse, gpio_direction, travelPerStep, numMotors, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return EXIT_FAILURE;
    }
    
    //Leave 4 bytes of header to give location for start of verification
    skipBytes = 0;
    if (fwrite(&skipBytes, 4, 1, out_fp) < 1) {
        perror("ERROR: Could not write to output file");
        cleanandclose(&out_fp, motor_fp, gpio_pulse, gpio_direction, travelPerStep, numMotors, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return ERR_DRIVE;
    }
    
    printf("Arranging and writing sequence of operations on BBB %d...\n", BBBid);
    
    start_set = (long long*) malloc(numMotors * sizeof(long long));
    end_set = (long long*) malloc(numMotors * sizeof(long long));
    next_clear = (long long*) malloc(numMotors * sizeof(long long));
    cur_direction = (signed char*) malloc(numMotors * sizeof(signed char));
    next_direction = (signed char*) malloc(numMotors * sizeof(signed char));
    switch_direction = (signed char*) malloc(numMotors * sizeof(signed char));
    
    //These variable names refer to a falling edge driver board
    next_clear_motor = -1;
    first_start_set_motor = -1;
    first_end_set_motor = -1;
    assemble_instruction(delay_instr, 0, 0, MAX_TIME_DIF);
    next_clear_t = LLONG_MAX;
    first_start_set = LLONG_MAX;
    first_end_set = LLONG_MAX;
    time_index = -MAX_TIME_DIF;
    
    for (motor_id = 0; motor_id < numMotors; motor_id++)
    {
        //Set pulses to high and set initial direction
        assemble_instruction(instr, gpio_pulse[motor_id], fallingEdge, 0);
        if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
            perror("ERROR: Could not write to output file");
            err_occ = ERR_DRIVE;
            break;
        }
        
        if (fread(&motor_instr, sizeof motor_instr, 1, motor_fp[motor_id]) == 0) {
            //Motor done
            start_set[motor_id] = LLONG_MAX;
            end_set[motor_id] = LLONG_MAX;
            next_clear[motor_id] = LLONG_MAX;
        } else {
            if (motor_instr > 0) {
                cur_direction[motor_id] = 1;
                assemble_instruction(instr, gpio_direction[motor_id], 0, 0);
            } else {
                //Direction pin set = displacement decreasing
                cur_direction[motor_id] = -1;
                assemble_instruction(instr, gpio_direction[motor_id], 1, 0);
            }
            
            next_clear[motor_id] = motor_instr * (long long)cur_direction[motor_id];
            switch_direction[motor_id] = 0;		//Direction pin has now been set
            start_set[motor_id] = LLONG_MAX;
            end_set[motor_id] = LLONG_MAX;
            
            if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                perror("ERROR: Could not write to output file");
                err_occ = ERR_DRIVE;
                break;
            }
        }
    }
    
    if (*stopSig && err_occ == 0) { err_occ = ERR_STOPSIG; }
    
    if (err_occ) {
        free(start_set);
        free(end_set);
        free(next_clear);
        free(cur_direction);
        free(next_direction);
        free(switch_direction);
        free(motor_steps);
        cleanandclose(&out_fp, motor_fp, gpio_pulse, gpio_direction, travelPerStep, numMotors, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return err_occ;
    }
    
    next_clear_t = select_first(next_clear, numMotors, &next_clear_motor);
    
    while (next_clear_t < LLONG_MAX)
    {
        //Pulse pin must be set at least pulse_min_half_period after clear and at least pulse_min_half_period before next clear of that pin
        //first_start_set >= time_index - always ensured
        if (first_start_set < next_clear_t - CLEAR_MARGIN)
        {
            //There is time to safely set a pulse pin before the next pulse clear is required.
            //Set always follows less than MAX_TIME_DIF after a clear, so don't need to insert blanks before.
            //Do set and direction trying to optimise data usage
            if ((first_end_set < next_clear_t - CLEAR_MARGIN) && (first_start_set + 5 >= first_end_set))
            {
                //Do first_end_set as more time critical (2nd condition), but don't do it at end time if it's less than a clear margin before next clear. If the end set time is before next clear, but gets excluded by this, its start set time will be before this and probably picked up by this else statement.
                unsigned timeDif = 0;
                first_start_set_motor = first_end_set_motor;		//For code to say set is no longer due
                if (start_set[first_end_set_motor] > time_index) {
                    timeDif = (unsigned)(start_set[first_end_set_motor] - time_index);	//Less than MAX_TIME_DIF
                    time_index = start_set[first_end_set_motor];
                }
                assemble_instruction(instr, gpio_pulse[first_end_set_motor], fallingEdge, timeDif);
                if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                    perror("ERROR: Could not write to output file");
                    err_occ = ERR_DRIVE;
                    break;
                }
                
                if (switch_direction[first_end_set_motor] == 1)
                {
                    assemble_instruction(instr, gpio_direction[first_start_set_motor], next_direction[first_end_set_motor] < 0, 0);
                    cur_direction[first_end_set_motor] = next_direction[first_end_set_motor];
                    if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                        perror("ERROR: Could not write to output file");
                        err_occ = ERR_DRIVE;
                        break;
                    }
                }
            }
            else
            {
                //Do first_start_set
                unsigned timeDif;
                
                if (first_end_set >= next_clear_t - CLEAR_MARGIN) { first_end_set = next_clear_t; }
                //Next_clear_t is the time-constraint; two cases are structurally the same and first_end_set will be reset shortly, not next_clear_t
                //first_end_set contains time of next required set/clear operation
                //If first_end_set was before cutoff margin before next clear, this set will be taken as the next instruction (even if it ends up being done here - it's only an optimisation).
                
                //Can set first motor up to and including the margin before the next clear, since its end is after this point
                if (first_start_set + 2 * MAX_TIME_DIF < first_end_set) {
                    //Next operation is more than 2*MAX_TIME_DIF away, so set this time difference to MAX_TIME_DIF to avoid a blank instruction
                    //Need to allow for a change of direction being required, which gives an extra instruction to insert
                    timeDif = MAX_TIME_DIF;
                } else if (first_start_set + MAX_TIME_DIF < first_end_set && switch_direction[first_start_set_motor] == 0) {
                    //Don't need to change direction pin, so can reduce time to next required operation
                    timeDif = MAX_TIME_DIF;
                } else if (first_start_set > time_index + 2) {
                    //Else crack on with it now - don't wait
                    timeDif = (unsigned)(first_start_set - time_index); }	//Less than MAX_TIME_DIF because pulse_min_full_period < MAX_TIME_DIF
                else { timeDif = 2; }		//Minimum actual separation of instructions
                
                time_index += timeDif;
                
                assemble_instruction(instr, gpio_pulse[first_start_set_motor], fallingEdge, timeDif);
                if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                    perror("ERROR: Could not write to output file");
                    err_occ = ERR_DRIVE;
                    break;
                }
                
                if (switch_direction[first_start_set_motor] == 1)
                {
                    //Need to switch motor direction before end of set period. Either do it immediately (after 2 clock ticks) or give it a maximum time difference if there is time available.
                    if (time_index + MAX_TIME_DIF < first_end_set) { timeDif = MAX_TIME_DIF; }
                    else { timeDif = 2; }
                    time_index += timeDif;
                    
                    assemble_instruction(instr, gpio_direction[first_start_set_motor], next_direction[first_start_set_motor] < 0, timeDif);
                    cur_direction[first_start_set_motor] = next_direction[first_start_set_motor];
                    if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                        perror("ERROR: Could not write to output file");
                        err_occ = ERR_DRIVE;
                        break;
                    }
                }
            }
            
            //No set due for this motor: awaiting clear
            start_set[first_start_set_motor] = LLONG_MAX;
            end_set[first_start_set_motor] = LLONG_MAX;
            
            first_start_set = select_first(start_set, numMotors, &first_start_set_motor);
            first_end_set = select_first(end_set, numMotors, &first_end_set_motor);
            if (first_start_set < time_index) { first_start_set = time_index; }			//Only first_start_set can be ignored such that it is less than time_index
        }
        else if (first_end_set < next_clear_t)
        {
            //Do urgent set and direction - at risk of poor timing on next_clear_t - but can't do it until have reached its start_set time
            unsigned timeDif = (unsigned)(first_start_set - time_index);	//Less than MAX_TIME_DIF because clear_margin < MAX_TIME_DIF and first_start_set was after the safe cutoff for next_clear; first_start_set >= time_index
            time_index = first_start_set;   //time_index < next_clear_t
            
            assemble_instruction(instr, gpio_pulse[first_end_set_motor], fallingEdge, timeDif);
            if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                perror("ERROR: Could not write to output file");
                err_occ = ERR_DRIVE;
                break;
            }
            
            if (switch_direction[first_end_set_motor] == 1)
            {
                //Set time delay to 0 to maximise clearence from next clear: no data wastage issues
                assemble_instruction(instr, gpio_direction[first_end_set_motor], next_direction[first_end_set_motor] < 0, 0);
                if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                    perror("ERROR: Could not write to output file");
                    err_occ = ERR_DRIVE;
                    break;
                }
            }
            
            //No set due for this motor: awaiting clear
            start_set[first_end_set_motor] = LLONG_MAX;
            end_set[first_end_set_motor] = LLONG_MAX;
            
            first_start_set = select_first(start_set, numMotors, &first_start_set_motor);
            first_end_set = select_first(end_set, numMotors, &first_end_set_motor);
            if (first_start_set < time_index) { first_start_set = time_index; }			//Only first_start_set can be ignored such that it is less than time_index
        }
        else
        {
            //Next instruction is a clear pulse
            unsigned long long timeDif = next_clear_t - time_index;
            while (timeDif > MAX_TIME_DIF)
            {
                if (fwrite(delay_instr, sizeof delay_instr, 1, out_fp) < 1) {
                    perror("ERROR: Could not write to output file");
                    err_occ = ERR_DRIVE;
                    break;
                }
                timeDif -= MAX_TIME_DIF;
            }
            time_index = next_clear_t;
            assemble_instruction(instr, gpio_pulse[next_clear_motor], !fallingEdge, (const unsigned)timeDif);	//Set pulse for a rising edge driver board
            if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                perror("ERROR: Could not write to output file");
                err_occ = ERR_DRIVE;
                break;
            }
            
            //Read in next_clear and calculate set time frame
            if (fread(&motor_instr, sizeof motor_instr, 1, motor_fp[next_clear_motor]) == 0)
            {
                //Motor done
                start_set[next_clear_motor] = LLONG_MAX;
                end_set[next_clear_motor] = LLONG_MAX;
                next_clear[next_clear_motor] = LLONG_MAX;
            }
            else
            {
                //Direction of next step
                if (motor_instr > 0) { next_direction[next_clear_motor] = 1; }
                else { next_direction[next_clear_motor] = -1; }
                
                if (next_direction[next_clear_motor] * cur_direction[next_clear_motor] < 0) { switch_direction[next_clear_motor] = 1; }
                
                //Timing of next step
                next_clear[next_clear_motor] = motor_instr * next_direction[next_clear_motor];
                start_set[next_clear_motor] = time_index + pulse_min_half_period;
                end_set[next_clear_motor] = next_clear[next_clear_motor] - pulse_min_half_period;
                
                if (start_set[next_clear_motor] > end_set[next_clear_motor])
                {
                    fprintf(stderr, "ERROR: Motor %d on BBB %d moving too rapidly at time %f.\nThere could be a discontinuity in the input or the requested velocity (amplitude) is too large.\nThe motor may stall and not produce requested displacement.\n", next_clear_motor, BBBid, next_clear[next_clear_motor] * TICK_DURATION);
                    err_occ = ERR_DISCONTINUOUS;
                    break;
                }
            }
            
            next_clear_t = select_first(next_clear, numMotors, &next_clear_motor);
            first_start_set = select_first(start_set, numMotors, &first_start_set_motor);
            if (first_start_set < time_index) { first_start_set = time_index; }			//Only first_start_set can be ignored such that it is less than time_index
            first_end_set = select_first(end_set, numMotors, &first_end_set_motor);
        }
    }
    
    //Return performance parameters if no errors
    if (wavetime && (err_occ == 0)) {
        *wavetime = time_index * TICK_DURATION;
        *instructions = 0;
        for (motor_id = 0; motor_id < numMotors; motor_id++) {
            //FPs are all at end of files
            long int flength = ftell(motor_fp[motor_id]);
            flength /= sizeof(long long);
            *instructions += flength;
        }
    }
    
    //Close motor temporary files
    for (motor_id = 0; motor_id < numMotors; motor_id++) {
        fclose(motor_fp[motor_id]);
    }
    free(motor_fp);
    
    //Cleanup
    free(start_set);
    free(end_set);
    free(next_clear);
    free(next_direction);
    free(switch_direction);
    
    if (*stopSig && err_occ == 0) { err_occ = ERR_STOPSIG; }
    
    if (err_occ) {
        free(cur_direction);
        free(motor_steps);
        cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, travelPerStep, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return err_occ;
    }
    
    //Clear all pins
    for (motor_id = 0; motor_id < numMotors; motor_id++)
    {
        //Only clear pulses if a rising edge driver board
        if (!fallingEdge)
        {
            assemble_instruction(instr, gpio_pulse[motor_id], 0, 0);
            if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
                perror("ERROR: Could not write to output file");
                free(cur_direction);
                free(motor_steps);
                cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, travelPerStep, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
                return ERR_DRIVE;
            }
        }
        
        assemble_instruction(instr, gpio_direction[motor_id], 0, 0);
        if (fwrite(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
            perror("ERROR: Could not write to output file");
            free(cur_direction);
            free(motor_steps);
            cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, travelPerStep, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
            return ERR_DRIVE;
        }
    }
    
    //Write length of file as header
    skipBytes = ftell(out_fp);
    //Save as little endian
    instr[0] = (unsigned char)skipBytes;
    instr[1] = (unsigned char)(skipBytes >> 8);
    instr[2] = (unsigned char)(skipBytes >> 16);
    instr[3] = (unsigned char)(skipBytes >> 24);
    rewind(out_fp);
    //Overwrite first 4 bytes with total file length for input to PRUSS
    if (fwrite(instr, sizeof instr[0], 4, out_fp) < 4) {
        perror("ERROR: Output file is corrupted");
        free(cur_direction);
        free(motor_steps);
        cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, travelPerStep, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return EXIT_FAILURE;
    }
    
    //Store number of instructions for for loop
    numInstructions = (skipBytes - 4) / BYTES_PER_INSTR;
    
    skipBytes = ftell(out_fp);  //Now stores read location for verification
    
    //Verify output
    printf("Verifying output file on BBB%d...\n", BBBid);
    
    //Calculate boundaries of displacement animation steps
    //Place at halfway points
    stepBinMax = (int**) malloc(numMotors * sizeof *stepBinMax);
    for (motor_id = 0; motor_id < numMotors; motor_id++) {
        stepBinMax[motor_id] = (int*) malloc((DISP_STEPS - 1) * sizeof *stepBinMax[motor_id]);
        for (k = 0; k < DISP_STEPS - 1; k++) {
            stepBinMax[motor_id][k] = (int)floor((global_min_s + ((2 * k + 1) * (global_max_s - global_min_s)) / (2 * (DISP_STEPS - 1))) / travelPerStep[motor_id]);
        }
    }
    
    //Now finished with
    free(travelPerStep);
    
    //Move to end for writing
    if (fseek(out_fp, 0, SEEK_END)) {
        perror("Output file I/O error");
        cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        free(cur_direction);
        free(motor_steps);
        //2D free
        for (motor_id = 0; motor_id < numMotors; motor_id++) {
            free(stepBinMax[motor_id]);
        }
        free(stepBinMax);
        return 3;
    }
    
    //Write minimum and maximum displacements as 24 bit chars, with exponent in byte 2 and little endian with sign in bit 15 and 23 - not using 2s complement
    //Minimum
    fraction = frexp(global_min_s, &exponent);
    //0.5 <= abs(fraction) < 1
    if (exponent < 0) {
        instr[2] = 1 << 7;
    } else {
        instr[2] = 0;
    }
    instr[2] += (abs(exponent) % (1 << 7));
    if (fraction < 0) {
        instr[1] = 1 << 7;  //Write sign to bit 7
    } else {
        instr[1] = 0;
    }
    fraction = fabs(fraction);  //Make positive
    fraction *= 1 << 7; //Multiply by 2^7 to get 7 bits available for integer cast
    instr[1] += (unsigned char) fraction;   //Only adds to bits 0-6
    fraction *= 1 << 8; //Now integer part modulo 256 can be written
    instr[0] = (unsigned char) fraction;
    if (fwrite(instr, sizeof instr[0], 3, out_fp) < 3) {
        perror("ERROR: Could not write validation header");
        cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        free(cur_direction);
        free(motor_steps);
        //2D free
        for (motor_id = 0; motor_id < numMotors; motor_id++) {
            free(stepBinMax[motor_id]);
        }
        free(stepBinMax);
        return ERR_DRIVE;
    }
    
    //Maximum
    fraction = frexp(global_max_s, &exponent);
    if (exponent < 0) {
        instr[2] = 1 << 7;
    } else {
        instr[2] = 0;
    }
    instr[2] += (abs(exponent) % (1 << 7));    if (fraction < 0) {
        instr[1] = 1 << 7;  //Write to bit 7
    } else {
        instr[1] = 0;
    }
    fraction = fabs(fraction);  //Make positive
    fraction *= 1 << 7; //Multiply by 2^7 to get 7 bits available for integer cast
    instr[1] += (unsigned char) fraction;   //Only adds to bits 0-6
    fraction *= 1 << 8; //Now integer part modulo 256 can be written
    instr[0] = (unsigned char) fraction;
    if (fwrite(instr, sizeof instr[0], 3, out_fp) < 3) {
        perror("ERROR: Could not write validation header");
        cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        free(cur_direction);
        free(motor_steps);
        //2D free
        for (motor_id = 0; motor_id < numMotors; motor_id++) {
            free(stepBinMax[motor_id]);
        }
        free(stepBinMax);
        return ERR_DRIVE;
    }
    
    dispBin = (unsigned char*) malloc(numMotors * sizeof *dispBin);
    
    //Set blank instruction
    assemble_valiation(delay_instr, ANIMATION_MAXTIMEDIF, 31, 0);	//Motor 31 doesn't exist
    
    //Populate with initial motor displacements
    for (motor_id = 0; motor_id < numMotors; motor_id++) {
        dispBin[motor_id] = 0;
        while (motor_steps[motor_id] > stepBinMax[motor_id][dispBin[motor_id]]) {
            dispBin[motor_id]++;
            if (dispBin[motor_id] == DISP_STEPS - 1) {
                //Don't include this in when condition as can't guarantee which side of && is evaluated first
                break;
            }
        }
        assemble_valiation(instr, 0, motor_id, dispBin[motor_id]);
        if (fwrite(instr, sizeof instr[0], BYTES_PER_VERIFICATION, out_fp) < BYTES_PER_VERIFICATION) {
            perror("ERROR: Could not write validation");
            cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
            free(cur_direction);
            free(motor_steps);
            free(dispBin);
            //2D free
            for (motor_id = 0; motor_id < numMotors; motor_id++) {
                free(stepBinMax[motor_id]);
            }
            free(stepBinMax);
            return ERR_DRIVE;
        }
    }
    lastWriteTime = 0;
    
    //Go back to read point
    if (fseek(out_fp, skipBytes, SEEK_SET)) {
        perror("Output file I/O error");
        cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        free(cur_direction);
        free(motor_steps);
        free(dispBin);
        //2D free
        for (motor_id = 0; motor_id < numMotors; motor_id++) {
            free(stepBinMax[motor_id]);
        }
        free(stepBinMax);
        return 3;
    }
    
    time_index = -MAX_TIME_DIF;
    for (k = 0; k < numInstructions; k++) {
        unsigned char gpio_read;
        if (fread(instr, sizeof instr[0], BYTES_PER_INSTR, out_fp) < BYTES_PER_INSTR) {
            if (ferror(out_fp)) {
                perror("Error reading output file for verification");
                err_occ = 1;
            }   //Otherwise EOF
            break;
        }
        time_index += instr[1];
        time_index += (instr[2] << 8);
        gpio_read = instr[0] & 0x7f;
        if (gpio_read) {
            //Ignore if gpio_read = 0
            motor_id = (int) gpiomap_motor[gpio_read];
            if (motor_id == -1) {
                fprintf(stderr, "ERROR: Verification failed on BBB%d: output file is corrupted. Try rerunning the compiler.\n", BBBid);
                err_occ = 1;
                break;
            }
            if (gpiomap_directionpin[gpio_read]) {
                if (instr[0] >> SC_BIT_NUM) {
                    //Direction pin set = displacement decreasing
                    cur_direction[motor_id] = -1;
                } else {
                    cur_direction[motor_id] = 1;
                }
            } else {
                int changedBin = 0;
                
                //Do we need to act on this pulse? Only if one of pulse set and falling edge is true
                if (!((instr[0] >> SC_BIT_NUM) ^ fallingEdge)) { continue; }
                
                motor_steps[motor_id] += cur_direction[motor_id];
                //Check whether has changed displacement bin - may have to change multiple bins if range is less than DISP_STEPS steps
                while (dispBin[motor_id] > 0 && motor_steps[motor_id] <= stepBinMax[motor_id][dispBin[motor_id] - 1]) {
                    dispBin[motor_id]--;
                    changedBin = 1;
                }
                while (dispBin[motor_id] < DISP_STEPS - 1 && motor_steps[motor_id] > stepBinMax[motor_id][dispBin[motor_id]]) {
                    dispBin[motor_id]++;
                    changedBin = 1;
                }
                if (changedBin == 0) { continue; } //Loop
                
                //Write verification output
                skipBytes = ftell(out_fp);  //Store read location to return to
                if (fseek(out_fp, 0, SEEK_END)) {
                    perror("Output file I/O error");
                    err_occ = 1;
                    break;
                }
                
                newWriteTime = roundll(time_index * ANIMATION_TIMESCALE);
                while (newWriteTime - lastWriteTime > ANIMATION_MAXTIMEDIF) {
                    if (fwrite(delay_instr, sizeof delay_instr[0], BYTES_PER_VERIFICATION, out_fp) < BYTES_PER_VERIFICATION) {
                        perror("ERROR: Could not write to output file");
                        err_occ = ERR_DRIVE;
                        break;
                    }
                    lastWriteTime += ANIMATION_MAXTIMEDIF;
                }
                
                assemble_valiation(instr, (const unsigned char)(newWriteTime - lastWriteTime), motor_id, dispBin[motor_id]);
                if (fwrite(instr, sizeof delay_instr[0], BYTES_PER_VERIFICATION, out_fp) < BYTES_PER_VERIFICATION) {
                    perror("ERROR: Could not write to output file");
                    err_occ = ERR_DRIVE;
                    break;
                }
                
                //Update last write time
                lastWriteTime = newWriteTime;
                //Return to read location
                if (fseek(out_fp, skipBytes, SEEK_SET)) {
                    perror("Output file I/O error");
                    err_occ = 1;
                    break;
                }
            }
        }
    }
    
    free(cur_direction);
    free(motor_steps);
    free(dispBin);
    //2D free
    for (motor_id = 0; motor_id < numMotors; motor_id++) {
        free(stepBinMax[motor_id]);
    }
    free(stepBinMax);
    
    if (err_occ) {
        rtn = cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
        return err_occ;
    }
    
    rtn = cleanandclose(&out_fp, NULL, gpio_pulse, gpio_direction, NULL, 0, BBBthreads, ThreadCounterMutex, ThreadCounterCondition);
    printf("Calculation complete for BBB %d. Output file saved.\nDisplacements ok: minimum = %f, maximum = %f.\n", BBBid, global_min_s, global_max_s);
    
    return rtn;
}

int compile_from_master(double* wavetime, long int* instructions, const double startPos, const char* waveDocString, const char* binaryfilename, const char* pinsfilename, const int BBBid, int* BBBthreads, int* motorthreads, const FLIM_TYPE filelimit, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition, int *stopSig)
{
    int status;
    
    xmlDocPtr waveDoc, xmlRawDoc, schema_doc;
    xmlNodePtr waveNode, xmlNode, subNode;
    xmlChar* xmlText;
    char schema_path[28];
    xmlSchemaParserCtxtPtr parser_ctxt;
    xmlSchemaPtr schema;
    xmlSchemaValidCtxtPtr valid_ctxt;
    int validationResult;
    char tempfoldername[23] = "tmp";
    struct stat folderstat;
    char xmlTempFileName[30];
    
    //Parse wave document fragment XML, will already be valid unless transmission fault
    waveDoc = xmlParseMemory(waveDocString, strlen(waveDocString + 1));
    if (!waveDoc) {
        fprintf(stderr, "Error in transferring wave XML.\n");
        return 91;
    }
    
    //Load pins XML file
    xmlRawDoc = xmlParseFile(pinsfilename);
    if (xmlRawDoc == NULL)
    {
        fprintf(stderr, "XML pins file parse unsuccessful.\n");
        if (waveDoc) xmlFreeDoc(waveDoc);
        return 1;
    }
    
    //Validate XML doc
    //Use ASWaMPinsSchema.xsd if end attribute is missing from <wave>, otherwise ASWaMWaveSchema.xsd
    xmlNode = xmlDocGetRootElement(xmlRawDoc);  //<wave>
    if ((xmlText = xmlGetProp(xmlNode, (const xmlChar*)"end"))) {
        //Use ASWaMWaveSchema
        STR_CPY(schema_path, "ASWaMWaveSchema.xsd", 20)
    } else {
        //Use ASWaMPinsSchema
        STR_CPY(schema_path, "ASWaMPinsSchema.xsd", 20)
    }
    xmlFree(xmlText);
    
    schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
    parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
    schema = xmlSchemaParse(parser_ctxt);
    valid_ctxt = xmlSchemaNewValidCtxt(schema);
    validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
    if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
    if (schema) xmlSchemaFree(schema);
    if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
    if (schema_doc) xmlFreeDoc(schema_doc);
    if (validationResult != 0)
    {
        fprintf(stderr, "Incorrect syntax in XML pins file.\n");
        if (xmlRawDoc) xmlFreeDoc(xmlRawDoc);
        if (waveDoc) xmlFreeDoc(waveDoc);
        return 1;
    }
    
    //Copy end attribute (overwrites)
    waveNode = xmlDocGetRootElement(waveDoc);
    xmlText = xmlGetProp(waveNode, (const xmlChar*)"end");
    xmlSetProp(xmlNode, (const xmlChar*) "end", xmlText);
    xmlFree(xmlText);
    
    //Copy resolution attribute if set
    if (xmlHasProp(waveNode, (const xmlChar*)"resolution")) {
        xmlText = xmlGetProp(waveNode, (const xmlChar*)"resolution");
        xmlSetProp(xmlNode, (const xmlChar*) "resolution", xmlText);
        xmlFree(xmlText);
    }
    
    //Copy initialSplit attribute
    xmlText = xmlGetProp(waveNode, (const xmlChar*)"initialSplit");
    xmlSetProp(xmlNode, (const xmlChar*) "initialSplit", xmlText);
    xmlFree(xmlText);
    
    if (startPos < DBL_MAX) {
        //Overwrite motor0xPos attribute, keep the spacing attributes as specified in pins file
        //DBL_MAX indicates unset - can't have a motor0xPos that large
        char startPosString[25];
        S_PRINTF(startPosString, 25, "%.16g", startPos)
        xmlSetProp(xmlNode, (const xmlChar*) "motor0xPos", (const xmlChar*) startPosString);
    }   //Else: XSLT defaults to 0
    
    //Remove all existing variable definitions from pins file (if it was a wave file, but code is still ok otherwise)
    //xmlNode currently points to <wave>
    subNode = xmlNode->children;
    while (xmlStrEqual(subNode->name, (const xmlChar*)"def")) {
        xmlUnlinkNode(subNode);
        xmlFreeNode(subNode);
        subNode = xmlNode->children;
    }
    xmlNode = subNode;  //xmlNode = first <motor>
    
    //Copy variable definitions from waveDoc in correct order
    waveNode = xmlFirstElementChild(waveNode);  //<def> or <motor>
    while (xmlStrEqual(waveNode->name, (const xmlChar*)"def")) {
        xmlNodePtr newNode = xmlDocCopyNode(waveNode, waveDoc, 1);
        xmlUnlinkNode(newNode); //Need to unlink node before adding to a new tree
        newNode = xmlAddPrevSibling(xmlNode, newNode);
        if (!subNode) {
            //Error
            fprintf(stderr, "Failed to add <def> node to XML tree.\n");
            if (xmlRawDoc) xmlFreeDoc(xmlRawDoc);
            if (waveDoc) xmlFreeDoc(waveDoc);
            return 90;
        }
        waveNode = xmlNextElementSibling(waveNode);
    }
    
    //Populate <motor> elements with the children of <wave>, deleting anything pre-existing.
    //xmlNode is pointing to first <motor>
    
    do {
        //Remove all old children from <motor> elements
        xmlNodePtr subNode = xmlNode->children;     //Gets first child element
        while (subNode) {
            xmlUnlinkNode(subNode);
            xmlFreeNode(subNode);
            subNode = xmlNode->children;
        }
        //subNode is now NULL
        
        //Reset waveNode to first <interval> as may be NULL
        waveNode = xmlDocGetRootElement(waveDoc);
        waveNode = xmlFirstElementChild(waveNode);      //<def> or <interval>
        while (!xmlStrEqual(waveNode->name, (const xmlChar*)"interval")) {
            waveNode = xmlNextElementSibling(waveNode);
        }
        
        do {
            //Duplicate <interval> nodes, use xmlAddChild (adds to end of list), replace (x) with x value
            subNode = xmlDocCopyNode(waveNode, waveDoc, 1);     //1 gets children recursively
            xmlUnlinkNode(subNode);                             //Need to unlink node before adding to a new tree
            subNode = xmlAddChild(xmlNode, subNode);
            if (!subNode) {
                //Error
                fprintf(stderr, "Failed to add <interval> node to XML tree.\n");
                if (xmlRawDoc) xmlFreeDoc(xmlRawDoc);
                if (waveDoc) xmlFreeDoc(waveDoc);
                return 90;
            }
            waveNode = xmlNextElementSibling(waveNode);
        } while (waveNode);     //While there is another <interval> element
        
        xmlNode = xmlNextElementSibling(xmlNode);
    } while (xmlNode);      //While there is another <motor> element
    
    //Set up temporary folder
    if (stat(tempfoldername, &folderstat) == -1) {
        if (MKDIR(tempfoldername)) {
            perror("Failed to create temporary folder");
            //Cleanup XML
            if (xmlRawDoc) xmlFreeDoc(xmlRawDoc);
            if (waveDoc) xmlFreeDoc(waveDoc);
            return 2;
        }
    }
    S_PRINTF(tempfoldername, 23, "tmp/compiler%d", BBBid)
    if (stat(tempfoldername, &folderstat) == -1) {
        if (MKDIR(tempfoldername)) {
            perror("Failed to create temporary folder");
            //Cleanup XML
            if (xmlRawDoc) xmlFreeDoc(xmlRawDoc);
            if (waveDoc) xmlFreeDoc(waveDoc);
            return 2;
        }
    }
    
    //Save XML file
    S_PRINTF(xmlTempFileName, 30, "%s/xmlinput.xml", tempfoldername)
    status = xmlSaveFile(xmlTempFileName, xmlRawDoc);
    
    if (xmlRawDoc) xmlFreeDoc(xmlRawDoc);
    if (waveDoc) xmlFreeDoc(waveDoc);
    
    if (status == -1) {
        perror("XML file save failed");
        //Cleanup XML
        return 2;
    }
    
    //Compile binary file
    status = make_binary(wavetime, instructions, xmlTempFileName, binaryfilename, BBBid, BBBthreads, motorthreads, filelimit, ThreadCounterMutex, ThreadCounterCondition, stopSig);
    
    return status;
}

#ifndef _WIN32
rlim_t get_filelimit()
//Checks whether sufficiently large and returns maximum number of files that can be opened, accounting for stdin etc.
//Returns 0 on error
{
    //Get maximum number of open files
    struct rlimit resourcelimit;
    if (getrlimit(RLIMIT_NOFILE, &resourcelimit)) {
        perror("Could not get maximum number of open files");
        return 0;
    }
    rlim_t filelimit = resourcelimit.rlim_cur;
    const int corefiles = 4; //stdin, stdout, stderr, lock
    const int requiredfiles = 3 + MAX_MOTORS + corefiles;  //3 (outfile + (2 XSL or XSD for 1 BBB) or (interval + XSL for 1 motor)) + numMotors + lock + sysfiles -> more available = more parallelisation
    if (filelimit < requiredfiles) {
        fprintf(stderr, "ERROR: need to be able to open %d files, but system only permits %ju.\n", requiredfiles, (uintmax_t)filelimit);
        return 0;
    }
    filelimit -= corefiles; //filelimit now contains number of BBB or motor files that can be opened
    
    return filelimit;
}
#endif
