//Activates PRUSS, connects all required GPIO. To be run before anything else on ASWaM. Must ensure that HDMI is disabled on boot in uEnv.txt.

/*Version control
v0.01 TD 15/01/15
v0.02 TD 30/01/15 Added stopping of master timing beacon
v0.03 TD 19/02/15 Added sleep before stopping timing beacon, to avoid segmentation fault
v0.04 TD 20/05/15 Added invertTimeBeacon
 v0.05 TD 19/01/16 Removed redundant headers
 v0.06 TD 20/04/17 Use snprintf
 v0.07 TD 11/08/17 Allow 3 digit pin numbers
*/

/* Driver header file */
#include "ctrl_pruss.h"

#define NUM_GPIO 50

int setup_pin(int number)
{
	char numstr[4];
	snprintf(numstr, 4, "%d", number);
	FILE *fp;
	//Export
	if ((fp = fopen("/sys/class/gpio/export", "w")) == NULL)
	{
		return 1;
	}
	fprintf(fp, numstr);
	fclose(fp);
	//Enable for output
	char dirstr[34];
	snprintf(dirstr, 34, "/sys/class/gpio/gpio%s/direction", numstr);
	if ((fp = fopen(dirstr, "w")) == NULL)
	{
		return 1;
	}
	fprintf(fp, "out");
	fclose(fp);
	return 0;
}

int main (void)
{
	//Load device tree overlay
	FILE *fp;
	if ((fp = fopen("/sys/devices/bone_capemgr.9/slots", "a")) == NULL)		//Overwrite
	{
		fprintf(stderr, "Failed to open slot manager\n");
		return 1;
	}
	fprintf(fp, "ASWaM-04");
	if (fclose(fp))
	{
		fprintf(stderr, "Failed to load ASWaM-04 overlay.\n");
		return 1;
	}
	printf("ASWaM-04 overlay enabled.\n");
	
	int pins[NUM_GPIO] = { 30, 60, 31, 50, 48, 51, 5, 4, 3, 2, 49, 15, 117, 14, 115, 113, 66, 67, 69, 68, 45, 44, 23, 26, 47, 46, 27, 65, 22, 61, 86, 88, 87, 89, 10, 11, 9, 81, 8, 80, 78, 79, 76, 77, 74, 75, 72, 73, 70, 71 };
	int k;

	for (k = 0; k < NUM_GPIO; k++)
	{
		if (setup_pin(pins[k])) { printf("Failed to setup GPIO%d.\n", pins[k]); }
	}
	printf("All required GPIOs enabled.\n");

	sleep(1);
    
    char response;
    do {
        printf("Is the timing beacon inverted? [y/n]: ");
        response = getchar();
        if (response != '\n') {
            while (getchar() != '\n');
        }
    } while (response != 'y' && response != 'n');

    unsigned char invertTimeBeacon = 0;
    if (response == 'y') {
        invertTimeBeacon = 1;
    }
	if (reset_clock(invertTimeBeacon)) { return 1; }		//Ensure master timing beacon is off

	return 0;
}
