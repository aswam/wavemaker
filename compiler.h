// Compiler for wave maker. This can be run on any computer.

/* Version control
 v0.01 TD 18/01/16 Migrated from ctrl_pruss, Temporary files placed in separate folder
 v0.02 TD 20/01/16 Made some function parameters constant for safer code
 v0.03 TD 27/01/16 Malloc xPos list
 v0.04 TD 28/01/16 Mallocs cast to type
 v0.05 TD 25/11/16 Variable travel per step and max step rate
 v0.06 TD 02/12/16 Return error message if file write error & store new XML input in tmp folder
 v0.07 TD 02/02/17 XML parser made thread safe
 v0.08 TD 13/02/17 Wave resolution parameter optional
 v0.09 TD 24/03/17 Start and end interval as function of x
 v0.10 TD 26/03/17 Repeat periodic section of interval
 v0.11 TD 05/04/17 Improved discontinuity error messages & return compiled wave time
 v0.12 TD 08/04/17 Each interval calculated in own thread
 v0.13 TD 19/04/17 evalfx() free XSLT context and add error message
 v0.14 TD 20/04/17 Substitute once for numerical value of x & use heap for step array
 v0.15 TD 16/05/17 Resolution = desired effective clock tick and now used in minimiser
 v0.16 TD 19/05/17 Step times calculated at halfway between steps
 v0.17 TD 22/05/17 XML names changed to camel case & motor0xPos optional in master file
 v0.18 TD 23/05/17 Choose min/max first & number of instructions in performance stats
 v0.19 TD 26/05/17 Improved error message for bad output file
 v0.20 TD 20/06/17 Ported for MSVS2012 and throw error on fast step
 v0.21 TD 22/06/17 Interval start, end and period XSLTed to elements
 v0.22 TD 23/06/17 Read GPIO map from file
 v0.23 TD 28/06/17 Always compiles little endian, independent of machine
 v0.24 TD 30/06/17 Verification appended to output file
 v0.25 TD 17/07/17 Verification continues beyond first few instructions
 v0.26 TD 24/07/17 Evaluates constant sub function trees & Verification copes with variable travel per step
 v0.27 TD 02/08/17 Optimises function tree & reverse attribute moved to pins file
 v0.28 TD 07/11/17 Continue looping optimisation of multiplication and addition until no more collective changes
 v0.29 TD 21/11/17 Abort compile on error and return some messages
 v0.30 TD 11/12/17 Custom defined variables
 v0.31 TD 04/01/18 Bug fixes for Windows
 v0.32 TD 05/01/18 Determine extremum at end of subinterval by time
 v0.33 TD 14/02/19 Increase string copy length and added missing brackets around negation of IS_FINITE macro
 v0.34 TD 06/09/19 Removed possible infinite loops in optimisation of divisions and subtractions
*/

#ifndef compiler_h  //Prevents double inclusion
#define compiler_h

#ifdef _WIN32
#include <Windows.h>
#include <direct.h>	//For _mkdir()
#else
#include <unistd.h>
#include <sys/resource.h>

#if (_POSIX_TIMERS > 0)
#include <time.h>	//May need <sys/time.h> instead?
#else
#include <sys/time.h>
#endif
#endif

#include <stdint.h>
#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <sys/stat.h>
#include <libxml/xmlschemas.h>
#include <libxslt/transform.h>
#include "calcalgebraicfunc.h"
#include "brent.h"

//Define safe string & file IO functions as macros
#if (defined _WIN32 || defined __STDC_LIB_EXT1__)
#ifndef __STDC_WANT_LIB_EXT1__
#define __STDC_WANT_LIB_EXT1__ 1
#endif
#define S_PRINTF(buffer, size, ...) if (sprintf_s(buffer, size, __VA_ARGS__) < 1) { perror("String formatting error"); return -1; }
#define F_OPEN(fp, name, mode) fopen_s(&fp, name, mode)
#define F_SCANF fscanf_s	//Returns EOF on error, must state string size with %s
#define S_SCANF sscanf_s
#else
#define S_PRINTF(buffer, size, ...) sprintf(buffer, __VA_ARGS__);
#define F_OPEN(fp, name, mode) fp = fopen(name, mode)
#define F_SCANF fscanf
#define S_SCANF sscanf
#endif

//Process ID
#ifdef _WIN32
#define PID_TYPE DWORD	//Unsigned long
#define PID_STRING_SPEC "%lu"
#define GET_PID GetCurrentProcessId()
#else
#define PID_TYPE pid_t
#define PID_STRING_SPEC "%u"
#define GET_PID getpid()
#endif

//Timing
#ifdef _WIN32
#define SLEEP_MS(ms) Sleep(ms)
#define TIME_TYPE ULONGLONG
#define GET_TIME(time) time = GetTickCount64()
#define TIME_DIF(t_dif, start_t, end_t) t_dif = (double)(end_t - start_t) * 1e-3	//Times are integers of milliseconds
#else
#define SLEEP_MS(ms) usleep(ms * 1000)
//Convert subfields to double or other signed format before subtraction
#if (_POSIX_TIMERS > 0)
#define TIME_TYPE struct timespec
#define GET_TIME(time) clock_gettime(CLOCK_MONOTONIC, &time)
#define TIME_DIF(t_dif, start_t, end_t) t_dif = (double)(end_t.tv_sec - start_t.tv_sec) + ((double)end_t.tv_nsec - (double)start_t.tv_nsec) * 1e-9	//Timespec in ns
#else
#define TIME_TYPE struct timeval
#define GET_TIME(time) gettimeofday(&time, NULL)	//Obsolete
#define TIME_DIF(t_dif, start_t, end_t) t_dif = (double)(end_t.tv_sec - start_t.tv_sec) + ((double)end_t.tv_usec - (double)start_t.tv_usec) * 1e-6	//Timeval in us
#endif
#endif

//File limit
#ifdef _WIN32
#define FLIM_TYPE int
#define GET_FILELIMIT _getmaxstdio()
#else
#define FLIM_TYPE rlim_t
#define GET_FILELIMIT get_filelimit()
#endif

//Make directory
#ifdef _WIN32
#define MKDIR(foldername) _mkdir(foldername)
#else
#define MKDIR(foldername) mkdir(foldername, S_IRWXU)
#endif

//Mac OSX does not have MSG_NOSIGNAL
#ifdef __APPLE__
#define MSG_NOSIGNAL SO_NOSIGPIPE
#endif

#define MAX_MOTORS 27   //Set by hardware limitations on BBB, but also verifier can only cope with up to 30 motors
#define BYTES_PER_INSTR 3
#define SC_BIT_NUM 7
#define SC_BIT (1 << SC_BIT_NUM)     //Bit to toggle in register to set/clear GPIO

#define TICK_DURATION 3e-8		//Number of seconds per tick of clock in PRU0
#define CLEAR_MARGIN 165		//Number of clock cycles to let set operations finish before next clear: 2*27*3+1

#define MAX_TIME_DIF 0xffff

//For calcalgebraicfunc - only ever insert function of x or t, not both
#define INDEP_VARS 1

#define PORTNUM "2300"  //Port number for network communication

//Network messages setting mode from master
#define ASWAM_COMPILE 'b'   //Compile binary file from XML
#define ASWAM_MAKEXML 'x'   //Make XMLs from master file

//Network message from slave
#define COMPILE_SUCCESS 'b' //Binary file compiled successfully
#define COMPILE_FAIL 'x'    //Binary file compile failed

//Error messages
#define ERR_DRIVE 101   //File writing error
#define ERR_STOPSIG 102 //Aborted requested by calling function
#define ERR_DISCONTINUOUS 103    //Discontinuous function
#define ERR_PERIOD 104  //Function not periodic

//Verification
#define BYTES_PER_VERIFICATION 2    //Number of bytes per verification entry
#define DISP_STEPS 128  //Number of bins for displacment in animation
#define ANIMATION_TICKDUR 0.1   //Number of seconds per animation clock tick
#define ANIMATION_TIMESCALE (TICK_DURATION / ANIMATION_TICKDUR)  //Multiply PRU ticks by this to get animation clock ticks
#define ANIMATION_MAXTIMEDIF 15 //Maximum time difference supported by 4 bits

unsigned char expansion_to_gpio(int header, int pin, FILE* gpiomap);
long long roundll(double x);
long long select_first(long long values[], int size, int *minimiser);
int start_interval(long long time_index, double value, int *previous_s_steps, double travelPerStep, FILE **fp, const int motor_id, const int BBBid);
int monotonic_interval(double start_t, double end_t, double start_s, double end_s, long long *time_index, int *previous_s_steps, double resolution, double travelPerStep, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode, FILE **fp, const int motor_id, const int BBBid);
int find_min(double* min_t, double* min_s, double start_t, double end_t, const double start_s, const double end_s, const double minitol, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode);
int find_max(double* max_t, double* max_s, double start_t, double end_t, const double start_s, const double end_s, const double minitol, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode);
int split_interval(char tryMinFirst, double start_t, double end_t, double start_s, double end_s, double *min_s, double *max_s, long long *time_index, int *previous_s_steps, double resolution, double travelPerStep, const xmlDocPtr xmlTree, xmlNodePtr *intervalNode, FILE **fp, const int motor_id, const int BBBid, int *stopSig);
int cleanandclose(FILE **out_fp, FILE *motor_fp[], unsigned char gpio_pulse[], unsigned char gpio_direction[], double travelPerStep[], int motors, int* BBBthreads, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition);
int evalConstNode(xmlDocPtr xmlDocument, xmlNodePtr parentNode);
int checkUpTreeConstant(xmlDocPtr xmlDocument, xmlNodePtr funcNode, xmlNodePtr curNode);
void insertUnaryMinus(xmlNodePtr oldNode);
int insertConstantMultiply(xmlDocPtr xmlDocument, xmlNodePtr oldNode, double nodeValue, const int divide);
int insertConstantAdd(xmlDocPtr xmlDocument, xmlNodePtr oldNode, double nodeValue, const int subtract);
xmlNodePtr swapLeftRightNodes(xmlNodePtr leftNode);
int optimiseFuncTree(xmlDocPtr xmlDocument, xmlNodePtr intervalNode, double xPos);
void* calc_motor_steps_unpack(void* params);
int calc_motor_steps(const double global_end_time, const double global_step_size, const double resolution, const double xPos, const double travelPerStep, const xmlDocPtr xmlTree, const xmlNodePtr motorNode, FILE** motor_fp, FILE** interval_fp, const int motor_id, const int BBBid, int* initialMotorSteps, double *min_s, double *max_s, int* motorthreads, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition, int *stopSig);
void assemble_instruction(unsigned char instr[3], const unsigned char gpio, const int set, const unsigned timeDif);
void assemble_valiation(uint8_t instr[], const unsigned char timeDif, const unsigned char motor_id, const unsigned char dispBin);
int make_binary(double* wavetime, long int* instructions, const char *xmlFile_addr, const char *binFile_addr, const int BBBid, int* BBBthreads, int* motorthreads, const FLIM_TYPE filelimit, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition, int *stopSig);
int compile_from_master(double* wavetime, long int* instructions, const double startPos, const char* waveDocString, const char* binaryfilename, const char* pinsfilename, const int BBBid, int* BBBthreads, int* motorthreads, const FLIM_TYPE filelimit, pthread_mutex_t* ThreadCounterMutex, pthread_cond_t* ThreadCounterCondition, int *stopSig);

#ifndef _WIN32
rlim_t get_filelimit();
#endif //_WIN32

#endif /* compiler_h */
