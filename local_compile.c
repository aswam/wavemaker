//Reads master XML wave files and compiles all the files on the local computer

/* Version control
 v0.01 TD 19/01/16
 v0.02 TD 20/01/16 Multithreaded
 v0.03 TD 28/01/16 Mallocs cast to type
 v0.04 TD 02/02/17 XML parser made thread safe
 v0.05 TD 10/02/17 Added file lock to prevent two instances of program in same folder
 v0.06 TD 13/02/17 Performance output, Cleaner thread closing
 v0.07 TD 05/04/17 Return wave time
 v0.08 TD 08/04/17 Each interval created in own thread
 v0.09 TD 20/04/17 Initialise XPath
 v0.10 TD 22/05/17 XML names changed to camel case & motor0xPos optional in master file
 v0.11 TD 23/05/17 Number of instructions in performance stats
 v0.12 TD 20/06/17 Ported for MSVS2012
 v0.13 TD 22/06/17 Filelimit updated to v0.21 of compiler.c
 v0.14 TD 25/07/17 Invalid free on compile failure
 v0.15 TD 28/07/17 Use xmlStrEqual
 v0.16 TD 21/11/17 Compiler stops on error and returns some error codes
 */

#include "compiler.h"

struct compile_params {
    //Contains all input arguments to compile_from_master() and make_binary()
    double* wavetime;
    long* instructions;
    double startPos;    //compile_from_master() only
    char* waveDocString;    //compile_from_master() only
    char* binaryfilename;
    char* pinsfilename;
    int BBBid;
    int* BBBthreads;    //For tracking number of open files
    int* motorthreads;  //For tracking number of open files
    FLIM_TYPE filelimit; //Unsigned long long
    pthread_mutex_t* ThreadCounterMutex;
    pthread_cond_t* ThreadCounterCondition;
    int *stopSig;
};

void free2d(void** inArray, int firstDimLength) {
    //Frees a 2D array with length firstDimLength in 1st dimension
    int k;
    for (k = 0; k < firstDimLength; k++) {
        free(inArray[k]);
    }
    free(inArray);
}

void* compile_from_master_unpack(void* params) {
    //For use in pthreads to call compile_from_master()
    struct compile_params *params_struct = (struct compile_params*) params;
    int* rtn = (int*) malloc(sizeof(int));
    *rtn = compile_from_master(params_struct->wavetime, params_struct->instructions, params_struct->startPos, params_struct->waveDocString, params_struct->binaryfilename, params_struct->pinsfilename, params_struct->BBBid, params_struct->BBBthreads, params_struct->motorthreads, params_struct->filelimit, params_struct->ThreadCounterMutex, params_struct->ThreadCounterCondition, params_struct->stopSig);
    return rtn;
}

void* make_binary_unpack(void* params) {
    //For use in pthreads to call make_binary()
    struct compile_params *params_struct = (struct compile_params*) params;
    int* rtn = (int*) malloc(sizeof(int));
    *rtn = make_binary(params_struct->wavetime, params_struct->instructions, params_struct->pinsfilename, params_struct->binaryfilename, params_struct->BBBid, params_struct->BBBthreads, params_struct->motorthreads, params_struct->filelimit, params_struct->ThreadCounterMutex, params_struct->ThreadCounterCondition, params_struct->stopSig);
    return rtn;
}

int local_make_from_master(char* masterfilename)
{
    //Define variables for C90 compliance
    double total_t;
    TIME_TYPE start_t, end_t;
    FLIM_TYPE filelimit;
    xmlDocPtr xmlRawDoc, schema_doc;
    xmlNodePtr xmlNode, waveNode, xmlProperty, xmlTextNode;
    xmlChar *xmlText;
    const char schema_path[] = "ASWaMMasterSchema.xsd";
    xmlSchemaParserCtxtPtr parser_ctxt;
    xmlSchemaPtr schema;
    xmlSchemaValidCtxtPtr valid_ctxt;
    int validationResult, noBBBs, WaveDocSize, BBBid, *thread_rtn, BBBthreads, motorthreads, *compile_rtn;
    char *WaveDocString, **binaryfilename, **pinsfilename;
    double *wave_end, *startPos, global_wave_end;
    long *wave_instructions, total_wave_instructions;
    unsigned char master_found, err_occ;
    pthread_t *thread;
    pthread_attr_t attr;
    struct compile_params *compiler_params;
    pthread_mutex_t ThreadCounterMutex;
    pthread_cond_t ThreadCounterCondition;
    int stopSig = 0;
    
    //Start timer - for performance benchmarking
    GET_TIME(start_t);
    
    //Check and get maximum number of open files
    filelimit = GET_FILELIMIT;
    if (filelimit == 0) { return EXIT_FAILURE; }   //Error message displayed by get_filelimit()
    
    //Open and read XML input
    xmlRawDoc = xmlParseFile(masterfilename);
    if (xmlRawDoc == NULL)
    {
        fprintf(stderr, "XML input file parse unsuccessful\n");
        return(EXIT_FAILURE);
    }
    
    //Validate XML doc
    schema_doc = xmlReadFile(schema_path, NULL, XML_PARSE_NONET);
    parser_ctxt = xmlSchemaNewDocParserCtxt(schema_doc);
    schema = xmlSchemaParse(parser_ctxt);
    valid_ctxt = xmlSchemaNewValidCtxt(schema);
    validationResult = xmlSchemaValidateDoc(valid_ctxt, xmlRawDoc);
    if (valid_ctxt) xmlSchemaFreeValidCtxt(valid_ctxt);
    if (schema) xmlSchemaFree(schema);
    if (parser_ctxt) xmlSchemaFreeParserCtxt(parser_ctxt);
    if (schema_doc) xmlFreeDoc(schema_doc);
    if (validationResult != 0)
    {
        fprintf(stderr, "Invalid syntax in XML input file: %d\n", validationResult);
        //Avoid memory leaks
        xmlFreeDoc(xmlRawDoc);
        return(EXIT_FAILURE);
    }
    
    //WARNING: Have not applied a stylesheet so there may be comment nodes, avoided using e.g. xmlFirstElementChild, but ->children is dangerous
    
    xmlNode = xmlDocGetRootElement(xmlRawDoc);  //<master>
    noBBBs = xmlChildElementCount(xmlNode); //Number of BBBs
    xmlNode = xmlFirstElementChild(xmlNode);    //<wave> or <bbb>
    
    waveNode = NULL;
    WaveDocString = NULL;
    WaveDocSize = 0;
    
    if (xmlStrEqual(xmlNode->name, (const xmlChar*) "wave")) {
        //Wave form given: send a copy of the wave form to each slave, which will then modify the pins file
        //Need to send: x position of motor 0 on each slave & <wave> with all descendents
        
        xmlDocPtr xmlWaveDoc;
        xmlNodePtr waveNodeCopy;
        xmlChar* xmlWaveDocString;
        
        waveNode = xmlNode;
        
        //Put <wave> element and all children into a buffer
        //Create new XML document with <wave> as the root and save it as string WaveDocString
        xmlWaveDoc = xmlNewDoc((const xmlChar*) "1.0");
        waveNodeCopy = xmlDocCopyNode(waveNode, xmlRawDoc, 1);
        xmlDocSetRootElement(xmlWaveDoc, waveNodeCopy);
        
        xmlDocDumpMemory(xmlWaveDoc, &xmlWaveDocString, &WaveDocSize);	//WaveDocSize does not include terminating NULL character
        xmlFreeDoc(xmlWaveDoc);
        
        WaveDocSize++;	//+1 to include terminating NULL character
        WaveDocString = (char*) malloc(WaveDocSize * sizeof(char));
        STR_CPY(WaveDocString, (const char*) xmlWaveDocString, WaveDocSize)
        xmlFree(xmlWaveDocString);
        
        xmlNode = xmlNextElementSibling(xmlNode);       //First <bbb>
        noBBBs--;       //One of the child elements of <master> is this <wave> node
    }
    //Else: use wave XML file at address pinsfilename already present on slave
    
    wave_end = (double*) malloc(noBBBs * sizeof(double));    //Length of compiled input on each BBB
    wave_instructions = (long*) malloc(noBBBs * sizeof(long)); //Number of instructions compiled on each BBB
    binaryfilename = (char**) malloc(noBBBs * sizeof(char*));
    pinsfilename = (char**) malloc(noBBBs * sizeof(char*));
    startPos = (double*) malloc(noBBBs * sizeof(double));
    master_found = 0;
    err_occ = 0;
    
    for (BBBid = 0; BBBid < noBBBs; BBBid++) {
        int stringLength;
        xmlProperty = xmlFirstElementChild(xmlNode);    //<ip>
        
        //Get binary file name
        xmlProperty = xmlNextElementSibling(xmlProperty);   //<binary>
        xmlTextNode = xmlProperty->children;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        xmlText = xmlNodeListGetString(xmlRawDoc, xmlTextNode, 1);
        stringLength = xmlStrlen(xmlText) + 1;	//+1 to account for terminating NULL
        binaryfilename[BBBid] = (char*) malloc(stringLength * sizeof(char));
        STR_CPY(binaryfilename[BBBid], (char *) xmlText, stringLength)
        xmlFree(xmlText);
        
        //Get pins file name
        xmlProperty = xmlNextElementSibling(xmlProperty);   //<pins>
        xmlTextNode = xmlProperty->children;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        xmlText = xmlNodeListGetString(xmlRawDoc, xmlTextNode, 1);
        stringLength = xmlStrlen(xmlText) + 1;	//+1 to account for terminating NULL
        pinsfilename[BBBid] = (char*) malloc(stringLength * sizeof(char));
        STR_CPY(pinsfilename[BBBid], (char *) xmlText, stringLength)
        xmlFree(xmlText);
        
        if (waveNode) {
            //Get pins file attributes: x position of motor 0 in mm
            xmlText = xmlGetProp(xmlProperty, (const xmlChar*)"motor0xPos");
            if (xmlText) {
                startPos[BBBid] = atof((const char*) xmlText);
                xmlFree(xmlText);
            } else {
                startPos[BBBid] = DBL_MAX;  //An unusably large number to indicate moto0xPos not set
            }
        }
        
        xmlProperty = xmlFirstElementChild(xmlNode);   //<ip>
        
        xmlTextNode = xmlProperty->children;
        //Search for text node, schema says it must exist
        while (xmlTextNode && xmlTextNode->type != XML_TEXT_NODE) {
            xmlTextNode = xmlTextNode->next;
        }
        
        if (!(xmlTextNode) || xmlIsBlankNode(xmlTextNode)) {
            //Refers to this BBB, the master
            if (master_found) {
                //Found two masters: error
                fprintf(stderr, "ERROR: Cannot have two master BBBs. Must specify an IP address for all slaves.\n");
                err_occ = 1;
                break;
            }
            master_found = 1;
        }
        
        xmlNode = xmlNextElementSibling(xmlNode);   //<bbb>, will go to NULL on last iteration
    }
    
    //XML Cleanup
    xmlFreeDoc(xmlRawDoc);
    
    //Check that a master has been specified
    if (master_found == 0) {
        fprintf(stderr, "No master specified. One BBB must have an empty IP address.\n");
        err_occ = 2;
    }
    
    //Exit if an error has occurred
    if (err_occ) {
        free(WaveDocString);
        free(wave_end);
        free(wave_instructions);
        free2d((void**) binaryfilename, noBBBs);
        free2d((void**) pinsfilename, noBBBs);
        free(startPos);
        return EXIT_FAILURE;
    }
    
    //Setup pthreads
    thread = (pthread_t*) malloc(noBBBs * sizeof(pthread_t));
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);    //Make threads joinable
    
    //If having stack overflow (bus error/segmentation fault), increase thread stack size
    //pthread_attr_setstacksize (&attr, 8388608);
    
    compiler_params = (struct compile_params*) malloc(noBBBs * sizeof(struct compile_params));
    thread_rtn = (int*) malloc(noBBBs * sizeof(int));
    
    //Open file counters
    BBBthreads = 0;
    motorthreads = 0;
    if (pthread_mutex_init(&ThreadCounterMutex, NULL)) {
        perror("ERROR: Could not initialise mutex");
        free(WaveDocString);
        free(wave_end);
        free(wave_instructions);
        free2d((void**) binaryfilename, noBBBs);
        free2d((void**) pinsfilename, noBBBs);
        free(startPos);
        free(thread);
        free(compiler_params);
        free(thread_rtn);
        pthread_attr_destroy(&attr);
        return EXIT_FAILURE;
    }
    if (pthread_cond_init(&ThreadCounterCondition, NULL)) {
        perror("ERROR: Could not initialise mutex condition");
        free(WaveDocString);
        free(wave_end);
        free(wave_instructions);
        free2d((void**) binaryfilename, noBBBs);
        free2d((void**) pinsfilename, noBBBs);
        free(startPos);
        free(thread);
        free(compiler_params);
        free(thread_rtn);
        pthread_attr_destroy(&attr);        pthread_mutex_destroy(&ThreadCounterMutex);
        return EXIT_FAILURE;
    }
    
    compile_rtn = (int*) malloc(noBBBs * sizeof(int));
    
    for (BBBid = 0; BBBid < noBBBs; BBBid++) {
        //Don't need to use strcpy as sub functions will not modify strings
        compiler_params[BBBid].wavetime = &wave_end[BBBid];
        compiler_params[BBBid].instructions = &wave_instructions[BBBid];
        compiler_params[BBBid].binaryfilename = binaryfilename[BBBid];
        compiler_params[BBBid].pinsfilename = pinsfilename[BBBid];
        compiler_params[BBBid].BBBid = BBBid;
        compiler_params[BBBid].BBBthreads = &BBBthreads;
        compiler_params[BBBid].motorthreads = &motorthreads;
        compiler_params[BBBid].filelimit = filelimit;
        compiler_params[BBBid].ThreadCounterMutex = &ThreadCounterMutex;
        compiler_params[BBBid].ThreadCounterCondition = &ThreadCounterCondition;
        compiler_params[BBBid].stopSig = &stopSig;
        
        if (WaveDocString) {
            compiler_params[BBBid].startPos = startPos[BBBid];
            //Libxml must not be reading same document concurrently, as mutux may lock - make copies of WaveDocString
            compiler_params[BBBid].waveDocString = (char*) malloc(WaveDocSize * sizeof(char));
            STR_CPY(compiler_params[BBBid].waveDocString, WaveDocString, WaveDocSize)
            
            //Try to allocate file quota for a new thread
            pthread_mutex_lock(&ThreadCounterMutex);
            while ((BBBthreads + 1) * (MAX_MOTORS + 3) + motorthreads > filelimit) {
                //Not enough file quota available -> wait - releases mutex
                //See make_binary() for explanation of formula
                pthread_cond_wait(&ThreadCounterCondition, &ThreadCounterMutex);
            }
            BBBthreads++;
            pthread_mutex_unlock(&ThreadCounterMutex);
            
            if ((thread_rtn[BBBid] = pthread_create(&thread[BBBid], &attr, compile_from_master_unpack, (void*) &compiler_params[BBBid]))) {
                fprintf(stderr, "Failed to create thread for BBB %d; will calculate in main thread.\n", BBBid);
                compile_rtn[BBBid] = compile_from_master(&wave_end[BBBid], &wave_instructions[BBBid], startPos[BBBid], WaveDocString, binaryfilename[BBBid], pinsfilename[BBBid], BBBid, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
            }
        } else {
            if ((thread_rtn[BBBid] = pthread_create(&thread[BBBid], &attr, make_binary_unpack, (void*) &compiler_params[BBBid]))) {
                fprintf(stderr, "Failed to create thread for BBB %d; will calculate in main thread.\n", BBBid);
                compile_rtn[BBBid] = make_binary(&wave_end[BBBid], &wave_instructions[BBBid], pinsfilename[BBBid], binaryfilename[BBBid], BBBid, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
            }
        }
    }
    
    //Cleanup attr - does not have any effect on already created threads
    pthread_attr_destroy(&attr);
    
    //Wait for threads to terminate, only if thread_rtn = 0
    for (BBBid = 0; BBBid < noBBBs; BBBid++) {
        if (thread_rtn[BBBid] == 0) {
            void* val;
            if (!pthread_join(thread[BBBid], &val)) {
                int* intptr = (int*) val;
                compile_rtn[BBBid] = *intptr;
                free(val);
            }
        }
    }
    
    //Cleanup - cannot do until all threads have terminated
    pthread_mutex_destroy(&ThreadCounterMutex);
    pthread_cond_destroy(&ThreadCounterCondition);
    free(thread);
    free(thread_rtn);
    free(startPos);
    free2d((void**)binaryfilename, noBBBs);
    free2d((void**)pinsfilename, noBBBs);
    
    if (WaveDocString) {
        for (BBBid = 0; BBBid < noBBBs; BBBid++) {
            free(compiler_params[BBBid].waveDocString);
        }
    }
    free(WaveDocString);        //Allocated using malloc, or still set to NULL
    free(compiler_params);
    
    global_wave_end = 0; //For performance benchmarking
    total_wave_instructions = 0;
    
    for (BBBid = 0; BBBid < noBBBs; BBBid++) {
        //Return an error code if required
        if (compile_rtn[BBBid] != 0 && compile_rtn[BBBid] != ERR_STOPSIG) {
            //Don't return error of stop signalled
            int rtn = compile_rtn[BBBid];
            switch (rtn) {
                case ERR_DRIVE:
                    fprintf(stderr, "ERROR: Could not write output files. The hard drive might be full.\n");
                    break;
                case ERR_DISCONTINUOUS:
                    fprintf(stderr, "ERROR: Discontinuity in input function. Scroll up to find error message.\n");
                    break;
                case ERR_PERIOD:
                    fprintf(stderr, "ERROR: Period is set incorrectly. Scroll up to find error message.\n");
                    break;
                default:
                    fprintf(stderr, "ERROR: Input failed to compile. Scroll up to find error message.\n");
            }
            free(wave_end);
            free(wave_instructions);
            free(compile_rtn);
            return rtn;
        }
        
        //Find global wave_end time
        if (wave_end[BBBid] > global_wave_end) {
            global_wave_end = wave_end[BBBid];
        }
        
        total_wave_instructions += wave_instructions[BBBid];
    }
    free(wave_end);
    free(wave_instructions);
    free(compile_rtn);
    
    //Display compile time
    GET_TIME(end_t);
    TIME_DIF(total_t, start_t, end_t);
    printf("Total time to compile = %g s.\n", total_t);
    
    if (global_wave_end > 0) {
        //Don't want to divide by zero
        double perf_ratio = global_wave_end / total_t;
        double perf_score = (double)total_wave_instructions / total_t;
        printf("Waveform length = %g s, ratio = %g.\nNumber of instructions = %ld, score = %g.\n", global_wave_end, perf_ratio, total_wave_instructions, perf_score);
    }
    
    return 0;   //Success
}

int main(int argc, char *argv[])
{
    //To use MSVS2012 (required for Libxml), code must be in C90 plus a few embellishments-> define variables at beginning of scope
    char tempfname[14];
    struct stat folderstat;
    FILE *lockfd;
    PID_TYPE readpid;
    int rtn;
    
    //Program information
    if (argc != 2 || !strcmp(argv[1], "help") || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
    {
        printf("Please provide one argument: XML input file\n");
        return EXIT_FAILURE;
    }
    
    //Prevent multiple instances running in same folder - other programs ok as behind dtach
    //Set up temporary folder
    STR_CPY(tempfname, "tmp", 4)
    if (stat(tempfname, &folderstat) == -1) {
        if (MKDIR(tempfname)) {
            perror("Failed to create temporary folder");
            return 2;
        }
    }
    
    STR_CAT(tempfname, "/pid.txt", 14)
    //Check if file already exists
    F_OPEN(lockfd, tempfname, "r");
    if (lockfd) {
        fclose(lockfd);
        fprintf(stderr, "Cannot have two open instances of compiler in same folder. This may have occurred because a previous instance was terminated early. Check %s for process id. Delete file if it has finished.\n", tempfname);
        return 3;
    }
    
    F_OPEN(lockfd, tempfname, "w+");
    if (!lockfd) {
        fprintf(stderr, "Cannot have two open instances of compiler in same folder. This may have occurred because a previous instance was terminated early. Check %s for process id. Delete file if it has finished.\n", tempfname);
        return 3;
    }
    
    //Print PID to file
    fprintf(lockfd, PID_STRING_SPEC, GET_PID);	//GetCurrentProcessId() returns DWORD = unsigned long
    //Wait to see if another process has also written to file
    SLEEP_MS(10);
    //Check PID
    fseek(lockfd, 0, SEEK_SET); //Move file pointer to start of file
    readpid = 0;	//Make sure it is initialised for if statement
    rtn = F_SCANF(lockfd, PID_STRING_SPEC, &readpid);
    if (rtn != 1 || readpid != GET_PID) {
        fclose(lockfd);
        fprintf(stderr, "Cannot have two open instances of compiler in same folder. This may have occurred because a previous instance was terminated early. Check %s for process id. Delete file if it has finished.\n", tempfname);
        return 3;
    }
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();
    
    rtn = local_make_from_master(argv[1]);
    
    //Cleanup XML & XSLT once all threads are done with it
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    //Unlock, i.e. delete lock file
    fclose(lockfd);
    remove(tempfname);
    
    return rtn;
}
