# README #

This repository contains the source code and documentation for the wave maker.

### Prerequisites ###

Beaglebone Black

(All other prerequisites are contained in the repository for an offline installation.)

### Installation Instructions ###

Disable the HDMI by uncommenting the line "optargs=capemgr.disable_partno=BB-BONELT-HDMI,BB-BONELT-HDMIN" in uEnv.txt on the BeagleboneBlack. Do not disable the eMMC.

Copy the repository onto the Beaglebone Black, run "cd Packages", then "chmod u+x install_ASWaM.sh" and "./install_ASWaM.sh" to install the prerequisites and compile the source code. The Beaglebone Black will reboot on completion. After every reboot, it is necessary to run "./initialise".

The documentation is contained in the LaTeX file ASWaMDocumentation.tex which needs compiling using either "make" or "make doc". Alternatively, upload the file into https://www.overleaf.com/.

### Contribution guidelines ###

You may create a new branch and edit the source code, or suggest improvements to the owner. The owner will in time merge these updates into the main branch.

### Repository Owner ###

Tom Dobra

Email: magiccarpetwavemaker@gmail.com
