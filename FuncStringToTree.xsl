﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name="funcstring">
    <xsl:param name="wholestring" />
    <xsl:variable name="numstring" select="number($wholestring)" />
    <xsl:choose>
      <xsl:when test="$numstring=0">0</xsl:when>
      <xsl:when test="$numstring">
        <xsl:value-of select="$numstring"/>
      </xsl:when>
      <xsl:when test="not($wholestring)">
				<error message="Two adjacent operators" />
        <xsl:message terminate="yes">Two adjacent operators</xsl:message>
      </xsl:when>
      <xsl:when test="starts-with($wholestring,')')">
				<error message="Missing (" />
        <xsl:message terminate="yes">Missing (</xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="rightadd">
          <xsl:call-template name="findbinop">
            <xsl:with-param name="wholestring" select="$wholestring" />
            <xsl:with-param name="op">+</xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="string($rightadd)">
            <xsl:call-template name="binfunc">
              <xsl:with-param name="funcname">+</xsl:with-param>
              <xsl:with-param name="wholestring" select="$wholestring" />
              <xsl:with-param name="right" select="$rightadd" />
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="rightsub">
              <xsl:call-template name="findbinop">
                <xsl:with-param name="wholestring" select="$wholestring" />
                <xsl:with-param name="op">-</xsl:with-param>
              </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="string($rightsub)">
                <xsl:call-template name="binfunc">
                  <xsl:with-param name="funcname">-</xsl:with-param>
                  <xsl:with-param name="wholestring" select="$wholestring" />
                  <xsl:with-param name="right" select="$rightsub" />
                </xsl:call-template>
              </xsl:when>
              <xsl:otherwise>
                <xsl:variable name="rightmult">
                  <xsl:call-template name="findbinop">
                    <xsl:with-param name="wholestring" select="$wholestring" />
                    <xsl:with-param name="op">*</xsl:with-param>
                  </xsl:call-template>
                </xsl:variable>
                <xsl:choose>
                  <xsl:when test="string($rightmult)">
                    <xsl:call-template name="binfunc">
                      <xsl:with-param name="funcname">*</xsl:with-param>
                      <xsl:with-param name="wholestring" select="$wholestring" />
                      <xsl:with-param name="right" select="$rightmult" />
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:variable name="rightdiv">
                      <xsl:call-template name="findbinop">
                        <xsl:with-param name="wholestring" select="$wholestring" />
                        <xsl:with-param name="op">/</xsl:with-param>
                      </xsl:call-template>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="string($rightdiv)">
                        <xsl:call-template name="binfunc">
                          <xsl:with-param name="funcname">/</xsl:with-param>
                          <xsl:with-param name="wholestring" select="$wholestring" />
                          <xsl:with-param name="right" select="$rightdiv" />
                        </xsl:call-template>
                      </xsl:when>
                      <xsl:when test="starts-with($wholestring,'-')">
												<!--Unary minus-->
                        <xsl:if test="string-length($wholestring)=1">
													<error message="Nothing after a minus sign" />
                          <xsl:message terminate="yes">Nothing after a minus sign</xsl:message>          
                        </xsl:if>
										    <unifunc>
										      <xsl:attribute name="name">minus</xsl:attribute>  
										      <xsl:call-template name="funcstring">
										        <xsl:with-param name="wholestring" select="substring($wholestring,2)" />
										      </xsl:call-template>
										    </unifunc>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:variable name="rightpow">
                          <xsl:call-template name="findbinop">
                            <xsl:with-param name="wholestring" select="$wholestring" />
                            <xsl:with-param name="op">^</xsl:with-param>
                          </xsl:call-template>
                        </xsl:variable>
                        <xsl:choose>
                          <xsl:when test="string($rightpow)">
                            <xsl:call-template name="binfunc">
                              <xsl:with-param name="funcname">^</xsl:with-param>
                              <xsl:with-param name="wholestring" select="$wholestring" />
                              <xsl:with-param name="right" select="$rightpow" />
                            </xsl:call-template>
                          </xsl:when>
                          <xsl:when test="starts-with($wholestring,'(')">
                            <xsl:if test="substring($wholestring,string-length($wholestring))!=')'">
															<error message="Missing ) or operator" />
                              <xsl:message terminate="yes">Missing ) or operator</xsl:message>
                            </xsl:if>
                            <xsl:call-template name="funcstring">
                              <xsl:with-param name="wholestring" select="substring($wholestring,2,string-length($wholestring)-2)" />
                            </xsl:call-template>
                          </xsl:when>
                          <xsl:when test="contains($wholestring,'(')">
                            <xsl:call-template name="unifunc">
                              <xsl:with-param name="wholestring" select="$wholestring" />
                            </xsl:call-template>
                          </xsl:when>
                          <xsl:otherwise>
                            <var>
                              <xsl:attribute name="name">
                                <xsl:value-of select="$wholestring" />
                              </xsl:attribute>
                            </var>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
	<xsl:template name="findbinop">
		<!--Find right-hand most binary operator in string. Returns empty if no valid operator found in search string in context of whole string.-->
		<xsl:param name="wholestring" />
		<xsl:param name="searchstring" select="$wholestring" />
		<xsl:param name="op" />
		<xsl:variable name="afterop" select="substring-after($searchstring,$op)" />
		
		<xsl:if test="string($afterop)">
			<!--Operator found, now check to the right-->
			<xsl:variable name="afterfinalop">
				<!--Is empty if no more of op-->
				<xsl:call-template name="findbinop">
					<xsl:with-param name="wholestring" select="$wholestring" />
					<xsl:with-param name="searchstring" select="$afterop" />
					<xsl:with-param name="op" select="$op" />
				</xsl:call-template>
			</xsl:variable>
			
			<!--boolean(string(0)) = true-->			
			<xsl:choose>
				<xsl:when test="string($afterfinalop)">
					<xsl:value-of select="$afterfinalop"/>
				</xsl:when>
				<xsl:otherwise>
					<!--Check the operator found in this level of the recursive function-->
					<xsl:variable name="beforeop" select="substring($wholestring,1,string-length($wholestring)-string-length($afterop)-1)" />
					<xsl:if test="string-length(translate($beforeop,'(','')) = string-length(translate($beforeop,')','')) and string-length(translate($afterop,'(','')) = string-length(translate($afterop,')',''))">
						<!--Operator is not splitting brackets-->
						<xsl:if test="not($op='-' and not(translate(substring($beforeop,string-length($beforeop)),'+-*/^(','')))">
							<!--<xsl:if test="not($op='-' and not(translate(substring($beforeop,string-length($beforeop)),'+-*/^(',''))))">-->
							<!--Not a binary operator if the last character (excluding spaces) before the minus sign is one of +*/^( to allow for the minus sign being a unary operator.-->
							<!--This operator is ok, so return it-->
							<xsl:value-of select="$afterop"/>
						</xsl:if>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
  
    
	<xsl:template name="binfunc">
		<xsl:param name="funcname" />
		<xsl:param name="wholestring" />
		<xsl:param name="right" />
		<binfunc>
			<xsl:attribute name="name">
				<xsl:value-of select="$funcname"/>
			</xsl:attribute>
			<left>
				<xsl:call-template name="funcstring">
					<xsl:with-param name="wholestring" select="substring($wholestring,1,string-length($wholestring)-string-length($right)-1)" />
				</xsl:call-template>
			</left>
			<right>
				<xsl:call-template name="funcstring">
					<xsl:with-param name="wholestring" select="$right" />
				</xsl:call-template>
			</right>
		</binfunc>
	</xsl:template>
  
  <xsl:template name="unifunc">
    <xsl:param name="wholestring" />
    <xsl:variable name="funcname" select="substring-before($wholestring,'(')" />
    <xsl:if test="substring($wholestring,string-length($wholestring)) != ')'">
			<error message="Missing )" />
      <xsl:message terminate="yes">
        <xsl:text>Missing ) in function </xsl:text>
        <xsl:value-of select="$funcname"/>
      </xsl:message>
    </xsl:if>
    <unifunc>
      <xsl:attribute name="name">
        <xsl:value-of select="$funcname"/>
      </xsl:attribute>  
      <xsl:call-template name="funcstring">
        <xsl:with-param name="wholestring" select="substring-after(substring($wholestring,1,string-length($wholestring)-1),'(')" />
      </xsl:call-template>
    </unifunc>
  </xsl:template>
	
	<!--For use as a standalone stylesheet-->
	<xsl:template match="func">
		<xsl:copy>
			<xsl:variable name="wholestring" select="translate(normalize-space(),' ','')" />
			<xsl:if test="$wholestring">
				<xsl:call-template name="funcstring">
					<xsl:with-param name="wholestring" select="$wholestring" />
				</xsl:call-template>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
