//This C code was originally written by John Burkardt, based on Fortran code by Richard Brent.

/*Version control
 v0.01 TD 20/01/16 Static variables made thread safe, only enabled in C11
 v0.02 TD 09/02/16 local_min() commented out as won't compile with optimisation
 v0.03 TD 08/04/17 Removed static variables & unused functions
 v0.04 TD 15/05/17 Changed two ifs to if-else at end of local_min_rc()
*/

# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <time.h>

# include "brent.h"

double local_min_rc ( double *a, double *b, int *status, double value, struct local_min_rc_statics* statics )

/******************************************************************************/
/*
  Purpose:
 
    LOCAL_MIN_RC seeks a minimizer of a scalar function of a scalar variable.
 
  Discussion:
 
    This routine seeks an approximation to the point where a function
    F attains a minimum on the interval (A,B).
 
    The method used is a combination of golden section search and
    successive parabolic interpolation.  Convergence is never much
    slower than that for a Fibonacci search.  If F has a continuous
    second derivative which is positive at the minimum (which is not
    at A or B), then convergence is superlinear, and usually of the
    order of about 1.324...
 
    The routine is a revised version of the Brent local minimization 
    algorithm, using reverse communication.
 
    It is worth stating explicitly that this routine will NOT be
    able to detect a minimizer that occurs at either initial endpoint
    A or B.  If this is a concern to the user, then the user must
    either ensure that the initial interval is larger, or to check
    the function value at the returned minimizer against the values
    at either endpoint.
 
  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:
 
    16 April 2008
 
  Author:
 
    John Burkardt
 
  Reference:
 
    Richard Brent,
    Algorithms for Minimization Without Derivatives,
    Dover, 2002,
    ISBN: 0-486-41998-3,
    LC: QA402.5.B74.
 
    David Kahaner, Cleve Moler, Steven Nash,
    Numerical Methods and Software,
    Prentice Hall, 1989,
    ISBN: 0-13-627258-4,
    LC: TA345.K34.
 
  Parameters
 
    Input/output, double *A, *B.  On input, the left and right
    endpoints of the initial interval.  On output, the lower and upper
    bounds for an interval containing the minimizer.  It is required
    that A < B.
 
    Input/output, int *STATUS, used to communicate between 
    the user and the routine.  The user only sets STATUS to zero on the first 
    call, to indicate that this is a startup call.  The routine returns STATUS
    positive to request that the function be evaluated at ARG, or returns
    STATUS as 0, to indicate that the iteration is complete and that
    ARG is the estimated minimizer.
 
    Input, double VALUE, the function value at ARG, as requested
    by the routine on the previous call.
 
    Output, double LOCAL_MIN_RC, the currently considered point.  
    On return with STATUS positive, the user is requested to evaluate the 
    function at this point, and return the value in VALUE.  On return with
    STATUS zero, this is the routine's estimate for the function minimizer.
 
  Local parameters:
 
    C is the squared inverse of the golden ratio.
 
    EPS is the square root of the relative machine precision.
*/
{
/*
   STATUS (INPUT) = 0, startup.
*/
  if ( *status == 0 )
  {
    if ( *b <= *a )
    {
      printf ( "\n" );
      printf ( "LOCAL_MIN_RC - Fatal error!\n" );
      printf ( "  A < B is required, but\n" );
      printf ( "  A = %f\n", *a );
      printf ( "  B = %f\n", *b );
      *status = -1;
      return *a;    //Must be something initialised
    }
    statics->c = 0.5 * ( 3.0 - sqrt ( 5.0 ) );

    statics->eps = sqrt ( r8_epsilon ( ) );
    statics->tol = r8_epsilon ( );

    statics->v = *a + statics->c * ( *b - *a );
    statics->w = statics->v;
    statics->x = statics->v;
    statics->e = 0.0;

    *status = 1;
    statics->arg = statics->x;

    return statics->arg;
  }
/*
   STATUS (INPUT) = 1, return with initial function value of FX.
*/
  else if ( *status == 1 )
  {
    statics->fx = value;
    statics->fv = statics->fx;
    statics->fw = statics->fx;
  }
/*
   STATUS (INPUT) = 2 or more, update the data.
*/
  else if ( 2 <= *status )
  {
    statics->fu = value;

      //No special case for fu==fx where a & b can be changed simultaneously, because it only works if the function is known to be unimodal
    if ( statics->fu <= statics->fx )
    {
      if ( statics->x <= statics->u )
      {
        *a = statics->x;
      }
      else
      {
        *b = statics->x;
      }
      statics->v = statics->w;
      statics->fv = statics->fw;
      statics->w = statics->x;
      statics->fw = statics->fx;
      statics->x = statics->u;
      statics->fx = statics->fu;
    }
    else
    {
      if ( statics->u < statics->x )
      {
        *a = statics->u;
      }
      else
      {
        *b = statics->u;
      }

      if ( statics->fu <= statics->fw || statics->w == statics->x )
      {
        statics->v = statics->w;
        statics->fv = statics->fw;
        statics->w = statics->u;
        statics->fw = statics->fu;
      }
      else if ( statics->fu <= statics->fv || statics->v == statics->x || statics->v == statics->w )
      {
        statics->v = statics->u;
        statics->fv = statics->fu;
      }
    }
  }
/*
   Take the next step.
*/
  statics->midpoint = 0.5 * ( *a + *b );
  statics->tol1 = statics->eps * fabs ( statics->x ) + statics->tol / 3.0;
  statics->tol2 = 2.0 * statics->tol1;
/*
   If the stopping criterion is satisfied, we can exit: max(x-a, b-x) <= tol2, which is the limit of machine precision.
*/
  if ( fabs ( statics->x - statics->midpoint ) <= ( statics->tol2 - 0.5 * ( *b - *a ) ) )
  {
    *status = 0;
    return statics->arg;
  }
/*
   Is golden-section necessary?
*/
  if ( fabs ( statics->e ) <= statics->tol1 )
  {
    if ( statics->midpoint <= statics->x )
    {
      statics->e = *a - statics->x;
    }
    else
    {
      statics->e = *b - statics->x;
    }
    statics->d = statics->c * statics->e;
  }
/*
   Consider fitting a parabola.
*/
  else
  {
    statics->r = ( statics->x - statics->w ) * ( statics->fx - statics->fv );
    statics->q = ( statics->x - statics->v ) * ( statics->fx - statics->fw );
    statics->p = ( statics->x - statics->v ) * statics->q - ( statics->x - statics->w ) * statics->r;
    statics->q = 2.0 * ( statics->q - statics->r );
    if ( 0.0 < statics->q )
    {
      statics->p = - statics->p;
    }
    statics->q = fabs ( statics->q );
    statics->r = statics->e;
    statics->e = statics->d;
/*
   Choose a golden-section step if the parabola is not advised.
*/
    if ( 
      ( fabs ( 0.5 * statics->q * statics->r ) <= fabs ( statics->p ) ) ||
      ( statics->p <= statics->q * ( *a - statics->x ) ) ||
      ( statics->q * ( *b - statics->x ) <= statics->p ) )
    {
      if ( statics->midpoint <= statics->x )
      {
        statics->e = *a - statics->x;
      }
      else
      {
        statics->e = *b - statics->x;
      }
      statics->d = statics->c * statics->e;
    }
/*
   Choose a parabolic interpolation step.
*/
    else
    {
      statics->d = statics->p / statics->q;
      statics->u = statics->x + statics->d;

      if ( ( statics->u - *a ) < statics->tol2 )
      {
        statics->d = statics->tol1 * r8_sign ( statics->midpoint - statics->x );
      }

      if ( ( *b - statics->u ) < statics->tol2 )
      {
        statics->d = statics->tol1 * r8_sign ( statics->midpoint - statics->x );
      }
    }
  }
/*
   F must not be evaluated too close to X.
*/
  if ( statics->tol1 <= fabs ( statics->d ) ) {
    statics->u = statics->x + statics->d;
  } else {
    statics->u = statics->x + statics->tol1 * r8_sign ( statics->d );
  }
/*
   Request value of F(U).
*/
  statics->arg = statics->u;
  *status = *status + 1;

  return statics->arg;
}
/******************************************************************************/


double r8_epsilon ( )

/******************************************************************************/
/*
  Purpose:

    R8_EPSILON returns the R8 round off unit.

  Discussion:

    R8_EPSILON is a number R which is a power of 2 with the property that,
    to the precision of the computer's arithmetic,
      1 < 1 + R
    but
      1 = ( 1 + R / 2 )

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    01 September 2012

  Author:

    John Burkardt

  Parameters:

    Output, double R8_EPSILON, the R8 round-off unit.
*/
{
  const double value = 2.220446049250313E-016;

  return value;
}
/******************************************************************************/


double r8_sign ( double x )

/******************************************************************************/
/*
  Purpose:

    R8_SIGN returns the sign of an R8.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    18 October 2004

  Author:

    John Burkardt

  Parameters:

    Input, double X, the number whose sign is desired.

    Output, double R8_SIGN, the sign of X.
*/
{
  double value;

  if ( x < 0.0 )
  {
    value = -1.0;
  } 
  else
  {
    value = 1.0;
  }
  return value;
}
/******************************************************************************/


void zero_rc ( double a, double b, double t, double *arg, int *status, 
  double value, struct zero_rc_statics* statics )

/******************************************************************************/
/*
  Purpose:

    ZERO_RC seeks the root of a function F(X) using reverse communication.

  Discussion:

    The interval [A,B] must be a change of sign interval for F.
    That is, F(A) and F(B) must be of opposite signs.  Then
    assuming that F is continuous implies the existence of at least
    one value C between A and B for which F(C) = 0.

    The location of the zero is determined to within an accuracy
    of 6 * MACHEPS * abs ( C ) + 2 * T.

    The routine is a revised version of the Brent zero finder 
    algorithm, using reverse communication.

    Thanks to Thomas Secretin for pointing out a transcription error in the
    setting of the value of P, 11 February 2013.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    11 February 2013

  Author:

    John Burkardt

  Reference:

    Richard Brent,
    Algorithms for Minimization Without Derivatives,
    Dover, 2002,
    ISBN: 0-486-41998-3,
    LC: QA402.5.B74.

  Parameters:

    Input, double A, B, the endpoints of the change of sign interval.

    Input, double T, a positive error tolerance.

    Output, double *ARG, the currently considered point.  The user
    does not need to initialize this value.  On return with STATUS positive,
    the user is requested to evaluate the function at ARG, and return
    the value in VALUE.  On return with STATUS zero, ARG is the routine's
    estimate for the function's zero.

    Input/output, int *STATUS, used to communicate between 
    the user and the routine.  The user only sets STATUS to zero on the first 
    call, to indicate that this is a startup call.  The routine returns STATUS
    positive to request that the function be evaluated at ARG, or returns
    STATUS as 0, to indicate that the iteration is complete and that
    ARG is the estimated zero

    Input, double VALUE, the function value at ARG, as requested
    by the routine on the previous call.
*/
{
  double m;
  double p;
  double q;
  double r;
  double s;
  double tol;
/*
  Input STATUS = 0.
  Initialize, request F(A).
*/
  if ( *status == 0 )
  {
    statics->machep = r8_epsilon ( );

    statics->sa = a;
    statics->sb = b;
    statics->e = statics->sb - statics->sa;
    statics->d = statics->e;

    *status = 1;
    *arg = a;
    return;
  }
/*
  Input STATUS = 1.
  Receive F(A), request F(B).
*/
  else if ( *status == 1 )
  {
    statics->fa = value;
    *status = 2;
    *arg = statics->sb;
    return;
  }
/*
  Input STATUS = 2
  Receive F(B).
*/
  else if ( *status == 2 )
  {
    statics->fb = value;

    if ( 0.0 < statics->fa * statics->fb )
    {
      *status = -1;
      return;
    }
    statics->c = statics->sa;
    statics->fc = statics->fa;
  }
  else
  {
    statics->fb = value;

    if ( ( 0.0 < statics->fb && 0.0 < statics->fc ) || ( statics->fb <= 0.0 && statics->fc <= 0.0 ) )
    {
      statics->c = statics->sa;
      statics->fc = statics->fa;
      statics->e = statics->sb - statics->sa;
      statics->d = statics->e;
    }
  }
/*
  Compute the next point at which a function value is requested.
*/
  if ( fabs ( statics->fc ) < fabs ( statics->fb ) )
  {
    statics->sa = statics->sb;
    statics->sb = statics->c;
    statics->c = statics->sa;
    statics->fa = statics->fb;
    statics->fb = statics->fc;
    statics->fc = statics->fa;
  }

  tol = 2.0 * statics->machep * fabs ( statics->sb ) + t;
  m = 0.5 * ( statics->c - statics->sb );

  if ( fabs ( m ) <= tol || statics->fb == 0.0 )
  {
    *status = 0;
    *arg = statics->sb;
    return;
  }

  if ( fabs ( statics->e ) < tol || fabs ( statics->fa ) <= fabs ( statics->fb ) )
  {
    statics->e = m;
    statics->d = statics->e;
  }
  else
  {
    s = statics->fb / statics->fa;

    if ( statics->sa == statics->c )
    {
      p = 2.0 * m * s;
      q = 1.0 - s;
    }
    else
    {
      q = statics->fa / statics->fc;
      r = statics->fb / statics->fc;
      p = s * ( 2.0 * m * q * ( q - r ) - ( statics->sb - statics->sa ) * ( r - 1.0 ) );
      q = ( q - 1.0 ) * ( r - 1.0 ) * ( s - 1.0 );
    }

    if ( 0.0 < p )
    {
      q = - q;
    }
    else
    {
      p = - p;
    }
    s = statics->e;
    statics->e = statics->d;

    if ( 2.0 * p < 3.0 * m * q - fabs ( tol * q ) && 
         p < fabs ( 0.5 * s * q ) )
    {
      statics->d = p / q;
    }
    else
    {
      statics->e = m;
      statics->d = statics->e;
    }
  }

  statics->sa = statics->sb;
  statics->fa = statics->fb;

  if ( tol < fabs ( statics->d ) )
  {
    statics->sb = statics->sb + statics->d;
  }
  else if ( 0.0 < m )
  {
    statics->sb = statics->sb + tol;
  }
  else
  {
    statics->sb = statics->sb - tol;
  }

  *arg = statics->sb;
  *status = *status + 1;

  return;
}
