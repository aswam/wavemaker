//Put script in body of HTML

//Global variables
var xslFuncStringToTree, unifuncList;

//Prevent sloppy programming and throw more errors
"use strict";

//Check browser for XML support
if (window.DOMParser && window.XSLTProcessor && window.XMLSerializer && window.Blob) {
    document.getElementById("missingAPIs").hidden = true;   //Hide error message that shows by default
} else {
    document.getElementById("mainView").hidden = true;
}

//Define for speed to avoid recreating array each time
unifuncList = ["acos", "acosh", "asin", "asinh", "atan", "atanh", "ceil", "cos", "cosh", "exp", "fabs", "abs", "floor", "log", "ln", "log10", "sin", "sinh", "sqrt", "tan", "tanh", "H", "minus"];

//Complete initiation dependent on which webpage is calling
function initMaster() {
    //Load then hide these - otherwise Edge doesn't unhide them properly
    document.getElementById("mainView").hidden = true;
    document.getElementById("FuncXSLLabel").hidden = true;
    document.getElementsByClassName("ip")[0].hidden = true;
    
    if (XMLHttpRequest) {
        //Load FuncStringToTree.xsl for later
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) { //HTTP status may be undefined for local access so don't use
                if (!xhttp.responseXML) {
                    //Could not automatically load FuncStringToTree.xsl, possibly due to security settings. Enable manual loading.
                    document.getElementById("FuncXSLLabel").hidden = false;
                } else {
                    //Create only once for speed, as only ever use the same XSLT
                    xslFuncStringToTree = new XSLTProcessor();
                    //Only gets to this point if valid XML - assume right file
                    xslFuncStringToTree.importStylesheet(xhttp.responseXML);
                    //Show form, as everything is ready
                    document.getElementById("mainView").hidden = false;
                }
            }
        };
        try {
            xhttp.open("GET", "FuncStringToTree.xsl", true);    //Load asynchronously
            xhttp.send();
        } catch (err) {
            document.getElementById("FuncXSLLabel").hidden = false;
        }
    } else {
        //Don't have XMLHttpRequest
        document.getElementById("FuncXSLLabel").hidden = false;
    }
}

function initPins() {
    document.getElementById("FuncXSLLabel").hidden = true;
    
    //Read URL
    if (location.search.startsWith("?type=wave")) {
        document.querySelectorAll("input[name=specifyWave]")[1].checked = true; //Wave
    }
    specifyWaveChangePins();    //Hide any wave sections that should be hidden
    document.getElementsByClassName("setSpacing")[0].hidden = true;    //Hide spacing override on motor 0
}

function loadXSLFromInput(fileInput) {
    //For manual file selection when security policy restricts autoloading
    var fileobj, freader;
    //Check browser for file reading support
    if (!window.FileReader) {
        window.alert("Web browser does not support FileReader API. Need to update to newer browser to read files.");
    } else {
        fileobj = fileInput.files[0];
        //Check filename
        if (fileobj.name != "FuncStringToTree.xsl") {
            window.alert("File must be named FuncStringToTree.xsl");
        } else {
            freader = new FileReader();
            freader.onload = function () {
                var xmlParser, xslobj, parsererrorNS;
                xmlParser = new DOMParser();
                xslobj = xmlParser.parseFromString(freader.result, "text/xml");
                //Check XML is well-formed - see Rast on https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross - will have a parsererror element somewhere in a namespace that depends on the browser
                try {
                    //Reports error on console in Edge, but ok
                    parsererrorNS = xmlParser.parseFromString("INVALID", "text/xml").getElementsByTagName("parsererror")[0].namespaceURI;
                } catch (err) {
                    //Method of getting namespace is too harsh for browser; try no namespace
                    parsererrorNS = "";
                }
                
                if (xslobj.getElementsByTagNameNS(parsererrorNS, "parsererror").length > 0) {
                    window.alert("FuncStringToTree.xsl is corrupted. Reinstall the software.");
                } else {
                    //Assumed file is correct stylesheet
                    xslFuncStringToTree = new XSLTProcessor();
                    xslFuncStringToTree.importStylesheet(xslobj);
                    //Show form, as everything is ready
                    document.getElementById("FuncXSLLabel").hidden = true;
                    document.getElementById("mainView").hidden = false;
                    
                    if (location.pathname.endsWith("pinswave_wizard.html")) {
                        setTimeout(saveFilePins, 0);
                    }
                }
            };
            freader.onerror = function () { window.alert("Could not read file"); };
            freader.readAsText(fileobj);   //Reads as UTF-8
        }
    }
}

//Functions largely in order from top to bottom in webpage

//XML private functions
function xmlBoolean(inString) {
    //Converts XML attribute string to boolean or null if unspecified; case insensitive
    if (inString) {
        if (inString.toLowerCase() == "true" || inString == "1") { return true; }
        else {return false; }
    } else { return null; }
}

function checkItemExists(testArray, validItems, description) {
    //Checks whether the name of every item in testArray is listed in validItems. Returns 0 on success, 4 on failure. Description is text used in the error message if not found.
    var numTest, numItems, testId, itemId, elementName, elementFound;
    numTest = testArray.length;
    numItems = validItems.length;
    for (testId = 0; testId < numTest; testId++) {
        elementName = testArray[testId].getAttribute("name");
        elementFound = false;
        for (itemId = 0; itemId < numItems; itemId++) {
            if (elementName == validItems[itemId]) {
                elementFound = true;
                break;
            }
        }
        if (elementFound == false) {
            window.alert("Error in function string: unrecognised " + description + " name \"" + elementName + "\".");
            return 4;
        }
    }
    return 0;
}

function validateFuncString(inString, tDependentVars, tIndependentVars) {
    //Tests to see whether inString is a valid function string, restricted to variables listed in the array of strings. Takes two arrays of independent variables to validate against, returning 1 on success if a t-dependent variable was used.
    
    //Declare all variables at start of scope
    var funcParser, inDoc, transDoc, variables, elementList, elementID, numElements;
    
    //Construct XML document
    funcParser = new DOMParser();
    inDoc = funcParser.parseFromString("<?xml version=\"1.0\"?><func />", "application/xml");
    inDoc.documentElement.textContent = inString;
    
    //Apply XSLT stylesheet
    try {
        transDoc = xslFuncStringToTree.transformToDocument(inDoc);
    } catch (err) {
        window.alert("Error in function string: " + err.message);  //Nothing throws on Safari nor Chrome
        return -2;
    }
    
    if (!transDoc) {
        window.alert("FuncStringToTree.xsl is corrupted. Reinstall the software.");
        return -1;
    }
    
    //Check for empty string
    if (!transDoc.documentElement.hasChildNodes()) {
        window.alert("Function string cannot be empty. Select separate option if an empty string is required.");
        return -6;
    }
    
    //Check for error elements
    if (transDoc.getElementsByTagName("error").length > 0) {
        window.alert("Error in function string: " + transDoc.getElementsByTagName("error")[0].getAttribute("message"));
        return -3;
    }
    
    //Check unifunc names
    elementList = transDoc.getElementsByTagName("unifunc");
    if (checkItemExists(elementList, unifuncList, "function")) {
        return -5;
    }
    
    //Check variable names
    variables = tDependentVars.concat(tIndependentVars);
    elementList = transDoc.getElementsByTagName("var");
    if (checkItemExists(elementList, variables, "variable")) {
        return -4;
    }
    
    //Binary operators are hard coded into XSLT, so will always be valid
    //Valid function string syntax, but not check for discontinuities nor out of range
    
    //Return 1 if function string depends on t; 0 otherwise
    numElements = elementList.length;
    for (elementID = 0; elementID < numElements; elementID++) {
        if (tDependentVars.indexOf(elementList[elementID].getAttribute("name")) >= 0) {
            return 1;
        }
    }
    return 0;
}

//Load existing XML file
function loadFile(fileInput) {
    var fileobj, freader, fname;
    //Check browser for file reading support
    if (!window.FileReader) {
        window.alert("Web browser does not support FileReader API. Need to update to newer browser to read files.");
        return -1;
    } else {
        fileobj = fileInput.files[0];
        fname = fileobj.name;
        freader = new FileReader();
        freader.onload = function () {
            if (location.pathname.endsWith("master_wizard.html")) {
                readFileMaster(freader.result);
            } else {
                readFilePins(freader.result);
            }
            if (fname) {
                document.getElementById("inputForm").elements.namedItem("outfname").value = fname;
            }
        };
        freader.onerror = function () { window.alert("Could not read file"); };
        freader.readAsText(fileobj);   //Reads as UTF-8
    }
}

function readFileMaster(xmlString) {
    //Declare all variables at start, as they'll be hoisted up anyway and do not lose their value on redeclaration
    var xmlParser, xmlDoc, parsererrorNS, xmlNode, formElements, attrValue, inputList, intervalNodes, numIntervals, intervalID, intervalFieldsets, BBBnodes, numBBBs, BBBid, BBBFieldsets, masterBBB, elementNodes;
    
    xmlParser = new DOMParser();
    xmlDoc = xmlParser.parseFromString(xmlString, "text/xml");
    if (xmlDoc == null) {
        window.alert("Not a valid XML file");
        return 1;
    }
    
    //Check XML is well-formed - see Rast on https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross - will have a parsererror element somewhere in a namespace that depends on the browser
    parsererrorNS = xmlParser.parseFromString("INVALID", "text/xml").getElementsByTagName("parsererror")[0].namespaceURI;
    if (xmlDoc.getElementsByTagNameNS(parsererrorNS, "parsererror").length > 0) {
        window.alert("XML input file is not well formed, so cannot be read");
        return 1;
    }
    
    //Don't validate against XSD to allow incomplete/incorrect files to be partially read
    xmlNode = xmlDoc.documentElement;
    if (xmlNode.nodeName == "wave") {
        window.alert("This is a pins or wave file. To edit it, use the pins/wave input file wizard instead.");
        return 1;
    } else if (xmlNode.nodeName != "master") {
        window.alert("Not a master input file: must have <master> as its root element.");
        return 1;
    }
    
    //Reading data out of file - reset form
    resetMasterForm();
    
    formElements = document.getElementById("inputForm").elements;
    
    //Is the waveform specified?
    xmlNode = xmlNode.firstElementChild;
    
    if (xmlNode.nodeName == "wave") {
        attrValue = xmlNode.getAttribute("end");
        if (attrValue != null) {
            formElements.namedItem("globalEnd").value = attrValue;
        }
        
        attrValue = xmlNode.getAttribute("initialSplit");
        if (attrValue != null) {
            formElements.namedItem("initialSplit").value = attrValue;
        }
        
        attrValue = xmlNode.getAttribute("resolution");
        if (Number(attrValue) > 0) {   //Evaluates false if attribute doesn't exist
            formElements.namedItem("reduceResolution").checked = true;
            formElements.namedItem("resolution").disabled = false;
            formElements.namedItem("resolution").value = attrValue;
        }
        
        //Add and populate custom variables
        intervalNodes = xmlNode.getElementsByTagName("def");
        numIntervals = intervalNodes.length;
        //Create variable slots; none already exist
        for (intervalID = 0; intervalID < numIntervals; intervalID++) {
            addVar(formElements.namedItem("addVariable"));
        }
        intervalFieldsets = document.getElementsByClassName("variable");
        for (intervalID = 0; intervalID < numIntervals; intervalID++) {
            //Don't populate first fieldset, as it is a hidden blank for copying
            inputList = intervalFieldsets[intervalID + 1].getElementsByTagName("input");
            
            attrValue = intervalNodes[intervalID].getAttribute("name");
            if (attrValue != null) {
                inputList[0].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].textContent;
            if (attrValue != null) {
                inputList[1].value = attrValue;
            }
        }

        //Add and populate intervals
        intervalNodes = xmlNode.getElementsByTagName("interval");
        numIntervals = intervalNodes.length;
        for (intervalID = 1; intervalID < numIntervals; intervalID++) {
            addInt(formElements.namedItem("addInterval"));
        }
        intervalFieldsets = document.getElementsByClassName("interval");
        for (intervalID = 0; intervalID < numIntervals; intervalID++) {
            inputList = intervalFieldsets[intervalID].getElementsByTagName("input");
            
            attrValue = intervalNodes[intervalID].getAttribute("start");
            if (attrValue != null) {
                inputList[1].checked = true;
                tickToEnable(inputList[1]);
                inputList[2].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].getAttribute("end");
            if (attrValue != null) {
                inputList[4].checked = true;
                tickToEnable(inputList[4]);
                inputList[5].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].getAttribute("period");
            if (attrValue) {
                inputList[8].checked = true;
                tickToEnable(inputList[8]);
                inputList[9].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].textContent;
            if (attrValue) {
                inputList[7].value = attrValue;   //Picks up all child nodes too, but they shouldn't exist
            } else {
                inputList[6].checked = true;
                tickToHide(inputList[6]);
            }
        }
    } else if (xmlNode.nodeName == "bbb") {
        document.querySelector("input[name=specifyWave,value=pins]").checked = true;
        specifyWaveChangeMaster();
    }   //Else: not recognised, leave set to default of specify wave
    
    //Add BBBs
    xmlNode = xmlDoc.documentElement;
    BBBnodes = xmlNode.getElementsByTagName("bbb");
    numBBBs = BBBnodes.length;
    for (BBBid = 1; BBBid < numBBBs; BBBid++) {
        addBBB(formElements.namedItem("addB"));
    }
    
    //Populate BBBs
    BBBFieldsets = document.getElementsByClassName("bbb");
    masterBBB = -1; //Determines master BBB
    
    for (BBBid = 0; BBBid < numBBBs; BBBid++) {
        inputList = BBBFieldsets[BBBid].getElementsByTagName("input");
        
        elementNodes = BBBnodes[BBBid].getElementsByTagName("ip");
        if (elementNodes.length > 0) {
            attrValue = elementNodes[0].textContent;
            inputList[1].value = attrValue;
            if (masterBBB < 0 && !attrValue) {
                //If no master is found, form reset will default to the first BBB
                masterBBB = BBBid;
                inputList[0].checked = true;
                changeMasterBBB(inputList[0]);
            }
        }
        
        elementNodes = BBBnodes[BBBid].getElementsByTagName("binary");
        if (elementNodes.length > 0) {
            inputList[2].value = elementNodes[0].textContent;
        }
        
        elementNodes = BBBnodes[BBBid].getElementsByTagName("pins");
        if (elementNodes.length > 0) {
            inputList[3].value = elementNodes[0].textContent;
            attrValue = elementNodes[0].getAttribute("motor0xPos");
            if (attrValue != null) {
                inputList[4].checked = true;
                tickToEnable(inputList[4]);
                inputList[5].value = attrValue;
            }
        }
    }
    
    //Unfocus elements
    document.activeElement.blur();
}

function readFilePins(xmlString) {
    //Declare all variables at start, as they'll be hoisted up anyway and do not lose their value on redeclaration
    var xmlParser, xmlDoc, parsererrorNS, xmlNode, formElements, attrValue, motorNode, numMotors, inputList, intervalNodes, numIntervals, intervalID, intervalFieldsets;
    
    xmlParser = new DOMParser();
    xmlDoc = xmlParser.parseFromString(xmlString, "text/xml");
    if (xmlDoc == null) {
        window.alert("Not a valid XML file");
        return 1;
    }
    
    //Check XML is well-formed - see Rast on https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross - will have a parsererror element somewhere in a namespace that depends on the browser
    parsererrorNS = xmlParser.parseFromString("INVALID", "text/xml").getElementsByTagName("parsererror")[0].namespaceURI;
    if (xmlDoc.getElementsByTagNameNS(parsererrorNS, "parsererror").length > 0) {
        window.alert("XML input file is not well formed, so cannot be read");
        return 1;
    }
    
    //Don't validate against XSD to allow incomplete/incorrect files to be partially read
    xmlNode = xmlDoc.documentElement;
    if (xmlNode.nodeName == "master") {
        window.alert("This is a master input file. To edit it, use the master input file wizard instead.");
        return 1;
    } else if (xmlNode.nodeName != "wave") {
        window.alert("Neither pins nor wave input file: must have <wave> as its root element.");
        return 1;
    }
    
    //Reading data out of file - reset form
    resetPinsForm();
    
    formElements = document.getElementById("inputForm").elements;
    
    //Pins or wave?
    if (xmlDoc.getElementsByTagName("interval").length > 0) {
        //Wave
        formElements.namedItem("specifyWave")[1].checked = true;    //specify
        specifyWaveChangePins();
    }
    
    //Hardware
    attrValue = xmlNode.getAttribute("gpioMapFile");
    if (attrValue != null) {
        formElements.namedItem("pinsmap").value = attrValue;
    }
    
    attrValue = xmlNode.getAttribute("minDisplacement");
    if (attrValue != null) {
        formElements.namedItem("mindisplacement").value = attrValue;
    }
    
    attrValue = xmlNode.getAttribute("maxDisplacement");
    if (attrValue != null) {
        formElements.namedItem("maxdisplacement").value = attrValue;
    }
    
    attrValue = xmlNode.getAttribute("maxStepRate");
    if (attrValue != null) {
        formElements.namedItem("maxStepRate").value = attrValue;
    }
    
    if (xmlBoolean(xmlNode.getAttribute("fallingEdge"))) {
        formElements.namedItem("fallingEdge").value = "true";
    }
    
    attrValue = xmlNode.getAttribute("motor0xPos");
    if (attrValue != null) {
        formElements.namedItem("motor0xPos").value = attrValue;
    }
    
    attrValue = xmlNode.getAttribute("defaultSpacing");
    if (attrValue != null) {
        formElements.namedItem("defaultSpacingValue").value = attrValue;
    } else {
        formElements.namedItem("defaultSpacing").checked = false;
        formElements.namedItem("defaultSpacingValue").disabled = true;
    }
    
    attrValue = xmlNode.getAttribute("defaultTravelPerStep");
    if (attrValue != null) {
        formElements.namedItem("defaultTravelPerStepValue").value = attrValue;
    } else {
        formElements.namedItem("defaultTravelPerStep").checked = false;
        formElements.namedItem("defaultTravelPerStepValue").disabled = true;
    }
	
    if (xmlBoolean(xmlNode.getAttribute("invertTimeBeacon"))) {
        formElements.namedItem("invertTimeBeacon").checked = true;
    }
    
    //Waveform
    attrValue = xmlNode.getAttribute("end");
    if (attrValue != null) {
        formElements.namedItem("globalEnd").value = attrValue;
    }
    
    attrValue = xmlNode.getAttribute("initialSplit");
    if (attrValue != null) {
        formElements.namedItem("initialSplit").value = attrValue;
    }
    
    attrValue = xmlNode.getAttribute("resolution");
    if (Number(attrValue) > 0) {   //Evaluates false if attribute doesn't exist
        formElements.namedItem("reduceResolution").checked = true;
        formElements.namedItem("resolution").disabled = false;
        formElements.namedItem("resolution").value = attrValue;
    }
    
    if (xmlBoolean(xmlNode.getAttribute("reverse"))) {
        formElements.namedItem("reverseAll").checked = true;
        changeReverseAll(formElements.namedItem("reverseAll"));
    }
    
    //Add and populate custom variables
    intervalNodes = xmlNode.getElementsByTagName("def");
    numIntervals = intervalNodes.length;
    //Create variable slots; none already exist
    for (intervalID = 0; intervalID < numIntervals; intervalID++) {
        addVar(formElements.namedItem("addVariable"));
    }
    intervalFieldsets = document.getElementsByClassName("variable");
    for (intervalID = 0; intervalID < numIntervals; intervalID++) {
        //Don't populate first fieldset, as it is a hidden blank for copying
        inputList = intervalFieldsets[intervalID + 1].getElementsByTagName("input");
        
        attrValue = intervalNodes[intervalID].getAttribute("name");
        if (attrValue != null) {
            inputList[0].value = attrValue;
        }
        
        attrValue = intervalNodes[intervalID].textContent;
        if (attrValue != null) {
            inputList[1].value = attrValue;
        }
    }
    
    //Add motors
    motorNodes = xmlNode.getElementsByTagName("motor");
    numMotors = motorNodes.length;
    for (motorId = 1; motorId < numMotors; motorId++) {
        addMotor(formElements.namedItem("addM"));
    }
    
    //Populate motors
    motorFieldsets = document.getElementsByClassName("motor");
    for (motorId = 0; motorId < numMotors; motorId++) {
        inputList = motorFieldsets[motorId].getElementsByTagName("input");
        
        attrValue = motorNodes[motorId].getAttribute("pulseHeader");
        if (attrValue != null) {
            inputList[0].value = attrValue;
        }
        
        attrValue = motorNodes[motorId].getAttribute("pulsePin");
        if (attrValue != null) {
            inputList[1].value = attrValue;
        }
        
        attrValue = motorNodes[motorId].getAttribute("directionHeader");
        if (attrValue != null) {
            inputList[2].value = attrValue;
        }
        
        attrValue = motorNodes[motorId].getAttribute("directionPin");
        if (attrValue != null) {
            inputList[3].value = attrValue;
        }
        
        attrValue = motorNodes[motorId].getAttribute("spacing");
        if (attrValue != null) {
            inputList[4].checked = true;
            inputList[5].disabled = false;
            inputList[5].value = attrValue;
        }
        
        attrValue = motorNodes[motorId].getAttribute("travelPerStep");
        if (attrValue != null) {
            inputList[6].checked = true;
            inputList[7].disabled = false;
            inputList[7].value = attrValue;
        }
        
        if (xmlBoolean(motorNodes[motorId].getAttribute("reverse"))) {
            inputList[8].checked = true;
        }
        
        //Add intervals
        intervalNodes = motorNodes[motorId].getElementsByTagName("interval");
        numIntervals = intervalNodes.length;
        for (intervalId = 1; intervalId < numIntervals; intervalId++) {
            addInt(motorFieldsets[motorId].querySelector("input[name=addInterval]"));
        }
        
        //Populate intervals
        intervalFieldsets = motorFieldsets[motorId].getElementsByClassName("interval");
        for (intervalID = 0; intervalID < numIntervals; intervalID++) {
            inputList = intervalFieldsets[intervalID].getElementsByTagName("input");
            
            attrValue = intervalNodes[intervalID].getAttribute("start");
            if (attrValue != null) {
                inputList[1].checked = true;
                tickToEnable(inputList[1]);
                inputList[2].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].getAttribute("end");
            if (attrValue != null) {
                inputList[4].checked = true;
                tickToEnable(inputList[4]);
                inputList[5].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].getAttribute("period");
            if (attrValue) {
                inputList[8].checked = true;
                tickToEnable(inputList[8]);
                inputList[9].value = attrValue;
            }
            
            attrValue = intervalNodes[intervalID].textContent;
            if (attrValue) {
                inputList[7].value = attrValue;   //Picks up all child nodes too, but they shouldn't exist
            } else {
                inputList[6].checked = true;
                tickToHide(inputList[6]);
            }
        }
    }
    
    //Unfocus elements
    document.activeElement.blur();
}

//Form reset
function resetMasterForm() {
    //Resets everything that HTML forms reset function doesn't do
    
    //Declare variables
    var formDOMNode, intervalFieldsets, numIntervals, k, BBBFieldsets, numBBBs;
    
    formDOMNode = document.getElementById("inputForm");
    
    //Remove all visible custom variables
    intervalFieldsets = document.getElementsByClassName("variable");
    numIntervals = intervalFieldsets.length;
    for (k = numIntervals - 1; k > 0; k--) {
        //Remove in descending order to always take off last
        intervalFieldsets[0].parentNode.removeChild(intervalFieldsets[k]);
    }
    renumber(intervalFieldsets[0].parentNode);  //Disables move buttons
    
    //Remove extra intervals
    intervalFieldsets = document.getElementsByClassName("interval");
    numIntervals = intervalFieldsets.length;
    for (k = numIntervals - 1; k > 0; k--) {
        //Remove in descending order to always take off last
        intervalFieldsets[0].parentNode.removeChild(intervalFieldsets[k]);
    }
    formDOMNode.elements.namedItem("removeInterval").disabled = true;
    renumber(intervalFieldsets[0].parentNode);  //Disables move buttons
    //Reset doesn't change intervalsCreated counter, so don't need to rename radio buttons
    
    //Remove extra BBBs
    BBBFieldsets = document.getElementsByClassName("bbb");
    numBBBs = BBBFieldsets.length;
    for (k = numBBBs - 1; k > 0; k--) {
        BBBFieldsets[0].parentNode.removeChild(BBBFieldsets[k]);
    }
    formDOMNode.elements.namedItem("removeB").disabled = true;
    renumber(BBBFieldsets[0].parentNode);  //Disables move buttons
    
    formDOMNode.reset();    //Need to reset form before clearing up the rest
    
    specifyWaveChangeMaster();
    tickToEnable(formDOMNode.elements.namedItem("reduceResolution"));
    formDOMNode.elements.namedItem("startInterval").disabled = true;    //Only one of these exists -> no node list
    formDOMNode.elements.namedItem("endInterval").disabled = true;
    tickToEnable(formDOMNode.elements.namedItem("periodic"));
    tickToHide(formDOMNode.elements.namedItem("stationary"));
    tickToEnable(formDOMNode.elements.namedItem("motor0xPosOverride"));
    
    document.activeElement.blur();
}

function resetPinsForm() {
    var formDOMNode, motorFieldsets, numItems, k;
    
    formDOMNode = document.getElementById("inputForm");
    
    //Remove all visible custom variables
    intervalFieldsets = document.getElementsByClassName("variable");
    numIntervals = intervalFieldsets.length;
    for (k = numIntervals - 1; k > 0; k--) {
        //Remove in descending order to always take off last
        intervalFieldsets[0].parentNode.removeChild(intervalFieldsets[k]);
    }
    renumber(intervalFieldsets[0].parentNode);  //Disables move buttons
    
    //Remove extra motors
    motorFieldsets = document.getElementsByClassName("motor");
    numItems = motorFieldsets.length;
    for (k = numItems - 1; k > 0; k--) {
        motorFieldsets[0].parentNode.removeChild(motorFieldsets[k]);
    }
    formDOMNode.querySelector("input.removeMotor").disabled = true;
    renumber(motorFieldsets[0].parentNode);  //Disables move buttons
    
    //Remove extra intervals
    motorFieldsets = document.getElementsByClassName("interval");
    numItems = motorFieldsets.length;
    for (k = numItems - 1; k > 0; k--) {
        motorFieldsets[0].parentNode.removeChild(motorFieldsets[k]);
    }
    formDOMNode.querySelector("input[name=removeInterval]").disabled = true;
    renumber(motorFieldsets[0].parentNode);  //Disables move buttons
    
    formDOMNode.reset();
    
    //Hide/disable stuff based on reset values
    specifyWaveChangePins();
    tickToEnable(formDOMNode.elements.namedItem("reduceResolution"));
    
    //Wise to do these before default checkboxes
    tickToEnable(formDOMNode.elements.namedItem("overrideSpacing"));
    tickToEnable(formDOMNode.elements.namedItem("overrideTravelPerStep"));
    //Default checkboxes
    tickToEnable(formDOMNode.elements.namedItem("defaultSpacing"));
    tickToEnable(formDOMNode.elements.namedItem("defaultTravelPerStep"));
    
    changeReverseAll(formDOMNode.elements.namedItem("reverseAll"));
    
    formDOMNode.elements.namedItem("startInterval").disabled = true;    //Only one of these exists -> no node list
    formDOMNode.elements.namedItem("endInterval").disabled = true;
    tickToEnable(formDOMNode.elements.namedItem("periodic"));
    tickToHide(formDOMNode.elements.namedItem("stationary"));
    
    //Unfocus elements
    document.activeElement.blur();
}

//Middle content shared functions

//Private functions
function fieldsetChildren(collectionNode) {
    //Gets fieldset elements that are children, not grandchildren, of collectionNode
    var nodeList, nodeListLength, outArray, k;
    nodeList = collectionNode.getElementsByTagName("fieldset");    //Convert nodelist object into array
    outArray = [];
    nodeListLength = nodeList.length;   //Define to avoid re-evaluating each time in for loop
    for (k = 0; k < nodeListLength; k++) {
        if (nodeList[k].parentNode.isSameNode(collectionNode)) {
            //Include in output if direct child element
            outArray.push(nodeList[k]);
        }
    }
    return outArray;
}

function renumber(collectionNode) {
    //Changes labels, move buttons, but not remove button
    var collectionName0, collectionName, fieldsetNodes, numItems, k, inputArray, numInputs;
    //Base name for labels
    if (collectionNode.getElementsByTagName("fieldset")[0].getElementsByTagName("legend").length > 0) {
        collectionName0 = collectionNode.getElementsByTagName("fieldset")[0].getElementsByTagName("legend")[0].innerHTML;
        collectionName = collectionName0.substring(0, collectionName0.indexOf(" "));
    } else {
        collectionName = "";
    }
    
    fieldsetNodes = fieldsetChildren(collectionNode);   //Get only first generation children
    numItems = fieldsetNodes.length;
    
    for (k = 0; k < numItems; k++) {
        if (collectionName != "") {
            fieldsetNodes[k].getElementsByTagName("legend")[0].innerHTML = collectionName + " " + k;
        }
        
        inputArray = fieldsetNodes[k].getElementsByTagName("input");
        numInputs = inputArray.length;
        
        //Move up button - disable on 1st visible entry - variables have a hidden fieldset
        if (k == 0 || (collectionName == "" && k == 1)) {
            inputArray[numInputs-2].disabled = true;
        } else {
            inputArray[numInputs-2].disabled = false;
        }
        
        //Move down button - disable on last entry
        if (k == numItems - 1) {
            inputArray[numInputs-1].disabled = true;
        } else {
            inputArray[numInputs-1].disabled = false;
        }
        
        if (collectionName == "Interval") {
            //Interval start time default label
            if (k == 0) {
                collectionNode.getElementsByClassName("startTimeDefaultLabel")[k].innerHTML = "Start interval time from 0";
            } else {
                collectionNode.getElementsByClassName("startTimeDefaultLabel")[k].innerHTML = "Continue time from previous interval";
            }
            
            //Force end time to be set if not last interval
            if (k == numItems - 1) {
                collectionNode.getElementsByClassName("continueToGlobalEnd")[k].hidden = false;
            } else {
                var endSpan, endInputs;
                endSpan = collectionNode.getElementsByClassName("continueToGlobalEnd")[k];
                endSpan.hidden = true;
                endInputs = endSpan.parentNode.getElementsByTagName("input");
                endInputs[0].checked = false;
                endInputs[1].checked = true;
                endInputs[2].disabled = false;
            }
        } else if (collectionName == "Motor") {
            //Hide set spacing on first motor
            if (k == 0) {
                collectionNode.getElementsByClassName("setSpacing")[k].hidden = true;
            } else {
                collectionNode.getElementsByClassName("setSpacing")[k].hidden = false;
            }
        }
    }
}

//Events
function tickToEnable(checkBox) {
    //Enables the next element if box is ticked or radio "use" is selected and vice versa
    var otherBox, useOtherBox, overrideSpans, numSpans, spanId;
    if (checkBox.type == "radio") {
        //Only to be used for radio buttons in a fieldset
        //Go up to get a fieldset
        otherBox = checkBox;
        do {
            otherBox = otherBox.parentNode;
        } while (otherBox.nodeName != "FIELDSET");
        otherBox = otherBox.lastElementChild;
        useOtherBox = (checkBox.value == "set");
    } else {
        otherBox = checkBox.parentNode.nextElementSibling;
        useOtherBox = checkBox.checked;
    }
    otherBox.disabled = !useOtherBox;
    
    //Send focus to text box if applicable
    if (useOtherBox) {
        otherBox.focus();
    }
    
    if (checkBox.name == "defaultSpacing") {
        overrideSpans = document.getElementsByClassName("overrideSpacingText");
        numSpans = overrideSpans.length;
        if (useOtherBox) {
            for (spanId = 0; spanId < numSpans; spanId++) {
                overrideSpans[spanId].innerHTML = "Override default spacing";
                otherBox = overrideSpans[spanId].previousElementSibling;
                otherBox.hidden = false;
                tickToEnable(otherBox); //Enable text box according to tick state
            }
        } else {
            for (spanId = 0; spanId < numSpans; spanId++) {
                overrideSpans[spanId].innerHTML = "Spacing";
                overrideSpans[spanId].previousElementSibling.hidden = true;
                overrideSpans[spanId].parentNode.nextElementSibling.disabled = false;   //Enable text box
            }
        }
    } else if (checkBox.name == "defaultTravelPerStep") {
        overrideSpans = document.getElementsByClassName("overrideTravelPerStepText");
        numSpans = overrideSpans.length;
        if (useOtherBox) {
            for (spanId = 0; spanId < numSpans; spanId++) {
                overrideSpans[spanId].innerHTML = "Override default vertical";
                otherBox = overrideSpans[spanId].previousElementSibling;
                otherBox.hidden = false;
                tickToEnable(otherBox); //Enable text box according to tick state
            }
        } else {
            for (spanId = 0; spanId < numSpans; spanId++) {
                overrideSpans[spanId].innerHTML = "Vertical";
                overrideSpans[spanId].previousElementSibling.hidden = true;
                overrideSpans[spanId].parentNode.nextElementSibling.disabled = false;   //Enable text box
            }
        }
    }
}

function tickToHide(checkBox) {
    //Hides the following element, typically a span, when ticked
    checkBox.parentNode.nextElementSibling.hidden = checkBox.checked;
}

function changeReverseAll(checkBox) {
    var reverseDivs, numItems, itemId;
    reverseDivs = document.getElementsByClassName("reverseMotor");
    numItems = reverseDivs.length;
    if (checkBox.checked) {
        //Reverse all ticked
        for (itemId = 0; itemId < numItems; itemId++) {
            reverseDivs[itemId].hidden = true;
        }
    } else {
        for (itemId = 0; itemId < numItems; itemId++) {
            reverseDivs[itemId].hidden = false;
        }
    }
}

function removeIt(buttonPressed) {
    //Removes the fieldset containing the button and renumbers as appropriate
    //Button is disabled when on master BBB
    var removedNode, collectionNode, inputArray, numInputs;
    removedNode = buttonPressed.parentNode;
    collectionNode = removedNode.parentNode;
    collectionNode.removeChild(removedNode);
    renumber(collectionNode);
    
    //Disable remove button if only one interval is left - ok to apply to BBB too
    if (fieldsetChildren(collectionNode).length == 1) {
        inputArray = collectionNode.getElementsByTagName("fieldset")[0].getElementsByTagName("input");
        numInputs = inputArray.length;
        inputArray[numInputs-3].disabled = true;
    }
}

function moveUp(buttonPressed) {
    var currentItem, previousItem, fieldsetNode;
    currentItem = buttonPressed.parentNode;
    previousItem = currentItem.previousElementSibling;
    fieldsetNode = currentItem.parentNode;
    fieldsetNode.insertBefore(currentItem, previousItem);
    renumber(fieldsetNode);
}

function moveDown(buttonPressed) {
    var currentItem, nextItem, fieldsetNode;
    currentItem = buttonPressed.parentNode;
    nextItem = currentItem.nextElementSibling.nextElementSibling;
    fieldsetNode = currentItem.parentNode;
    fieldsetNode.insertBefore(currentItem, nextItem);
    renumber(fieldsetNode);
}

//Waveform
function specifyWaveChangeMaster() {
    //Shows/hides waveform specification depending on radio button
    var pinswaveTexts, numPinswaveTexts, overrideSpans, k;
    pinswaveTexts = document.getElementsByClassName("pinswave");
    numPinswaveTexts = pinswaveTexts.length;    //Avoid re-evaluating each for loop
    overrideSpans = document.getElementsByClassName("motor0xPosOverride");
    if (document.querySelector("input[name=specifyWave]:checked").value == "specify") {
        document.getElementById("waveform").hidden = false;
        for (k = 0; k < numPinswaveTexts; k++) {
            pinswaveTexts[k].innerHTML = "pins";
            pinswaveTexts[k].href = "pinswave_wizard.html?type=pins";
            overrideSpans[k].hidden = false;
        }
    } else {
        document.getElementById("waveform").hidden = true;
        for (k = 0; k < numPinswaveTexts; k++) {
            pinswaveTexts[k].innerHTML = "wave";
            pinswaveTexts[k].href = "pinswave_wizard.html?type=wave";
            overrideSpans[k].hidden = true;
        }
    }
}

function specifyWaveChangePins() {
    //Shows/hides waveform specification depending on radio button
    var waveDivs, divId, numDivs;
    waveDivs = document.querySelectorAll(".wave");
    numDivs = waveDivs.length;
    if (document.querySelector("input[name=specifyWave]:checked").value == "specify") {
        for (divId = 0; divId < numDivs; divId++) {
            waveDivs[divId].hidden = false;
        }
    } else {
        for (divId = 0; divId < numDivs; divId++) {
            waveDivs[divId].hidden = true;
        }
    }
}

function addVar(buttonPressed) {
    //Adds a custom variable
    var parentNode, blankVar;
    
    parNode = buttonPressed.parentNode;
    blankVar = parNode.getElementsByTagName("fieldset")[0].cloneNode(true);
    
    //Unhide - everything else is already blank
    blankVar.hidden = false;
    
    parNode.insertBefore(blankVar, buttonPressed);
    renumber(parNode);
}

function addInt(buttonPressed) {
    //Adds an interval
    var waveformNode, fieldsetNodes, intervalsCreated, blankInt, inputList;
    
    waveformNode = buttonPressed.parentNode;
    fieldsetNodes = fieldsetChildren(waveformNode);
    intervalsCreated = document.getElementById("inputForm").elements.namedItem("intervalsCreated").value;
    blankInt = fieldsetNodes[0].cloneNode(true);
    
    //Enable remove button
    fieldsetNodes[0].getElementsByTagName("input")[10].disabled = false;
    
    inputList = blankInt.getElementsByTagName("input");
    
    //Rename radio buttons to avoid clash
    inputList[0].name = "startSet" + intervalsCreated;
    inputList[1].name = "startSet" + intervalsCreated;
    inputList[3].name = "endSet" + intervalsCreated;
    inputList[4].name = "endSet" + intervalsCreated;
    
    //Update hidden field
    document.getElementById("inputForm").elements.namedItem("intervalsCreated").value = Number(intervalsCreated) + 1;
    
    //Clear data - getElementsByTagName() gets all descendents
    inputList[1].checked = false;   //Deselect radio button
    inputList[0].checked = true;   //Follow on start time
    inputList[2].disabled = true;  //Specified start time
    inputList[2].value = "";
    inputList[4].checked = false;   //Deselect radio button
    inputList[3].checked = true;   //Continue until global end time
    inputList[5].disabled = true;  //Specified end time
    inputList[5].value = "";
    inputList[6].chcked = false;    //Stationary
    inputList[7].value = "";   //Displacement function
    inputList[8].checked = false;  //Periodic
    inputList[9].disabled = true;  //Period
    inputList[9].value = "";
    inputList[10].disabled = false; //Remove button
    
    waveformNode.insertBefore(blankInt, buttonPressed);
    
    //Renumber legend and activate move buttons
    renumber(waveformNode);
}

//BBBs

//Events
function changeMasterBBB(radioClicked) {
    var collectionNode, numBBBs, k, masterStatus;
    collectionNode = radioClicked.parentNode.parentNode.parentNode; //Up: label, fieldset, fieldset
    numBBBs = fieldsetChildren(collectionNode).length;
    
    if (numBBBs == 1) {
        //namedItem is not a list
        document.getElementsByClassName("ip")[0].hidden = true;
        document.getElementById("inputForm").elements.namedItem("removeB").disabled = true;
    } else {
        //namedItem is a list
        for (k = 0; k < numBBBs; k++) {
            masterStatus = document.getElementById("inputForm").elements.namedItem("masterBBB")[k].checked;
            document.getElementsByClassName("ip")[k].hidden = masterStatus;
            document.getElementById("inputForm").elements.namedItem("removeB")[k].disabled = masterStatus;
        }
    }
}

function addBBB(buttonPressed) {
    var BBBsInput, blankBBB, inputList;
    
    BBBsInput = buttonPressed.parentNode;
    blankBBB = BBBsInput.getElementsByTagName("fieldset")[0].cloneNode(true);
    
    //Clear data
    inputList = blankBBB.getElementsByTagName("input");
    inputList[0].checked = false;   //Master
    inputList[1].value = "";    //IP
    inputList[2].value = "";    //Binary
    inputList[3].value = "";    //Pins
    inputList[4].checked = false;
    inputList[5].value = "";
    inputList[6].disabled = false;  //Remove BBB
    
    blankBBB.getElementsByTagName("span")[0].hidden = false;
    BBBsInput.insertBefore(blankBBB, buttonPressed);
    
    renumber(BBBsInput);
}

//Motors
function addMotor(buttonPressed) {
    var motorFieldset, fieldsetNodes, blankMotor, blankInterval, intervalNodes, k, inputList;
    
    motorFieldset = buttonPressed.parentNode;
    fieldsetNodes = fieldsetChildren(motorFieldset);
    
    //Enable remove button (or is already enabled)
    fieldsetNodes[0].querySelector("input.removeMotor").disabled = false;
    
    blankMotor = fieldsetNodes[0].cloneNode(true);
    
    //Remove all intervals
    intervalNodes = blankMotor.getElementsByClassName("interval");
    numIntervals = intervalNodes.length;
    for (k = numIntervals - 1; k >= 0; k--) {
        intervalNodes[0].parentNode.removeChild(intervalNodes[k]);
    }
    
    //Clear data
    inputList = blankMotor.getElementsByTagName("input");
    inputList[0].value = "";
    inputList[1].value = "";
    inputList[2].value = "";
    inputList[3].value = "";
    inputList[4].value = "";
    inputList[5].value = "";
    
    //Add a new interval in existing motor and move across - ensures unique ID is created for radio buttons
    addInt(fieldsetNodes[0].querySelector("input[name=addInterval]"));
    intervalNodes = fieldsetNodes[0].getElementsByClassName("interval");
    blankInterval = intervalNodes[0].parentNode.removeChild(intervalNodes[intervalNodes.length - 1]);
    renumber(intervalNodes[0].parentNode);    //Renumber intervals now that a node has been removed
    if (intervalNodes.length == 1) {
        //Disable remove button
        intervalNodes[0].querySelector("input[name=removeInterval]").disabled = true;
    }
    //Insert into new motor
    blankMotor.querySelector("div.wave").insertBefore(blankInterval, inputList[9]);
    //Disable remove interval button
    blankMotor.querySelector("input[name=removeInterval]").disabled = true;
    renumber(blankMotor.querySelector("div.wave"));
    
    //Insert new motor
    motorFieldset.insertBefore(blankMotor, buttonPressed);
    renumber(motorFieldset);    //Renumber motors
}

//Generate XML file
function saveFileMaster() {
    //Generate file - encoding should be UTF-8, although UTF-16 and UCS-4 will be handled appropriately by compiler
    var xmlString, parser, outDoc, rootNode, firstNode, secondNode, contentField, inputList, formElements, intervalSet, numIntervals, intervalID, serialiser, xmlString, filename, xmlBlob, docBlob, url, binaryNames, IPAddresses, rtn, tDependentVars, tIndependentVars;
    
    xmlString = "<?xml version=\"1.0\"?><master />";
    parser = new DOMParser();
    outDoc = parser.parseFromString(xmlString, "application/xml");
    rootNode = outDoc.documentElement;
    
    //Populate XML DOM, validating input
    formElements = document.getElementById("inputForm").elements;
    
    if (document.querySelector("input[name=specifyWave]:checked").value == "specify") {
        firstNode = outDoc.createElement("wave");
        rootNode.appendChild(firstNode);
        
        contentField = formElements.namedItem("globalEnd");
        if (contentField.checkValidity() == false) {
            window.alert("The maximum wave duration must be a non-negative number and is required.");
            contentField.focus();
            return 2;
        }
        firstNode.setAttribute("end", contentField.value);
        
        contentField = formElements.namedItem("initialSplit");
        if (contentField.checkValidity() == false || Number(contentField.value) == 0) {
            window.alert("The initial evaluation time step must be a strictly positive number and is required.");
            contentField.focus();
            return 2;
        }
        firstNode.setAttribute("initialSplit", contentField.value);
        
        if (formElements.namedItem("reduceResolution").checked) {
            contentField = formElements.namedItem("resolution");
            if (contentField.checkValidity() == false) {
                window.alert("Effective clock period must be a positive number.");
                contentField.focus();
                return 2;
            }
            firstNode.setAttribute("resolution", contentField.value);
        }
        
        //Add custom variables
        intervalSet = document.getElementsByClassName("variable");
        numIntervals = intervalSet.length;
        
        //Create list of recognised variables
        tDependentVars = ["t"];
        tIndependentVars = ["x", "pi"];
        
        //Don't add the first variable as it is hidden and empty
        for (intervalID = 1; intervalID < numIntervals; intervalID++) {
            secondNode = outDoc.createElement("def");
            firstNode.appendChild(secondNode);
            inputList = intervalSet[intervalID].getElementsByTagName("input");
            
            //Function string
            contentField = inputList[1];
            rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
            if (rtn < 0) {
                //Error message already displayed
                contentField.focus();
                return 2;
            }
            secondNode.textContent = contentField.value;
            
            //Name
            contentField = inputList[0];
            if (contentField.checkValidity() == false) {
                window.alert("Variable name can only contain alphanumeric characters and underscores, and must begin with a letter.");
                contentField.focus();
                return 2;
            }
            secondNode.setAttribute("name", contentField.value);
            
            //Check uniqueness of variable name
            if (tDependentVars.indexOf(contentField.value) >= 0 || tIndependentVars.indexOf(contentField.value) >= 0) {
                window.alert("Variable " + contentField.value + " defined twice.");
                contentField.focus();
                return 2;
            }
            
            //Save variable to appropriate list of defined variables
            if (rtn == 1) {
                //Custom variable depends on t
                tDependentVars.push(contentField.value);
            } else {
                tIndependentVars.push(contentField.value);
            }
        }
        
        //Add intervals
        intervalSet = document.getElementsByClassName("interval");
        numIntervals = intervalSet.length;
        
        for (intervalID = 0; intervalID < numIntervals; intervalID++) {
            secondNode = outDoc.createElement("interval");
            firstNode.appendChild(secondNode);
            inputList = intervalSet[intervalID].getElementsByTagName("input");
            
            if (inputList[1].checked) {
                //Specify start time
                contentField = inputList[2];
                rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
                if (rtn != 0) {
                    if (rtn == 1) {
                        window.alert("Interval start time cannot depend on t.");
                    }
                    contentField.focus();
                    return 2;
                }    //Error message already displayed
                secondNode.setAttribute("start", contentField.value);
            }
            
            if (inputList[4].checked) {
                //Specify end time
                contentField = inputList[5];
                rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
                if (rtn != 0) {
                    if (rtn == 1) {
                        window.alert("Interval end time cannot depend on t.");
                    }
                    contentField.focus();
                    return 2;
                }    //Error message already displayed
                secondNode.setAttribute("end", contentField.value);
            }
            
            if (inputList[6].checked == false) {
                //Not a blank interval
                if (inputList[8].checked) {
                    //Specify period
                    contentField = inputList[9];
                    rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
                    if (rtn != 0) {
                        if (rtn == 1) {
                            window.alert("Interval period cannot depend on t.");
                        }
                        contentField.focus();
                        return 2;
                    }    //Error message already displayed
                    secondNode.setAttribute("period", contentField.value);
                }
                
                //Function string
                contentField = inputList[7];
                if (validateFuncString(contentField.value, tDependentVars, tIndependentVars) < 0) {
                    contentField.focus();
                    return 2;
                }    //Error message already displayed
                secondNode.textContent = contentField.value;
            }
        }
    }
    
    //Add BBBs
    intervalSet = document.getElementsByClassName("bbb");
    numIntervals = intervalSet.length;
    
    //Initialise binaryNames and IPAddresses to make sure they are unique
    binaryNames = [];
    IPAddresses = [];
    
    for (intervalID = 0; intervalID < numIntervals; intervalID++) {
        firstNode = outDoc.createElement("bbb");
        rootNode.appendChild(firstNode);
        
        inputList = intervalSet[intervalID].getElementsByTagName("input");
        
        //IP
        secondNode = outDoc.createElement("ip");
        firstNode.appendChild(secondNode);
        if (inputList[0].checked == false) {
            contentField = inputList[1];
            if (!contentField.checkValidity()) {
                window.alert("IP address cannot be empty");
                contentField.focus();
                return 2;
            }
            //Check it is unique
            if (IPAddresses.indexOf(contentField.value) >= 0) {
                window.alert("IP address must be unique");
                contentField.focus();
                return 2;
            }
            IPAddresses.push(contentField.value);
            secondNode.textContent = contentField.value;
        }
        
        //Binary filename
        contentField = inputList[2];
        if (contentField.checkValidity() == false) {
            window.alert("Invalid file path");
            contentField.focus();
            return 2;
        }
        //Check it is unique
        if (binaryNames.indexOf(contentField.value) >= 0) {
            window.alert("Path to save compiled file must be unique");
            contentField.focus();
            return 2;
        }
        binaryNames.push(contentField.value);
        //Add node
        secondNode = outDoc.createElement("binary");
        secondNode.textContent = contentField.value;
        firstNode.appendChild(secondNode);
        
        //Pins filename
        contentField = inputList[3];
        if (!contentField.checkValidity()) {
            window.alert("Invalid file path");
            contentField.focus();
            return 2;
        }
        secondNode = outDoc.createElement("pins");
        secondNode.textContent = contentField.value;
        firstNode.appendChild(secondNode);
        
        //Motor0xPos
        if (inputList[4].checked) {
            contentField = inputList[5];
            if (!contentField.checkValidity()) {
                window.alert("x coordinate of first motor must be a number.");
                contentField.focus();
                return 2;
            }
            secondNode.setAttribute("motor0xPos", contentField.value);
        }
    }
    
    //Save file
    serialiser = new XMLSerializer();
    xmlString = serialiser.serializeToString(outDoc);
    
    //Source: https://stackoverflow.com/questions/13405129/javascript-create-and-save-file
    contentField = formElements.namedItem("outfname");
    if (!contentField.checkValidity()) {
        window.alert("Invalid output filename");
        contentField.focus();
        return 2;
    }
    filename = contentField.value;
    
    xmlBlob = new Blob([xmlString], { type: "application/xml" });  //Change to xml data type
    if (window.navigator.msSaveBlob) {
        //Microsoft
        window.navigator.msSaveBlob(xmlBlob, filename);
    } else {
        //Other browsers
        docBlob = document.createElement("a");
        url = URL.createObjectURL(xmlBlob);
        docBlob.href = url;
        docBlob.download = filename;
        document.body.appendChild(docBlob);
        docBlob.click();
        setTimeout(function () {
            document.body.removeChild(docBlob);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

function saveFilePinsCaller() {
    //Loads FuncStringToTree.xsl if required - only for wave files
    if (document.querySelector("input[name=specifyWave]:checked").value == "specify" && !xslFuncStringToTree) {
        if (XMLHttpRequest) {
            //Load FuncStringToTree.xsl for later
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) { //HTTP status may be undefined for local access so don't use
                    if (!xhttp.responseXML) {
                        //Could not automatically load FuncStringToTree.xsl, possibly due to security settings. Enable manual loading.
                        document.getElementById("mainView").hidden = true;
                        document.getElementById("FuncXSLLabel").hidden = false;
                    } else {
                        //Automatic loading succeeded
                        //Create only once for speed, as only ever use the same XSLT
                        xslFuncStringToTree = new XSLTProcessor();
                        //Only gets to this point if valid XML - assume right file
                        xslFuncStringToTree.importStylesheet(xhttp.responseXML);
                        //Generate output file
                        saveFilePins();
                    }
                }
            };
            try {
                xhttp.open("GET", "FuncStringToTree.xsl", true);    //Load asynchronously
                xhttp.send();
            } catch (err) {
                document.getElementById("mainView").hidden = true;
                document.getElementById("FuncXSLLabel").hidden = false;
            }
        } else {
            //Don't have XMLHttpRequest
            document.getElementById("mainView").hidden = true;
            document.getElementById("FuncXSLLabel").hidden = false;
        }
    } else {
        setTimeout(saveFilePins, 0);
    }
}

function saveFilePins() {
    //Generate file - encoding should be UTF-8, although UTF-16 and UCS-4 will be handled appropriately by compiler
    var xmlString, parser, outDoc, rootNode, waveDoc, motorNode, intervalNode, contentField, inputList, formElements, motorSet, numMotors, motorId, intervalSet, numIntervals, intervalID, serialiser, xmlString, filename, xmlBlob, docBlob, url, rtn;
    
    xmlString = "<?xml version=\"1.0\"?><wave />";
    parser = new DOMParser();
    outDoc = parser.parseFromString(xmlString, "application/xml");
    rootNode = outDoc.documentElement;
    
    //Pins or wave?
    if (document.querySelector("input[name=specifyWave]:checked").value == "specify") {
        waveDoc = true;
    } else {
        waveDoc = false;
    }
    
    //Populate XML DOM, validating input
    formElements = document.getElementById("inputForm").elements;
    
    contentField = formElements.namedItem("pinsmap");
    if (contentField.checkValidity() == false) {
        window.alert("Invalid file path");
        contentField.focus();
        return 2;
    }
    rootNode.setAttribute("gpioMapFile", contentField.value);
    
    contentField = formElements.namedItem("mindisplacement");
    if (contentField.checkValidity() == false) {
        window.alert("Displacement must be a number and is required.");
        contentField.focus();
        return 2;
    }
    rootNode.setAttribute("minDisplacement", contentField.value);
    
    contentField = formElements.namedItem("maxdisplacement");
    if (contentField.checkValidity() == false) {
        window.alert("Displacement must be a number and is required.");
        contentField.focus();
        return 2;
    }
    rootNode.setAttribute("maxDisplacement", contentField.value);
    
    contentField = formElements.namedItem("maxStepRate");
    if (contentField.checkValidity() == false) {
        window.alert("Maximum step rate must be at least 255. You will need faster driver boards if you cannot satisfy this requirement.");
        contentField.focus();
        return 2;
    }
    rootNode.setAttribute("maxStepRate", contentField.value);
    
    rootNode.setAttribute("fallingEdge", formElements.namedItem("fallingEdge").value);
    
    contentField = formElements.namedItem("motor0xPos");
    if (contentField.checkValidity() == false) {
        window.alert("Position of first motor must be a number.");
        contentField.focus();
        return 2;
    }
    if (contentField.value != "") {
        //Optional attribute
        rootNode.setAttribute("motor0xPos", contentField.value);
    }
    
    if (formElements.namedItem("defaultSpacing").checked) {
        contentField = formElements.namedItem("defaultSpacingValue");
        if (contentField.checkValidity() == false || Number(contentField.value) == 0) {
            window.alert("Motor spacing must be a strictly positive number.");
            contentField.focus();
            return 2;
        }
        rootNode.setAttribute("defaultSpacing", contentField.value);
    }
    
    if (formElements.namedItem("defaultTravelPerStep").checked) {
        contentField = formElements.namedItem("defaultTravelPerStepValue");
        if (contentField.checkValidity() == false || Number(contentField.value) == 0) {
            window.alert("Travel per step must be a strictly positive number.");
            contentField.focus();
            return 2;
        }
        rootNode.setAttribute("defaultTravelPerStep", contentField.value);
    }
    
    rootNode.setAttribute("reverse", formElements.namedItem("reverseAll").checked);
    rootNode.setAttribute("invertTimeBeacon", formElements.namedItem("invertTimeBeacon").checked);
    
    if (waveDoc) {
        contentField = formElements.namedItem("globalEnd");
        if (contentField.checkValidity() == false) {
            window.alert("The maximum wave duration must be a non-negative number and is required.");
            contentField.focus();
            return 2;
        }
        rootNode.setAttribute("end", contentField.value);
        
        contentField = formElements.namedItem("initialSplit");
        if (contentField.checkValidity() == false || Number(contentField.value) == 0) {
            window.alert("The initial evaluation time step must be a strictly positive number and is required.");
            contentField.focus();
            return 2;
        }
        rootNode.setAttribute("initialSplit", contentField.value);
        
        if (formElements.namedItem("reduceResolution").checked) {
            contentField = formElements.namedItem("resolution");
            if (contentField.checkValidity() == false) {
                window.alert("Effective clock period must be a positive number.");
                contentField.focus();
                return 2;
            }
            rootNode.setAttribute("resolution", contentField.value);
        }
    }
    
    //Add custom variables
    if (waveDoc) {
        intervalSet = document.getElementsByClassName("variable");
        numIntervals = intervalSet.length;
        
        //Create list of recognised variables
        tDependentVars = ["t"];
        tIndependentVars = ["x", "pi"];
        
        //Don't add the first interval as it is hidden and empty
        for (intervalID = 1; intervalID < numIntervals; intervalID++) {
            secondNode = outDoc.createElement("def");
            firstNode.appendChild(secondNode);
            inputList = intervalSet[intervalID].getElementsByTagName("input");
            
            //Function string
            contentField = inputList[1];
            rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
            if (rtn < 0) {
                //Error message already displayed
                contentField.focus();
                return 2;
            }
            secondNode.textContent = contentField.value;
            
            //Name
            contentField = inputList[0];
            if (contentField.checkValidity() == false) {
                window.alert("Variable name can only contain alphanumeric characters and underscores, and must begin with a letter.");
                contentField.focus();
                return 2;
            }
            secondNode.setAttribute("name", contentField.value);
            
            //Check uniqueness of variable name
            if (tDependentVars.indexOf(contentField.value) >= 0 || tIndependentVars.indexOf(contentField.value) >= 0) {
                window.alert("Variable " + contentField.value + " defined twice.");
                contentField.focus();
                return 2;
            }
            
            //Save variable to appropriate list of defined variables
            if (rtn == 1) {
                //Custom variable depends on t
                tDependentVars.push(contentField.value);
            } else {
                tIndependentVars.push(contentField.value);
            }
        }
    }
    
    //Add motors
    motorSet = document.getElementsByClassName("motor");
    numMotors = motorSet.length;
    
    //Initialise binaryNames
    binaryNames = [];
    
    for (motorId = 0; motorId < numMotors; motorId++) {
        motorNode = outDoc.createElement("motor");
        rootNode.appendChild(motorNode);
        
        inputList = motorSet[motorId].getElementsByTagName("input");
        
        //Pulse pin
        contentField = inputList[0];
        if (contentField.checkValidity() == false) {
            window.alert("Header number must be a positive integer and is required.");
            contentField.focus();
            return 2;
        }
        motorNode.setAttribute("pulseHeader", contentField.value);
        
        contentField = inputList[1];
        if (contentField.checkValidity() == false) {
            window.alert("Pin number must be a positive integer and is required.");
            contentField.focus();
            return 2;
        }
        motorNode.setAttribute("pulsePin", contentField.value);
        
        //Direction pin
        contentField = inputList[2];
        if (contentField.checkValidity() == false) {
            window.alert("Header number must be a positive integer and is required.");
            contentField.focus();
            return 2;
        }
        motorNode.setAttribute("directionHeader", contentField.value);
        
        contentField = inputList[3];
        if (contentField.checkValidity() == false) {
            window.alert("Pin number must be a positive integer and is required.");
            contentField.focus();
            return 2;
        }
        motorNode.setAttribute("directionPin", contentField.value);
        
        //Spacing
        if (inputList[4].checked) {
            contentField = inputList[5];
            if (contentField.checkValidity() == false || Number(contentField.value) == 0) {
                window.alert("Motor spacing must be a strictly positive number.");
                contentField.focus();
                return 2;
            }
            motorNode.setAttribute("spacing", contentField.value);
        }
        
        if (inputList[6].checked) {
            contentField = inputList[7];
            if (contentField.checkValidity() == false || Number(contentField.value) == 0) {
                window.alert("Travel per step must be a strictly positive number.");
                contentField.focus();
                return 2;
            }
            motorNode.setAttribute("travelPerStep", contentField.value);
        }
        
        //Reverse
        motorNode.setAttribute("reverse", inputList[8].checked);
        
        if (waveDoc) {
            //Add intervals
            intervalSet = motorSet[motorId].getElementsByClassName("interval");
            numIntervals = intervalSet.length;
            
            for (intervalID = 0; intervalID < numIntervals; intervalID++) {
                intervalNode = outDoc.createElement("interval");
                motorNode.appendChild(intervalNode);
                inputList = intervalSet[intervalID].getElementsByTagName("input");
                
                if (inputList[1].checked) {
                    //Specify start time
                    contentField = inputList[2];
                    rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
                    if (rtn != 0) {
                        if (rtn == 1) {
                            window.alert("Interval start time cannot depend on t.");
                        }
                        contentField.focus();
                        return 2;
                    }    //Error message already displayed
                    intervalNode.setAttribute("start", contentField.value);
                }
                
                if (inputList[4].checked) {
                    //Specify end time
                    contentField = inputList[5];
                    rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
                    if (rtn != 0) {
                        if (rtn == 1) {
                            window.alert("Interval end time cannot depend on t.");
                        }
                        contentField.focus();
                        return 2;
                    }    //Error message already displayed
                    intervalNode.setAttribute("end", contentField.value);
                }
                
                if (inputList[6].checked == false) {
                    //Not a blank interval
                    if (inputList[8].checked) {
                        //Specify period
                        contentField = inputList[9];
                        rtn = validateFuncString(contentField.value, tDependentVars, tIndependentVars);
                        if (rtn != 0) {
                            if (rtn == 1) {
                                window.alert("Interval period cannot depend on t.");
                            }
                            contentField.focus();
                            return 2;
                        }    //Error message already displayed
                        intervalNode.setAttribute("period", contentField.value);
                    }
                    
                    //Function string
                    contentField = inputList[7];
                    if (validateFuncString(contentField.value, tDependentVars, tIndependentVars) < 0) {
                        contentField.focus();
                        return 2;
                    }    //Error message already displayed
                    intervalNode.textContent = contentField.value;
                }
            }
        }
    }
    
    //Save file
    serialiser = new XMLSerializer();
    xmlString = serialiser.serializeToString(outDoc);
    
    //Source: https://stackoverflow.com/questions/13405129/javascript-create-and-save-file
    contentField = formElements.namedItem("outfname");
    if (!contentField.checkValidity()) {
        window.alert("Invalid output filename");
        contentField.focus();
        return 2;
    }
    filename = contentField.value;
    
    xmlBlob = new Blob([xmlString], {type: "application/xml"});  //Change to xml data type
    if (window.navigator.msSaveBlob) {
        //Microsoft
        window.navigator.msSaveBlob(xmlBlob, filename);
    } else {
        //Other browsers
        docBlob = document.createElement("a");
        url = URL.createObjectURL(xmlBlob);
        docBlob.href = url;
        docBlob.download = filename;
        document.body.appendChild(docBlob);
        docBlob.click();
        setTimeout(function () {
            document.body.removeChild(docBlob);
            URL.revokeObjectURL(url);
        }, 0);
    }
}
