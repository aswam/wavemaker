//Provides networking interface for a slave BBB

/*Version control
 v0.01 TD 13/04/15
 v0.02 TD 15/04/15 Completed with pause and abort capability
 v0.03 TD 16/04/15 Error corrected in receiving file name
 v0.04 TD 17/04/15 Bug fixes
 v0.05 TD 23/04/15 Updated to ctrl_pruss v0.09 with pins file
 v0.06 TD 27/04/15 Removed read receipt, moved complete message to run_pruss
 v0.07 TD 28/04/15 Extra argument in run_pruss
 v0.08 TD 06/05/15 Allowed for different operation modes
 v0.09 TD 11/05/15 Added modes b and x (untested)
 v0.10 TD 12/05/15 Bug fixes
 v0.11 TD 13/05/15 Removed spacing attribute
 v0.12 TD 15/05/15 Memory leak fixes
 v0.13 TD 20/05/15 Updated to v0.21 of ctrl_pruss.c
 v0.14 TD 19/01/16 Updated to v0.01 of compiler.c, removed redundant headers
 v0.15 TD 09/02/16 Macros for modes and messages
 v0.16 TD 01/12/16 Return compile status to master
 v0.17 TD 02/02/17 XML parser made tread safe
 v0.18 TD 05/04/17 Return wave duration
 v0.19 TD 08/04/17 Each interval created in own thread
 v0.20 TD 20/04/17 Use snprintf and initialise XPath
 v0.21 TD 23/05/17 Number of instructions in performance stats
 v0.22 TD 21/11/17 Abort compile on error and return some messages
 */

#include <netdb.h>
#include <arpa/inet.h>
#include "ctrl_pruss.h"

#define QUEUESIZE 1

struct listen_params {
    //For listenStopSig()
    int socket;
    int *stopSig;       //Tells all compiler threads to stop
    int *stopListen;    //Tells listener thread to terminate
};

void* listenStopSig(void *params)
{
    //Listens for stop message from slave BBBs - to be run in separate thread
    struct listen_params *params_struct = (struct listen_params*) params;
    
    char buffer[1];
  
    while (*(params_struct->stopListen) == 0) {
        if (recv(params_struct->socket, buffer, 1, MSG_DONTWAIT) == 1) {
            switch (buffer[0]) {
                case COMPILE_FAIL:
                    *(params_struct->stopSig) = 1;
                case COMPILE_SUCCESS:
                    break;
                default:
                    fprintf(stderr, "WARNING: Unexpected message from the master: %c\n", buffer[0]);
            }
            break;
        }
        //If recv() fails due to broken connection, will keep retrying until stopListen is set, which occurs on a compile outcome from this BBB. This is a little crude but works.
        SLEEP_MS(1000); //Wait 1s before trying again
    }
    
    return NULL;
}
    
int modeb(int comsocket, rlim_t filelimit)
//Compiles binary file from a given wave "pins" XML file
{
    unsigned char buffer[256];
    
    //Receive size of address of binary file (destination)
    int status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    int filenamesize = (int) buffer[0];
    int bytesrcv = 0;
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    //Terminate string with null character
    buffer[filenamesize] = '\0';
    
    char binaryfilename[256];
    strcpy(binaryfilename, (char*) buffer);
    
    //Receive size of address of pins file
    status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    filenamesize = (int) buffer[0];
    bytesrcv = 0;
    //To reach this point, status >= 1
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    //Terminate string with null character
    buffer[filenamesize] = '\0';
    
    //Compiled wave length for performance benchmarking
    double wave_end;
    long int wave_instructions;
    
    //Open file counters
    int BBBthreads = 1;
    int motorthreads = 0;
    pthread_mutex_t ThreadCounterMutex;
    if (pthread_mutex_init(&ThreadCounterMutex, NULL)) {
        perror("ERROR: Could not initialise mutex");
        return 6;
    }
    pthread_cond_t ThreadCounterCondition;
    if (pthread_cond_init(&ThreadCounterCondition, NULL)) {
        perror("ERROR: Could not initialise mutex condition");
        pthread_mutex_destroy(&ThreadCounterMutex);
        return 6;
    }
    
    //Start listening for compile failures from slaves
    pthread_t listenThread;
    pthread_attr_t listenThreadAttr;
    struct listen_params listenThreadParams;
    int stopSig = 0;
    int stopListen = 0;
    listenThreadParams.socket = comsocket;
    listenThreadParams.stopSig = &stopSig;
    listenThreadParams.stopListen = &stopListen;
    pthread_attr_init(&listenThreadAttr);
    pthread_attr_setdetachstate(&listenThreadAttr, PTHREAD_CREATE_DETACHED);
    
    //Don't bother checking whether thread was created successfully, as will never need to be joined for terminationi: set stopListen = 1
    pthread_create(&listenThread, &listenThreadAttr, listenStopSig, (void*)&listenThreadParams);
    pthread_attr_destroy(&listenThreadAttr);
    
    //Compile binary file
    int compile_status = make_binary(&wave_end, &wave_instructions, (char*) buffer, (char*) binaryfilename, 0, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
    
    //Cleanup pthreads
    stopListen = 1; //Stop listener thread
    pthread_mutex_destroy(&ThreadCounterMutex);
    pthread_cond_destroy(&ThreadCounterCondition);
    
    //Send outcome of compile to master
    int bufferLength;
    if (compile_status) {
        buffer[0] = COMPILE_FAIL;
        buffer[1] = compile_status; //Send error message
        bufferLength = 2;
    } else {
        buffer[0] = COMPILE_SUCCESS;
        bufferLength = 1;
    }
    
    int bytessent = 0;
    status = 1;
    while (bytessent < bufferLength && status > 0) {
        status = send(comsocket, buffer + bytessent, bufferLength - bytessent, MSG_NOSIGNAL);
        bytessent += status;
    }
    
    if (status < 1) {
        perror("Connection closed unexpectedly");
    } else {
        //Send wave_end to master as string to retain portability
        if (compile_status == 0) {
            char numString[25];
            unsigned char numString_len;
            int bytessent;
            
            //Send wave_end
            snprintf(numString, 25, "a%.16g", wave_end);      //a is to fill first character to be overwritten shortly
            numString_len = strlen(numString) - 1;
            numString[0] = numString_len;      //Message size
            
            //Keep sending number until all is sent
            bytessent = 0;
            status = 1;
            while (bytessent < (numString_len + 1) && status > 0) {
                status = send(comsocket, numString + bytessent, (numString_len + 1) - bytessent, MSG_NOSIGNAL);
                bytessent += status;
            }
            if (status < 1) {
                perror("Connection closed unexpectedly");
            }
            
            //Send wave_instructions
            snprintf(numString, 25, "a%lu", wave_instructions);      //a is to fill first character to be overwritten shortly
            numString_len = strlen(numString) - 1;
            numString[0] = numString_len;      //Message size
            
            //Keep sending number until all is sent
            bytessent = 0;
            status = 1;
            while (bytessent < (numString_len + 1) && status > 0) {
                status = send(comsocket, numString + bytessent, (numString_len + 1) - bytessent, MSG_NOSIGNAL);
                bytessent += status;
            }
            if (status < 1) {
                perror("Connection closed unexpectedly");
            }
        }
    }
    
    return compile_status;
}

int modex(int comsocket, rlim_t filelimit)
//Compiles binary file from an (x,t) wave instruction
{
    unsigned char buffer[256];
    int status, bytesrcv, filenamesize, bufferLength;
    
    //Receive startPos (double) - the x position of motor 0
    status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    filenamesize = (int) buffer[0];
    bytesrcv = 0;
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    buffer[filenamesize] = '\0';
    double startPos = atof((const char*) buffer);
    
    //Receive size of <wave> XML document fragment
    status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    filenamesize = (int) buffer[0];
    bytesrcv = 0;
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    buffer[filenamesize] = '\0';
    filenamesize = atoi((const char*) buffer);
    
    //Receive <wave> XML document fragment
    char custombuffer[filenamesize + 1];
    bytesrcv = 0;
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, custombuffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    custombuffer[filenamesize] = '\0';
    
    //Receive size of address of binary file (destination)
    status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    filenamesize = (int) buffer[0];
    bytesrcv = 0;
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    //Terminate string with null character
    buffer[filenamesize] = '\0';
    
    char binaryfilename[256];
    strcpy(binaryfilename, (const char*) buffer);
    
    //Receive size of address of pins file
    status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    filenamesize = (int) buffer[0];
    bytesrcv = 0;
    //To reach this point, status >= 1
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    //Terminate string with null character
    buffer[filenamesize] = '\0';
    
    //Buffer contains address of pins file, which now needs to be read and modified with wave fragment
    char pinsfilename[256];
    strcpy(pinsfilename, (const char*) buffer);
    
    //Compiled wave length for performance benchmarking
    double wave_end;
    long int wave_instructions;
    
    //Open file counters
    int BBBthreads = 1;
    int motorthreads = 0;
    pthread_mutex_t ThreadCounterMutex;
    if (pthread_mutex_init(&ThreadCounterMutex, NULL)) {
        perror("ERROR: Could not initialise mutex");
        return 6;
    }
    pthread_cond_t ThreadCounterCondition;
    if (pthread_cond_init(&ThreadCounterCondition, NULL)) {
        perror("ERROR: Could not initialise mutex condition");
        pthread_mutex_destroy(&ThreadCounterMutex);
        return 6;
    }
    
    //Start listening for compile failures from slaves
    pthread_t listenThread;
    pthread_attr_t listenThreadAttr;
    struct listen_params listenThreadParams;
    int stopSig = 0;
    int stopListen = 0;
    listenThreadParams.socket = comsocket;
    listenThreadParams.stopSig = &stopSig;
    listenThreadParams.stopListen = &stopListen;
    pthread_attr_init(&listenThreadAttr);
    pthread_attr_setdetachstate(&listenThreadAttr, PTHREAD_CREATE_DETACHED);
    
    //Don't bother checking whether thread was created successfully, as will never need to be joined for terminationi: set stopListen = 1
    pthread_create(&listenThread, &listenThreadAttr, listenStopSig, (void*)&listenThreadParams);
    pthread_attr_destroy(&listenThreadAttr);
    
    int compile_status = compile_from_master(&wave_end, &wave_instructions, startPos, custombuffer, binaryfilename, pinsfilename, 0, &BBBthreads, &motorthreads, filelimit, &ThreadCounterMutex, &ThreadCounterCondition, &stopSig);
    
    //Cleanup pthreads
    stopListen = 1; //Stops detached thread
    pthread_mutex_destroy(&ThreadCounterMutex);
    pthread_cond_destroy(&ThreadCounterCondition);
    
    //Send outcome of compile to master
    if (compile_status) {
        buffer[0] = COMPILE_FAIL;
        buffer[1] = compile_status; //Send error message
        bufferLength = 2;
    } else {
        buffer[0] = COMPILE_SUCCESS;
        bufferLength = 1;
    }
    
    int bytessent = 0;
    status = 1;
    while (bytessent < bufferLength && status > 0) {
        status = send(comsocket, buffer + bytessent, bufferLength - bytessent, MSG_NOSIGNAL);
        bytessent += status;
    }
    
    if (status < 1) {
        perror("Connection closed unexpectedly");
    } else {
        //Send wave_end to master as string to retain portability
        if (compile_status == 0) {
            char numString[25];
            unsigned char numString_len;
            int bytessent;
            
            //Send wave_end
            snprintf(numString, 25, "a%.16g", wave_end);      //a is to fill first character to be overwritten shortly
            numString_len = strlen(numString) - 1;
            numString[0] = numString_len;      //Message size
            
            //Keep sending number until all is sent
            bytessent = 0;
            status = 1;
            while (bytessent < (numString_len + 1) && status > 0) {
                status = send(comsocket, numString + bytessent, (numString_len + 1) - bytessent, MSG_NOSIGNAL);
                bytessent += status;
            }
            if (status < 1) {
                perror("Connection closed unexpectedly");
            }
            
            //Send wave_instructions
            snprintf(numString, 25, "a%lu", wave_instructions);      //a is to fill first character to be overwritten shortly
            numString_len = strlen(numString) - 1;
            numString[0] = numString_len;      //Message size
            
            //Keep sending number until all is sent
            bytessent = 0;
            status = 1;
            while (bytessent < (numString_len + 1) && status > 0) {
                status = send(comsocket, numString + bytessent, (numString_len + 1) - bytessent, MSG_NOSIGNAL);
                bytessent += status;
            }
            if (status < 1) {
                perror("Connection closed unexpectedly");
            }
        }
    }
    
    return compile_status;
}

int run_wavemaker(int comsocket)
//Run a pre-compiled file on the wave maker. Returns 0 on success
{
    char buffer[256];
    
    //Receive size of address of input file
    int status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    int filenamesize = (int) buffer[0];
    int bytesrcv = 0;
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    //Terminate string with null character
    buffer[filenamesize] = '\0';
    
    char binaryfilename[256];
    strcpy(binaryfilename, buffer);
    
    //Receive size of address of pins file
    status = recv(comsocket, buffer, 1, 0);
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    filenamesize = (int) buffer[0];
    bytesrcv = 0;
    //To reach this point, status >= 1
    while (bytesrcv < filenamesize && status > 0) {
        status = recv(comsocket, buffer + bytesrcv, filenamesize - bytesrcv, 0);
        bytesrcv += status;
    }
    if (status < 1) {
        perror("Connection closed unexpectedly");
        return 5;
    }
    
    //Terminate string with null character
    buffer[filenamesize] = '\0';
    
    status = run_pruss(MODE_NET_SLAVE, binaryfilename, buffer, &comsocket, 1, 1, 0);
    
    if (status == 0) {
        printf("Operation completed successfully.\n");
    }
    
    return status;
}

int main(int argc, char *argv[])
{
    //Check and get maximum number of open files
    rlim_t filelimit = get_filelimit();
    if (filelimit == 0) { return EXIT_FAILURE; }   //Error message displayed by get_filelimit()
    
    //Setup server
    int status, listener, comsocket;
    struct sockaddr_storage client_addr; /* socket info about the machine connecting to us */
    socklen_t addr_size = sizeof(client_addr);
    struct addrinfo hints;
    struct addrinfo *servinfo;  // will point to the results
    fd_set readfds;
    char buffer[256];   //Max length is 255 + 1 null byte
    
    memset(&hints, 0, sizeof hints); // make sure the struct is empty
    hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
    hints.ai_flags = AI_PASSIVE;    //Fill in my IP automatically
    
    // get ready to connect
    status = getaddrinfo(NULL, PORTNUM, &hints, &servinfo);
    if (status != 0) {
        fprintf(stderr, "Failed to get server (self) address info: %s\n", gai_strerror(status));
        return(status);
    }
    
    listener = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (listener == -1) {
        perror("Failed to open listening socket");
        freeaddrinfo(servinfo);     //Free to avoid memory leak
        return(EXIT_FAILURE);
    }
    
    /* bind serv information to listener */
    status = bind(listener, servinfo->ai_addr, servinfo->ai_addrlen);
    freeaddrinfo(servinfo);     //Free to avoid memory leak
    if (status == -1) {
        perror("Failed to bind listening socket");
        return(EXIT_FAILURE);
    }
    
    /* start listening, allowing a queue of up to 1 pending connection */
    status = listen(listener, QUEUESIZE);
    if (status == -1) {
        perror("Failed to start listening for incoming connections");
        return(EXIT_FAILURE);
    }
    
    //Check compiled version - macro is of type void, throws an error if required
    LIBXML_TEST_VERSION;
    
    //Initialise and clean up XML parser only once to avoid multithreading issues
    xmlInitParser();
    xmlXPathInit();
    
    while (1) {
        printf("Listening for instructions from master. Press enter to exit.\n");
        
        //Add STDIN and listening socket to readfds
        FD_ZERO(&readfds);
        FD_SET(STDIN_FILENO, &readfds);
        FD_SET(listener, &readfds);
        
        //Listen, or exit if enter is pressed
        status = select(FD_SETSIZE, &readfds, NULL, NULL, NULL);
        if (FD_ISSET(STDIN_FILENO, &readfds)) {
            //Clear stdin buffer
            while (getchar() != '\n');
            break;
        }
        
        comsocket = accept(listener, (struct sockaddr *)&client_addr, &addr_size);
        if (comsocket == -1) {
            perror("Error in accepting link from master");
            continue;
        }
        printf("Connection established.\n");
        
        //Receive operating mode
        status = recv(comsocket, buffer, 1, 0);
        if (status < 1) {
            perror("Connection closed unexpectedly");
            close(comsocket);
            continue;
        }
        
        switch (buffer[0]) {
            case ASWAM_COMPILE:
                //Compile binary file from XML
                modeb(comsocket, filelimit);
                break;
            case ASWAM_RUN:
                //Run wave maker = "go"
                run_wavemaker(comsocket);
                break;
            case ASWAM_MAKEXML:
                //Make XML file
                modex(comsocket, filelimit);
                break;
            default:
                fprintf(stderr, "Unexpected message from the master: %c\n", buffer[0]);
        }
        
        //Close the socket
        close(comsocket);
    }
    
    //Cleanup XML & XSLT once all threads are done with it
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    close(listener);
    return EXIT_SUCCESS;
}
