//Maintains master clock on PRU0 - PRU0 has priority over PRU1 on scratch

//Version control
//v0.01 TD 07/01/15

//Register names
#define R_CLOCK_L r2		//Little endian
#define R_CLOCK_S r1
#define TIME_SIG_BYTE r30.b0

#define CLOCK_SCRATCH 10
#define CLOCK_TICK_BYTE R_CLOCK_S.b3
#define TICK_BIT (1 << 1)		//Connect tick sig cable to r30.t1; signal has period of 1s (approx); toggle t25

//set ARM such that PRU can write to GPIO
LBCO r0, C4, 4, 4
CLR r0, r0, 4
SBCO r0, C4, 4, 4

zero &R_CLOCK_S, 8
xor TIME_SIG_BYTE, CLOCK_TICK_BYTE, TICK_BIT

ADV_CLOCK_S:
add R_CLOCK_S, R_CLOCK_S, 1
qbne NOT_ADV_CLOCK_L, R_CLOCK_S, 0
add R_CLOCK_L, R_CLOCK_L, 1

WRITEMEM:
xout CLOCK_SCRATCH, R_CLOCK_S, 8		//Use scratch as can be written to/accessed in one clock cycle
xor TIME_SIG_BYTE, CLOCK_TICK_BYTE, TICK_BIT
qba ADV_CLOCK_S

NOT_ADV_CLOCK_L:
qba WRITEMEM