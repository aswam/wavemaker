#!/bin/bash
dpkg -i am335x-pru-package_0.20141125-1~20141201+1_armhf.deb
dpkg -i dtach_0.8-2.1_armhf.deb
dpkg -i libxml2_2.8.0+dfsg1-7+wheezy2_armhf.deb
dpkg -i libxml2-dev_2.8.0+dfsg1-7+wheezy2_armhf.deb
dpkg -i libxslt1.1_1.1.26-14.1_armhf.deb
dpkg -i libxslt1-dev_1.1.26-14.1_armhf.deb
dtc -I dts -O dtb -o ASWaM-04-00A0.dtbo -b 0 -@ ASWaM-04.dts
cp ASWaM-04-00A0.dtbo /lib/firmware/
chmod u+x ../initialise
chmod u+x ../compile
chmod u+x ../run
chmod u+x ../slave
make -C .. nodoc
echo 'The documentation has not been compiled: install a LaTeX compiler then run "make doc".'
echo 'After every reboot, run "./initialise".'
read -p 'Press Enter to reboot the system...'
shutdown -r 0