//This C code was originally written by John Burkardt, based on Fortran code by Richard Brent.

/*Version control
 v0.01 TD 20/01/16 Static variables made thread safe, only enabled in C11
 v0.02 TD 09/02/16 local_min() commented out as won't compile with optimisation
 v0.03 TD 08/04/17 Removed static variables & unused functions
 v0.04 TD 15/05/17 Changed two ifs to if-else at end of local_min_rc()
*/

#ifndef brent_h  //Prevents double inclusion
#define brent_h

//Static variables bundled into a struct for thread safety
struct local_min_rc_statics {
    double arg;
    double c;
    double d;
    double e;
    double eps;
    double fu;
    double fv;
    double fw;
    double fx;
    double midpoint;
    double p;
    double q;
    double r;
    double tol;
    double tol1;
    double tol2;
    double u;
    double v;
    double w;
    double x;
};

struct zero_rc_statics {
    double c;
    double d;
    double e;
    double fa;
    double fb;
    double fc;
    double machep;
    double sa;
    double sb;
};

double local_min_rc ( double *a, double *b, int *status, double value, struct local_min_rc_statics* statics );
double r8_epsilon ( void );
double r8_sign ( double x );
void zero_rc ( double a, double b, double t, double *arg, int *status, 
  double value, struct zero_rc_statics* statics );

#endif
