/*Reads XML tree, taking children from a given node, having applied FuncStringToTree.xsl, calculates output values[] based on tree. Note non-standard definition 0/0 = 0^x = 0.
 
 Versions:
 v0.01 27/11/14 TD Written for v0.03 of fill.cpp
 v0.02 20/01/15 TD Any single character usable as independent variable, changed from z. Removed print_error support, changed to return boolean.
 v0.03 26/01/15 TD Removed obsolete header ncurses.h
 v0.04 28/02/15 TD Libxml pointers > pointer pointers & added xmlFree(xmlText) to avoid memory leak
 v0.05 TD 05/05/15 Added Heaviside step function
 v0.06 TD 12/05/15 Bug fix: variable names in heaviside()
 v0.07 TD 13/05/15 An arbitrary number of string variable names can be used
 v0.08 TD 14/05/15 Check for division by zero, Tidy error messages
 v0.09 TD 14/05/15 Passing 2D array inlist modified for C++ compatibility
 v0.10 TD 19/01/16 fprintf format correction
 v0.11 TD 27/01/16 Use heap for left and right arrays
 v0.12 TD 28/01/16 Malloc cast to type (bug fix)
 v0.13 TD 05/02/16 Added inverse hyperbolic functions
 v0.14 TD 07/04/17 xmlTree made constant single pointer
 v0.15 TD 18/05/17 Do not calculate RHS of times, divide or raise to power when LHS = 0 for end of array
 v0.16 TD 18/06/17 Ported for MSVS2012
 v0.17 TD 28/07/17 Defined 0/0 = 0^x = 0 for consistency with optimisation - does not throw error
 v0.18 TD 04/01/18 Corrected IS_FINITE macro
 v0.19 TD 14/02/19 Added missing brackets around negation of IS_FINITE macro
 */

#ifndef calcalgebraicfunc_h  //Prevents double inclusion
#define calcalgebraicfunc_h

#include <math.h>
#include <float.h>
#include <string.h>
#include <libxml/tree.h>

 //Define safe string & file IO functions as macros
#if (defined _WIN32 || defined __STDC_LIB_EXT1__)
#ifndef __STDC_WANT_LIB_EXT1__
#define __STDC_WANT_LIB_EXT1__ 1
#endif
#define STR_CPY(dest, src, length) if (strcpy_s(dest, length, src)) { perror("String copy error"); return -1; }
#define STR_CAT(dest, src, length) if (strcat_s(dest, length, src)) { perror("String catentation error"); return -1; }
#else
//Ignore length
#define STR_CPY(dest, src, length) strcpy(dest, src);
#define STR_CAT(dest, src, length) strcat(dest, src);
#endif

//Define is finite function in case it's not
#ifdef INFINITY
#define IS_FINITE(val) isfinite(val)
#else
#define IS_FINITE(val) fabs(val) <= DBL_MAX ? 1 : 0
#endif

//calcalgebraicfunc.c
void heaviside(double output[], const double input[], const int points);
int findlastnonzero(double values[], const int length);
int calcalgfunc(double values[], const int points, const double* inlist[], const char* invar[], const int variables, const xmlDocPtr xmlTree, const xmlNodePtr *parentnode);

#endif
