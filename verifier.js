//Put script in body of HTML

//Global variables
var stepTime = [];  //Must be initialised to an array at all times
var stepMotor;
var stepDisplacement;
var animation, animationCxt;
var motorPos;   //Array holding current motor positions
var maxMotorId = -1;    //Initialised to no motors
var minDisplacement, maxDisplacement, displacementRange;
var animStepFunc;   //Holds function called by setInterval()
var processingFunc; //Holds function called by setTimeout()
var animClock, animReadPos;

//Prevent sloppy programming and throw more errors
"use strict";

//Check APIs
if (window.FileReader && window.Blob) {
    document.getElementById("missingAPIs").hidden = true;   //Hide error message that shows by default
    //Animation canvas is HTML5, so probably only available on newer browsers
    animation = document.getElementById("animation");
    animationCxt = animation.getContext("2d");
} else {
    document.getElementsByTagName("main")[0].hidden = true;
}

//File selection
function activateBBBButtons() {
    //Enables/disables BBB buttons as required
    var BBBlist, numBBBs, k;
    BBBlist = document.getElementById("infileList").children;
    numBBBs = BBBlist.length;
    //Enable
    BBBlist[0].getElementsByTagName("input")[3].disabled = false;    //Move down on first row enabled
    BBBlist[numBBBs - 1].getElementsByTagName("input")[2].disabled = false; //Move up on last row enabled
    //Override with disable
    BBBlist[0].getElementsByTagName("input")[2].disabled = true;    //Move up on first row disabled
    BBBlist[numBBBs - 1].getElementsByTagName("input")[3].disabled = true;    //Move down on last row disabled
    if (numBBBs == 1) {
        BBBlist[0].getElementsByTagName("input")[1].disabled = true; //Disable remove button
    } else {
        BBBlist[0].getElementsByTagName("input")[1].disabled = false; //Disable remove button
        BBBlist[numBBBs - 1].getElementsByTagName("input")[1].disabled = false; //Disable remove button
        for (k = 1; k < numBBBs - 1; k++) {
            BBBlist[k].getElementsByTagName("input")[1].disabled = false;    //Enable remove button
            BBBlist[k].getElementsByTagName("input")[2].disabled = false;    //Move up
            BBBlist[k].getElementsByTagName("input")[3].disabled = false;    //Move down
        }
    }
}

function removeIt(buttonPressed) {
    //Removes the fieldset containing the button and renumbers as appropriate
    //Button is disabled when on master BBB
    var removedNode, collectionNode, inputArray, numInputs;
    removedNode = buttonPressed.parentNode;
    collectionNode = removedNode.parentNode;
    collectionNode.removeChild(removedNode);
    activateBBBButtons();
}

function moveUp(buttonPressed) {
    var currentItem, previousItem, fieldsetNode;
    currentItem = buttonPressed.parentNode;
    previousItem = currentItem.previousElementSibling;
    fieldsetNode = currentItem.parentNode;
    fieldsetNode.insertBefore(currentItem, previousItem);
    activateBBBButtons();
}

function moveDown(buttonPressed) {
    var currentItem, nextItem, fieldsetNode;
    currentItem = buttonPressed.parentNode;
    nextItem = currentItem.nextElementSibling.nextElementSibling;
    fieldsetNode = currentItem.parentNode;
    fieldsetNode.insertBefore(currentItem, nextItem);
    activateBBBButtons();
}

function addBBB() {
    var clonedNode;
    clonedNode = document.getElementById("infileList").children[0].cloneNode(true);
    clonedNode.getElementsByTagName("input")[0].value = ""; //Clear the display
    document.getElementById("infileList").appendChild(clonedNode);
    activateBBBButtons();
}

function makeOneBBB() {
    //Removes all but one BBB, called by reset button
    var BBBnode1;
    while (BBBnode1 = document.getElementById("infileList").children[1]) {  //Assign to BBBnode1 and check for NULL
        BBBnode1.parentNode.removeChild(BBBnode1);
    }
    activateBBBButtons();
}

//Read selected binary files
function readFilesStart(startBtn) {
    var rtn;
    var fileInputs, numBBBs, BBBid;
    var fileArray = []; //Array of binary files
    var promiseArray;
    
    startBtn.disabled = true;
    document.getElementById("abortReadBtn").disabled = false;
    document.getElementById("statusLine").innerHTML = "Reading files...";
    
    fileInputs = document.getElementById("inputForm").elements.namedItem("filename");   //May be a node or a list - need to account for both cases
    numBBBs = document.getElementById("infileList").childElementCount;
    
    //Fill fileArray
    if (numBBBs == 1) {
        if (fileInputs.checkValidity() == false) {
            document.getElementById("statusLine").innerHTML = "ERROR: No file selected for BBB0.";
            startBtn.disabled = false;
            return 2;
        }
        fileArray.push(fileInputs.files[0]);
    } else {
        for (BBBid = 0; BBBid < numBBBs; BBBid++) {
            if (fileInputs[BBBid].checkValidity() == false) {
                document.getElementById("statusLine").innerHTML = "ERROR: No file selected for BBB" + BBBid + ".";
                startBtn.disabled = false;
                return 2;
            }
            fileArray.push(fileInputs[BBBid].files[0]);
        }
    }
    
    //Read them using promises
    promiseArray = fileArray.map(function(fileobj) {
                                 return new Promise(function(resolve, reject) {
                                                    var freader = new FileReader();
                                                    freader.onload = function() { resolve(freader.result); };
                                                    freader.onerror = reject;
                                                    freader.readAsArrayBuffer(fileobj);
                                                    });
                                 });
    Promise.all(promiseArray).then(function(result) {
                                   readHeaders(result);
                                   }).catch(function(msg) {
                                            document.getElementById("statusLine").innerHTML = "Error reading file: " + msg;
                                            //Return buttons to normal state
                                            startBtn.disabled = false;
                                            abortReadBtn.disabled = true;
                                            });
}

function expandStepInstr(instr, prevTime, stepPoints) {
    //Extracts the data contained within a verification instruction of Uint16 LE
    var time, timeDif, motor, step, displacement;
    timeDif = instr & 0x000f;  //4 bits
    time = prevTime + 0.1 * timeDif;    //Animation clock tick is 0.1 s
    motor = (instr & 0x01f0) >> 4; //5 bits
    step = (instr & 0xfe00) >> 9;  //7 bits
    displacement = stepPoints[step];
    return {time:time, motor:motor, displacement:displacement};
}

function findNextBBB(stepInstr) {
    //Returns BBBid of next BBB to step
    var BBBid, numBBBs, nextTime, nextBBB;
    nextTime = -1;  //Negative indicates error
    nextBBB = -1;
    numBBBs = stepInstr.length;
    for (BBBid = 0; BBBid < numBBBs; BBBid++) {
        if (stepInstr[BBBid].time >= 0) {
            if (nextTime < 0 || stepInstr[BBBid].time < nextTime) {
                nextTime = stepInstr[BBBid].time;
                nextBBB = BBBid;
            }
        }   //Else ignore
    }
    return nextBBB;
}

function readHeaders(files) {
    //files is an array of FileReader ArrayBuffer results
    var stepInstr = [];  //Instructions for next operation on each BBB: time (absolute), motor, step (displacement)
    var stepPoints = []; //2d array - lookup table of displacements corresponding to each step position on each BBB
    var fileView = []; //Array of DataViews of files
    var readOffset = [];    //Array of offsets for next read of DataView
    var motorNumOffset = [0];   //Array of numbers to add onto BBB motor ID to get global motor ID
    var numBBBs = files.length;
    var BBBid, motor_id, valueRead, stepMax, pointId;
    
    //Reinitialise global arrays
    stepTime = [];
    stepMotor = [];
    stepDisplacement = [];
    motorPos = [];
    minDisplacement = Number.MAX_VALUE;
    maxDisplacement = -Number.MAX_VALUE;
    
    for (BBBid = 0; BBBid < numBBBs; BBBid++) {
        stepInstr.push(0);
        stepPoints.push([]);
        fileView.push(new DataView(files[BBBid]));
        //Read byte offset for start of verification section of files - little endian 4 byte
        readOffset.push(fileView[BBBid].getUint32(0, true)); //true to little endian
        
        //Read minimum displacment
        valueRead = fileView[BBBid].getUint16(readOffset[BBBid], true);
        readOffset[BBBid] += 2;
        if (valueRead >= (1 << 15)) {
            //Bit 15 gives sign, other bits give absolute value times 2^15
            stepPoints[BBBid].push(((1 << 15) - valueRead) * Math.pow(2,-15));    //Negative number of bits 0-14
        } else {
            stepPoints[BBBid].push(valueRead * Math.pow(2,-15));
        }
        valueRead = fileView[BBBid].getUint8(readOffset[BBBid]);
        readOffset[BBBid] += 1;
        if (valueRead >= (1 << 7)) {
            //Bit 7 gives sign
            stepPoints[BBBid][0] *= Math.pow(2, (1 << 7) - valueRead);  //Don't assume bit shift of signed integer is ok
        } else {
            stepPoints[BBBid][0] *= 1 << valueRead;
        }
        
        //Update global minumum displacement
        if (stepPoints[BBBid][0] < minDisplacement) {
            minDisplacement = stepPoints[BBBid][0];
        }
        
        //Read maximum displacment
        valueRead = fileView[BBBid].getUint16(readOffset[BBBid], true);
        readOffset[BBBid] += 2;
        if (valueRead >= (1 << 15)) {
            //Bit 15 gives sign, other bits give absolute value
            stepMax = ((1 << 15) - valueRead) * Math.pow(2,-15);    //Negative number
        } else {
            stepMax = valueRead * Math.pow(2,-15);
        }
        valueRead = fileView[BBBid].getUint8(readOffset[BBBid]);
        readOffset[BBBid] += 1;
        if (valueRead >= (1 << 7)) {
            //Bit 7 gives sign
            stepMax *= Math.pow(2, (1 << 7) - valueRead);  //Don't assume bit shift of signed integer is ok
        } else {
            stepMax *= 1 << valueRead;
        }
        
        //Update global maximum displacement
        if (stepMax > maxDisplacement) {
            maxDisplacement = stepMax;
        }
        
        //Populate displacement points
        for (pointId = 1; pointId < 128; pointId++) {
            stepPoints[BBBid].push(stepPoints[BBBid][0] + pointId / 127 * (stepMax - stepPoints[BBBid][0]));
        }
        
        //Read initial motor steps. Motor numbers increase by 1 during initiation. When pattern is broken -> all motors found.
        //LIMITATION: Depends on maximum motor number = 29, as 31 is used for blank instruction - ok as XSD allows up to 27 motors.
        motor_id = 0;
        for (;;) {
            try {
                valueRead = fileView[BBBid].getUint16(readOffset[BBBid], true);
            } catch (err) {
                //End of file
                stepInstr[BBBid].time = -1;   //Less than zero indicates no more instructions
                break;
            }
            readOffset[BBBid] += 2;
            stepInstr[BBBid] = expandStepInstr(valueRead, 0, stepPoints[BBBid]);
            if (stepInstr[BBBid].motor != motor_id) {
                break;
            }
            
            //Write to array
            stepTime.push(0);
            stepMotor.push(motor_id + motorNumOffset[BBBid]);
            stepDisplacement.push(stepInstr[BBBid].displacement);
            motorPos.push(stepInstr[BBBid].displacement);   //Current/initial motor position
            
            motor_id++;
        }
        
        motorNumOffset.push(motor_id + motorNumOffset[BBBid]);  //Last entry gives total number of motors
    }
    
    //Save global variable
    maxMotorId = motorNumOffset[numBBBs] - 1;
    displacementRange = maxDisplacement - minDisplacement;
    if (displacementRange == 0) {
        //Will get a division by zero error later, but wave maker doesn't move, so value doesn't matter
        displacementRange = 1;
    }
    
    //Read in the instructions in chunks to avoid unresponsive page
    processingFunc = setTimeout(fillStepArrays, 0, fileView, readOffset, stepInstr, motorNumOffset, stepPoints, 0);
}

function fillStepArrays(fileView, readOffset, stepInstr, motorNumOffset, stepPoints, curTime) {
    //Fills step arrays in blocks of 1e6, to ensure page remains responsive
    //stepInstr is an array containing the next instruction for each BBB
    var instrId, BBBid, valueRead;
    for (instrId = 0; instrId < 1e6; instrId++) {
        if ((BBBid = findNextBBB(stepInstr)) < 0) {
            //All instructions processed
            break;
        }

        if (stepInstr[BBBid].motor < 31) {
            //Write to arrays, only if motor number not = 31, which means blank
            stepTime.push(stepInstr[BBBid].time);
            stepMotor.push(stepInstr[BBBid].motor + motorNumOffset[BBBid]);
            stepDisplacement.push(stepInstr[BBBid].displacement);
        }
        
        //Update current time - this is necessary for handling blank instructions correctly
        curTime = stepInstr[BBBid].time;
        
        //Read next instruction
        try {
            valueRead = fileView[BBBid].getUint16(readOffset[BBBid], true);
            readOffset[BBBid] += 2;
            stepInstr[BBBid] = expandStepInstr(valueRead, curTime, stepPoints[BBBid]);
        } catch (err) {
            //End of file
            stepInstr[BBBid].time = -1;   //Less than zero indicates no more instructions
        }
    }
    
    if (instrId < 1e6) {
        //All instructions processed
        //Setup animation
        animClock = 0;
        animReadPos = motorNumOffset[motorNumOffset.length - 1];  //Read point is after number of motors
        resizeAnimation();
        window.onresize = resizeAnimation;
        document.getElementById("animTimeBar").max = stepTime[stepTime.length - 1];
        document.getElementById("animTimeBar").value = 0;
        document.getElementById("animTimeText").innerHTML = 0;
        document.getElementById("animationControls").scrollIntoView(true);
        
        //Finished
        document.getElementById("statusLine").innerHTML = "Files loaded. Animation ready.";
        
        //Set buttons to read-done state
        document.getElementById("inputFieldset").disabled = true;   //Can disable fieldset, but not form, to disable all inputs inside
        document.getElementById("abortReadBtn").disabled = true;
        document.getElementById("changeFilesBtn").disabled = false;
        document.getElementById("csvBtn").disabled = false;
        document.getElementById("animationControls").disabled = false;
    } else {
        //Update progress and read next 1000 instructions
        document.getElementById("statusLine").innerHTML = "Reading files... " + stepInstr[BBBid].time.toFixed(1) + " s read";
        processingFunc = setTimeout(fillStepArrays, 0, fileView, readOffset, stepInstr, motorNumOffset, stepPoints);
    }
}

//Choose to change files, then need to reread
function changeFiles(changeBtn) {
    changeBtn.disabled = true;
    pauseAnim(document.getElementById("pauseBtn")); //Stop animation
    document.getElementById("statusLine").innerHTML = "Select files, then press <code>Read files</code>.";
    //Enable form
    document.getElementById("inputFieldset").disabled = false;
    document.getElementById("readFilesBtn").disabled = false;
    document.getElementById("csvBtn").disabled = true;
    document.getElementById("animationControls").disabled = true;
    
    document.getElementById("inputFieldset").scrollIntoView(true);
}

function abortTask(btnPressed) {
    //Stop task
    clearTimeout(processingFunc);
    btnPressed.disabled = true;
    if (btnPressed.id == "abortReadBtn") {
        document.getElementById("readFilesBtn").disabled = false;
        document.getElementById("statusLine").innerHTML = "File read aborted. Select files, then press <code>Read files</code>.";
    } else {
        document.getElementById("csvBtn").disabled = false;
        document.getElementById("statusLine").innerHTML = "CSV generation aborted. Animation ready.";
    }
}

function makeCSVCaller(btnPressed) {
    //Run asynchronously
    var wholeCSV, instrId;
    
    document.getElementById("statusLine").innerHTML = "Generating CSV file...";
    btnPressed.disabled = true; //Disable button until operation completed
    document.getElementById("abortCSVBtn").disabled = false;
    
    wholeCSV = "Time,Motor,Displacement\n";
    instrId = 0;
    
    setTimeout(makeCSVAssembler, 0, wholeCSV, instrId, btnPressed);
}

function makeCSVAssembler(wholeCSV, instrId, btnPressed) {
    //Assembles CSV lines 1e6 at a time and outputs status update
    var fileBlob, url, docBlob;
    var indexCutoff = Math.min(instrId + 1e6, stepTime.length);
    
    //Do not reinitialise instrId
    for (; instrId < indexCutoff; instrId++) {
        wholeCSV += stepTime[instrId].toFixed(1) + "," + stepMotor[instrId].toFixed(0) + "," + stepDisplacement[instrId].toPrecision(4) + "\n"; //Correct to 1dp, integer, 4sf, with new line at end
    }
    
    if (instrId < stepTime.length) {
        //More instructions to process
        document.getElementById("statusLine").innerHTML = "Generating CSV file... " + stepTime[instrId].toFixed(1) + " s written";
        processingFunc = setTimeout(makeCSVAssembler, 0, wholeCSV, instrId, btnPressed);
    } else {
        //Put in file
        fileBlob = new Blob([wholeCSV], {type:"text/csv"});
        if (window.navigator.msSaveBlob) {
            //Microsoft
            window.navigator.msSaveBlob(fileBlob, "WaveForm.csv");
        } else {
            //Other browsers
            url = URL.createObjectURL(fileBlob);

            //Download file
            docBlob = document.createElement("a");
            docBlob.href = url;
            docBlob.download = "WaveForm.csv";
            document.body.appendChild(docBlob);
            docBlob.click();
            setTimeout(function () {
                document.body.removeChild(docBlob);
                URL.revokeObjectURL(url);
            }, 0);
        }
        
        //Re-enable button as finished
        document.getElementById("statusLine").innerHTML = "CSV file generated and downloaded. Animation ready.";
        document.getElementById("abortCSVBtn").disabled = true;
        btnPressed.disabled = false;
    }
}

function resizeAnimation() {
    animation.width = 0.95 * window.innerWidth;
    animation.height = 0.5 * window.innerHeight;
    drawAnimation();
}

function drawAnimation() {
    //Whole canvas must be redrawn each time
    var width, height;
    
    if (maxMotorId >= 0) {
        //Get current canvas dimensions with margin=16
        width = animation.width - 6;
        height = animation.height - 6;

        //Clear canvas
        animationCxt.clearRect(0, 0, width + 6, height + 6);

        //Add labels
        animationCxt.font = "16px/16px Arial";
        animationCxt.fillStyle = "black";
        if (document.getElementById("flipy").checked) {
            animationCxt.fillText(maxDisplacement.toPrecision(3), 0, height + 3);
            animationCxt.fillText(minDisplacement.toPrecision(3), 0, 16);
        } else {
            animationCxt.fillText(minDisplacement.toPrecision(3), 0, height + 3);
            animationCxt.fillText(maxDisplacement.toPrecision(3), 0, 16);
        }

        //Add wave maker surface - as straight lines - bezier curves blow up/badly behaved
        animationCxt.beginPath();
        animationCxt.moveTo(xCoord(0, width), yCoord(motorPos[0], height));
        for (motorId = 1; motorId <= maxMotorId; motorId++) { //3 -> maxMotorId
            animationCxt.lineTo(xCoord(motorId, width), yCoord(motorPos[motorId], height));
        }
        animationCxt.stroke();

        //Add cicles on rods
        animationCxt.fillStyle = "red";
        for (motorId = 0; motorId <= maxMotorId; motorId++) {
            animationCxt.beginPath();
            animationCxt.arc(xCoord(motorId, width), yCoord(motorPos[motorId], height), 3, 0, 6.283185307179586);
            animationCxt.fill();
        }
    }
}

function xCoord(motorId, canvasWidth) {
    //Transforms x coordinate with motor spacing of 1 to canvas pixels
    var unflipped;
    if (maxMotorId == 0) {
        //Place single rod in centre of screen
        unflipped = 0.5 * canvasWidth + 3;
    } else {
        //Won't divide by zero
        unflipped = motorId / maxMotorId * canvasWidth + 3;
    }
    if (document.getElementById("flipx").checked) {
        return canvasWidth + 6 - unflipped;
    } else {
        return unflipped;
    }
}

function yCoord(displacement, canvasHeight) {
    //Transforms y coordinate to canvas pixels
    var unflipped = (maxDisplacement - displacement) / displacementRange * canvasHeight + 3;
    if (document.getElementById("flipy").checked) {
        return canvasHeight + 6 - unflipped;
    } else {
        return unflipped;
    }
}

function playAnim(buttonPressed) {
    if (animStepFunc) {
        clearInterval(animStepFunc);
    }
    pauseAnim(document.getElementById("pauseBtn"));
    buttonPressed.disabled = true;
    document.getElementById("rewindBtn").disabled = false;
    document.getElementById("pauseBtn").disabled = false;
    animStepFunc = setInterval(advanceTime, 100 / Number(document.getElementById("playSpeed").value));
}

function pauseAnim(buttonPressed) {
    if (animStepFunc) {
        clearInterval(animStepFunc);
    }
    buttonPressed.disabled = true;
    document.getElementById("playBtn").disabled = false;
    document.getElementById("rewindBtn").disabled = false;
}

function rewindAnim(buttonPressed) {
    if (animStepFunc) {
        clearInterval(animStepFunc);
    }
    buttonPressed.disabled = true;
    document.getElementById("playBtn").disabled = false;
    document.getElementById("pauseBtn").disabled = false;
    animStepFunc = setInterval(rewind, 100 / Number(document.getElementById("playSpeed").value));
}

function advanceTime() {
    if (animClock + 0.05 >= animTimeBar.max) {  //Rounding error allowance
        //Stop animation
        pauseAnim(document.getElementById("pauseBtn"));
    } else {
        animClock += 0.1;
        document.getElementById("animTimeBar").value = animClock;
        document.getElementById("animTimeText").innerHTML = animClock.toFixed(1);
        
        //Update motor positions
        while (stepTime[animReadPos] <= animClock + 0.05) { //Allow for rounding error
            motorPos[stepMotor[animReadPos]] = stepDisplacement[animReadPos];
            animReadPos++;
            //Prevent read position from going out of range
            if (animReadPos >= stepTime.length) {
                animReadPos = stepTime.length - 1;
                break;
            }
        }
        
        drawAnimation();
    }
}

function rewind() {
    if (animClock < 0.05) {
        //Stop animation
        pauseAnim(document.getElementById("pauseBtn"));
    } else {
        animClock -= 0.1;
        document.getElementById("animTimeBar").value = animClock;
        document.getElementById("animTimeText").innerHTML = animClock.toFixed(1);
        
        //Update motor positions
        while (stepTime[animReadPos] > animClock - 0.05) { //Allow for rounding error
            motorPos[stepMotor[animReadPos]] = stepDisplacement[animReadPos];
            animReadPos--;
            //Prevent read position from going out of range
            if (animReadPos < 0) {
                animReadPos = 0;
                break;
            }
        }
        
        drawAnimation();
    }
}

function zeroTime() {
    //Stop animation
    pauseAnim(document.getElementById("pauseBtn"));
    animClock = 0.1;
    animReadPos = maxMotorId;
    rewind();
}

function changeSpeed() {
    clearInterval(animStepFunc);
    if (document.getElementById("playBtn").disabled) {
        //Playing forwards
        animStepFunc = setInterval(advanceTime, 100 / Number(document.getElementById("playSpeed").value));
    } else {
        animStepFunc = setInterval(rewind, 100 / Number(document.getElementById("playSpeed").value));
    }
}
